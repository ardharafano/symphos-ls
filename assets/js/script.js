! function(e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
            if (!e.document)
                throw new Error("jQuery requires a window with a document");
            return t(e)
        } :
        t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    "use strict";
    var i = [],
        s = e.document,
        n = Object.getPrototypeOf,
        a = i.slice,
        o = i.concat,
        r = i.push,
        l = i.indexOf,
        h = {},
        u = h.toString,
        c = h.hasOwnProperty,
        d = c.toString,
        p = d.call(Object),
        f = {},
        m = function(e) {
            return "function" == typeof e && "number" != typeof e.nodeType
        },
        g = function(e) {
            return null != e && e === e.window
        },
        v = {
            type: !0,
            src: !0,
            noModule: !0
        };

    function b(e, t, i) {
        var n, a = (t = t || s).createElement("script");
        if (a.text = e,
            i)
            for (n in v)
                i[n] && (a[n] = i[n]);
        t.head.appendChild(a).parentNode.removeChild(a)
    }

    function y(e) {
        return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? h[u.call(e)] || "object" : typeof e
    }
    var _ = function(e, t) {
            return new _.fn.init(e, t)
        },
        w = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

    function x(e) {
        var t = !!e && "length" in e && e.length,
            i = y(e);
        return !m(e) && !g(e) && ("array" === i || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }
    _.fn = _.prototype = {
            jquery: "3.3.1",
            constructor: _,
            length: 0,
            toArray: function() {
                return a.call(this)
            },
            get: function(e) {
                return null == e ? a.call(this) : e < 0 ? this[e + this.length] : this[e]
            },
            pushStack: function(e) {
                var t = _.merge(this.constructor(), e);
                return t.prevObject = this,
                    t
            },
            each: function(e) {
                return _.each(this, e)
            },
            map: function(e) {
                return this.pushStack(_.map(this, function(t, i) {
                    return e.call(t, i, t)
                }))
            },
            slice: function() {
                return this.pushStack(a.apply(this, arguments))
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            eq: function(e) {
                var t = this.length,
                    i = +e + (e < 0 ? t : 0);
                return this.pushStack(i >= 0 && i < t ? [this[i]] : [])
            },
            end: function() {
                return this.prevObject || this.constructor()
            },
            push: r,
            sort: i.sort,
            splice: i.splice
        },
        _.extend = _.fn.extend = function() {
            var e, t, i, s, n, a, o = arguments[0] || {},
                r = 1,
                l = arguments.length,
                h = !1;
            for ("boolean" == typeof o && (h = o,
                    o = arguments[r] || {},
                    r++),
                "object" == typeof o || m(o) || (o = {}),
                r === l && (o = this,
                    r--); r < l; r++)
                if (null != (e = arguments[r]))
                    for (t in e)
                        i = o[t],
                        o !== (s = e[t]) && (h && s && (_.isPlainObject(s) || (n = Array.isArray(s))) ? (n ? (n = !1,
                                a = i && Array.isArray(i) ? i : []) : a = i && _.isPlainObject(i) ? i : {},
                            o[t] = _.extend(h, a, s)) : void 0 !== s && (o[t] = s));
            return o
        },
        _.extend({
            expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function(e) {
                throw new Error(e)
            },
            noop: function() {},
            isPlainObject: function(e) {
                var t, i;
                return !(!e || "[object Object]" !== u.call(e) || (t = n(e)) && ("function" != typeof(i = c.call(t, "constructor") && t.constructor) || d.call(i) !== p))
            },
            isEmptyObject: function(e) {
                var t;
                for (t in e)
                    return !1;
                return !0
            },
            globalEval: function(e) {
                b(e)
            },
            each: function(e, t) {
                var i, s = 0;
                if (x(e))
                    for (i = e.length; s < i && !1 !== t.call(e[s], s, e[s]); s++)
                ;
                else
                    for (s in e)
                        if (!1 === t.call(e[s], s, e[s]))
                            break;
                return e
            },
            trim: function(e) {
                return null == e ? "" : (e + "").replace(w, "")
            },
            makeArray: function(e, t) {
                var i = t || [];
                return null != e && (x(Object(e)) ? _.merge(i, "string" == typeof e ? [e] : e) : r.call(i, e)),
                    i
            },
            inArray: function(e, t, i) {
                return null == t ? -1 : l.call(t, e, i)
            },
            merge: function(e, t) {
                for (var i = +t.length, s = 0, n = e.length; s < i; s++)
                    e[n++] = t[s];
                return e.length = n,
                    e
            },
            grep: function(e, t, i) {
                for (var s = [], n = 0, a = e.length, o = !i; n < a; n++)
                    !t(e[n], n) !== o && s.push(e[n]);
                return s
            },
            map: function(e, t, i) {
                var s, n, a = 0,
                    r = [];
                if (x(e))
                    for (s = e.length; a < s; a++)
                        null != (n = t(e[a], a, i)) && r.push(n);
                else
                    for (a in e)
                        null != (n = t(e[a], a, i)) && r.push(n);
                return o.apply([], r)
            },
            guid: 1,
            support: f
        }),
        "function" == typeof Symbol && (_.fn[Symbol.iterator] = i[Symbol.iterator]),
        _.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
            h["[object " + t + "]"] = t.toLowerCase()
        });
    var C = function(e) {
        var t, i, s, n, a, o, r, l, h, u, c, d, p, f, m, g, v, b, y, _ = "sizzle" + 1 * new Date,
            w = e.document,
            x = 0,
            C = 0,
            T = oe(),
            k = oe(),
            S = oe(),
            E = function(e, t) {
                return e === t && (c = !0),
                    0
            },
            D = {}.hasOwnProperty,
            M = [],
            P = M.pop,
            I = M.push,
            z = M.push,
            A = M.slice,
            O = function(e, t) {
                for (var i = 0, s = e.length; i < s; i++)
                    if (e[i] === t)
                        return i;
                return -1
            },
            H = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            N = "[\\x20\\t\\r\\n\\f]",
            L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            W = "\\[" + N + "*(" + L + ")(?:" + N + "*([*^$|!~]?=)" + N + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + N + "*\\]",
            $ = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
            F = new RegExp(N + "+", "g"),
            R = new RegExp("^" + N + "+|((?:^|[^\\\\])(?:\\\\.)*)" + N + "+$", "g"),
            B = new RegExp("^" + N + "*," + N + "*"),
            j = new RegExp("^" + N + "*([>+~]|" + N + ")" + N + "*"),
            q = new RegExp("=" + N + "*([^\\]'\"]*?)" + N + "*\\]", "g"),
            Y = new RegExp($),
            V = new RegExp("^" + L + "$"),
            X = {
                ID: new RegExp("^#(" + L + ")"),
                CLASS: new RegExp("^\\.(" + L + ")"),
                TAG: new RegExp("^(" + L + "|[*])"),
                ATTR: new RegExp("^" + W),
                PSEUDO: new RegExp("^" + $),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + N + "*(even|odd|(([+-]|)(\\d*)n|)" + N + "*(?:([+-]|)" + N + "*(\\d+)|))" + N + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + H + ")$", "i"),
                needsContext: new RegExp("^" + N + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + N + "*((?:-\\d)?\\d*)" + N + "*\\)|)(?=[^-]|$)", "i")
            },
            G = /^(?:input|select|textarea|button)$/i,
            U = /^h\d$/i,
            K = /^[^{]+\{\s*\[native \w/,
            Q = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            J = /[+~]/,
            Z = new RegExp("\\\\([\\da-f]{1,6}" + N + "?|(" + N + ")|.)", "ig"),
            ee = function(e, t, i) {
                var s = "0x" + t - 65536;
                return s != s || i ? t : s < 0 ? String.fromCharCode(s + 65536) : String.fromCharCode(s >> 10 | 55296, 1023 & s | 56320)
            },
            te = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            ie = function(e, t) {
                return t ? "\0" === e ? "\ufffd" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            },
            se = function() {
                d()
            },
            ne = be(function(e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            z.apply(M = A.call(w.childNodes), w.childNodes)
        } catch (e) {
            z = {
                apply: M.length ? function(e, t) {
                        I.apply(e, A.call(t))
                    } :
                    function(e, t) {
                        for (var i = e.length, s = 0; e[i++] = t[s++];)
                        ;
                        e.length = i - 1
                    }
            }
        }

        function ae(e, t, s, n) {
            var a, r, h, u, c, f, v, b = t && t.ownerDocument,
                x = t ? t.nodeType : 9;
            if (s = s || [],
                "string" != typeof e || !e || 1 !== x && 9 !== x && 11 !== x)
                return s;
            if (!n && ((t ? t.ownerDocument || t : w) !== p && d(t),
                    t = t || p,
                    m)) {
                if (11 !== x && (c = Q.exec(e)))
                    if (a = c[1]) {
                        if (9 === x) {
                            if (!(h = t.getElementById(a)))
                                return s;
                            if (h.id === a)
                                return s.push(h),
                                    s
                        } else if (b && (h = b.getElementById(a)) && y(t, h) && h.id === a)
                            return s.push(h),
                                s
                    } else {
                        if (c[2])
                            return z.apply(s, t.getElementsByTagName(e)),
                                s;
                        if ((a = c[3]) && i.getElementsByClassName && t.getElementsByClassName)
                            return z.apply(s, t.getElementsByClassName(a)),
                                s
                    }
                if (i.qsa && !S[e + " "] && (!g || !g.test(e))) {
                    if (1 !== x)
                        b = t,
                        v = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((u = t.getAttribute("id")) ? u = u.replace(te, ie) : t.setAttribute("id", u = _),
                            r = (f = o(e)).length; r--;)
                            f[r] = "#" + u + " " + ve(f[r]);
                        v = f.join(","),
                            b = J.test(e) && me(t.parentNode) || t
                    }
                    if (v)
                        try {
                            return z.apply(s, b.querySelectorAll(v)),
                                s
                        } catch (e) {} finally {
                            u === _ && t.removeAttribute("id")
                        }
                }
            }
            return l(e.replace(R, "$1"), t, s, n)
        }

        function oe() {
            var e = [];
            return function t(i, n) {
                return e.push(i + " ") > s.cacheLength && delete t[e.shift()],
                    t[i + " "] = n
            }
        }

        function re(e) {
            return e[_] = !0,
                e
        }

        function le(e) {
            var t = p.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t),
                    t = null
            }
        }

        function he(e, t) {
            for (var i = e.split("|"), n = i.length; n--;)
                s.attrHandle[i[n]] = t
        }

        function ue(e, t) {
            var i = t && e,
                s = i && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (s)
                return s;
            if (i)
                for (; i = i.nextSibling;)
                    if (i === t)
                        return -1;
            return e ? 1 : -1
        }

        function ce(e) {
            return function(t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function de(e) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && t.type === e
            }
        }

        function pe(e) {
            return function(t) {
                return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && ne(t) === e : t.disabled === e : "label" in t && t.disabled === e
            }
        }

        function fe(e) {
            return re(function(t) {
                return t = +t,
                    re(function(i, s) {
                        for (var n, a = e([], i.length, t), o = a.length; o--;)
                            i[n = a[o]] && (i[n] = !(s[n] = i[n]))
                    })
            })
        }

        function me(e) {
            return e && void 0 !== e.getElementsByTagName && e
        }
        for (t in i = ae.support = {},
            a = ae.isXML = function(e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return !!t && "HTML" !== t.nodeName
            },
            d = ae.setDocument = function(e) {
                var t, n, o = e ? e.ownerDocument || e : w;
                return o !== p && 9 === o.nodeType && o.documentElement ? (f = (p = o).documentElement,
                    m = !a(p),
                    w !== p && (n = p.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", se, !1) : n.attachEvent && n.attachEvent("onunload", se)),
                    i.attributes = le(function(e) {
                        return e.className = "i", !e.getAttribute("className")
                    }),
                    i.getElementsByTagName = le(function(e) {
                        return e.appendChild(p.createComment("")), !e.getElementsByTagName("*").length
                    }),
                    i.getElementsByClassName = K.test(p.getElementsByClassName),
                    i.getById = le(function(e) {
                        return f.appendChild(e).id = _, !p.getElementsByName || !p.getElementsByName(_).length
                    }),
                    i.getById ? (s.filter.ID = function(e) {
                            var t = e.replace(Z, ee);
                            return function(e) {
                                return e.getAttribute("id") === t
                            }
                        },
                        s.find.ID = function(e, t) {
                            if (void 0 !== t.getElementById && m) {
                                var i = t.getElementById(e);
                                return i ? [i] : []
                            }
                        }
                    ) : (s.filter.ID = function(e) {
                            var t = e.replace(Z, ee);
                            return function(e) {
                                var i = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                                return i && i.value === t
                            }
                        },
                        s.find.ID = function(e, t) {
                            if (void 0 !== t.getElementById && m) {
                                var i, s, n, a = t.getElementById(e);
                                if (a) {
                                    if ((i = a.getAttributeNode("id")) && i.value === e)
                                        return [a];
                                    for (n = t.getElementsByName(e),
                                        s = 0; a = n[s++];)
                                        if ((i = a.getAttributeNode("id")) && i.value === e)
                                            return [a]
                                }
                                return []
                            }
                        }
                    ),
                    s.find.TAG = i.getElementsByTagName ? function(e, t) {
                        return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : i.qsa ? t.querySelectorAll(e) : void 0
                    } :
                    function(e, t) {
                        var i, s = [],
                            n = 0,
                            a = t.getElementsByTagName(e);
                        if ("*" === e) {
                            for (; i = a[n++];)
                                1 === i.nodeType && s.push(i);
                            return s
                        }
                        return a
                    },
                    s.find.CLASS = i.getElementsByClassName && function(e, t) {
                        if (void 0 !== t.getElementsByClassName && m)
                            return t.getElementsByClassName(e)
                    },
                    v = [],
                    g = [],
                    (i.qsa = K.test(p.querySelectorAll)) && (le(function(e) {
                            f.appendChild(e).innerHTML = "<a id='" + _ + "'></a><select id='" + _ + "-\r\\' msallowcapture=''><option selected=''></option></select>",
                                e.querySelectorAll("[msallowcapture^='']").length && g.push("[*^$]=" + N + "*(?:''|\"\")"),
                                e.querySelectorAll("[selected]").length || g.push("\\[" + N + "*(?:value|" + H + ")"),
                                e.querySelectorAll("[id~=" + _ + "-]").length || g.push("~="),
                                e.querySelectorAll(":checked").length || g.push(":checked"),
                                e.querySelectorAll("a#" + _ + "+*").length || g.push(".#.+[+~]")
                        }),
                        le(function(e) {
                            e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                            var t = p.createElement("input");
                            t.setAttribute("type", "hidden"),
                                e.appendChild(t).setAttribute("name", "D"),
                                e.querySelectorAll("[name=d]").length && g.push("name" + N + "*[*^$|!~]?="),
                                2 !== e.querySelectorAll(":enabled").length && g.push(":enabled", ":disabled"),
                                f.appendChild(e).disabled = !0,
                                2 !== e.querySelectorAll(":disabled").length && g.push(":enabled", ":disabled"),
                                e.querySelectorAll("*,:x"),
                                g.push(",.*:")
                        })),
                    (i.matchesSelector = K.test(b = f.matches || f.webkitMatchesSelector || f.mozMatchesSelector || f.oMatchesSelector || f.msMatchesSelector)) && le(function(e) {
                        i.disconnectedMatch = b.call(e, "*"),
                            b.call(e, "[s!='']:x"),
                            v.push("!=", $)
                    }),
                    g = g.length && new RegExp(g.join("|")),
                    v = v.length && new RegExp(v.join("|")),
                    t = K.test(f.compareDocumentPosition),
                    y = t || K.test(f.contains) ? function(e, t) {
                        var i = 9 === e.nodeType ? e.documentElement : e,
                            s = t && t.parentNode;
                        return e === s || !(!s || 1 !== s.nodeType || !(i.contains ? i.contains(s) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(s)))
                    } :
                    function(e, t) {
                        if (t)
                            for (; t = t.parentNode;)
                                if (t === e)
                                    return !0;
                        return !1
                    },
                    E = t ? function(e, t) {
                        if (e === t)
                            return c = !0,
                                0;
                        var s = !e.compareDocumentPosition - !t.compareDocumentPosition;
                        return s || (1 & (s = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !i.sortDetached && t.compareDocumentPosition(e) === s ? e === p || e.ownerDocument === w && y(w, e) ? -1 : t === p || t.ownerDocument === w && y(w, t) ? 1 : u ? O(u, e) - O(u, t) : 0 : 4 & s ? -1 : 1)
                    } :
                    function(e, t) {
                        if (e === t)
                            return c = !0,
                                0;
                        var i, s = 0,
                            n = e.parentNode,
                            a = t.parentNode,
                            o = [e],
                            r = [t];
                        if (!n || !a)
                            return e === p ? -1 : t === p ? 1 : n ? -1 : a ? 1 : u ? O(u, e) - O(u, t) : 0;
                        if (n === a)
                            return ue(e, t);
                        for (i = e; i = i.parentNode;)
                            o.unshift(i);
                        for (i = t; i = i.parentNode;)
                            r.unshift(i);
                        for (; o[s] === r[s];)
                            s++;
                        return s ? ue(o[s], r[s]) : o[s] === w ? -1 : r[s] === w ? 1 : 0
                    },
                    p) : p
            },
            ae.matches = function(e, t) {
                return ae(e, null, null, t)
            },
            ae.matchesSelector = function(e, t) {
                if ((e.ownerDocument || e) !== p && d(e),
                    t = t.replace(q, "='$1']"),
                    i.matchesSelector && m && !S[t + " "] && (!v || !v.test(t)) && (!g || !g.test(t)))
                    try {
                        var s = b.call(e, t);
                        if (s || i.disconnectedMatch || e.document && 11 !== e.document.nodeType)
                            return s
                    } catch (e) {}
                return ae(t, p, null, [e]).length > 0
            },
            ae.contains = function(e, t) {
                return (e.ownerDocument || e) !== p && d(e),
                    y(e, t)
            },
            ae.attr = function(e, t) {
                (e.ownerDocument || e) !== p && d(e);
                var n = s.attrHandle[t.toLowerCase()],
                    a = n && D.call(s.attrHandle, t.toLowerCase()) ? n(e, t, !m) : void 0;
                return void 0 !== a ? a : i.attributes || !m ? e.getAttribute(t) : (a = e.getAttributeNode(t)) && a.specified ? a.value : null
            },
            ae.escape = function(e) {
                return (e + "").replace(te, ie)
            },
            ae.error = function(e) {
                throw new Error("Syntax error, unrecognized expression: " + e)
            },
            ae.uniqueSort = function(e) {
                var t, s = [],
                    n = 0,
                    a = 0;
                if (c = !i.detectDuplicates,
                    u = !i.sortStable && e.slice(0),
                    e.sort(E),
                    c) {
                    for (; t = e[a++];)
                        t === e[a] && (n = s.push(a));
                    for (; n--;)
                        e.splice(s[n], 1)
                }
                return u = null,
                    e
            },
            n = ae.getText = function(e) {
                var t, i = "",
                    s = 0,
                    a = e.nodeType;
                if (a) {
                    if (1 === a || 9 === a || 11 === a) {
                        if ("string" == typeof e.textContent)
                            return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling)
                            i += n(e)
                    } else if (3 === a || 4 === a)
                        return e.nodeValue
                } else
                    for (; t = e[s++];)
                        i += n(t);
                return i
            },
            (s = ae.selectors = {
                cacheLength: 50,
                createPseudo: re,
                match: X,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(e) {
                        return e[1] = e[1].replace(Z, ee),
                            e[3] = (e[3] || e[4] || e[5] || "").replace(Z, ee),
                            "~=" === e[2] && (e[3] = " " + e[3] + " "),
                            e.slice(0, 4)
                    },
                    CHILD: function(e) {
                        return e[1] = e[1].toLowerCase(),
                            "nth" === e[1].slice(0, 3) ? (e[3] || ae.error(e[0]),
                                e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])),
                                e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && ae.error(e[0]),
                            e
                    },
                    PSEUDO: function(e) {
                        var t, i = !e[6] && e[2];
                        return X.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : i && Y.test(i) && (t = o(i, !0)) && (t = i.indexOf(")", i.length - t) - i.length) && (e[0] = e[0].slice(0, t),
                                e[2] = i.slice(0, t)),
                            e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(e) {
                        var t = e.replace(Z, ee).toLowerCase();
                        return "*" === e ? function() {
                                return !0
                            } :
                            function(e) {
                                return e.nodeName && e.nodeName.toLowerCase() === t
                            }
                    },
                    CLASS: function(e) {
                        var t = T[e + " "];
                        return t || (t = new RegExp("(^|" + N + ")" + e + "(" + N + "|$)")) && T(e, function(e) {
                            return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(e, t, i) {
                        return function(s) {
                            var n = ae.attr(s, e);
                            return null == n ? "!=" === t : !t || (n += "",
                                "=" === t ? n === i : "!=" === t ? n !== i : "^=" === t ? i && 0 === n.indexOf(i) : "*=" === t ? i && n.indexOf(i) > -1 : "$=" === t ? i && n.slice(-i.length) === i : "~=" === t ? (" " + n.replace(F, " ") + " ").indexOf(i) > -1 : "|=" === t && (n === i || n.slice(0, i.length + 1) === i + "-"))
                        }
                    },
                    CHILD: function(e, t, i, s, n) {
                        var a = "nth" !== e.slice(0, 3),
                            o = "last" !== e.slice(-4),
                            r = "of-type" === t;
                        return 1 === s && 0 === n ? function(e) {
                                return !!e.parentNode
                            } :
                            function(t, i, l) {
                                var h, u, c, d, p, f, m = a !== o ? "nextSibling" : "previousSibling",
                                    g = t.parentNode,
                                    v = r && t.nodeName.toLowerCase(),
                                    b = !l && !r,
                                    y = !1;
                                if (g) {
                                    if (a) {
                                        for (; m;) {
                                            for (d = t; d = d[m];)
                                                if (r ? d.nodeName.toLowerCase() === v : 1 === d.nodeType)
                                                    return !1;
                                            f = m = "only" === e && !f && "nextSibling"
                                        }
                                        return !0
                                    }
                                    if (f = [o ? g.firstChild : g.lastChild],
                                        o && b) {
                                        for (y = (p = (h = (u = (c = (d = g)[_] || (d[_] = {}))[d.uniqueID] || (c[d.uniqueID] = {}))[e] || [])[0] === x && h[1]) && h[2],
                                            d = p && g.childNodes[p]; d = ++p && d && d[m] || (y = p = 0) || f.pop();)
                                            if (1 === d.nodeType && ++y && d === t) {
                                                u[e] = [x, p, y];
                                                break
                                            }
                                    } else if (b && (y = p = (h = (u = (c = (d = t)[_] || (d[_] = {}))[d.uniqueID] || (c[d.uniqueID] = {}))[e] || [])[0] === x && h[1]), !1 === y)
                                        for (;
                                            (d = ++p && d && d[m] || (y = p = 0) || f.pop()) && ((r ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++y || (b && ((u = (c = d[_] || (d[_] = {}))[d.uniqueID] || (c[d.uniqueID] = {}))[e] = [x, y]),
                                                d !== t));)
                                    ;
                                    return (y -= n) === s || y % s == 0 && y / s >= 0
                                }
                            }
                    },
                    PSEUDO: function(e, t) {
                        var i, n = s.pseudos[e] || s.setFilters[e.toLowerCase()] || ae.error("unsupported pseudo: " + e);
                        return n[_] ? n(t) : n.length > 1 ? (i = [e, e, "", t],
                            s.setFilters.hasOwnProperty(e.toLowerCase()) ? re(function(e, i) {
                                for (var s, a = n(e, t), o = a.length; o--;)
                                    e[s = O(e, a[o])] = !(i[s] = a[o])
                            }) : function(e) {
                                return n(e, 0, i)
                            }
                        ) : n
                    }
                },
                pseudos: {
                    not: re(function(e) {
                        var t = [],
                            i = [],
                            s = r(e.replace(R, "$1"));
                        return s[_] ? re(function(e, t, i, n) {
                            for (var a, o = s(e, null, n, []), r = e.length; r--;)
                                (a = o[r]) && (e[r] = !(t[r] = a))
                        }) : function(e, n, a) {
                            return t[0] = e,
                                s(t, null, a, i),
                                t[0] = null, !i.pop()
                        }
                    }),
                    has: re(function(e) {
                        return function(t) {
                            return ae(e, t).length > 0
                        }
                    }),
                    contains: re(function(e) {
                        return e = e.replace(Z, ee),
                            function(t) {
                                return (t.textContent || t.innerText || n(t)).indexOf(e) > -1
                            }
                    }),
                    lang: re(function(e) {
                        return V.test(e || "") || ae.error("unsupported lang: " + e),
                            e = e.replace(Z, ee).toLowerCase(),
                            function(t) {
                                var i;
                                do {
                                    if (i = m ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))
                                        return (i = i.toLowerCase()) === e || 0 === i.indexOf(e + "-")
                                } while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    }),
                    target: function(t) {
                        var i = e.location && e.location.hash;
                        return i && i.slice(1) === t.id
                    },
                    root: function(e) {
                        return e === f
                    },
                    focus: function(e) {
                        return e === p.activeElement && (!p.hasFocus || p.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    enabled: pe(!1),
                    disabled: pe(!0),
                    checked: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    },
                    selected: function(e) {
                        return !0 === e.selected
                    },
                    empty: function(e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeType < 6)
                                return !1;
                        return !0
                    },
                    parent: function(e) {
                        return !s.pseudos.empty(e)
                    },
                    header: function(e) {
                        return U.test(e.nodeName)
                    },
                    input: function(e) {
                        return G.test(e.nodeName)
                    },
                    button: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function(e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                    },
                    first: fe(function() {
                        return [0]
                    }),
                    last: fe(function(e, t) {
                        return [t - 1]
                    }),
                    eq: fe(function(e, t, i) {
                        return [i < 0 ? i + t : i]
                    }),
                    even: fe(function(e, t) {
                        for (var i = 0; i < t; i += 2)
                            e.push(i);
                        return e
                    }),
                    odd: fe(function(e, t) {
                        for (var i = 1; i < t; i += 2)
                            e.push(i);
                        return e
                    }),
                    lt: fe(function(e, t, i) {
                        for (var s = i < 0 ? i + t : i; --s >= 0;)
                            e.push(s);
                        return e
                    }),
                    gt: fe(function(e, t, i) {
                        for (var s = i < 0 ? i + t : i; ++s < t;)
                            e.push(s);
                        return e
                    })
                }
            }).pseudos.nth = s.pseudos.eq, {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            })
            s.pseudos[t] = ce(t);
        for (t in {
                submit: !0,
                reset: !0
            })
            s.pseudos[t] = de(t);

        function ge() {}

        function ve(e) {
            for (var t = 0, i = e.length, s = ""; t < i; t++)
                s += e[t].value;
            return s
        }

        function be(e, t, i) {
            var s = t.dir,
                n = t.next,
                a = n || s,
                o = i && "parentNode" === a,
                r = C++;
            return t.first ? function(t, i, n) {
                    for (; t = t[s];)
                        if (1 === t.nodeType || o)
                            return e(t, i, n);
                    return !1
                } :
                function(t, i, l) {
                    var h, u, c, d = [x, r];
                    if (l) {
                        for (; t = t[s];)
                            if ((1 === t.nodeType || o) && e(t, i, l))
                                return !0
                    } else
                        for (; t = t[s];)
                            if (1 === t.nodeType || o)
                                if (u = (c = t[_] || (t[_] = {}))[t.uniqueID] || (c[t.uniqueID] = {}),
                                    n && n === t.nodeName.toLowerCase())
                                    t = t[s] || t;
                                else {
                                    if ((h = u[a]) && h[0] === x && h[1] === r)
                                        return d[2] = h[2];
                                    if (u[a] = d,
                                        d[2] = e(t, i, l))
                                        return !0
                                }
                    return !1
                }
        }

        function ye(e) {
            return e.length > 1 ? function(t, i, s) {
                    for (var n = e.length; n--;)
                        if (!e[n](t, i, s))
                            return !1;
                    return !0
                } :
                e[0]
        }

        function _e(e, t, i, s, n) {
            for (var a, o = [], r = 0, l = e.length, h = null != t; r < l; r++)
                (a = e[r]) && (i && !i(a, s, n) || (o.push(a),
                    h && t.push(r)));
            return o
        }

        function we(e, t, i, s, n, a) {
            return s && !s[_] && (s = we(s)),
                n && !n[_] && (n = we(n, a)),
                re(function(a, o, r, l) {
                    var h, u, c, d = [],
                        p = [],
                        f = o.length,
                        m = a || function(e, t, i) {
                            for (var s = 0, n = t.length; s < n; s++)
                                ae(e, t[s], i);
                            return i
                        }(t || "*", r.nodeType ? [r] : r, []),
                        g = !e || !a && t ? m : _e(m, d, e, r, l),
                        v = i ? n || (a ? e : f || s) ? [] : o : g;
                    if (i && i(g, v, r, l),
                        s)
                        for (h = _e(v, p),
                            s(h, [], r, l),
                            u = h.length; u--;)
                            (c = h[u]) && (v[p[u]] = !(g[p[u]] = c));
                    if (a) {
                        if (n || e) {
                            if (n) {
                                for (h = [],
                                    u = v.length; u--;)
                                    (c = v[u]) && h.push(g[u] = c);
                                n(null, v = [], h, l)
                            }
                            for (u = v.length; u--;)
                                (c = v[u]) && (h = n ? O(a, c) : d[u]) > -1 && (a[h] = !(o[h] = c))
                        }
                    } else
                        v = _e(v === o ? v.splice(f, v.length) : v),
                        n ? n(null, o, v, l) : z.apply(o, v)
                })
        }

        function xe(e) {
            for (var t, i, n, a = e.length, o = s.relative[e[0].type], r = o || s.relative[" "], l = o ? 1 : 0, u = be(function(e) {
                    return e === t
                }, r, !0), c = be(function(e) {
                    return O(t, e) > -1
                }, r, !0), d = [function(e, i, s) {
                    var n = !o && (s || i !== h) || ((t = i).nodeType ? u(e, i, s) : c(e, i, s));
                    return t = null,
                        n
                }]; l < a; l++)
                if (i = s.relative[e[l].type])
                    d = [be(ye(d), i)];
                else {
                    if ((i = s.filter[e[l].type].apply(null, e[l].matches))[_]) {
                        for (n = ++l; n < a && !s.relative[e[n].type]; n++)
                        ;
                        return we(l > 1 && ye(d), l > 1 && ve(e.slice(0, l - 1).concat({
                            value: " " === e[l - 2].type ? "*" : ""
                        })).replace(R, "$1"), i, l < n && xe(e.slice(l, n)), n < a && xe(e = e.slice(n)), n < a && ve(e))
                    }
                    d.push(i)
                }
            return ye(d)
        }

        function Ce(e, t) {
            var i = t.length > 0,
                n = e.length > 0,
                a = function(a, o, r, l, u) {
                    var c, f, g, v = 0,
                        b = "0",
                        y = a && [],
                        _ = [],
                        w = h,
                        C = a || n && s.find.TAG("*", u),
                        T = x += null == w ? 1 : Math.random() || .1,
                        k = C.length;
                    for (u && (h = o === p || o || u); b !== k && null != (c = C[b]); b++) {
                        if (n && c) {
                            for (f = 0,
                                o || c.ownerDocument === p || (d(c),
                                    r = !m); g = e[f++];)
                                if (g(c, o || p, r)) {
                                    l.push(c);
                                    break
                                }
                            u && (x = T)
                        }
                        i && ((c = !g && c) && v--,
                            a && y.push(c))
                    }
                    if (v += b,
                        i && b !== v) {
                        for (f = 0; g = t[f++];)
                            g(y, _, o, r);
                        if (a) {
                            if (v > 0)
                                for (; b--;)
                                    y[b] || _[b] || (_[b] = P.call(l));
                            _ = _e(_)
                        }
                        z.apply(l, _),
                            u && !a && _.length > 0 && v + t.length > 1 && ae.uniqueSort(l)
                    }
                    return u && (x = T,
                            h = w),
                        y
                };
            return i ? re(a) : a
        }
        return ge.prototype = s.filters = s.pseudos,
            s.setFilters = new ge,
            o = ae.tokenize = function(e, t) {
                var i, n, a, o, r, l, h, u = k[e + " "];
                if (u)
                    return t ? 0 : u.slice(0);
                for (r = e,
                    l = [],
                    h = s.preFilter; r;) {
                    for (o in i && !(n = B.exec(r)) || (n && (r = r.slice(n[0].length) || r),
                            l.push(a = [])),
                        i = !1,
                        (n = j.exec(r)) && (i = n.shift(),
                            a.push({
                                value: i,
                                type: n[0].replace(R, " ")
                            }),
                            r = r.slice(i.length)),
                        s.filter)
                        !(n = X[o].exec(r)) || h[o] && !(n = h[o](n)) || (i = n.shift(),
                            a.push({
                                value: i,
                                type: o,
                                matches: n
                            }),
                            r = r.slice(i.length));
                    if (!i)
                        break
                }
                return t ? r.length : r ? ae.error(e) : k(e, l).slice(0)
            },
            r = ae.compile = function(e, t) {
                var i, s = [],
                    n = [],
                    a = S[e + " "];
                if (!a) {
                    for (t || (t = o(e)),
                        i = t.length; i--;)
                        (a = xe(t[i]))[_] ? s.push(a) : n.push(a);
                    (a = S(e, Ce(n, s))).selector = e
                }
                return a
            },
            l = ae.select = function(e, t, i, n) {
                var a, l, h, u, c, d = "function" == typeof e && e,
                    p = !n && o(e = d.selector || e);
                if (i = i || [],
                    1 === p.length) {
                    if ((l = p[0] = p[0].slice(0)).length > 2 && "ID" === (h = l[0]).type && 9 === t.nodeType && m && s.relative[l[1].type]) {
                        if (!(t = (s.find.ID(h.matches[0].replace(Z, ee), t) || [])[0]))
                            return i;
                        d && (t = t.parentNode),
                            e = e.slice(l.shift().value.length)
                    }
                    for (a = X.needsContext.test(e) ? 0 : l.length; a-- && !s.relative[u = (h = l[a]).type];)
                        if ((c = s.find[u]) && (n = c(h.matches[0].replace(Z, ee), J.test(l[0].type) && me(t.parentNode) || t))) {
                            if (l.splice(a, 1), !(e = n.length && ve(l)))
                                return z.apply(i, n),
                                    i;
                            break
                        }
                }
                return (d || r(e, p))(n, t, !m, i, !t || J.test(e) && me(t.parentNode) || t),
                    i
            },
            i.sortStable = _.split("").sort(E).join("") === _,
            i.detectDuplicates = !!c,
            d(),
            i.sortDetached = le(function(e) {
                return 1 & e.compareDocumentPosition(p.createElement("fieldset"))
            }),
            le(function(e) {
                return e.innerHTML = "<a href='#'></a>",
                    "#" === e.firstChild.getAttribute("href")
            }) || he("type|href|height|width", function(e, t, i) {
                if (!i)
                    return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
            }),
            i.attributes && le(function(e) {
                return e.innerHTML = "<input/>",
                    e.firstChild.setAttribute("value", ""),
                    "" === e.firstChild.getAttribute("value")
            }) || he("value", function(e, t, i) {
                if (!i && "input" === e.nodeName.toLowerCase())
                    return e.defaultValue
            }),
            le(function(e) {
                return null == e.getAttribute("disabled")
            }) || he(H, function(e, t, i) {
                var s;
                if (!i)
                    return !0 === e[t] ? t.toLowerCase() : (s = e.getAttributeNode(t)) && s.specified ? s.value : null
            }),
            ae
    }(e);
    _.find = C,
        _.expr = C.selectors,
        _.expr[":"] = _.expr.pseudos,
        _.uniqueSort = _.unique = C.uniqueSort,
        _.text = C.getText,
        _.isXMLDoc = C.isXML,
        _.contains = C.contains,
        _.escapeSelector = C.escape;
    var T = function(e, t, i) {
            for (var s = [], n = void 0 !== i;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (n && _(e).is(i))
                        break;
                    s.push(e)
                }
            return s
        },
        k = function(e, t) {
            for (var i = []; e; e = e.nextSibling)
                1 === e.nodeType && e !== t && i.push(e);
            return i
        },
        S = _.expr.match.needsContext;

    function E(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }
    var D = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

    function M(e, t, i) {
        return m(t) ? _.grep(e, function(e, s) {
            return !!t.call(e, s, e) !== i
        }) : t.nodeType ? _.grep(e, function(e) {
            return e === t !== i
        }) : "string" != typeof t ? _.grep(e, function(e) {
            return l.call(t, e) > -1 !== i
        }) : _.filter(t, e, i)
    }
    _.filter = function(e, t, i) {
            var s = t[0];
            return i && (e = ":not(" + e + ")"),
                1 === t.length && 1 === s.nodeType ? _.find.matchesSelector(s, e) ? [s] : [] : _.find.matches(e, _.grep(t, function(e) {
                    return 1 === e.nodeType
                }))
        },
        _.fn.extend({
            find: function(e) {
                var t, i, s = this.length,
                    n = this;
                if ("string" != typeof e)
                    return this.pushStack(_(e).filter(function() {
                        for (t = 0; t < s; t++)
                            if (_.contains(n[t], this))
                                return !0
                    }));
                for (i = this.pushStack([]),
                    t = 0; t < s; t++)
                    _.find(e, n[t], i);
                return s > 1 ? _.uniqueSort(i) : i
            },
            filter: function(e) {
                return this.pushStack(M(this, e || [], !1))
            },
            not: function(e) {
                return this.pushStack(M(this, e || [], !0))
            },
            is: function(e) {
                return !!M(this, "string" == typeof e && S.test(e) ? _(e) : e || [], !1).length
            }
        });
    var P, I = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (_.fn.init = function(e, t, i) {
        var n, a;
        if (!e)
            return this;
        if (i = i || P,
            "string" == typeof e) {
            if (!(n = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : I.exec(e)) || !n[1] && t)
                return !t || t.jquery ? (t || i).find(e) : this.constructor(t).find(e);
            if (n[1]) {
                if (_.merge(this, _.parseHTML(n[1], (t = t instanceof _ ? t[0] : t) && t.nodeType ? t.ownerDocument || t : s, !0)),
                    D.test(n[1]) && _.isPlainObject(t))
                    for (n in t)
                        m(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                return this
            }
            return (a = s.getElementById(n[2])) && (this[0] = a,
                    this.length = 1),
                this
        }
        return e.nodeType ? (this[0] = e,
            this.length = 1,
            this) : m(e) ? void 0 !== i.ready ? i.ready(e) : e(_) : _.makeArray(e, this)
    }).prototype = _.fn,
        P = _(s);
    var z = /^(?:parents|prev(?:Until|All))/,
        A = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };

    function O(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;)
        ;
        return e
    }
    _.fn.extend({
            has: function(e) {
                var t = _(e, this),
                    i = t.length;
                return this.filter(function() {
                    for (var e = 0; e < i; e++)
                        if (_.contains(this, t[e]))
                            return !0
                })
            },
            closest: function(e, t) {
                var i, s = 0,
                    n = this.length,
                    a = [],
                    o = "string" != typeof e && _(e);
                if (!S.test(e))
                    for (; s < n; s++)
                        for (i = this[s]; i && i !== t; i = i.parentNode)
                            if (i.nodeType < 11 && (o ? o.index(i) > -1 : 1 === i.nodeType && _.find.matchesSelector(i, e))) {
                                a.push(i);
                                break
                            }
                return this.pushStack(a.length > 1 ? _.uniqueSort(a) : a)
            },
            index: function(e) {
                return e ? "string" == typeof e ? l.call(_(e), this[0]) : l.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function(e, t) {
                return this.pushStack(_.uniqueSort(_.merge(this.get(), _(e, t))))
            },
            addBack: function(e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        }),
        _.each({
            parent: function(e) {
                var t = e.parentNode;
                return t && 11 !== t.nodeType ? t : null
            },
            parents: function(e) {
                return T(e, "parentNode")
            },
            parentsUntil: function(e, t, i) {
                return T(e, "parentNode", i)
            },
            next: function(e) {
                return O(e, "nextSibling")
            },
            prev: function(e) {
                return O(e, "previousSibling")
            },
            nextAll: function(e) {
                return T(e, "nextSibling")
            },
            prevAll: function(e) {
                return T(e, "previousSibling")
            },
            nextUntil: function(e, t, i) {
                return T(e, "nextSibling", i)
            },
            prevUntil: function(e, t, i) {
                return T(e, "previousSibling", i)
            },
            siblings: function(e) {
                return k((e.parentNode || {}).firstChild, e)
            },
            children: function(e) {
                return k(e.firstChild)
            },
            contents: function(e) {
                return E(e, "iframe") ? e.contentDocument : (E(e, "template") && (e = e.content || e),
                    _.merge([], e.childNodes))
            }
        }, function(e, t) {
            _.fn[e] = function(i, s) {
                var n = _.map(this, t, i);
                return "Until" !== e.slice(-5) && (s = i),
                    s && "string" == typeof s && (n = _.filter(s, n)),
                    this.length > 1 && (A[e] || _.uniqueSort(n),
                        z.test(e) && n.reverse()),
                    this.pushStack(n)
            }
        });
    var H = /[^\x20\t\r\n\f]+/g;

    function N(e) {
        return e
    }

    function L(e) {
        throw e
    }

    function W(e, t, i, s) {
        var n;
        try {
            e && m(n = e.promise) ? n.call(e).done(t).fail(i) : e && m(n = e.then) ? n.call(e, t, i) : t.apply(void 0, [e].slice(s))
        } catch (e) {
            i.apply(void 0, [e])
        }
    }
    _.Callbacks = function(e) {
            e = "string" == typeof e ? function(e) {
                var t = {};
                return _.each(e.match(H) || [], function(e, i) {
                        t[i] = !0
                    }),
                    t
            }(e) : _.extend({}, e);
            var t, i, s, n, a = [],
                o = [],
                r = -1,
                l = function() {
                    for (n = n || e.once,
                        s = t = !0; o.length; r = -1)
                        for (i = o.shift(); ++r < a.length;)
                            !1 === a[r].apply(i[0], i[1]) && e.stopOnFalse && (r = a.length,
                                i = !1);
                    e.memory || (i = !1),
                        t = !1,
                        n && (a = i ? [] : "")
                },
                h = {
                    add: function() {
                        return a && (i && !t && (r = a.length - 1,
                                    o.push(i)),
                                function t(i) {
                                    _.each(i, function(i, s) {
                                        m(s) ? e.unique && h.has(s) || a.push(s) : s && s.length && "string" !== y(s) && t(s)
                                    })
                                }(arguments),
                                i && !t && l()),
                            this
                    },
                    remove: function() {
                        return _.each(arguments, function(e, t) {
                                for (var i;
                                    (i = _.inArray(t, a, i)) > -1;)
                                    a.splice(i, 1),
                                    i <= r && r--
                            }),
                            this
                    },
                    has: function(e) {
                        return e ? _.inArray(e, a) > -1 : a.length > 0
                    },
                    empty: function() {
                        return a && (a = []),
                            this
                    },
                    disable: function() {
                        return n = o = [],
                            a = i = "",
                            this
                    },
                    disabled: function() {
                        return !a
                    },
                    lock: function() {
                        return n = o = [],
                            i || t || (a = i = ""),
                            this
                    },
                    locked: function() {
                        return !!n
                    },
                    fireWith: function(e, i) {
                        return n || (i = [e, (i = i || []).slice ? i.slice() : i],
                                o.push(i),
                                t || l()),
                            this
                    },
                    fire: function() {
                        return h.fireWith(this, arguments),
                            this
                    },
                    fired: function() {
                        return !!s
                    }
                };
            return h
        },
        _.extend({
            Deferred: function(t) {
                var i = [
                        ["notify", "progress", _.Callbacks("memory"), _.Callbacks("memory"), 2],
                        ["resolve", "done", _.Callbacks("once memory"), _.Callbacks("once memory"), 0, "resolved"],
                        ["reject", "fail", _.Callbacks("once memory"), _.Callbacks("once memory"), 1, "rejected"]
                    ],
                    s = "pending",
                    n = {
                        state: function() {
                            return s
                        },
                        always: function() {
                            return a.done(arguments).fail(arguments),
                                this
                        },
                        catch: function(e) {
                            return n.then(null, e)
                        },
                        pipe: function() {
                            var e = arguments;
                            return _.Deferred(function(t) {
                                _.each(i, function(i, s) {
                                        var n = m(e[s[4]]) && e[s[4]];
                                        a[s[1]](function() {
                                            var e = n && n.apply(this, arguments);
                                            e && m(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[s[0] + "With"](this, n ? [e] : arguments)
                                        })
                                    }),
                                    e = null
                            }).promise()
                        },
                        then: function(t, s, n) {
                            var a = 0;

                            function o(t, i, s, n) {
                                return function() {
                                    var r = this,
                                        l = arguments,
                                        h = function() {
                                            var e, h;
                                            if (!(t < a)) {
                                                if ((e = s.apply(r, l)) === i.promise())
                                                    throw new TypeError("Thenable self-resolution");
                                                m(h = e && ("object" == typeof e || "function" == typeof e) && e.then) ? n ? h.call(e, o(a, i, N, n), o(a, i, L, n)) : h.call(e, o(++a, i, N, n), o(a, i, L, n), o(a, i, N, i.notifyWith)) : (s !== N && (r = void 0,
                                                        l = [e]),
                                                    (n || i.resolveWith)(r, l))
                                            }
                                        },
                                        u = n ? h : function() {
                                            try {
                                                h()
                                            } catch (e) {
                                                _.Deferred.exceptionHook && _.Deferred.exceptionHook(e, u.stackTrace),
                                                    t + 1 >= a && (s !== L && (r = void 0,
                                                            l = [e]),
                                                        i.rejectWith(r, l))
                                            }
                                        };
                                    t ? u() : (_.Deferred.getStackHook && (u.stackTrace = _.Deferred.getStackHook()),
                                        e.setTimeout(u))
                                }
                            }
                            return _.Deferred(function(e) {
                                i[0][3].add(o(0, e, m(n) ? n : N, e.notifyWith)),
                                    i[1][3].add(o(0, e, m(t) ? t : N)),
                                    i[2][3].add(o(0, e, m(s) ? s : L))
                            }).promise()
                        },
                        promise: function(e) {
                            return null != e ? _.extend(e, n) : n
                        }
                    },
                    a = {};
                return _.each(i, function(e, t) {
                        var o = t[2],
                            r = t[5];
                        n[t[1]] = o.add,
                            r && o.add(function() {
                                s = r
                            }, i[3 - e][2].disable, i[3 - e][3].disable, i[0][2].lock, i[0][3].lock),
                            o.add(t[3].fire),
                            a[t[0]] = function() {
                                return a[t[0] + "With"](this === a ? void 0 : this, arguments),
                                    this
                            },
                            a[t[0] + "With"] = o.fireWith
                    }),
                    n.promise(a),
                    t && t.call(a, a),
                    a
            },
            when: function(e) {
                var t = arguments.length,
                    i = t,
                    s = Array(i),
                    n = a.call(arguments),
                    o = _.Deferred(),
                    r = function(e) {
                        return function(i) {
                            s[e] = this,
                                n[e] = arguments.length > 1 ? a.call(arguments) : i,
                                --t || o.resolveWith(s, n)
                        }
                    };
                if (t <= 1 && (W(e, o.done(r(i)).resolve, o.reject, !t),
                        "pending" === o.state() || m(n[i] && n[i].then)))
                    return o.then();
                for (; i--;)
                    W(n[i], r(i), o.reject);
                return o.promise()
            }
        });
    var $ = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    _.Deferred.exceptionHook = function(t, i) {
            e.console && e.console.warn && t && $.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, i)
        },
        _.readyException = function(t) {
            e.setTimeout(function() {
                throw t
            })
        };
    var F = _.Deferred();

    function R() {
        s.removeEventListener("DOMContentLoaded", R),
            e.removeEventListener("load", R),
            _.ready()
    }
    _.fn.ready = function(e) {
            return F.then(e).catch(function(e) {
                    _.readyException(e)
                }),
                this
        },
        _.extend({
            isReady: !1,
            readyWait: 1,
            ready: function(e) {
                (!0 === e ? --_.readyWait : _.isReady) || (_.isReady = !0, !0 !== e && --_.readyWait > 0 || F.resolveWith(s, [_]))
            }
        }),
        _.ready.then = F.then,
        "complete" === s.readyState || "loading" !== s.readyState && !s.documentElement.doScroll ? e.setTimeout(_.ready) : (s.addEventListener("DOMContentLoaded", R),
            e.addEventListener("load", R));
    var B = function(e, t, i, s, n, a, o) {
            var r = 0,
                l = e.length,
                h = null == i;
            if ("object" === y(i))
                for (r in n = !0,
                    i)
                    B(e, t, r, i[r], !0, a, o);
            else if (void 0 !== s && (n = !0,
                    m(s) || (o = !0),
                    h && (o ? (t.call(e, s),
                        t = null) : (h = t,
                        t = function(e, t, i) {
                            return h.call(_(e), i)
                        }
                    )),
                    t))
                for (; r < l; r++)
                    t(e[r], i, o ? s : s.call(e[r], r, t(e[r], i)));
            return n ? e : h ? t.call(e) : l ? t(e[0], i) : a
        },
        j = /^-ms-/,
        q = /-([a-z])/g;

    function Y(e, t) {
        return t.toUpperCase()
    }

    function V(e) {
        return e.replace(j, "ms-").replace(q, Y)
    }
    var X = function(e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
    };

    function G() {
        this.expando = _.expando + G.uid++
    }
    G.uid = 1,
        G.prototype = {
            cache: function(e) {
                var t = e[this.expando];
                return t || (t = {},
                        X(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                            value: t,
                            configurable: !0
                        }))),
                    t
            },
            set: function(e, t, i) {
                var s, n = this.cache(e);
                if ("string" == typeof t)
                    n[V(t)] = i;
                else
                    for (s in t)
                        n[V(s)] = t[s];
                return n
            },
            get: function(e, t) {
                return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][V(t)]
            },
            access: function(e, t, i) {
                return void 0 === t || t && "string" == typeof t && void 0 === i ? this.get(e, t) : (this.set(e, t, i),
                    void 0 !== i ? i : t)
            },
            remove: function(e, t) {
                var i, s = e[this.expando];
                if (void 0 !== s) {
                    if (void 0 !== t) {
                        i = (t = Array.isArray(t) ? t.map(V) : (t = V(t)) in s ? [t] : t.match(H) || []).length;
                        for (; i--;)
                            delete s[t[i]]
                    }
                    (void 0 === t || _.isEmptyObject(s)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
                }
            },
            hasData: function(e) {
                var t = e[this.expando];
                return void 0 !== t && !_.isEmptyObject(t)
            }
        };
    var U = new G,
        K = new G,
        Q = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        J = /[A-Z]/g;

    function Z(e, t, i) {
        var s;
        if (void 0 === i && 1 === e.nodeType)
            if (s = "data-" + t.replace(J, "-$&").toLowerCase(),
                "string" == typeof(i = e.getAttribute(s))) {
                try {
                    i = function(e) {
                        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Q.test(e) ? JSON.parse(e) : e)
                    }(i)
                } catch (e) {}
                K.set(e, t, i)
            } else
                i = void 0;
        return i
    }
    _.extend({
            hasData: function(e) {
                return K.hasData(e) || U.hasData(e)
            },
            data: function(e, t, i) {
                return K.access(e, t, i)
            },
            removeData: function(e, t) {
                K.remove(e, t)
            },
            _data: function(e, t, i) {
                return U.access(e, t, i)
            },
            _removeData: function(e, t) {
                U.remove(e, t)
            }
        }),
        _.fn.extend({
            data: function(e, t) {
                var i, s, n, a = this[0],
                    o = a && a.attributes;
                if (void 0 === e) {
                    if (this.length && (n = K.get(a),
                            1 === a.nodeType && !U.get(a, "hasDataAttrs"))) {
                        for (i = o.length; i--;)
                            o[i] && 0 === (s = o[i].name).indexOf("data-") && (s = V(s.slice(5)),
                                Z(a, s, n[s]));
                        U.set(a, "hasDataAttrs", !0)
                    }
                    return n
                }
                return "object" == typeof e ? this.each(function() {
                    K.set(this, e)
                }) : B(this, function(t) {
                    var i;
                    if (a && void 0 === t) {
                        if (void 0 !== (i = K.get(a, e)))
                            return i;
                        if (void 0 !== (i = Z(a, e)))
                            return i
                    } else
                        this.each(function() {
                            K.set(this, e, t)
                        })
                }, null, t, arguments.length > 1, null, !0)
            },
            removeData: function(e) {
                return this.each(function() {
                    K.remove(this, e)
                })
            }
        }),
        _.extend({
            queue: function(e, t, i) {
                var s;
                if (e)
                    return s = U.get(e, t = (t || "fx") + "queue"),
                        i && (!s || Array.isArray(i) ? s = U.access(e, t, _.makeArray(i)) : s.push(i)),
                        s || []
            },
            dequeue: function(e, t) {
                var i = _.queue(e, t = t || "fx"),
                    s = i.length,
                    n = i.shift(),
                    a = _._queueHooks(e, t);
                "inprogress" === n && (n = i.shift(),
                        s--),
                    n && ("fx" === t && i.unshift("inprogress"),
                        delete a.stop,
                        n.call(e, function() {
                            _.dequeue(e, t)
                        }, a)), !s && a && a.empty.fire()
            },
            _queueHooks: function(e, t) {
                var i = t + "queueHooks";
                return U.get(e, i) || U.access(e, i, {
                    empty: _.Callbacks("once memory").add(function() {
                        U.remove(e, [t + "queue", i])
                    })
                })
            }
        }),
        _.fn.extend({
            queue: function(e, t) {
                var i = 2;
                return "string" != typeof e && (t = e,
                        e = "fx",
                        i--),
                    arguments.length < i ? _.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                        var i = _.queue(this, e, t);
                        _._queueHooks(this, e),
                            "fx" === e && "inprogress" !== i[0] && _.dequeue(this, e)
                    })
            },
            dequeue: function(e) {
                return this.each(function() {
                    _.dequeue(this, e)
                })
            },
            clearQueue: function(e) {
                return this.queue(e || "fx", [])
            },
            promise: function(e, t) {
                var i, s = 1,
                    n = _.Deferred(),
                    a = this,
                    o = this.length,
                    r = function() {
                        --s || n.resolveWith(a, [a])
                    };
                for ("string" != typeof e && (t = e,
                        e = void 0),
                    e = e || "fx"; o--;)
                    (i = U.get(a[o], e + "queueHooks")) && i.empty && (s++,
                        i.empty.add(r));
                return r(),
                    n.promise(t)
            }
        });
    var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
        ie = ["Top", "Right", "Bottom", "Left"],
        se = function(e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && _.contains(e.ownerDocument, e) && "none" === _.css(e, "display")
        },
        ne = function(e, t, i, s) {
            var n, a, o = {};
            for (a in t)
                o[a] = e.style[a],
                e.style[a] = t[a];
            for (a in n = i.apply(e, s || []),
                t)
                e.style[a] = o[a];
            return n
        };

    function ae(e, t, i, s) {
        var n, a, o = 20,
            r = s ? function() {
                return s.cur()
            } :
            function() {
                return _.css(e, t, "")
            },
            l = r(),
            h = i && i[3] || (_.cssNumber[t] ? "" : "px"),
            u = (_.cssNumber[t] || "px" !== h && +l) && te.exec(_.css(e, t));
        if (u && u[3] !== h) {
            for (h = h || u[3],
                u = +(l /= 2) || 1; o--;)
                _.style(e, t, u + h),
                (1 - a) * (1 - (a = r() / l || .5)) <= 0 && (o = 0),
                u /= a;
            _.style(e, t, (u *= 2) + h),
                i = i || []
        }
        return i && (u = +u || +l || 0,
                n = i[1] ? u + (i[1] + 1) * i[2] : +i[2],
                s && (s.unit = h,
                    s.start = u,
                    s.end = n)),
            n
    }
    var oe = {};

    function re(e) {
        var t, i = e.ownerDocument,
            s = e.nodeName,
            n = oe[s];
        return n || (t = i.body.appendChild(i.createElement(s)),
            n = _.css(t, "display"),
            t.parentNode.removeChild(t),
            "none" === n && (n = "block"),
            oe[s] = n,
            n)
    }

    function le(e, t) {
        for (var i, s, n = [], a = 0, o = e.length; a < o; a++)
            (s = e[a]).style && (i = s.style.display,
                t ? ("none" === i && (n[a] = U.get(s, "display") || null,
                        n[a] || (s.style.display = "")),
                    "" === s.style.display && se(s) && (n[a] = re(s))) : "none" !== i && (n[a] = "none",
                    U.set(s, "display", i)));
        for (a = 0; a < o; a++)
            null != n[a] && (e[a].style.display = n[a]);
        return e
    }
    _.fn.extend({
        show: function() {
            return le(this, !0)
        },
        hide: function() {
            return le(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                se(this) ? _(this).show() : _(this).hide()
            })
        }
    });
    var he = /^(?:checkbox|radio)$/i,
        ue = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        ce = /^$|^module$|\/(?:java|ecma)script/i,
        de = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };

    function pe(e, t) {
        var i;
        return i = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [],
            void 0 === t || t && E(e, t) ? _.merge([e], i) : i
    }

    function fe(e, t) {
        for (var i = 0, s = e.length; i < s; i++)
            U.set(e[i], "globalEval", !t || U.get(t[i], "globalEval"))
    }
    de.optgroup = de.option,
        de.tbody = de.tfoot = de.colgroup = de.caption = de.thead,
        de.th = de.td;
    var me = /<|&#?\w+;/;

    function ge(e, t, i, s, n) {
        for (var a, o, r, l, h, u, c = t.createDocumentFragment(), d = [], p = 0, f = e.length; p < f; p++)
            if ((a = e[p]) || 0 === a)
                if ("object" === y(a))
                    _.merge(d, a.nodeType ? [a] : a);
                else if (me.test(a)) {
            for (o = o || c.appendChild(t.createElement("div")),
                r = (ue.exec(a) || ["", ""])[1].toLowerCase(),
                o.innerHTML = (l = de[r] || de._default)[1] + _.htmlPrefilter(a) + l[2],
                u = l[0]; u--;)
                o = o.lastChild;
            _.merge(d, o.childNodes),
                (o = c.firstChild).textContent = ""
        } else
            d.push(t.createTextNode(a));
        for (c.textContent = "",
            p = 0; a = d[p++];)
            if (s && _.inArray(a, s) > -1)
                n && n.push(a);
            else if (h = _.contains(a.ownerDocument, a),
            o = pe(c.appendChild(a), "script"),
            h && fe(o),
            i)
            for (u = 0; a = o[u++];)
                ce.test(a.type || "") && i.push(a);
        return c
    }! function() {
        var e = s.createDocumentFragment().appendChild(s.createElement("div")),
            t = s.createElement("input");
        t.setAttribute("type", "radio"),
            t.setAttribute("checked", "checked"),
            t.setAttribute("name", "t"),
            e.appendChild(t),
            f.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked,
            e.innerHTML = "<textarea>x</textarea>",
            f.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var ve = s.documentElement,
        be = /^key/,
        ye = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        _e = /^([^.]*)(?:\.(.+)|)/;

    function we() {
        return !0
    }

    function xe() {
        return !1
    }

    function Ce() {
        try {
            return s.activeElement
        } catch (e) {}
    }

    function Te(e, t, i, s, n, a) {
        var o, r;
        if ("object" == typeof t) {
            for (r in "string" != typeof i && (s = s || i,
                    i = void 0),
                t)
                Te(e, r, i, s, t[r], a);
            return e
        }
        if (null == s && null == n ? (n = i,
                s = i = void 0) : null == n && ("string" == typeof i ? (n = s,
                s = void 0) : (n = s,
                s = i,
                i = void 0)), !1 === n)
            n = xe;
        else if (!n)
            return e;
        return 1 === a && (o = n,
                (n = function(e) {
                    return _().off(e),
                        o.apply(this, arguments)
                }).guid = o.guid || (o.guid = _.guid++)),
            e.each(function() {
                _.event.add(this, t, n, s, i)
            })
    }
    _.event = {
            global: {},
            add: function(e, t, i, s, n) {
                var a, o, r, l, h, u, c, d, p, f, m, g = U.get(e);
                if (g)
                    for (i.handler && (i = (a = i).handler,
                            n = a.selector),
                        n && _.find.matchesSelector(ve, n),
                        i.guid || (i.guid = _.guid++),
                        (l = g.events) || (l = g.events = {}),
                        (o = g.handle) || (o = g.handle = function(t) {
                            return void 0 !== _ && _.event.triggered !== t.type ? _.event.dispatch.apply(e, arguments) : void 0
                        }),
                        h = (t = (t || "").match(H) || [""]).length; h--;)
                        p = m = (r = _e.exec(t[h]) || [])[1],
                        f = (r[2] || "").split(".").sort(),
                        p && (c = _.event.special[p] || {},
                            c = _.event.special[p = (n ? c.delegateType : c.bindType) || p] || {},
                            u = _.extend({
                                type: p,
                                origType: m,
                                data: s,
                                handler: i,
                                guid: i.guid,
                                selector: n,
                                needsContext: n && _.expr.match.needsContext.test(n),
                                namespace: f.join(".")
                            }, a),
                            (d = l[p]) || ((d = l[p] = []).delegateCount = 0,
                                c.setup && !1 !== c.setup.call(e, s, f, o) || e.addEventListener && e.addEventListener(p, o)),
                            c.add && (c.add.call(e, u),
                                u.handler.guid || (u.handler.guid = i.guid)),
                            n ? d.splice(d.delegateCount++, 0, u) : d.push(u),
                            _.event.global[p] = !0)
            },
            remove: function(e, t, i, s, n) {
                var a, o, r, l, h, u, c, d, p, f, m, g = U.hasData(e) && U.get(e);
                if (g && (l = g.events)) {
                    for (h = (t = (t || "").match(H) || [""]).length; h--;)
                        if (p = m = (r = _e.exec(t[h]) || [])[1],
                            f = (r[2] || "").split(".").sort(),
                            p) {
                            for (c = _.event.special[p] || {},
                                d = l[p = (s ? c.delegateType : c.bindType) || p] || [],
                                r = r[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                                o = a = d.length; a--;)
                                u = d[a], !n && m !== u.origType || i && i.guid !== u.guid || r && !r.test(u.namespace) || s && s !== u.selector && ("**" !== s || !u.selector) || (d.splice(a, 1),
                                    u.selector && d.delegateCount--,
                                    c.remove && c.remove.call(e, u));
                            o && !d.length && (c.teardown && !1 !== c.teardown.call(e, f, g.handle) || _.removeEvent(e, p, g.handle),
                                delete l[p])
                        } else
                            for (p in l)
                                _.event.remove(e, p + t[h], i, s, !0);
                    _.isEmptyObject(l) && U.remove(e, "handle events")
                }
            },
            dispatch: function(e) {
                var t, i, s, n, a, o, r = _.event.fix(e),
                    l = new Array(arguments.length),
                    h = (U.get(this, "events") || {})[r.type] || [],
                    u = _.event.special[r.type] || {};
                for (l[0] = r,
                    t = 1; t < arguments.length; t++)
                    l[t] = arguments[t];
                if (r.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, r)) {
                    for (o = _.event.handlers.call(this, r, h),
                        t = 0;
                        (n = o[t++]) && !r.isPropagationStopped();)
                        for (r.currentTarget = n.elem,
                            i = 0;
                            (a = n.handlers[i++]) && !r.isImmediatePropagationStopped();)
                            r.rnamespace && !r.rnamespace.test(a.namespace) || (r.handleObj = a,
                                r.data = a.data,
                                void 0 !== (s = ((_.event.special[a.origType] || {}).handle || a.handler).apply(n.elem, l)) && !1 === (r.result = s) && (r.preventDefault(),
                                    r.stopPropagation()));
                    return u.postDispatch && u.postDispatch.call(this, r),
                        r.result
                }
            },
            handlers: function(e, t) {
                var i, s, n, a, o, r = [],
                    l = t.delegateCount,
                    h = e.target;
                if (l && h.nodeType && !("click" === e.type && e.button >= 1))
                    for (; h !== this; h = h.parentNode || this)
                        if (1 === h.nodeType && ("click" !== e.type || !0 !== h.disabled)) {
                            for (a = [],
                                o = {},
                                i = 0; i < l; i++)
                                void 0 === o[n = (s = t[i]).selector + " "] && (o[n] = s.needsContext ? _(n, this).index(h) > -1 : _.find(n, this, null, [h]).length),
                                o[n] && a.push(s);
                            a.length && r.push({
                                elem: h,
                                handlers: a
                            })
                        }
                return h = this,
                    l < t.length && r.push({
                        elem: h,
                        handlers: t.slice(l)
                    }),
                    r
            },
            addProp: function(e, t) {
                Object.defineProperty(_.Event.prototype, e, {
                    enumerable: !0,
                    configurable: !0,
                    get: m(t) ? function() {
                            if (this.originalEvent)
                                return t(this.originalEvent)
                        } :
                        function() {
                            if (this.originalEvent)
                                return this.originalEvent[e]
                        },
                    set: function(t) {
                        Object.defineProperty(this, e, {
                            enumerable: !0,
                            configurable: !0,
                            writable: !0,
                            value: t
                        })
                    }
                })
            },
            fix: function(e) {
                return e[_.expando] ? e : new _.Event(e)
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        if (this !== Ce() && this.focus)
                            return this.focus(), !1
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        if (this === Ce() && this.blur)
                            return this.blur(), !1
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        if ("checkbox" === this.type && this.click && E(this, "input"))
                            return this.click(), !1
                    },
                    _default: function(e) {
                        return E(e.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(e) {
                        void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                    }
                }
            }
        },
        _.removeEvent = function(e, t, i) {
            e.removeEventListener && e.removeEventListener(t, i)
        },
        _.Event = function(e, t) {
            if (!(this instanceof _.Event))
                return new _.Event(e, t);
            e && e.type ? (this.originalEvent = e,
                    this.type = e.type,
                    this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? we : xe,
                    this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target,
                    this.currentTarget = e.currentTarget,
                    this.relatedTarget = e.relatedTarget) : this.type = e,
                t && _.extend(this, t),
                this.timeStamp = e && e.timeStamp || Date.now(),
                this[_.expando] = !0
        },
        _.Event.prototype = {
            constructor: _.Event,
            isDefaultPrevented: xe,
            isPropagationStopped: xe,
            isImmediatePropagationStopped: xe,
            isSimulated: !1,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = we,
                    e && !this.isSimulated && e.preventDefault()
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = we,
                    e && !this.isSimulated && e.stopPropagation()
            },
            stopImmediatePropagation: function() {
                var e = this.originalEvent;
                this.isImmediatePropagationStopped = we,
                    e && !this.isSimulated && e.stopImmediatePropagation(),
                    this.stopPropagation()
            }
        },
        _.each({
            altKey: !0,
            bubbles: !0,
            cancelable: !0,
            changedTouches: !0,
            ctrlKey: !0,
            detail: !0,
            eventPhase: !0,
            metaKey: !0,
            pageX: !0,
            pageY: !0,
            shiftKey: !0,
            view: !0,
            char: !0,
            charCode: !0,
            key: !0,
            keyCode: !0,
            button: !0,
            buttons: !0,
            clientX: !0,
            clientY: !0,
            offsetX: !0,
            offsetY: !0,
            pointerId: !0,
            pointerType: !0,
            screenX: !0,
            screenY: !0,
            targetTouches: !0,
            toElement: !0,
            touches: !0,
            which: function(e) {
                var t = e.button;
                return null == e.which && be.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && ye.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
            }
        }, _.event.addProp),
        _.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout",
            pointerenter: "pointerover",
            pointerleave: "pointerout"
        }, function(e, t) {
            _.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var i, s = e.relatedTarget,
                        n = e.handleObj;
                    return s && (s === this || _.contains(this, s)) || (e.type = n.origType,
                            i = n.handler.apply(this, arguments),
                            e.type = t),
                        i
                }
            }
        }),
        _.fn.extend({
            on: function(e, t, i, s) {
                return Te(this, e, t, i, s)
            },
            one: function(e, t, i, s) {
                return Te(this, e, t, i, s, 1)
            },
            off: function(e, t, i) {
                var s, n;
                if (e && e.preventDefault && e.handleObj)
                    return s = e.handleObj,
                        _(e.delegateTarget).off(s.namespace ? s.origType + "." + s.namespace : s.origType, s.selector, s.handler),
                        this;
                if ("object" == typeof e) {
                    for (n in e)
                        this.off(n, t, e[n]);
                    return this
                }
                return !1 !== t && "function" != typeof t || (i = t,
                        t = void 0), !1 === i && (i = xe),
                    this.each(function() {
                        _.event.remove(this, e, i, t)
                    })
            }
        });
    var ke = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Se = /<script|<style|<link/i,
        Ee = /checked\s*(?:[^=]|=\s*.checked.)/i,
        De = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Me(e, t) {
        return E(e, "table") && E(11 !== t.nodeType ? t : t.firstChild, "tr") && _(e).children("tbody")[0] || e
    }

    function Pe(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type,
            e
    }

    function Ie(e) {
        return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"),
            e
    }

    function ze(e, t) {
        var i, s, n, a, o, r, l, h;
        if (1 === t.nodeType) {
            if (U.hasData(e) && (a = U.access(e),
                    o = U.set(t, a),
                    h = a.events))
                for (n in delete o.handle,
                    o.events = {},
                    h)
                    for (i = 0,
                        s = h[n].length; i < s; i++)
                        _.event.add(t, n, h[n][i]);
            K.hasData(e) && (r = K.access(e),
                l = _.extend({}, r),
                K.set(t, l))
        }
    }

    function Ae(e, t) {
        var i = t.nodeName.toLowerCase();
        "input" === i && he.test(e.type) ? t.checked = e.checked : "input" !== i && "textarea" !== i || (t.defaultValue = e.defaultValue)
    }

    function Oe(e, t, i, s) {
        t = o.apply([], t);
        var n, a, r, l, h, u, c = 0,
            d = e.length,
            p = d - 1,
            g = t[0],
            v = m(g);
        if (v || d > 1 && "string" == typeof g && !f.checkClone && Ee.test(g))
            return e.each(function(n) {
                var a = e.eq(n);
                v && (t[0] = g.call(this, n, a.html())),
                    Oe(a, t, i, s)
            });
        if (d && (a = (n = ge(t, e[0].ownerDocument, !1, e, s)).firstChild,
                1 === n.childNodes.length && (n = a),
                a || s)) {
            for (l = (r = _.map(pe(n, "script"), Pe)).length; c < d; c++)
                h = n,
                c !== p && (h = _.clone(h, !0, !0),
                    l && _.merge(r, pe(h, "script"))),
                i.call(e[c], h, c);
            if (l)
                for (u = r[r.length - 1].ownerDocument,
                    _.map(r, Ie),
                    c = 0; c < l; c++)
                    ce.test((h = r[c]).type || "") && !U.access(h, "globalEval") && _.contains(u, h) && (h.src && "module" !== (h.type || "").toLowerCase() ? _._evalUrl && _._evalUrl(h.src) : b(h.textContent.replace(De, ""), u, h))
        }
        return e
    }

    function He(e, t, i) {
        for (var s, n = t ? _.filter(t, e) : e, a = 0; null != (s = n[a]); a++)
            i || 1 !== s.nodeType || _.cleanData(pe(s)),
            s.parentNode && (i && _.contains(s.ownerDocument, s) && fe(pe(s, "script")),
                s.parentNode.removeChild(s));
        return e
    }
    _.extend({
            htmlPrefilter: function(e) {
                return e.replace(ke, "<$1></$2>")
            },
            clone: function(e, t, i) {
                var s, n, a, o, r = e.cloneNode(!0),
                    l = _.contains(e.ownerDocument, e);
                if (!(f.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || _.isXMLDoc(e)))
                    for (o = pe(r),
                        s = 0,
                        n = (a = pe(e)).length; s < n; s++)
                        Ae(a[s], o[s]);
                if (t)
                    if (i)
                        for (a = a || pe(e),
                            o = o || pe(r),
                            s = 0,
                            n = a.length; s < n; s++)
                            ze(a[s], o[s]);
                    else
                        ze(e, r);
                return (o = pe(r, "script")).length > 0 && fe(o, !l && pe(e, "script")),
                    r
            },
            cleanData: function(e) {
                for (var t, i, s, n = _.event.special, a = 0; void 0 !== (i = e[a]); a++)
                    if (X(i)) {
                        if (t = i[U.expando]) {
                            if (t.events)
                                for (s in t.events)
                                    n[s] ? _.event.remove(i, s) : _.removeEvent(i, s, t.handle);
                            i[U.expando] = void 0
                        }
                        i[K.expando] && (i[K.expando] = void 0)
                    }
            }
        }),
        _.fn.extend({
            detach: function(e) {
                return He(this, e, !0)
            },
            remove: function(e) {
                return He(this, e)
            },
            text: function(e) {
                return B(this, function(e) {
                    return void 0 === e ? _.text(this) : this.empty().each(function() {
                        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                    })
                }, null, e, arguments.length)
            },
            append: function() {
                return Oe(this, arguments, function(e) {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Me(this, e).appendChild(e)
                })
            },
            prepend: function() {
                return Oe(this, arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = Me(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                })
            },
            before: function() {
                return Oe(this, arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this)
                })
            },
            after: function() {
                return Oe(this, arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                })
            },
            empty: function() {
                for (var e, t = 0; null != (e = this[t]); t++)
                    1 === e.nodeType && (_.cleanData(pe(e, !1)),
                        e.textContent = "");
                return this
            },
            clone: function(e, t) {
                return e = null != e && e,
                    t = null == t ? e : t,
                    this.map(function() {
                        return _.clone(this, e, t)
                    })
            },
            html: function(e) {
                return B(this, function(e) {
                    var t = this[0] || {},
                        i = 0,
                        s = this.length;
                    if (void 0 === e && 1 === t.nodeType)
                        return t.innerHTML;
                    if ("string" == typeof e && !Se.test(e) && !de[(ue.exec(e) || ["", ""])[1].toLowerCase()]) {
                        e = _.htmlPrefilter(e);
                        try {
                            for (; i < s; i++)
                                1 === (t = this[i] || {}).nodeType && (_.cleanData(pe(t, !1)),
                                    t.innerHTML = e);
                            t = 0
                        } catch (e) {}
                    }
                    t && this.empty().append(e)
                }, null, e, arguments.length)
            },
            replaceWith: function() {
                var e = [];
                return Oe(this, arguments, function(t) {
                    var i = this.parentNode;
                    _.inArray(this, e) < 0 && (_.cleanData(pe(this)),
                        i && i.replaceChild(t, this))
                }, e)
            }
        }),
        _.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(e, t) {
            _.fn[e] = function(e) {
                for (var i, s = [], n = _(e), a = n.length - 1, o = 0; o <= a; o++)
                    i = o === a ? this : this.clone(!0),
                    _(n[o])[t](i),
                    r.apply(s, i.get());
                return this.pushStack(s)
            }
        });
    var Ne = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
        Le = function(t) {
            var i = t.ownerDocument.defaultView;
            return i && i.opener || (i = e),
                i.getComputedStyle(t)
        },
        We = new RegExp(ie.join("|"), "i");

    function $e(e, t, i) {
        var s, n, a, o, r = e.style;
        return (i = i || Le(e)) && ("" !== (o = i.getPropertyValue(t) || i[t]) || _.contains(e.ownerDocument, e) || (o = _.style(e, t)), !f.pixelBoxStyles() && Ne.test(o) && We.test(t) && (s = r.width,
                n = r.minWidth,
                a = r.maxWidth,
                r.minWidth = r.maxWidth = r.width = o,
                o = i.width,
                r.width = s,
                r.minWidth = n,
                r.maxWidth = a)),
            void 0 !== o ? o + "" : o
    }

    function Fe(e, t) {
        return {
            get: function() {
                if (!e())
                    return (this.get = t).apply(this, arguments);
                delete this.get
            }
        }
    }! function() {
        function t() {
            if (u) {
                h.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",
                    u.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",
                    ve.appendChild(h).appendChild(u);
                var t = e.getComputedStyle(u);
                n = "1%" !== t.top,
                    l = 12 === i(t.marginLeft),
                    u.style.right = "60%",
                    r = 36 === i(t.right),
                    a = 36 === i(t.width),
                    u.style.position = "absolute",
                    o = 36 === u.offsetWidth || "absolute",
                    ve.removeChild(h),
                    u = null
            }
        }

        function i(e) {
            return Math.round(parseFloat(e))
        }
        var n, a, o, r, l, h = s.createElement("div"),
            u = s.createElement("div");
        u.style && (u.style.backgroundClip = "content-box",
            u.cloneNode(!0).style.backgroundClip = "",
            f.clearCloneStyle = "content-box" === u.style.backgroundClip,
            _.extend(f, {
                boxSizingReliable: function() {
                    return t(),
                        a
                },
                pixelBoxStyles: function() {
                    return t(),
                        r
                },
                pixelPosition: function() {
                    return t(),
                        n
                },
                reliableMarginLeft: function() {
                    return t(),
                        l
                },
                scrollboxSize: function() {
                    return t(),
                        o
                }
            }))
    }();
    var Re = /^(none|table(?!-c[ea]).+)/,
        Be = /^--/,
        je = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        qe = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        Ye = ["Webkit", "Moz", "ms"],
        Ve = s.createElement("div").style;

    function Xe(e) {
        var t = _.cssProps[e];
        return t || (t = _.cssProps[e] = function(e) {
                if (e in Ve)
                    return e;
                for (var t = e[0].toUpperCase() + e.slice(1), i = Ye.length; i--;)
                    if ((e = Ye[i] + t) in Ve)
                        return e
            }(e) || e),
            t
    }

    function Ge(e, t, i) {
        var s = te.exec(t);
        return s ? Math.max(0, s[2] - (i || 0)) + (s[3] || "px") : t
    }

    function Ue(e, t, i, s, n, a) {
        var o = "width" === t ? 1 : 0,
            r = 0,
            l = 0;
        if (i === (s ? "border" : "content"))
            return 0;
        for (; o < 4; o += 2)
            "margin" === i && (l += _.css(e, i + ie[o], !0, n)),
            s ? ("content" === i && (l -= _.css(e, "padding" + ie[o], !0, n)),
                "margin" !== i && (l -= _.css(e, "border" + ie[o] + "Width", !0, n))) : (l += _.css(e, "padding" + ie[o], !0, n),
                "padding" !== i ? l += _.css(e, "border" + ie[o] + "Width", !0, n) : r += _.css(e, "border" + ie[o] + "Width", !0, n));
        return !s && a >= 0 && (l += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - a - l - r - .5))),
            l
    }

    function Ke(e, t, i) {
        var s = Le(e),
            n = $e(e, t, s),
            a = "border-box" === _.css(e, "boxSizing", !1, s),
            o = a;
        if (Ne.test(n)) {
            if (!i)
                return n;
            n = "auto"
        }
        return o = o && (f.boxSizingReliable() || n === e.style[t]),
            ("auto" === n || !parseFloat(n) && "inline" === _.css(e, "display", !1, s)) && (n = e["offset" + t[0].toUpperCase() + t.slice(1)],
                o = !0),
            (n = parseFloat(n) || 0) + Ue(e, t, i || (a ? "border" : "content"), o, s, n) + "px"
    }

    function Qe(e, t, i, s, n) {
        return new Qe.prototype.init(e, t, i, s, n)
    }
    _.extend({
            cssHooks: {
                opacity: {
                    get: function(e, t) {
                        if (t) {
                            var i = $e(e, "opacity");
                            return "" === i ? "1" : i
                        }
                    }
                }
            },
            cssNumber: {
                animationIterationCount: !0,
                columnCount: !0,
                fillOpacity: !0,
                flexGrow: !0,
                flexShrink: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {},
            style: function(e, t, i, s) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var n, a, o, r = V(t),
                        l = Be.test(t),
                        h = e.style;
                    if (l || (t = Xe(r)),
                        o = _.cssHooks[t] || _.cssHooks[r],
                        void 0 === i)
                        return o && "get" in o && void 0 !== (n = o.get(e, !1, s)) ? n : h[t];
                    "string" == (a = typeof i) && (n = te.exec(i)) && n[1] && (i = ae(e, t, n),
                            a = "number"),
                        null != i && i == i && ("number" === a && (i += n && n[3] || (_.cssNumber[r] ? "" : "px")),
                            f.clearCloneStyle || "" !== i || 0 !== t.indexOf("background") || (h[t] = "inherit"),
                            o && "set" in o && void 0 === (i = o.set(e, i, s)) || (l ? h.setProperty(t, i) : h[t] = i))
                }
            },
            css: function(e, t, i, s) {
                var n, a, o, r = V(t);
                return Be.test(t) || (t = Xe(r)),
                    (o = _.cssHooks[t] || _.cssHooks[r]) && "get" in o && (n = o.get(e, !0, i)),
                    void 0 === n && (n = $e(e, t, s)),
                    "normal" === n && t in qe && (n = qe[t]),
                    "" === i || i ? (a = parseFloat(n), !0 === i || isFinite(a) ? a || 0 : n) : n
            }
        }),
        _.each(["height", "width"], function(e, t) {
            _.cssHooks[t] = {
                get: function(e, i, s) {
                    if (i)
                        return !Re.test(_.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Ke(e, t, s) : ne(e, je, function() {
                            return Ke(e, t, s)
                        })
                },
                set: function(e, i, s) {
                    var n, a = Le(e),
                        o = "border-box" === _.css(e, "boxSizing", !1, a),
                        r = s && Ue(e, t, s, o, a);
                    return o && f.scrollboxSize() === a.position && (r -= Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - parseFloat(a[t]) - Ue(e, t, "border", !1, a) - .5)),
                        r && (n = te.exec(i)) && "px" !== (n[3] || "px") && (e.style[t] = i,
                            i = _.css(e, t)),
                        Ge(0, i, r)
                }
            }
        }),
        _.cssHooks.marginLeft = Fe(f.reliableMarginLeft, function(e, t) {
            if (t)
                return (parseFloat($e(e, "marginLeft")) || e.getBoundingClientRect().left - ne(e, {
                    marginLeft: 0
                }, function() {
                    return e.getBoundingClientRect().left
                })) + "px"
        }),
        _.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(e, t) {
            _.cssHooks[e + t] = {
                    expand: function(i) {
                        for (var s = 0, n = {}, a = "string" == typeof i ? i.split(" ") : [i]; s < 4; s++)
                            n[e + ie[s] + t] = a[s] || a[s - 2] || a[0];
                        return n
                    }
                },
                "margin" !== e && (_.cssHooks[e + t].set = Ge)
        }),
        _.fn.extend({
            css: function(e, t) {
                return B(this, function(e, t, i) {
                    var s, n, a = {},
                        o = 0;
                    if (Array.isArray(t)) {
                        for (s = Le(e),
                            n = t.length; o < n; o++)
                            a[t[o]] = _.css(e, t[o], !1, s);
                        return a
                    }
                    return void 0 !== i ? _.style(e, t, i) : _.css(e, t)
                }, e, t, arguments.length > 1)
            }
        }),
        _.Tween = Qe,
        (Qe.prototype = {
            constructor: Qe,
            init: function(e, t, i, s, n, a) {
                this.elem = e,
                    this.prop = i,
                    this.easing = n || _.easing._default,
                    this.options = t,
                    this.start = this.now = this.cur(),
                    this.end = s,
                    this.unit = a || (_.cssNumber[i] ? "" : "px")
            },
            cur: function() {
                var e = Qe.propHooks[this.prop];
                return e && e.get ? e.get(this) : Qe.propHooks._default.get(this)
            },
            run: function(e) {
                var t, i = Qe.propHooks[this.prop];
                return this.pos = t = this.options.duration ? _.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e,
                    this.now = (this.end - this.start) * t + this.start,
                    this.options.step && this.options.step.call(this.elem, this.now, this),
                    i && i.set ? i.set(this) : Qe.propHooks._default.set(this),
                    this
            }
        }).init.prototype = Qe.prototype,
        (Qe.propHooks = {
            _default: {
                get: function(e) {
                    var t;
                    return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = _.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
                },
                set: function(e) {
                    _.fx.step[e.prop] ? _.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[_.cssProps[e.prop]] && !_.cssHooks[e.prop] ? e.elem[e.prop] = e.now : _.style(e.elem, e.prop, e.now + e.unit)
                }
            }
        }).scrollTop = Qe.propHooks.scrollLeft = {
            set: function(e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
            }
        },
        _.easing = {
            linear: function(e) {
                return e
            },
            swing: function(e) {
                return .5 - Math.cos(e * Math.PI) / 2
            },
            _default: "swing"
        },
        _.fx = Qe.prototype.init,
        _.fx.step = {};
    var Je, Ze, et = /^(?:toggle|show|hide)$/,
        tt = /queueHooks$/;

    function it() {
        Ze && (!1 === s.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(it) : e.setTimeout(it, _.fx.interval),
            _.fx.tick())
    }

    function st() {
        return e.setTimeout(function() {
                Je = void 0
            }),
            Je = Date.now()
    }

    function nt(e, t) {
        var i, s = 0,
            n = {
                height: e
            };
        for (t = t ? 1 : 0; s < 4; s += 2 - t)
            n["margin" + (i = ie[s])] = n["padding" + i] = e;
        return t && (n.opacity = n.width = e),
            n
    }

    function at(e, t, i) {
        for (var s, n = (ot.tweeners[t] || []).concat(ot.tweeners["*"]), a = 0, o = n.length; a < o; a++)
            if (s = n[a].call(i, t, e))
                return s
    }

    function ot(e, t, i) {
        var s, n, a = 0,
            o = ot.prefilters.length,
            r = _.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (n)
                    return !1;
                for (var t = Je || st(), i = Math.max(0, h.startTime + h.duration - t), s = 1 - (i / h.duration || 0), a = 0, o = h.tweens.length; a < o; a++)
                    h.tweens[a].run(s);
                return r.notifyWith(e, [h, s, i]),
                    s < 1 && o ? i : (o || r.notifyWith(e, [h, 1, 0]),
                        r.resolveWith(e, [h]), !1)
            },
            h = r.promise({
                elem: e,
                props: _.extend({}, t),
                opts: _.extend(!0, {
                    specialEasing: {},
                    easing: _.easing._default
                }, i),
                originalProperties: t,
                originalOptions: i,
                startTime: Je || st(),
                duration: i.duration,
                tweens: [],
                createTween: function(t, i) {
                    var s = _.Tween(e, h.opts, t, i, h.opts.specialEasing[t] || h.opts.easing);
                    return h.tweens.push(s),
                        s
                },
                stop: function(t) {
                    var i = 0,
                        s = t ? h.tweens.length : 0;
                    if (n)
                        return this;
                    for (n = !0; i < s; i++)
                        h.tweens[i].run(1);
                    return t ? (r.notifyWith(e, [h, 1, 0]),
                            r.resolveWith(e, [h, t])) : r.rejectWith(e, [h, t]),
                        this
                }
            }),
            u = h.props;
        for (function(e, t) {
                var i, s, n, a, o;
                for (i in e)
                    if (n = t[s = V(i)],
                        a = e[i],
                        Array.isArray(a) && (n = a[1],
                            a = e[i] = a[0]),
                        i !== s && (e[s] = a,
                            delete e[i]),
                        (o = _.cssHooks[s]) && "expand" in o)
                        for (i in a = o.expand(a),
                            delete e[s],
                            a)
                            i in e || (e[i] = a[i],
                                t[i] = n);
                    else
                        t[s] = n
            }(u, h.opts.specialEasing); a < o; a++)
            if (s = ot.prefilters[a].call(h, e, u, h.opts))
                return m(s.stop) && (_._queueHooks(h.elem, h.opts.queue).stop = s.stop.bind(s)),
                    s;
        return _.map(u, at, h),
            m(h.opts.start) && h.opts.start.call(e, h),
            h.progress(h.opts.progress).done(h.opts.done, h.opts.complete).fail(h.opts.fail).always(h.opts.always),
            _.fx.timer(_.extend(l, {
                elem: e,
                anim: h,
                queue: h.opts.queue
            })),
            h
    }
    _.Animation = _.extend(ot, {
            tweeners: {
                "*": [function(e, t) {
                    var i = this.createTween(e, t);
                    return ae(i.elem, e, te.exec(t), i),
                        i
                }]
            },
            tweener: function(e, t) {
                m(e) ? (t = e,
                    e = ["*"]) : e = e.match(H);
                for (var i, s = 0, n = e.length; s < n; s++)
                    (ot.tweeners[i = e[s]] = ot.tweeners[i] || []).unshift(t)
            },
            prefilters: [function(e, t, i) {
                var s, n, a, o, r, l, h, u, c = "width" in t || "height" in t,
                    d = this,
                    p = {},
                    f = e.style,
                    m = e.nodeType && se(e),
                    g = U.get(e, "fxshow");
                for (s in i.queue || (null == (o = _._queueHooks(e, "fx")).unqueued && (o.unqueued = 0,
                            r = o.empty.fire,
                            o.empty.fire = function() {
                                o.unqueued || r()
                            }
                        ),
                        o.unqueued++,
                        d.always(function() {
                            d.always(function() {
                                o.unqueued--,
                                    _.queue(e, "fx").length || o.empty.fire()
                            })
                        })),
                    t)
                    if (et.test(n = t[s])) {
                        if (delete t[s],
                            a = a || "toggle" === n,
                            n === (m ? "hide" : "show")) {
                            if ("show" !== n || !g || void 0 === g[s])
                                continue;
                            m = !0
                        }
                        p[s] = g && g[s] || _.style(e, s)
                    }
                if ((l = !_.isEmptyObject(t)) || !_.isEmptyObject(p))
                    for (s in c && 1 === e.nodeType && (i.overflow = [f.overflow, f.overflowX, f.overflowY],
                            null == (h = g && g.display) && (h = U.get(e, "display")),
                            "none" === (u = _.css(e, "display")) && (h ? u = h : (le([e], !0),
                                h = e.style.display || h,
                                u = _.css(e, "display"),
                                le([e]))),
                            ("inline" === u || "inline-block" === u && null != h) && "none" === _.css(e, "float") && (l || (d.done(function() {
                                        f.display = h
                                    }),
                                    null == h && (h = "none" === (u = f.display) ? "" : u)),
                                f.display = "inline-block")),
                        i.overflow && (f.overflow = "hidden",
                            d.always(function() {
                                f.overflow = i.overflow[0],
                                    f.overflowX = i.overflow[1],
                                    f.overflowY = i.overflow[2]
                            })),
                        l = !1,
                        p)
                        l || (g ? "hidden" in g && (m = g.hidden) : g = U.access(e, "fxshow", {
                                display: h
                            }),
                            a && (g.hidden = !m),
                            m && le([e], !0),
                            d.done(function() {
                                for (s in m || le([e]),
                                    U.remove(e, "fxshow"),
                                    p)
                                    _.style(e, s, p[s])
                            })),
                        l = at(m ? g[s] : 0, s, d),
                        s in g || (g[s] = l.start,
                            m && (l.end = l.start,
                                l.start = 0))
            }],
            prefilter: function(e, t) {
                t ? ot.prefilters.unshift(e) : ot.prefilters.push(e)
            }
        }),
        _.speed = function(e, t, i) {
            var s = e && "object" == typeof e ? _.extend({}, e) : {
                complete: i || !i && t || m(e) && e,
                duration: e,
                easing: i && t || t && !m(t) && t
            };
            return _.fx.off ? s.duration = 0 : "number" != typeof s.duration && (s.duration = s.duration in _.fx.speeds ? _.fx.speeds[s.duration] : _.fx.speeds._default),
                null != s.queue && !0 !== s.queue || (s.queue = "fx"),
                s.old = s.complete,
                s.complete = function() {
                    m(s.old) && s.old.call(this),
                        s.queue && _.dequeue(this, s.queue)
                },
                s
        },
        _.fn.extend({
            fadeTo: function(e, t, i, s) {
                return this.filter(se).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, i, s)
            },
            animate: function(e, t, i, s) {
                var n = _.isEmptyObject(e),
                    a = _.speed(t, i, s),
                    o = function() {
                        var t = ot(this, _.extend({}, e), a);
                        (n || U.get(this, "finish")) && t.stop(!0)
                    };
                return o.finish = o,
                    n || !1 === a.queue ? this.each(o) : this.queue(a.queue, o)
            },
            stop: function(e, t, i) {
                var s = function(e) {
                    var t = e.stop;
                    delete e.stop,
                        t(i)
                };
                return "string" != typeof e && (i = t,
                        t = e,
                        e = void 0),
                    t && !1 !== e && this.queue(e || "fx", []),
                    this.each(function() {
                        var t = !0,
                            n = null != e && e + "queueHooks",
                            a = _.timers,
                            o = U.get(this);
                        if (n)
                            o[n] && o[n].stop && s(o[n]);
                        else
                            for (n in o)
                                o[n] && o[n].stop && tt.test(n) && s(o[n]);
                        for (n = a.length; n--;)
                            a[n].elem !== this || null != e && a[n].queue !== e || (a[n].anim.stop(i),
                                t = !1,
                                a.splice(n, 1));
                        !t && i || _.dequeue(this, e)
                    })
            },
            finish: function(e) {
                return !1 !== e && (e = e || "fx"),
                    this.each(function() {
                        var t, i = U.get(this),
                            s = i[e + "queue"],
                            n = i[e + "queueHooks"],
                            a = _.timers,
                            o = s ? s.length : 0;
                        for (i.finish = !0,
                            _.queue(this, e, []),
                            n && n.stop && n.stop.call(this, !0),
                            t = a.length; t--;)
                            a[t].elem === this && a[t].queue === e && (a[t].anim.stop(!0),
                                a.splice(t, 1));
                        for (t = 0; t < o; t++)
                            s[t] && s[t].finish && s[t].finish.call(this);
                        delete i.finish
                    })
            }
        }),
        _.each(["toggle", "show", "hide"], function(e, t) {
            var i = _.fn[t];
            _.fn[t] = function(e, s, n) {
                return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(nt(t, !0), e, s, n)
            }
        }),
        _.each({
            slideDown: nt("show"),
            slideUp: nt("hide"),
            slideToggle: nt("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            _.fn[e] = function(e, i, s) {
                return this.animate(t, e, i, s)
            }
        }),
        _.timers = [],
        _.fx.tick = function() {
            var e, t = 0,
                i = _.timers;
            for (Je = Date.now(); t < i.length; t++)
                (e = i[t])() || i[t] !== e || i.splice(t--, 1);
            i.length || _.fx.stop(),
                Je = void 0
        },
        _.fx.timer = function(e) {
            _.timers.push(e),
                _.fx.start()
        },
        _.fx.interval = 13,
        _.fx.start = function() {
            Ze || (Ze = !0,
                it())
        },
        _.fx.stop = function() {
            Ze = null
        },
        _.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        },
        _.fn.delay = function(t, i) {
            return t = _.fx && _.fx.speeds[t] || t,
                this.queue(i = i || "fx", function(i, s) {
                    var n = e.setTimeout(i, t);
                    s.stop = function() {
                        e.clearTimeout(n)
                    }
                })
        },
        function() {
            var e = s.createElement("input"),
                t = s.createElement("select").appendChild(s.createElement("option"));
            e.type = "checkbox",
                f.checkOn = "" !== e.value,
                f.optSelected = t.selected,
                (e = s.createElement("input")).value = "t",
                e.type = "radio",
                f.radioValue = "t" === e.value
        }();
    var rt, lt = _.expr.attrHandle;
    _.fn.extend({
            attr: function(e, t) {
                return B(this, _.attr, e, t, arguments.length > 1)
            },
            removeAttr: function(e) {
                return this.each(function() {
                    _.removeAttr(this, e)
                })
            }
        }),
        _.extend({
            attr: function(e, t, i) {
                var s, n, a = e.nodeType;
                if (3 !== a && 8 !== a && 2 !== a)
                    return void 0 === e.getAttribute ? _.prop(e, t, i) : (1 === a && _.isXMLDoc(e) || (n = _.attrHooks[t.toLowerCase()] || (_.expr.match.bool.test(t) ? rt : void 0)),
                        void 0 !== i ? null === i ? void _.removeAttr(e, t) : n && "set" in n && void 0 !== (s = n.set(e, i, t)) ? s : (e.setAttribute(t, i + ""),
                            i) : n && "get" in n && null !== (s = n.get(e, t)) ? s : null == (s = _.find.attr(e, t)) ? void 0 : s)
            },
            attrHooks: {
                type: {
                    set: function(e, t) {
                        if (!f.radioValue && "radio" === t && E(e, "input")) {
                            var i = e.value;
                            return e.setAttribute("type", t),
                                i && (e.value = i),
                                t
                        }
                    }
                }
            },
            removeAttr: function(e, t) {
                var i, s = 0,
                    n = t && t.match(H);
                if (n && 1 === e.nodeType)
                    for (; i = n[s++];)
                        e.removeAttribute(i)
            }
        }),
        rt = {
            set: function(e, t, i) {
                return !1 === t ? _.removeAttr(e, i) : e.setAttribute(i, i),
                    i
            }
        },
        _.each(_.expr.match.bool.source.match(/\w+/g), function(e, t) {
            var i = lt[t] || _.find.attr;
            lt[t] = function(e, t, s) {
                var n, a, o = t.toLowerCase();
                return s || (a = lt[o],
                        lt[o] = n,
                        n = null != i(e, t, s) ? o : null,
                        lt[o] = a),
                    n
            }
        });
    var ht = /^(?:input|select|textarea|button)$/i,
        ut = /^(?:a|area)$/i;

    function ct(e) {
        return (e.match(H) || []).join(" ")
    }

    function dt(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function pt(e) {
        return Array.isArray(e) ? e : "string" == typeof e && e.match(H) || []
    }
    _.fn.extend({
            prop: function(e, t) {
                return B(this, _.prop, e, t, arguments.length > 1)
            },
            removeProp: function(e) {
                return this.each(function() {
                    delete this[_.propFix[e] || e]
                })
            }
        }),
        _.extend({
            prop: function(e, t, i) {
                var s, n, a = e.nodeType;
                if (3 !== a && 8 !== a && 2 !== a)
                    return 1 === a && _.isXMLDoc(e) || (n = _.propHooks[t = _.propFix[t] || t]),
                        void 0 !== i ? n && "set" in n && void 0 !== (s = n.set(e, i, t)) ? s : e[t] = i : n && "get" in n && null !== (s = n.get(e, t)) ? s : e[t]
            },
            propHooks: {
                tabIndex: {
                    get: function(e) {
                        var t = _.find.attr(e, "tabindex");
                        return t ? parseInt(t, 10) : ht.test(e.nodeName) || ut.test(e.nodeName) && e.href ? 0 : -1
                    }
                }
            },
            propFix: {
                for: "htmlFor",
                class: "className"
            }
        }),
        f.optSelected || (_.propHooks.selected = {
            get: function(e) {
                return null
            },
            set: function(e) {}
        }),
        _.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            _.propFix[this.toLowerCase()] = this
        }),
        _.fn.extend({
            addClass: function(e) {
                var t, i, s, n, a, o, r, l = 0;
                if (m(e))
                    return this.each(function(t) {
                        _(this).addClass(e.call(this, t, dt(this)))
                    });
                if ((t = pt(e)).length)
                    for (; i = this[l++];)
                        if (n = dt(i),
                            s = 1 === i.nodeType && " " + ct(n) + " ") {
                            for (o = 0; a = t[o++];)
                                s.indexOf(" " + a + " ") < 0 && (s += a + " ");
                            n !== (r = ct(s)) && i.setAttribute("class", r)
                        }
                return this
            },
            removeClass: function(e) {
                var t, i, s, n, a, o, r, l = 0;
                if (m(e))
                    return this.each(function(t) {
                        _(this).removeClass(e.call(this, t, dt(this)))
                    });
                if (!arguments.length)
                    return this.attr("class", "");
                if ((t = pt(e)).length)
                    for (; i = this[l++];)
                        if (n = dt(i),
                            s = 1 === i.nodeType && " " + ct(n) + " ") {
                            for (o = 0; a = t[o++];)
                                for (; s.indexOf(" " + a + " ") > -1;)
                                    s = s.replace(" " + a + " ", " ");
                            n !== (r = ct(s)) && i.setAttribute("class", r)
                        }
                return this
            },
            toggleClass: function(e, t) {
                var i = typeof e,
                    s = "string" === i || Array.isArray(e);
                return "boolean" == typeof t && s ? t ? this.addClass(e) : this.removeClass(e) : m(e) ? this.each(function(i) {
                    _(this).toggleClass(e.call(this, i, dt(this), t), t)
                }) : this.each(function() {
                    var t, n, a, o;
                    if (s)
                        for (n = 0,
                            a = _(this),
                            o = pt(e); t = o[n++];)
                            a.hasClass(t) ? a.removeClass(t) : a.addClass(t);
                    else
                        void 0 !== e && "boolean" !== i || ((t = dt(this)) && U.set(this, "__className__", t),
                            this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : U.get(this, "__className__") || ""))
                })
            },
            hasClass: function(e) {
                var t, i, s = 0;
                for (t = " " + e + " "; i = this[s++];)
                    if (1 === i.nodeType && (" " + ct(dt(i)) + " ").indexOf(t) > -1)
                        return !0;
                return !1
            }
        });
    var ft = /\r/g;
    _.fn.extend({
            val: function(e) {
                var t, i, s, n = this[0];
                return arguments.length ? (s = m(e),
                    this.each(function(i) {
                        var n;
                        1 === this.nodeType && (null == (n = s ? e.call(this, i, _(this).val()) : e) ? n = "" : "number" == typeof n ? n += "" : Array.isArray(n) && (n = _.map(n, function(e) {
                                return null == e ? "" : e + ""
                            })),
                            (t = _.valHooks[this.type] || _.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, n, "value") || (this.value = n))
                    })) : n ? (t = _.valHooks[n.type] || _.valHooks[n.nodeName.toLowerCase()]) && "get" in t && void 0 !== (i = t.get(n, "value")) ? i : "string" == typeof(i = n.value) ? i.replace(ft, "") : null == i ? "" : i : void 0
            }
        }),
        _.extend({
            valHooks: {
                option: {
                    get: function(e) {
                        var t = _.find.attr(e, "value");
                        return null != t ? t : ct(_.text(e))
                    }
                },
                select: {
                    get: function(e) {
                        var t, i, s, n = e.options,
                            a = e.selectedIndex,
                            o = "select-one" === e.type,
                            r = o ? null : [],
                            l = o ? a + 1 : n.length;
                        for (s = a < 0 ? l : o ? a : 0; s < l; s++)
                            if (((i = n[s]).selected || s === a) && !i.disabled && (!i.parentNode.disabled || !E(i.parentNode, "optgroup"))) {
                                if (t = _(i).val(),
                                    o)
                                    return t;
                                r.push(t)
                            }
                        return r
                    },
                    set: function(e, t) {
                        for (var i, s, n = e.options, a = _.makeArray(t), o = n.length; o--;)
                            ((s = n[o]).selected = _.inArray(_.valHooks.option.get(s), a) > -1) && (i = !0);
                        return i || (e.selectedIndex = -1),
                            a
                    }
                }
            }
        }),
        _.each(["radio", "checkbox"], function() {
            _.valHooks[this] = {
                    set: function(e, t) {
                        if (Array.isArray(t))
                            return e.checked = _.inArray(_(e).val(), t) > -1
                    }
                },
                f.checkOn || (_.valHooks[this].get = function(e) {
                    return null === e.getAttribute("value") ? "on" : e.value
                })
        }),
        f.focusin = "onfocusin" in e;
    var mt = /^(?:focusinfocus|focusoutblur)$/,
        gt = function(e) {
            e.stopPropagation()
        };
    _.extend(_.event, {
            trigger: function(t, i, n, a) {
                var o, r, l, h, u, d, p, f, v = [n || s],
                    b = c.call(t, "type") ? t.type : t,
                    y = c.call(t, "namespace") ? t.namespace.split(".") : [];
                if (r = f = l = n = n || s,
                    3 !== n.nodeType && 8 !== n.nodeType && !mt.test(b + _.event.triggered) && (b.indexOf(".") > -1 && (b = (y = b.split(".")).shift(),
                            y.sort()),
                        u = b.indexOf(":") < 0 && "on" + b,
                        (t = t[_.expando] ? t : new _.Event(b, "object" == typeof t && t)).isTrigger = a ? 2 : 3,
                        t.namespace = y.join("."),
                        t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + y.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
                        t.result = void 0,
                        t.target || (t.target = n),
                        i = null == i ? [t] : _.makeArray(i, [t]),
                        p = _.event.special[b] || {},
                        a || !p.trigger || !1 !== p.trigger.apply(n, i))) {
                    if (!a && !p.noBubble && !g(n)) {
                        for (mt.test((h = p.delegateType || b) + b) || (r = r.parentNode); r; r = r.parentNode)
                            v.push(r),
                            l = r;
                        l === (n.ownerDocument || s) && v.push(l.defaultView || l.parentWindow || e)
                    }
                    for (o = 0;
                        (r = v[o++]) && !t.isPropagationStopped();)
                        f = r,
                        t.type = o > 1 ? h : p.bindType || b,
                        (d = (U.get(r, "events") || {})[t.type] && U.get(r, "handle")) && d.apply(r, i),
                        (d = u && r[u]) && d.apply && X(r) && (t.result = d.apply(r, i), !1 === t.result && t.preventDefault());
                    return t.type = b,
                        a || t.isDefaultPrevented() || p._default && !1 !== p._default.apply(v.pop(), i) || !X(n) || u && m(n[b]) && !g(n) && ((l = n[u]) && (n[u] = null),
                            _.event.triggered = b,
                            t.isPropagationStopped() && f.addEventListener(b, gt),
                            n[b](),
                            t.isPropagationStopped() && f.removeEventListener(b, gt),
                            _.event.triggered = void 0,
                            l && (n[u] = l)),
                        t.result
                }
            },
            simulate: function(e, t, i) {
                var s = _.extend(new _.Event, i, {
                    type: e,
                    isSimulated: !0
                });
                _.event.trigger(s, null, t)
            }
        }),
        _.fn.extend({
            trigger: function(e, t) {
                return this.each(function() {
                    _.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, t) {
                var i = this[0];
                if (i)
                    return _.event.trigger(e, t, i, !0)
            }
        }),
        f.focusin || _.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var i = function(e) {
                _.event.simulate(t, e.target, _.event.fix(e))
            };
            _.event.special[t] = {
                setup: function() {
                    var s = this.ownerDocument || this,
                        n = U.access(s, t);
                    n || s.addEventListener(e, i, !0),
                        U.access(s, t, (n || 0) + 1)
                },
                teardown: function() {
                    var s = this.ownerDocument || this,
                        n = U.access(s, t) - 1;
                    n ? U.access(s, t, n) : (s.removeEventListener(e, i, !0),
                        U.remove(s, t))
                }
            }
        });
    var vt = e.location,
        bt = Date.now(),
        yt = /\?/;
    _.parseXML = function(t) {
        var i;
        if (!t || "string" != typeof t)
            return null;
        try {
            i = (new e.DOMParser).parseFromString(t, "text/xml")
        } catch (e) {
            i = void 0
        }
        return i && !i.getElementsByTagName("parsererror").length || _.error("Invalid XML: " + t),
            i
    };
    var _t = /\[\]$/,
        wt = /\r?\n/g,
        xt = /^(?:submit|button|image|reset|file)$/i,
        Ct = /^(?:input|select|textarea|keygen)/i;

    function Tt(e, t, i, s) {
        var n;
        if (Array.isArray(t))
            _.each(t, function(t, n) {
                i || _t.test(e) ? s(e, n) : Tt(e + "[" + ("object" == typeof n && null != n ? t : "") + "]", n, i, s)
            });
        else if (i || "object" !== y(t))
            s(e, t);
        else
            for (n in t)
                Tt(e + "[" + n + "]", t[n], i, s)
    }
    _.param = function(e, t) {
            var i, s = [],
                n = function(e, t) {
                    var i = m(t) ? t() : t;
                    s[s.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == i ? "" : i)
                };
            if (Array.isArray(e) || e.jquery && !_.isPlainObject(e))
                _.each(e, function() {
                    n(this.name, this.value)
                });
            else
                for (i in e)
                    Tt(i, e[i], t, n);
            return s.join("&")
        },
        _.fn.extend({
            serialize: function() {
                return _.param(this.serializeArray())
            },
            serializeArray: function() {
                return this.map(function() {
                    var e = _.prop(this, "elements");
                    return e ? _.makeArray(e) : this
                }).filter(function() {
                    var e = this.type;
                    return this.name && !_(this).is(":disabled") && Ct.test(this.nodeName) && !xt.test(e) && (this.checked || !he.test(e))
                }).map(function(e, t) {
                    var i = _(this).val();
                    return null == i ? null : Array.isArray(i) ? _.map(i, function(e) {
                        return {
                            name: t.name,
                            value: e.replace(wt, "\r\n")
                        }
                    }) : {
                        name: t.name,
                        value: i.replace(wt, "\r\n")
                    }
                }).get()
            }
        });
    var kt = /%20/g,
        St = /#.*$/,
        Et = /([?&])_=[^&]*/,
        Dt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Mt = /^(?:GET|HEAD)$/,
        Pt = /^\/\//,
        It = {},
        zt = {},
        At = "*/".concat("*"),
        Ot = s.createElement("a");

    function Ht(e) {
        return function(t, i) {
            "string" != typeof t && (i = t,
                t = "*");
            var s, n = 0,
                a = t.toLowerCase().match(H) || [];
            if (m(i))
                for (; s = a[n++];)
                    "+" === s[0] ? (s = s.slice(1) || "*",
                        (e[s] = e[s] || []).unshift(i)) : (e[s] = e[s] || []).push(i)
        }
    }

    function Nt(e, t, i, s) {
        var n = {},
            a = e === zt;

        function o(r) {
            var l;
            return n[r] = !0,
                _.each(e[r] || [], function(e, r) {
                    var h = r(t, i, s);
                    return "string" != typeof h || a || n[h] ? a ? !(l = h) : void 0 : (t.dataTypes.unshift(h),
                        o(h), !1)
                }),
                l
        }
        return o(t.dataTypes[0]) || !n["*"] && o("*")
    }

    function Lt(e, t) {
        var i, s, n = _.ajaxSettings.flatOptions || {};
        for (i in t)
            void 0 !== t[i] && ((n[i] ? e : s || (s = {}))[i] = t[i]);
        return s && _.extend(!0, e, s),
            e
    }
    Ot.href = vt.href,
        _.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: vt.href,
                type: "GET",
                isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(vt.protocol),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": At,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /\bxml\b/,
                    html: /\bhtml/,
                    json: /\bjson\b/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": JSON.parse,
                    "text xml": _.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function(e, t) {
                return t ? Lt(Lt(e, _.ajaxSettings), t) : Lt(_.ajaxSettings, e)
            },
            ajaxPrefilter: Ht(It),
            ajaxTransport: Ht(zt),
            ajax: function(t, i) {
                "object" == typeof t && (i = t,
                    t = void 0);
                var n, a, o, r, l, h, u, c, d, p, f = _.ajaxSetup({}, i = i || {}),
                    m = f.context || f,
                    g = f.context && (m.nodeType || m.jquery) ? _(m) : _.event,
                    v = _.Deferred(),
                    b = _.Callbacks("once memory"),
                    y = f.statusCode || {},
                    w = {},
                    x = {},
                    C = "canceled",
                    T = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (u) {
                                if (!r)
                                    for (r = {}; t = Dt.exec(o);)
                                        r[t[1].toLowerCase()] = t[2];
                                t = r[e.toLowerCase()]
                            }
                            return null == t ? null : t
                        },
                        getAllResponseHeaders: function() {
                            return u ? o : null
                        },
                        setRequestHeader: function(e, t) {
                            return null == u && (e = x[e.toLowerCase()] = x[e.toLowerCase()] || e,
                                    w[e] = t),
                                this
                        },
                        overrideMimeType: function(e) {
                            return null == u && (f.mimeType = e),
                                this
                        },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (u)
                                    T.always(e[T.status]);
                                else
                                    for (t in e)
                                        y[t] = [y[t], e[t]];
                            return this
                        },
                        abort: function(e) {
                            var t = e || C;
                            return n && n.abort(t),
                                k(0, t),
                                this
                        }
                    };
                if (v.promise(T),
                    f.url = ((t || f.url || vt.href) + "").replace(Pt, vt.protocol + "//"),
                    f.type = i.method || i.type || f.method || f.type,
                    f.dataTypes = (f.dataType || "*").toLowerCase().match(H) || [""],
                    null == f.crossDomain) {
                    h = s.createElement("a");
                    try {
                        h.href = f.url,
                            h.href = h.href,
                            f.crossDomain = Ot.protocol + "//" + Ot.host != h.protocol + "//" + h.host
                    } catch (e) {
                        f.crossDomain = !0
                    }
                }
                if (f.data && f.processData && "string" != typeof f.data && (f.data = _.param(f.data, f.traditional)),
                    Nt(It, f, i, T),
                    u)
                    return T;
                for (d in (c = _.event && f.global) && 0 == _.active++ && _.event.trigger("ajaxStart"),
                    f.type = f.type.toUpperCase(),
                    f.hasContent = !Mt.test(f.type),
                    a = f.url.replace(St, ""),
                    f.hasContent ? f.data && f.processData && 0 === (f.contentType || "").indexOf("application/x-www-form-urlencoded") && (f.data = f.data.replace(kt, "+")) : (p = f.url.slice(a.length),
                        f.data && (f.processData || "string" == typeof f.data) && (a += (yt.test(a) ? "&" : "?") + f.data,
                            delete f.data), !1 === f.cache && (a = a.replace(Et, "$1"),
                            p = (yt.test(a) ? "&" : "?") + "_=" + bt++ + p),
                        f.url = a + p),
                    f.ifModified && (_.lastModified[a] && T.setRequestHeader("If-Modified-Since", _.lastModified[a]),
                        _.etag[a] && T.setRequestHeader("If-None-Match", _.etag[a])),
                    (f.data && f.hasContent && !1 !== f.contentType || i.contentType) && T.setRequestHeader("Content-Type", f.contentType),
                    T.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + At + "; q=0.01" : "") : f.accepts["*"]),
                    f.headers)
                    T.setRequestHeader(d, f.headers[d]);
                if (f.beforeSend && (!1 === f.beforeSend.call(m, T, f) || u))
                    return T.abort();
                if (C = "abort",
                    b.add(f.complete),
                    T.done(f.success),
                    T.fail(f.error),
                    n = Nt(zt, f, i, T)) {
                    if (T.readyState = 1,
                        c && g.trigger("ajaxSend", [T, f]),
                        u)
                        return T;
                    f.async && f.timeout > 0 && (l = e.setTimeout(function() {
                        T.abort("timeout")
                    }, f.timeout));
                    try {
                        u = !1,
                            n.send(w, k)
                    } catch (e) {
                        if (u)
                            throw e;
                        k(-1, e)
                    }
                } else
                    k(-1, "No Transport");

                function k(t, i, s, r) {
                    var h, d, p, w, x, C = i;
                    u || (u = !0,
                        l && e.clearTimeout(l),
                        n = void 0,
                        o = r || "",
                        T.readyState = t > 0 ? 4 : 0,
                        h = t >= 200 && t < 300 || 304 === t,
                        s && (w = function(e, t, i) {
                            for (var s, n, a, o, r = e.contents, l = e.dataTypes;
                                "*" === l[0];)
                                l.shift(),
                                void 0 === s && (s = e.mimeType || t.getResponseHeader("Content-Type"));
                            if (s)
                                for (n in r)
                                    if (r[n] && r[n].test(s)) {
                                        l.unshift(n);
                                        break
                                    }
                            if (l[0] in i)
                                a = l[0];
                            else {
                                for (n in i) {
                                    if (!l[0] || e.converters[n + " " + l[0]]) {
                                        a = n;
                                        break
                                    }
                                    o || (o = n)
                                }
                                a = a || o
                            }
                            if (a)
                                return a !== l[0] && l.unshift(a),
                                    i[a]
                        }(f, T, s)),
                        w = function(e, t, i, s) {
                            var n, a, o, r, l, h = {},
                                u = e.dataTypes.slice();
                            if (u[1])
                                for (o in e.converters)
                                    h[o.toLowerCase()] = e.converters[o];
                            for (a = u.shift(); a;)
                                if (e.responseFields[a] && (i[e.responseFields[a]] = t), !l && s && e.dataFilter && (t = e.dataFilter(t, e.dataType)),
                                    l = a,
                                    a = u.shift())
                                    if ("*" === a)
                                        a = l;
                                    else if ("*" !== l && l !== a) {
                                if (!(o = h[l + " " + a] || h["* " + a]))
                                    for (n in h)
                                        if ((r = n.split(" "))[1] === a && (o = h[l + " " + r[0]] || h["* " + r[0]])) {
                                            !0 === o ? o = h[n] : !0 !== h[n] && (a = r[0],
                                                u.unshift(r[1]));
                                            break
                                        }
                                if (!0 !== o)
                                    if (o && e.throws)
                                        t = o(t);
                                    else
                                        try {
                                            t = o(t)
                                        } catch (e) {
                                            return {
                                                state: "parsererror",
                                                error: o ? e : "No conversion from " + l + " to " + a
                                            }
                                        }
                            }
                            return {
                                state: "success",
                                data: t
                            }
                        }(f, w, T, h),
                        h ? (f.ifModified && ((x = T.getResponseHeader("Last-Modified")) && (_.lastModified[a] = x),
                                (x = T.getResponseHeader("etag")) && (_.etag[a] = x)),
                            204 === t || "HEAD" === f.type ? C = "nocontent" : 304 === t ? C = "notmodified" : (C = w.state,
                                d = w.data,
                                h = !(p = w.error))) : (p = C, !t && C || (C = "error",
                            t < 0 && (t = 0))),
                        T.status = t,
                        T.statusText = (i || C) + "",
                        h ? v.resolveWith(m, [d, C, T]) : v.rejectWith(m, [T, C, p]),
                        T.statusCode(y),
                        y = void 0,
                        c && g.trigger(h ? "ajaxSuccess" : "ajaxError", [T, f, h ? d : p]),
                        b.fireWith(m, [T, C]),
                        c && (g.trigger("ajaxComplete", [T, f]),
                            --_.active || _.event.trigger("ajaxStop")))
                }
                return T
            },
            getJSON: function(e, t, i) {
                return _.get(e, t, i, "json")
            },
            getScript: function(e, t) {
                return _.get(e, void 0, t, "script")
            }
        }),
        _.each(["get", "post"], function(e, t) {
            _[t] = function(e, i, s, n) {
                return m(i) && (n = n || s,
                        s = i,
                        i = void 0),
                    _.ajax(_.extend({
                        url: e,
                        type: t,
                        dataType: n,
                        data: i,
                        success: s
                    }, _.isPlainObject(e) && e))
            }
        }),
        _._evalUrl = function(e) {
            return _.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                cache: !0,
                async: !1,
                global: !1,
                throws: !0
            })
        },
        _.fn.extend({
            wrapAll: function(e) {
                var t;
                return this[0] && (m(e) && (e = e.call(this[0])),
                        t = _(e, this[0].ownerDocument).eq(0).clone(!0),
                        this[0].parentNode && t.insertBefore(this[0]),
                        t.map(function() {
                            for (var e = this; e.firstElementChild;)
                                e = e.firstElementChild;
                            return e
                        }).append(this)),
                    this
            },
            wrapInner: function(e) {
                return m(e) ? this.each(function(t) {
                    _(this).wrapInner(e.call(this, t))
                }) : this.each(function() {
                    var t = _(this),
                        i = t.contents();
                    i.length ? i.wrapAll(e) : t.append(e)
                })
            },
            wrap: function(e) {
                var t = m(e);
                return this.each(function(i) {
                    _(this).wrapAll(t ? e.call(this, i) : e)
                })
            },
            unwrap: function(e) {
                return this.parent(e).not("body").each(function() {
                        _(this).replaceWith(this.childNodes)
                    }),
                    this
            }
        }),
        _.expr.pseudos.hidden = function(e) {
            return !_.expr.pseudos.visible(e)
        },
        _.expr.pseudos.visible = function(e) {
            return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
        },
        _.ajaxSettings.xhr = function() {
            try {
                return new e.XMLHttpRequest
            } catch (e) {}
        };
    var Wt = {
            0: 200,
            1223: 204
        },
        $t = _.ajaxSettings.xhr();
    f.cors = !!$t && "withCredentials" in $t,
        f.ajax = $t = !!$t,
        _.ajaxTransport(function(t) {
            var i, s;
            if (f.cors || $t && !t.crossDomain)
                return {
                    send: function(n, a) {
                        var o, r = t.xhr();
                        if (r.open(t.type, t.url, t.async, t.username, t.password),
                            t.xhrFields)
                            for (o in t.xhrFields)
                                r[o] = t.xhrFields[o];
                        for (o in t.mimeType && r.overrideMimeType && r.overrideMimeType(t.mimeType),
                            t.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest"),
                            n)
                            r.setRequestHeader(o, n[o]);
                        i = function(e) {
                                return function() {
                                    i && (i = s = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null,
                                        "abort" === e ? r.abort() : "error" === e ? "number" != typeof r.status ? a(0, "error") : a(r.status, r.statusText) : a(Wt[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? {
                                            binary: r.response
                                        } : {
                                            text: r.responseText
                                        }, r.getAllResponseHeaders()))
                                }
                            },
                            r.onload = i(),
                            s = r.onerror = r.ontimeout = i("error"),
                            void 0 !== r.onabort ? r.onabort = s : r.onreadystatechange = function() {
                                4 === r.readyState && e.setTimeout(function() {
                                    i && s()
                                })
                            },
                            i = i("abort");
                        try {
                            r.send(t.hasContent && t.data || null)
                        } catch (e) {
                            if (i)
                                throw e
                        }
                    },
                    abort: function() {
                        i && i()
                    }
                }
        }),
        _.ajaxPrefilter(function(e) {
            e.crossDomain && (e.contents.script = !1)
        }),
        _.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /\b(?:java|ecma)script\b/
            },
            converters: {
                "text script": function(e) {
                    return _.globalEval(e),
                        e
                }
            }
        }),
        _.ajaxPrefilter("script", function(e) {
            void 0 === e.cache && (e.cache = !1),
                e.crossDomain && (e.type = "GET")
        }),
        _.ajaxTransport("script", function(e) {
            var t, i;
            if (e.crossDomain)
                return {
                    send: function(n, a) {
                        t = _("<script>").prop({
                                charset: e.scriptCharset,
                                src: e.url
                            }).on("load error", i = function(e) {
                                t.remove(),
                                    i = null,
                                    e && a("error" === e.type ? 404 : 200, e.type)
                            }),
                            s.head.appendChild(t[0])
                    },
                    abort: function() {
                        i && i()
                    }
                }
        });
    var Ft = [],
        Rt = /(=)\?(?=&|$)|\?\?/;
    _.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var e = Ft.pop() || _.expando + "_" + bt++;
                return this[e] = !0,
                    e
            }
        }),
        _.ajaxPrefilter("json jsonp", function(t, i, s) {
            var n, a, o, r = !1 !== t.jsonp && (Rt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Rt.test(t.data) && "data");
            if (r || "jsonp" === t.dataTypes[0])
                return n = t.jsonpCallback = m(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback,
                    r ? t[r] = t[r].replace(Rt, "$1" + n) : !1 !== t.jsonp && (t.url += (yt.test(t.url) ? "&" : "?") + t.jsonp + "=" + n),
                    t.converters["script json"] = function() {
                        return o || _.error(n + " was not called"),
                            o[0]
                    },
                    t.dataTypes[0] = "json",
                    a = e[n],
                    e[n] = function() {
                        o = arguments
                    },
                    s.always(function() {
                        void 0 === a ? _(e).removeProp(n) : e[n] = a,
                            t[n] && (t.jsonpCallback = i.jsonpCallback,
                                Ft.push(n)),
                            o && m(a) && a(o[0]),
                            o = a = void 0
                    }),
                    "script"
        }),
        f.createHTMLDocument = function() {
            var e = s.implementation.createHTMLDocument("").body;
            return e.innerHTML = "<form></form><form></form>",
                2 === e.childNodes.length
        }(),
        _.parseHTML = function(e, t, i) {
            return "string" != typeof e ? [] : ("boolean" == typeof t && (i = t,
                    t = !1),
                t || (f.createHTMLDocument ? ((n = (t = s.implementation.createHTMLDocument("")).createElement("base")).href = s.location.href,
                    t.head.appendChild(n)) : t = s),
                o = !i && [],
                (a = D.exec(e)) ? [t.createElement(a[1])] : (a = ge([e], t, o),
                    o && o.length && _(o).remove(),
                    _.merge([], a.childNodes)));
            var n, a, o
        },
        _.fn.load = function(e, t, i) {
            var s, n, a, o = this,
                r = e.indexOf(" ");
            return r > -1 && (s = ct(e.slice(r)),
                    e = e.slice(0, r)),
                m(t) ? (i = t,
                    t = void 0) : t && "object" == typeof t && (n = "POST"),
                o.length > 0 && _.ajax({
                    url: e,
                    type: n || "GET",
                    dataType: "html",
                    data: t
                }).done(function(e) {
                    a = arguments,
                        o.html(s ? _("<div>").append(_.parseHTML(e)).find(s) : e)
                }).always(i && function(e, t) {
                    o.each(function() {
                        i.apply(this, a || [e.responseText, t, e])
                    })
                }),
                this
        },
        _.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
            _.fn[t] = function(e) {
                return this.on(t, e)
            }
        }),
        _.expr.pseudos.animated = function(e) {
            return _.grep(_.timers, function(t) {
                return e === t.elem
            }).length
        },
        _.offset = {
            setOffset: function(e, t, i) {
                var s, n, a, o, r, l, h = _.css(e, "position"),
                    u = _(e),
                    c = {};
                "static" === h && (e.style.position = "relative"),
                    r = u.offset(),
                    a = _.css(e, "top"),
                    l = _.css(e, "left"),
                    ("absolute" === h || "fixed" === h) && (a + l).indexOf("auto") > -1 ? (o = (s = u.position()).top,
                        n = s.left) : (o = parseFloat(a) || 0,
                        n = parseFloat(l) || 0),
                    m(t) && (t = t.call(e, i, _.extend({}, r))),
                    null != t.top && (c.top = t.top - r.top + o),
                    null != t.left && (c.left = t.left - r.left + n),
                    "using" in t ? t.using.call(e, c) : u.css(c)
            }
        },
        _.fn.extend({
            offset: function(e) {
                if (arguments.length)
                    return void 0 === e ? this : this.each(function(t) {
                        _.offset.setOffset(this, e, t)
                    });
                var t, i, s = this[0];
                return s ? s.getClientRects().length ? {
                    top: (t = s.getBoundingClientRect()).top + (i = s.ownerDocument.defaultView).pageYOffset,
                    left: t.left + i.pageXOffset
                } : {
                    top: 0,
                    left: 0
                } : void 0
            },
            position: function() {
                if (this[0]) {
                    var e, t, i, s = this[0],
                        n = {
                            top: 0,
                            left: 0
                        };
                    if ("fixed" === _.css(s, "position"))
                        t = s.getBoundingClientRect();
                    else {
                        for (t = this.offset(),
                            i = s.ownerDocument,
                            e = s.offsetParent || i.documentElement; e && (e === i.body || e === i.documentElement) && "static" === _.css(e, "position");)
                            e = e.parentNode;
                        e && e !== s && 1 === e.nodeType && ((n = _(e).offset()).top += _.css(e, "borderTopWidth", !0),
                            n.left += _.css(e, "borderLeftWidth", !0))
                    }
                    return {
                        top: t.top - n.top - _.css(s, "marginTop", !0),
                        left: t.left - n.left - _.css(s, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var e = this.offsetParent; e && "static" === _.css(e, "position");)
                        e = e.offsetParent;
                    return e || ve
                })
            }
        }),
        _.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(e, t) {
            var i = "pageYOffset" === t;
            _.fn[e] = function(s) {
                return B(this, function(e, s, n) {
                    var a;
                    if (g(e) ? a = e : 9 === e.nodeType && (a = e.defaultView),
                        void 0 === n)
                        return a ? a[t] : e[s];
                    a ? a.scrollTo(i ? a.pageXOffset : n, i ? n : a.pageYOffset) : e[s] = n
                }, e, s, arguments.length)
            }
        }),
        _.each(["top", "left"], function(e, t) {
            _.cssHooks[t] = Fe(f.pixelPosition, function(e, i) {
                if (i)
                    return i = $e(e, t),
                        Ne.test(i) ? _(e).position()[t] + "px" : i
            })
        }),
        _.each({
            Height: "height",
            Width: "width"
        }, function(e, t) {
            _.each({
                padding: "inner" + e,
                content: t,
                "": "outer" + e
            }, function(i, s) {
                _.fn[s] = function(n, a) {
                    var o = arguments.length && (i || "boolean" != typeof n),
                        r = i || (!0 === n || !0 === a ? "margin" : "border");
                    return B(this, function(t, i, n) {
                        var a;
                        return g(t) ? 0 === s.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (a = t.documentElement,
                            Math.max(t.body["scroll" + e], a["scroll" + e], t.body["offset" + e], a["offset" + e], a["client" + e])) : void 0 === n ? _.css(t, i, r) : _.style(t, i, n, r)
                    }, t, o ? n : void 0, o)
                }
            })
        }),
        _.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, t) {
            _.fn[t] = function(e, i) {
                return arguments.length > 0 ? this.on(t, null, e, i) : this.trigger(t)
            }
        }),
        _.fn.extend({
            hover: function(e, t) {
                return this.mouseenter(e).mouseleave(t || e)
            }
        }),
        _.fn.extend({
            bind: function(e, t, i) {
                return this.on(e, null, t, i)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, i, s) {
                return this.on(t, e, i, s)
            },
            undelegate: function(e, t, i) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", i)
            }
        }),
        _.proxy = function(e, t) {
            var i, s, n;
            if ("string" == typeof t && (i = e[t],
                    t = e,
                    e = i),
                m(e))
                return s = a.call(arguments, 2),
                    (n = function() {
                        return e.apply(t || this, s.concat(a.call(arguments)))
                    }).guid = e.guid = e.guid || _.guid++,
                    n
        },
        _.holdReady = function(e) {
            e ? _.readyWait++ : _.ready(!0)
        },
        _.isArray = Array.isArray,
        _.parseJSON = JSON.parse,
        _.nodeName = E,
        _.isFunction = m,
        _.isWindow = g,
        _.camelCase = V,
        _.type = y,
        _.now = Date.now,
        _.isNumeric = function(e) {
            var t = _.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
        },
        "function" == typeof define && define.amd && define("jquery", [], function() {
            return _
        });
    var Bt = e.jQuery,
        jt = e.$;
    return _.noConflict = function(t) {
            return e.$ === _ && (e.$ = jt),
                t && e.jQuery === _ && (e.jQuery = Bt),
                _
        },
        t || (e.jQuery = e.$ = _),
        _
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function(e) {
    function t() {
        this._curInst = null,
            this._keyEvent = !1,
            this._disabledInputs = [],
            this._datepickerShowing = !1,
            this._inDialog = !1,
            this._mainDivId = "ui-datepicker-div",
            this._inlineClass = "ui-datepicker-inline",
            this._appendClass = "ui-datepicker-append",
            this._triggerClass = "ui-datepicker-trigger",
            this._dialogClass = "ui-datepicker-dialog",
            this._disableClass = "ui-datepicker-disabled",
            this._unselectableClass = "ui-datepicker-unselectable",
            this._currentClass = "ui-datepicker-current-day",
            this._dayOverClass = "ui-datepicker-days-cell-over",
            this.regional = [],
            this.regional[""] = {
                closeText: "Done",
                prevText: "Prev",
                nextText: "Next",
                currentText: "Today",
                monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                weekHeader: "Wk",
                dateFormat: "mm/dd/yy",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            },
            this._defaults = {
                showOn: "focus",
                showAnim: "fadeIn",
                showOptions: {},
                defaultDate: null,
                appendText: "",
                buttonText: "...",
                buttonImage: "",
                buttonImageOnly: !1,
                hideIfNoPrevNext: !1,
                navigationAsDateFormat: !1,
                gotoCurrent: !1,
                changeMonth: !1,
                changeYear: !1,
                yearRange: "c-10:c+10",
                showOtherMonths: !1,
                selectOtherMonths: !1,
                showWeek: !1,
                calculateWeek: this.iso8601Week,
                shortYearCutoff: "+10",
                minDate: null,
                maxDate: null,
                duration: "fast",
                beforeShowDay: null,
                beforeShow: null,
                onSelect: null,
                onChangeMonthYear: null,
                onClose: null,
                numberOfMonths: 1,
                showCurrentAtPos: 0,
                stepMonths: 1,
                stepBigMonths: 12,
                altField: "",
                altFormat: "",
                constrainInput: !0,
                showButtonPanel: !1,
                autoSize: !1,
                disabled: !1
            },
            e.extend(this._defaults, this.regional[""]),
            this.regional.en = e.extend(!0, {}, this.regional[""]),
            this.regional["en-US"] = e.extend(!0, {}, this.regional.en),
            this.dpDiv = i(e("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
    }

    function i(t) {
        var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return t.on("mouseout", i, function() {
            e(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && e(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && e(this).removeClass("ui-datepicker-next-hover")
        }).on("mouseover", i, s)
    }

    function s() {
        e.datepicker._isDisabledDatepicker(d.inline ? d.dpDiv.parent()[0] : d.input[0]) || (e(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),
            e(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && e(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && e(this).addClass("ui-datepicker-next-hover"))
    }

    function n(t, i) {
        for (var s in e.extend(t, i),
                i)
            null == i[s] && (t[s] = i[s]);
        return t
    }

    function a(e) {
        return function() {
            var t = this.element.val();
            e.apply(this, arguments),
                this._refresh(),
                t !== this.element.val() && this._trigger("change")
        }
    }
    e.ui = e.ui || {},
        e.ui.version = "1.12.1";
    var o = 0,
        r = Array.prototype.slice;
    e.cleanData = function(t) {
            return function(i) {
                var s, a, o;
                for (o = 0; null != (a = i[o]); o++)
                    try {
                        (s = e._data(a, "events")) && s.remove && e(a).triggerHandler("remove")
                    } catch (n) {}
                t(i)
            }
        }(e.cleanData),
        e.widget = function(t, i, s) {
            var n, a, o, r = {},
                l = t.split(".")[0],
                h = l + "-" + (t = t.split(".")[1]);
            return s || (s = i,
                    i = e.Widget),
                e.isArray(s) && (s = e.extend.apply(null, [{}].concat(s))),
                e.expr[":"][h.toLowerCase()] = function(t) {
                    return !!e.data(t, h)
                },
                e[l] = e[l] || {},
                n = e[l][t],
                a = e[l][t] = function(e, t) {
                    return this._createWidget ? void(arguments.length && this._createWidget(e, t)) : new a(e, t)
                },
                e.extend(a, n, {
                    version: s.version,
                    _proto: e.extend({}, s),
                    _childConstructors: []
                }),
                (o = new i).options = e.widget.extend({}, o.options),
                e.each(s, function(t, s) {
                    return e.isFunction(s) ? void(r[t] = function() {
                        function e() {
                            return i.prototype[t].apply(this, arguments)
                        }

                        function n(e) {
                            return i.prototype[t].apply(this, e)
                        }
                        return function() {
                            var t, i = this._super,
                                a = this._superApply;
                            return this._super = e,
                                this._superApply = n,
                                t = s.apply(this, arguments),
                                this._super = i,
                                this._superApply = a,
                                t
                        }
                    }()) : void(r[t] = s)
                }),
                a.prototype = e.widget.extend(o, {
                    widgetEventPrefix: n && o.widgetEventPrefix || t
                }, r, {
                    constructor: a,
                    namespace: l,
                    widgetName: t,
                    widgetFullName: h
                }),
                n ? (e.each(n._childConstructors, function(t, i) {
                        var s = i.prototype;
                        e.widget(s.namespace + "." + s.widgetName, a, i._proto)
                    }),
                    delete n._childConstructors) : i._childConstructors.push(a),
                e.widget.bridge(t, a),
                a
        },
        e.widget.extend = function(t) {
            for (var i, s, n = r.call(arguments, 1), a = 0, o = n.length; o > a; a++)
                for (i in n[a])
                    s = n[a][i],
                    n[a].hasOwnProperty(i) && void 0 !== s && (t[i] = e.isPlainObject(s) ? e.isPlainObject(t[i]) ? e.widget.extend({}, t[i], s) : e.widget.extend({}, s) : s);
            return t
        },
        e.widget.bridge = function(t, i) {
            var s = i.prototype.widgetFullName || t;
            e.fn[t] = function(n) {
                var a = "string" == typeof n,
                    o = r.call(arguments, 1),
                    l = this;
                return a ? this.length || "instance" !== n ? this.each(function() {
                        var i, a = e.data(this, s);
                        return "instance" === n ? (l = a, !1) : a ? e.isFunction(a[n]) && "_" !== n.charAt(0) ? (i = a[n].apply(a, o)) !== a && void 0 !== i ? (l = i && i.jquery ? l.pushStack(i.get()) : i, !1) : void 0 : e.error("no such method '" + n + "' for " + t + " widget instance") : e.error("cannot call methods on " + t + " prior to initialization; attempted to call method '" + n + "'")
                    }) : l = void 0 : (o.length && (n = e.widget.extend.apply(null, [n].concat(o))),
                        this.each(function() {
                            var t = e.data(this, s);
                            t ? (t.option(n || {}),
                                t._init && t._init()) : e.data(this, s, new i(n, this))
                        })),
                    l
            }
        },
        e.Widget = function() {},
        e.Widget._childConstructors = [],
        e.Widget.prototype = {
            widgetName: "widget",
            widgetEventPrefix: "",
            defaultElement: "<div>",
            options: {
                classes: {},
                disabled: !1,
                create: null
            },
            _createWidget: function(t, i) {
                i = e(i || this.defaultElement || this)[0],
                    this.element = e(i),
                    this.uuid = o++,
                    this.eventNamespace = "." + this.widgetName + this.uuid,
                    this.bindings = e(),
                    this.hoverable = e(),
                    this.focusable = e(),
                    this.classesElementLookup = {},
                    i !== this && (e.data(i, this.widgetFullName, this),
                        this._on(!0, this.element, {
                            remove: function(e) {
                                e.target === i && this.destroy()
                            }
                        }),
                        this.document = e(i.style ? i.ownerDocument : i.document || i),
                        this.window = e(this.document[0].defaultView || this.document[0].parentWindow)),
                    this.options = e.widget.extend({}, this.options, this._getCreateOptions(), t),
                    this._create(),
                    this.options.disabled && this._setOptionDisabled(this.options.disabled),
                    this._trigger("create", null, this._getCreateEventData()),
                    this._init()
            },
            _getCreateOptions: function() {
                return {}
            },
            _getCreateEventData: e.noop,
            _create: e.noop,
            _init: e.noop,
            destroy: function() {
                var t = this;
                this._destroy(),
                    e.each(this.classesElementLookup, function(e, i) {
                        t._removeClass(i, e)
                    }),
                    this.element.off(this.eventNamespace).removeData(this.widgetFullName),
                    this.widget().off(this.eventNamespace).removeAttr("aria-disabled"),
                    this.bindings.off(this.eventNamespace)
            },
            _destroy: e.noop,
            widget: function() {
                return this.element
            },
            option: function(t, i) {
                var s, n, a, o = t;
                if (0 === arguments.length)
                    return e.widget.extend({}, this.options);
                if ("string" == typeof t)
                    if (o = {},
                        s = t.split("."),
                        t = s.shift(),
                        s.length) {
                        for (n = o[t] = e.widget.extend({}, this.options[t]),
                            a = 0; s.length - 1 > a; a++)
                            n[s[a]] = n[s[a]] || {},
                            n = n[s[a]];
                        if (t = s.pop(),
                            1 === arguments.length)
                            return void 0 === n[t] ? null : n[t];
                        n[t] = i
                    } else {
                        if (1 === arguments.length)
                            return void 0 === this.options[t] ? null : this.options[t];
                        o[t] = i
                    }
                return this._setOptions(o),
                    this
            },
            _setOptions: function(e) {
                var t;
                for (t in e)
                    this._setOption(t, e[t]);
                return this
            },
            _setOption: function(e, t) {
                return "classes" === e && this._setOptionClasses(t),
                    this.options[e] = t,
                    "disabled" === e && this._setOptionDisabled(t),
                    this
            },
            _setOptionClasses: function(t) {
                var i, s, n;
                for (i in t)
                    n = this.classesElementLookup[i],
                    t[i] !== this.options.classes[i] && n && n.length && (s = e(n.get()),
                        this._removeClass(n, i),
                        s.addClass(this._classes({
                            element: s,
                            keys: i,
                            classes: t,
                            add: !0
                        })))
            },
            _setOptionDisabled: function(e) {
                this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!e),
                    e && (this._removeClass(this.hoverable, null, "ui-state-hover"),
                        this._removeClass(this.focusable, null, "ui-state-focus"))
            },
            enable: function() {
                return this._setOptions({
                    disabled: !1
                })
            },
            disable: function() {
                return this._setOptions({
                    disabled: !0
                })
            },
            _classes: function(t) {
                function i(i, a) {
                    var o, r;
                    for (r = 0; i.length > r; r++)
                        o = n.classesElementLookup[i[r]] || e(),
                        o = e(t.add ? e.unique(o.get().concat(t.element.get())) : o.not(t.element).get()),
                        n.classesElementLookup[i[r]] = o,
                        s.push(i[r]),
                        a && t.classes[i[r]] && s.push(t.classes[i[r]])
                }
                var s = [],
                    n = this;
                return t = e.extend({
                        element: this.element,
                        classes: this.options.classes || {}
                    }, t),
                    this._on(t.element, {
                        remove: "_untrackClassesElement"
                    }),
                    t.keys && i(t.keys.match(/\S+/g) || [], !0),
                    t.extra && i(t.extra.match(/\S+/g) || []),
                    s.join(" ")
            },
            _untrackClassesElement: function(t) {
                var i = this;
                e.each(i.classesElementLookup, function(s, n) {
                    -1 !== e.inArray(t.target, n) && (i.classesElementLookup[s] = e(n.not(t.target).get()))
                })
            },
            _removeClass: function(e, t, i) {
                return this._toggleClass(e, t, i, !1)
            },
            _addClass: function(e, t, i) {
                return this._toggleClass(e, t, i, !0)
            },
            _toggleClass: function(e, t, i, s) {
                var n = "string" == typeof e || null === e,
                    a = {
                        extra: n ? t : i,
                        keys: n ? e : t,
                        element: n ? this.element : e,
                        add: s = "boolean" == typeof s ? s : i
                    };
                return a.element.toggleClass(this._classes(a), s),
                    this
            },
            _on: function(t, i, s) {
                var n, a = this;
                "boolean" != typeof t && (s = i,
                        i = t,
                        t = !1),
                    s ? (i = n = e(i),
                        this.bindings = this.bindings.add(i)) : (s = i,
                        i = this.element,
                        n = this.widget()),
                    e.each(s, function(s, o) {
                        function r() {
                            return t || !0 !== a.options.disabled && !e(this).hasClass("ui-state-disabled") ? ("string" == typeof o ? a[o] : o).apply(a, arguments) : void 0
                        }
                        "string" != typeof o && (r.guid = o.guid = o.guid || r.guid || e.guid++);
                        var l = s.match(/^([\w:-]*)\s*(.*)$/),
                            h = l[1] + a.eventNamespace,
                            u = l[2];
                        u ? n.on(h, u, r) : i.on(h, r)
                    })
            },
            _off: function(t, i) {
                i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace,
                    t.off(i).off(i),
                    this.bindings = e(this.bindings.not(t).get()),
                    this.focusable = e(this.focusable.not(t).get()),
                    this.hoverable = e(this.hoverable.not(t).get())
            },
            _delay: function(e, t) {
                var i = this;
                return setTimeout(function() {
                    return ("string" == typeof e ? i[e] : e).apply(i, arguments)
                }, t || 0)
            },
            _hoverable: function(t) {
                this.hoverable = this.hoverable.add(t),
                    this._on(t, {
                        mouseenter: function(t) {
                            this._addClass(e(t.currentTarget), null, "ui-state-hover")
                        },
                        mouseleave: function(t) {
                            this._removeClass(e(t.currentTarget), null, "ui-state-hover")
                        }
                    })
            },
            _focusable: function(t) {
                this.focusable = this.focusable.add(t),
                    this._on(t, {
                        focusin: function(t) {
                            this._addClass(e(t.currentTarget), null, "ui-state-focus")
                        },
                        focusout: function(t) {
                            this._removeClass(e(t.currentTarget), null, "ui-state-focus")
                        }
                    })
            },
            _trigger: function(t, i, s) {
                var n, a, o = this.options[t];
                if (s = s || {},
                    (i = e.Event(i)).type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(),
                    i.target = this.element[0],
                    a = i.originalEvent)
                    for (n in a)
                        n in i || (i[n] = a[n]);
                return this.element.trigger(i, s), !(e.isFunction(o) && !1 === o.apply(this.element[0], [i].concat(s)) || i.isDefaultPrevented())
            }
        },
        e.each({
            show: "fadeIn",
            hide: "fadeOut"
        }, function(t, i) {
            e.Widget.prototype["_" + t] = function(s, n, a) {
                "string" == typeof n && (n = {
                    effect: n
                });
                var o, r = n ? !0 === n || "number" == typeof n ? i : n.effect || i : t;
                "number" == typeof(n = n || {}) && (n = {
                    duration: n
                }),
                o = !e.isEmptyObject(n),
                    n.complete = a,
                    n.delay && s.delay(n.delay),
                    o && e.effects && e.effects.effect[r] ? s[t](n) : r !== t && s[r] ? s[r](n.duration, n.easing, a) : s.queue(function(i) {
                        e(this)[t](),
                            a && a.call(s[0]),
                            i()
                    })
            }
        }),
        function() {
            function t(e, t, i) {
                return [parseFloat(e[0]) * (u.test(e[0]) ? t / 100 : 1), parseFloat(e[1]) * (u.test(e[1]) ? i / 100 : 1)]
            }

            function i(t, i) {
                return parseInt(e.css(t, i), 10) || 0
            }
            var s, n = Math.max,
                a = Math.abs,
                o = /left|center|right/,
                r = /top|center|bottom/,
                l = /[\+\-]\d+(\.[\d]+)?%?/,
                h = /^\w+/,
                u = /%$/,
                c = e.fn.position;
            e.position = {
                    scrollbarWidth: function() {
                        if (void 0 !== s)
                            return s;
                        var t, i, n = e("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                            a = n.children()[0];
                        return e("body").append(n),
                            t = a.offsetWidth,
                            n.css("overflow", "scroll"),
                            t === (i = a.offsetWidth) && (i = n[0].clientWidth),
                            n.remove(),
                            s = t - i
                    },
                    getScrollInfo: function(t) {
                        var i = t.isWindow || t.isDocument ? "" : t.element.css("overflow-x"),
                            s = t.isWindow || t.isDocument ? "" : t.element.css("overflow-y"),
                            n = "scroll" === i || "auto" === i && t.width < t.element[0].scrollWidth;
                        return {
                            width: "scroll" === s || "auto" === s && t.height < t.element[0].scrollHeight ? e.position.scrollbarWidth() : 0,
                            height: n ? e.position.scrollbarWidth() : 0
                        }
                    },
                    getWithinInfo: function(t) {
                        var i = e(t || window),
                            s = e.isWindow(i[0]),
                            n = !!i[0] && 9 === i[0].nodeType;
                        return {
                            element: i,
                            isWindow: s,
                            isDocument: n,
                            offset: s || n ? {
                                left: 0,
                                top: 0
                            } : e(t).offset(),
                            scrollLeft: i.scrollLeft(),
                            scrollTop: i.scrollTop(),
                            width: i.outerWidth(),
                            height: i.outerHeight()
                        }
                    }
                },
                e.fn.position = function(s) {
                    if (!s || !s.of)
                        return c.apply(this, arguments);
                    s = e.extend({}, s);
                    var u, d, p, f, m, g, v = e(s.of),
                        b = e.position.getWithinInfo(s.within),
                        y = e.position.getScrollInfo(b),
                        _ = (s.collision || "flip").split(" "),
                        w = {};
                    return g = function(t) {
                            var i = t[0];
                            return 9 === i.nodeType ? {
                                width: t.width(),
                                height: t.height(),
                                offset: {
                                    top: 0,
                                    left: 0
                                }
                            } : e.isWindow(i) ? {
                                width: t.width(),
                                height: t.height(),
                                offset: {
                                    top: t.scrollTop(),
                                    left: t.scrollLeft()
                                }
                            } : i.preventDefault ? {
                                width: 0,
                                height: 0,
                                offset: {
                                    top: i.pageY,
                                    left: i.pageX
                                }
                            } : {
                                width: t.outerWidth(),
                                height: t.outerHeight(),
                                offset: t.offset()
                            }
                        }(v),
                        v[0].preventDefault && (s.at = "left top"),
                        d = g.width,
                        p = g.height,
                        m = e.extend({}, f = g.offset),
                        e.each(["my", "at"], function() {
                            var e, t, i = (s[this] || "").split(" ");
                            1 === i.length && (i = o.test(i[0]) ? i.concat(["center"]) : r.test(i[0]) ? ["center"].concat(i) : ["center", "center"]),
                                i[0] = o.test(i[0]) ? i[0] : "center",
                                i[1] = r.test(i[1]) ? i[1] : "center",
                                e = l.exec(i[0]),
                                t = l.exec(i[1]),
                                w[this] = [e ? e[0] : 0, t ? t[0] : 0],
                                s[this] = [h.exec(i[0])[0], h.exec(i[1])[0]]
                        }),
                        1 === _.length && (_[1] = _[0]),
                        "right" === s.at[0] ? m.left += d : "center" === s.at[0] && (m.left += d / 2),
                        "bottom" === s.at[1] ? m.top += p : "center" === s.at[1] && (m.top += p / 2),
                        u = t(w.at, d, p),
                        m.left += u[0],
                        m.top += u[1],
                        this.each(function() {
                            var o, r, l = e(this),
                                h = l.outerWidth(),
                                c = l.outerHeight(),
                                g = i(this, "marginLeft"),
                                x = i(this, "marginTop"),
                                C = h + g + i(this, "marginRight") + y.width,
                                T = c + x + i(this, "marginBottom") + y.height,
                                k = e.extend({}, m),
                                S = t(w.my, l.outerWidth(), l.outerHeight());
                            "right" === s.my[0] ? k.left -= h : "center" === s.my[0] && (k.left -= h / 2),
                                "bottom" === s.my[1] ? k.top -= c : "center" === s.my[1] && (k.top -= c / 2),
                                k.left += S[0],
                                k.top += S[1],
                                o = {
                                    marginLeft: g,
                                    marginTop: x
                                },
                                e.each(["left", "top"], function(t, i) {
                                    e.ui.position[_[t]] && e.ui.position[_[t]][i](k, {
                                        targetWidth: d,
                                        targetHeight: p,
                                        elemWidth: h,
                                        elemHeight: c,
                                        collisionPosition: o,
                                        collisionWidth: C,
                                        collisionHeight: T,
                                        offset: [u[0] + S[0], u[1] + S[1]],
                                        my: s.my,
                                        at: s.at,
                                        within: b,
                                        elem: l
                                    })
                                }),
                                s.using && (r = function(e) {
                                    var t = f.left - k.left,
                                        i = t + d - h,
                                        o = f.top - k.top,
                                        r = o + p - c,
                                        u = {
                                            target: {
                                                element: v,
                                                left: f.left,
                                                top: f.top,
                                                width: d,
                                                height: p
                                            },
                                            element: {
                                                element: l,
                                                left: k.left,
                                                top: k.top,
                                                width: h,
                                                height: c
                                            },
                                            horizontal: 0 > i ? "left" : t > 0 ? "right" : "center",
                                            vertical: 0 > r ? "top" : o > 0 ? "bottom" : "middle"
                                        };
                                    h > d && d > a(t + i) && (u.horizontal = "center"),
                                        c > p && p > a(o + r) && (u.vertical = "middle"),
                                        u.important = n(a(t), a(i)) > n(a(o), a(r)) ? "horizontal" : "vertical",
                                        s.using.call(this, e, u)
                                }),
                                l.offset(e.extend(k, {
                                    using: r
                                }))
                        })
                },
                e.ui.position = {
                    fit: {
                        left: function(e, t) {
                            var i = t.within,
                                s = i.isWindow ? i.scrollLeft : i.offset.left,
                                a = i.width,
                                o = e.left - t.collisionPosition.marginLeft,
                                r = s - o,
                                l = o + t.collisionWidth - a - s;
                            t.collisionWidth > a ? r > 0 && 0 >= l ? e.left += r - (e.left + r + t.collisionWidth - a - s) : e.left = l > 0 && 0 >= r ? s : r > l ? s + a - t.collisionWidth : s : r > 0 ? e.left += r : l > 0 ? e.left -= l : e.left = n(e.left - o, e.left)
                        },
                        top: function(e, t) {
                            var i = t.within,
                                s = i.isWindow ? i.scrollTop : i.offset.top,
                                a = t.within.height,
                                o = e.top - t.collisionPosition.marginTop,
                                r = s - o,
                                l = o + t.collisionHeight - a - s;
                            t.collisionHeight > a ? r > 0 && 0 >= l ? e.top += r - (e.top + r + t.collisionHeight - a - s) : e.top = l > 0 && 0 >= r ? s : r > l ? s + a - t.collisionHeight : s : r > 0 ? e.top += r : l > 0 ? e.top -= l : e.top = n(e.top - o, e.top)
                        }
                    },
                    flip: {
                        left: function(e, t) {
                            var i, s, n = t.within,
                                o = n.width,
                                r = n.isWindow ? n.scrollLeft : n.offset.left,
                                l = e.left - t.collisionPosition.marginLeft,
                                h = l - r,
                                u = l + t.collisionWidth - o - r,
                                c = "left" === t.my[0] ? -t.elemWidth : "right" === t.my[0] ? t.elemWidth : 0,
                                d = "left" === t.at[0] ? t.targetWidth : "right" === t.at[0] ? -t.targetWidth : 0,
                                p = -2 * t.offset[0];
                            0 > h ? (0 > (i = e.left + c + d + p + t.collisionWidth - o - (n.offset.left + n.scrollLeft)) || a(h) > i) && (e.left += c + d + p) : u > 0 && ((s = e.left - t.collisionPosition.marginLeft + c + d + p - r) > 0 || u > a(s)) && (e.left += c + d + p)
                        },
                        top: function(e, t) {
                            var i, s, n = t.within,
                                o = n.height,
                                r = n.isWindow ? n.scrollTop : n.offset.top,
                                l = e.top - t.collisionPosition.marginTop,
                                h = l - r,
                                u = l + t.collisionHeight - o - r,
                                c = "top" === t.my[1] ? -t.elemHeight : "bottom" === t.my[1] ? t.elemHeight : 0,
                                d = "top" === t.at[1] ? t.targetHeight : "bottom" === t.at[1] ? -t.targetHeight : 0,
                                p = -2 * t.offset[1];
                            0 > h ? (0 > (s = e.top + c + d + p + t.collisionHeight - o - (n.offset.top + n.scrollTop)) || a(h) > s) && (e.top += c + d + p) : u > 0 && ((i = e.top - t.collisionPosition.marginTop + c + d + p - r) > 0 || u > a(i)) && (e.top += c + d + p)
                        }
                    },
                    flipfit: {
                        left: function() {
                            e.ui.position.flip.left.apply(this, arguments),
                                e.ui.position.fit.left.apply(this, arguments)
                        },
                        top: function() {
                            e.ui.position.flip.top.apply(this, arguments),
                                e.ui.position.fit.top.apply(this, arguments)
                        }
                    }
                }
        }(),
        e.extend(e.expr[":"], {
            data: e.expr.createPseudo ? e.expr.createPseudo(function(t) {
                return function(i) {
                    return !!e.data(i, t)
                }
            }) : function(t, i, s) {
                return !!e.data(t, s[3])
            }
        }),
        e.fn.extend({
            disableSelection: function() {
                var e = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
                return function() {
                    return this.on(e + ".ui-disableSelection", function(e) {
                        e.preventDefault()
                    })
                }
            }(),
            enableSelection: function() {
                return this.off(".ui-disableSelection")
            }
        });
    var l = "ui-effects-",
        h = "ui-effects-style",
        u = "ui-effects-animated",
        c = e;
    e.effects = {
            effect: {}
        },
        function(e, t) {
            function i(e, t, i) {
                var s = u[t.type] || {};
                return null == e ? i || !t.def ? null : t.def : (e = s.floor ? ~~e : parseFloat(e),
                    isNaN(e) ? t.def : s.mod ? (e + s.mod) % s.mod : 0 > e ? 0 : e > s.max ? s.max : e)
            }

            function s(i) {
                var s = l(),
                    n = s._rgba = [];
                return i = i.toLowerCase(),
                    p(r, function(e, a) {
                        var o, r = a.re.exec(i),
                            l = r && a.parse(r),
                            u = a.space || "rgba";
                        return l ? (o = s[u](l),
                            s[h[u].cache] = o[h[u].cache],
                            n = s._rgba = o._rgba, !1) : t
                    }),
                    n.length ? ("0,0,0,0" === n.join() && e.extend(n, a.transparent),
                        s) : a[i]
            }

            function n(e, t, i) {
                return 1 > 6 * (i = (i + 1) % 1) ? e + 6 * (t - e) * i : 1 > 2 * i ? t : 2 > 3 * i ? e + 6 * (t - e) * (2 / 3 - i) : e
            }
            var a, o = /^([\-+])=\s*(\d+\.?\d*)/,
                r = [{
                    re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                    parse: function(e) {
                        return [e[1], e[2], e[3], e[4]]
                    }
                }, {
                    re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                    parse: function(e) {
                        return [2.55 * e[1], 2.55 * e[2], 2.55 * e[3], e[4]]
                    }
                }, {
                    re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                    parse: function(e) {
                        return [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)]
                    }
                }, {
                    re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                    parse: function(e) {
                        return [parseInt(e[1] + e[1], 16), parseInt(e[2] + e[2], 16), parseInt(e[3] + e[3], 16)]
                    }
                }, {
                    re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                    space: "hsla",
                    parse: function(e) {
                        return [e[1], e[2] / 100, e[3] / 100, e[4]]
                    }
                }],
                l = e.Color = function(t, i, s, n) {
                    return new e.Color.fn.parse(t, i, s, n)
                },
                h = {
                    rgba: {
                        props: {
                            red: {
                                idx: 0,
                                type: "byte"
                            },
                            green: {
                                idx: 1,
                                type: "byte"
                            },
                            blue: {
                                idx: 2,
                                type: "byte"
                            }
                        }
                    },
                    hsla: {
                        props: {
                            hue: {
                                idx: 0,
                                type: "degrees"
                            },
                            saturation: {
                                idx: 1,
                                type: "percent"
                            },
                            lightness: {
                                idx: 2,
                                type: "percent"
                            }
                        }
                    }
                },
                u = {
                    byte: {
                        floor: !0,
                        max: 255
                    },
                    percent: {
                        max: 1
                    },
                    degrees: {
                        mod: 360,
                        floor: !0
                    }
                },
                c = l.support = {},
                d = e("<p>")[0],
                p = e.each;
            d.style.cssText = "background-color:rgba(1,1,1,.5)",
                c.rgba = d.style.backgroundColor.indexOf("rgba") > -1,
                p(h, function(e, t) {
                    t.cache = "_" + e,
                        t.props.alpha = {
                            idx: 3,
                            type: "percent",
                            def: 1
                        }
                }),
                l.fn = e.extend(l.prototype, {
                    parse: function(n, o, r, u) {
                        if (n === t)
                            return this._rgba = [null, null, null, null],
                                this;
                        (n.jquery || n.nodeType) && (n = e(n).css(o),
                            o = t);
                        var c = this,
                            d = e.type(n),
                            f = this._rgba = [];
                        return o !== t && (n = [n, o, r, u],
                                d = "array"),
                            "string" === d ? this.parse(s(n) || a._default) : "array" === d ? (p(h.rgba.props, function(e, t) {
                                    f[t.idx] = i(n[t.idx], t)
                                }),
                                this) : "object" === d ? (p(h, n instanceof l ? function(e, t) {
                                        n[t.cache] && (c[t.cache] = n[t.cache].slice())
                                    } :
                                    function(t, s) {
                                        var a = s.cache;
                                        p(s.props, function(e, t) {
                                                if (!c[a] && s.to) {
                                                    if ("alpha" === e || null == n[e])
                                                        return;
                                                    c[a] = s.to(c._rgba)
                                                }
                                                c[a][t.idx] = i(n[e], t, !0)
                                            }),
                                            c[a] && 0 > e.inArray(null, c[a].slice(0, 3)) && (c[a][3] = 1,
                                                s.from && (c._rgba = s.from(c[a])))
                                    }
                                ),
                                this) : t
                    },
                    is: function(e) {
                        var i = l(e),
                            s = !0,
                            n = this;
                        return p(h, function(e, a) {
                                var o, r = i[a.cache];
                                return r && (o = n[a.cache] || a.to && a.to(n._rgba) || [],
                                        p(a.props, function(e, i) {
                                            return null != r[i.idx] ? s = r[i.idx] === o[i.idx] : t
                                        })),
                                    s
                            }),
                            s
                    },
                    _space: function() {
                        var e = [],
                            t = this;
                        return p(h, function(i, s) {
                                t[s.cache] && e.push(i)
                            }),
                            e.pop()
                    },
                    transition: function(e, t) {
                        var s = l(e),
                            n = s._space(),
                            a = h[n],
                            o = 0 === this.alpha() ? l("transparent") : this,
                            r = o[a.cache] || a.to(o._rgba),
                            c = r.slice();
                        return s = s[a.cache],
                            p(a.props, function(e, n) {
                                var a = n.idx,
                                    o = r[a],
                                    l = s[a],
                                    h = u[n.type] || {};
                                null !== l && (null === o ? c[a] = l : (h.mod && (l - o > h.mod / 2 ? o += h.mod : o - l > h.mod / 2 && (o -= h.mod)),
                                    c[a] = i((l - o) * t + o, n)))
                            }),
                            this[n](c)
                    },
                    blend: function(t) {
                        if (1 === this._rgba[3])
                            return this;
                        var i = this._rgba.slice(),
                            s = i.pop(),
                            n = l(t)._rgba;
                        return l(e.map(i, function(e, t) {
                            return (1 - s) * n[t] + s * e
                        }))
                    },
                    toRgbaString: function() {
                        var t = "rgba(",
                            i = e.map(this._rgba, function(e, t) {
                                return null == e ? t > 2 ? 1 : 0 : e
                            });
                        return 1 === i[3] && (i.pop(),
                                t = "rgb("),
                            t + i.join() + ")"
                    },
                    toHslaString: function() {
                        var t = "hsla(",
                            i = e.map(this.hsla(), function(e, t) {
                                return null == e && (e = t > 2 ? 1 : 0),
                                    t && 3 > t && (e = Math.round(100 * e) + "%"),
                                    e
                            });
                        return 1 === i[3] && (i.pop(),
                                t = "hsl("),
                            t + i.join() + ")"
                    },
                    toHexString: function(t) {
                        var i = this._rgba.slice(),
                            s = i.pop();
                        return t && i.push(~~(255 * s)),
                            "#" + e.map(i, function(e) {
                                return 1 === (e = (e || 0).toString(16)).length ? "0" + e : e
                            }).join("")
                    },
                    toString: function() {
                        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
                    }
                }),
                l.fn.parse.prototype = l.fn,
                h.hsla.to = function(e) {
                    if (null == e[0] || null == e[1] || null == e[2])
                        return [null, null, null, e[3]];
                    var t, i = e[0] / 255,
                        s = e[1] / 255,
                        n = e[2] / 255,
                        a = e[3],
                        o = Math.max(i, s, n),
                        r = Math.min(i, s, n),
                        l = o - r,
                        h = o + r,
                        u = .5 * h;
                    return t = 0 === l ? 0 : .5 >= u ? l / h : l / (2 - h), [Math.round(r === o ? 0 : i === o ? 60 * (s - n) / l + 360 : s === o ? 60 * (n - i) / l + 120 : 60 * (i - s) / l + 240) % 360, t, u, null == a ? 1 : a]
                },
                h.hsla.from = function(e) {
                    if (null == e[0] || null == e[1] || null == e[2])
                        return [null, null, null, e[3]];
                    var t = e[0] / 360,
                        i = e[1],
                        s = e[2],
                        a = e[3],
                        o = .5 >= s ? s * (1 + i) : s + i - s * i,
                        r = 2 * s - o;
                    return [Math.round(255 * n(r, o, t + 1 / 3)), Math.round(255 * n(r, o, t)), Math.round(255 * n(r, o, t - 1 / 3)), a]
                },
                p(h, function(s, n) {
                    var a = n.props,
                        r = n.cache,
                        h = n.to,
                        u = n.from;
                    l.fn[s] = function(s) {
                            if (h && !this[r] && (this[r] = h(this._rgba)),
                                s === t)
                                return this[r].slice();
                            var n, o = e.type(s),
                                c = "array" === o || "object" === o ? s : arguments,
                                d = this[r].slice();
                            return p(a, function(e, t) {
                                    var s = c["object" === o ? e : t.idx];
                                    null == s && (s = d[t.idx]),
                                        d[t.idx] = i(s, t)
                                }),
                                u ? ((n = l(u(d)))[r] = d,
                                    n) : l(d)
                        },
                        p(a, function(t, i) {
                            l.fn[t] || (l.fn[t] = function(n) {
                                var a, r = e.type(n),
                                    l = "alpha" === t ? this._hsla ? "hsla" : "rgba" : s,
                                    h = this[l](),
                                    u = h[i.idx];
                                return "undefined" === r ? u : ("function" === r && (n = n.call(this, u),
                                        r = e.type(n)),
                                    null == n && i.empty ? this : ("string" === r && (a = o.exec(n)) && (n = u + parseFloat(a[2]) * ("+" === a[1] ? 1 : -1)),
                                        h[i.idx] = n,
                                        this[l](h)))
                            })
                        })
                }),
                l.hook = function(t) {
                    var i = t.split(" ");
                    p(i, function(t, i) {
                        e.cssHooks[i] = {
                                set: function(t, n) {
                                    var a, o, h = "";
                                    if ("transparent" !== n && ("string" !== e.type(n) || (a = s(n)))) {
                                        if (n = l(a || n), !c.rgba && 1 !== n._rgba[3]) {
                                            for (o = "backgroundColor" === i ? t.parentNode : t;
                                                ("" === h || "transparent" === h) && o && o.style;)
                                                try {
                                                    h = e.css(o, "backgroundColor"),
                                                        o = o.parentNode
                                                } catch (r) {}
                                            n = n.blend(h && "transparent" !== h ? h : "_default")
                                        }
                                        n = n.toRgbaString()
                                    }
                                    try {
                                        t.style[i] = n
                                    } catch (r) {}
                                }
                            },
                            e.fx.step[i] = function(t) {
                                t.colorInit || (t.start = l(t.elem, i),
                                        t.end = l(t.end),
                                        t.colorInit = !0),
                                    e.cssHooks[i].set(t.elem, t.start.transition(t.end, t.pos))
                            }
                    })
                },
                l.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor"),
                e.cssHooks.borderColor = {
                    expand: function(e) {
                        var t = {};
                        return p(["Top", "Right", "Bottom", "Left"], function(i, s) {
                                t["border" + s + "Color"] = e
                            }),
                            t
                    }
                },
                a = e.Color.names = {
                    aqua: "#00ffff",
                    black: "#000000",
                    blue: "#0000ff",
                    fuchsia: "#ff00ff",
                    gray: "#808080",
                    green: "#008000",
                    lime: "#00ff00",
                    maroon: "#800000",
                    navy: "#000080",
                    olive: "#808000",
                    purple: "#800080",
                    red: "#ff0000",
                    silver: "#c0c0c0",
                    teal: "#008080",
                    white: "#ffffff",
                    yellow: "#ffff00",
                    transparent: [null, null, null, 0],
                    _default: "#ffffff"
                }
        }(c),
        function() {
            function t(t) {
                var i, s, n = t.ownerDocument.defaultView ? t.ownerDocument.defaultView.getComputedStyle(t, null) : t.currentStyle,
                    a = {};
                if (n && n.length && n[0] && n[n[0]])
                    for (s = n.length; s--;)
                        "string" == typeof n[i = n[s]] && (a[e.camelCase(i)] = n[i]);
                else
                    for (i in n)
                        "string" == typeof n[i] && (a[i] = n[i]);
                return a
            }

            function i(t, i) {
                var s, a, o = {};
                for (s in i)
                    t[s] !== (a = i[s]) && (n[s] || (e.fx.step[s] || !isNaN(parseFloat(a))) && (o[s] = a));
                return o
            }
            var s = ["add", "remove", "toggle"],
                n = {
                    border: 1,
                    borderBottom: 1,
                    borderColor: 1,
                    borderLeft: 1,
                    borderRight: 1,
                    borderTop: 1,
                    borderWidth: 1,
                    margin: 1,
                    padding: 1
                };
            e.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(t, i) {
                    e.fx.step[i] = function(e) {
                        ("none" !== e.end && !e.setAttr || 1 === e.pos && !e.setAttr) && (c.style(e.elem, i, e.end),
                            e.setAttr = !0)
                    }
                }),
                e.fn.addBack || (e.fn.addBack = function(e) {
                    return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
                }),
                e.effects.animateClass = function(n, a, o, r) {
                    var l = e.speed(a, o, r);
                    return this.queue(function() {
                        var a, o = e(this),
                            r = o.attr("class") || "",
                            h = l.children ? o.find("*").addBack() : o;
                        h = h.map(function() {
                                return {
                                    el: e(this),
                                    start: t(this)
                                }
                            }),
                            (a = function() {
                                e.each(s, function(e, t) {
                                    n[t] && o[t + "Class"](n[t])
                                })
                            })(),
                            h = h.map(function() {
                                return this.end = t(this.el[0]),
                                    this.diff = i(this.start, this.end),
                                    this
                            }),
                            o.attr("class", r),
                            h = h.map(function() {
                                var t = this,
                                    i = e.Deferred(),
                                    s = e.extend({}, l, {
                                        queue: !1,
                                        complete: function() {
                                            i.resolve(t)
                                        }
                                    });
                                return this.el.animate(this.diff, s),
                                    i.promise()
                            }),
                            e.when.apply(e, h.get()).done(function() {
                                a(),
                                    e.each(arguments, function() {
                                        var t = this.el;
                                        e.each(this.diff, function(e) {
                                            t.css(e, "")
                                        })
                                    }),
                                    l.complete.call(o[0])
                            })
                    })
                },
                e.fn.extend({
                    addClass: function(t) {
                        return function(i, s, n, a) {
                            return s ? e.effects.animateClass.call(this, {
                                add: i
                            }, s, n, a) : t.apply(this, arguments)
                        }
                    }(e.fn.addClass),
                    removeClass: function(t) {
                        return function(i, s, n, a) {
                            return arguments.length > 1 ? e.effects.animateClass.call(this, {
                                remove: i
                            }, s, n, a) : t.apply(this, arguments)
                        }
                    }(e.fn.removeClass),
                    toggleClass: function(t) {
                        return function(i, s, n, a, o) {
                            return "boolean" == typeof s || void 0 === s ? n ? e.effects.animateClass.call(this, s ? {
                                add: i
                            } : {
                                remove: i
                            }, n, a, o) : t.apply(this, arguments) : e.effects.animateClass.call(this, {
                                toggle: i
                            }, s, n, a)
                        }
                    }(e.fn.toggleClass),
                    switchClass: function(t, i, s, n, a) {
                        return e.effects.animateClass.call(this, {
                            add: i,
                            remove: t
                        }, s, n, a)
                    }
                })
        }(),
        function() {
            function t(t, i, s, n) {
                return e.isPlainObject(t) && (i = t,
                        t = t.effect),
                    t = {
                        effect: t
                    },
                    null == i && (i = {}),
                    e.isFunction(i) && (n = i,
                        s = null,
                        i = {}),
                    ("number" == typeof i || e.fx.speeds[i]) && (n = s,
                        s = i,
                        i = {}),
                    e.isFunction(s) && (n = s,
                        s = null),
                    i && e.extend(t, i),
                    s = s || i.duration,
                    t.duration = e.fx.off ? 0 : "number" == typeof s ? s : s in e.fx.speeds ? e.fx.speeds[s] : e.fx.speeds._default,
                    t.complete = n || i.complete,
                    t
            }

            function i(t) {
                return !(t && "number" != typeof t && !e.fx.speeds[t]) || "string" == typeof t && !e.effects.effect[t] || !!e.isFunction(t) || "object" == typeof t && !t.effect
            }

            function s(e, t) {
                var i = t.outerWidth(),
                    s = t.outerHeight(),
                    n = /^rect\((-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto)\)$/.exec(e) || ["", 0, i, s, 0];
                return {
                    top: parseFloat(n[1]) || 0,
                    right: "auto" === n[2] ? i : parseFloat(n[2]),
                    bottom: "auto" === n[3] ? s : parseFloat(n[3]),
                    left: parseFloat(n[4]) || 0
                }
            }
            e.expr && e.expr.filters && e.expr.filters.animated && (e.expr.filters.animated = function(t) {
                    return function(i) {
                        return !!e(i).data(u) || t(i)
                    }
                }(e.expr.filters.animated)), !1 !== e.uiBackCompat && e.extend(e.effects, {
                    save: function(e, t) {
                        for (var i = 0, s = t.length; s > i; i++)
                            null !== t[i] && e.data(l + t[i], e[0].style[t[i]])
                    },
                    restore: function(e, t) {
                        for (var i, s = 0, n = t.length; n > s; s++)
                            null !== t[s] && (i = e.data(l + t[s]),
                                e.css(t[s], i))
                    },
                    setMode: function(e, t) {
                        return "toggle" === t && (t = e.is(":hidden") ? "show" : "hide"),
                            t
                    },
                    createWrapper: function(t) {
                        if (t.parent().is(".ui-effects-wrapper"))
                            return t.parent();
                        var i = {
                                width: t.outerWidth(!0),
                                height: t.outerHeight(!0),
                                float: t.css("float")
                            },
                            s = e("<div></div>").addClass("ui-effects-wrapper").css({
                                fontSize: "100%",
                                background: "transparent",
                                border: "none",
                                margin: 0,
                                padding: 0
                            }),
                            n = {
                                width: t.width(),
                                height: t.height()
                            },
                            a = document.activeElement;
                        return t.wrap(s),
                            (t[0] === a || e.contains(t[0], a)) && e(a).trigger("focus"),
                            s = t.parent(),
                            "static" === t.css("position") ? (s.css({
                                    position: "relative"
                                }),
                                t.css({
                                    position: "relative"
                                })) : (e.extend(i, {
                                    position: t.css("position"),
                                    zIndex: t.css("z-index")
                                }),
                                e.each(["top", "left", "bottom", "right"], function(e, s) {
                                    i[s] = t.css(s),
                                        isNaN(parseInt(i[s], 10)) && (i[s] = "auto")
                                }),
                                t.css({
                                    position: "relative",
                                    top: 0,
                                    left: 0,
                                    right: "auto",
                                    bottom: "auto"
                                })),
                            t.css(n),
                            s.css(i).show()
                    },
                    removeWrapper: function(t) {
                        var i = document.activeElement;
                        return t.parent().is(".ui-effects-wrapper") && (t.parent().replaceWith(t),
                                (t[0] === i || e.contains(t[0], i)) && e(i).trigger("focus")),
                            t
                    }
                }),
                e.extend(e.effects, {
                    version: "1.12.1",
                    define: function(t, i, s) {
                        return s || (s = i,
                                i = "effect"),
                            e.effects.effect[t] = s,
                            e.effects.effect[t].mode = i,
                            s
                    },
                    scaledDimensions: function(e, t, i) {
                        if (0 === t)
                            return {
                                height: 0,
                                width: 0,
                                outerHeight: 0,
                                outerWidth: 0
                            };
                        var s = "horizontal" !== i ? (t || 100) / 100 : 1,
                            n = "vertical" !== i ? (t || 100) / 100 : 1;
                        return {
                            height: e.height() * n,
                            width: e.width() * s,
                            outerHeight: e.outerHeight() * n,
                            outerWidth: e.outerWidth() * s
                        }
                    },
                    clipToBox: function(e) {
                        return {
                            width: e.clip.right - e.clip.left,
                            height: e.clip.bottom - e.clip.top,
                            left: e.clip.left,
                            top: e.clip.top
                        }
                    },
                    unshift: function(e, t, i) {
                        var s = e.queue();
                        t > 1 && s.splice.apply(s, [1, 0].concat(s.splice(t, i))),
                            e.dequeue()
                    },
                    saveStyle: function(e) {
                        e.data(h, e[0].style.cssText)
                    },
                    restoreStyle: function(e) {
                        e[0].style.cssText = e.data(h) || "",
                            e.removeData(h)
                    },
                    mode: function(e, t) {
                        var i = e.is(":hidden");
                        return "toggle" === t && (t = i ? "show" : "hide"),
                            (i ? "hide" === t : "show" === t) && (t = "none"),
                            t
                    },
                    getBaseline: function(e, t) {
                        var i, s;
                        switch (e[0]) {
                            case "top":
                                i = 0;
                                break;
                            case "middle":
                                i = .5;
                                break;
                            case "bottom":
                                i = 1;
                                break;
                            default:
                                i = e[0] / t.height
                        }
                        switch (e[1]) {
                            case "left":
                                s = 0;
                                break;
                            case "center":
                                s = .5;
                                break;
                            case "right":
                                s = 1;
                                break;
                            default:
                                s = e[1] / t.width
                        }
                        return {
                            x: s,
                            y: i
                        }
                    },
                    createPlaceholder: function(t) {
                        var i, s = t.css("position"),
                            n = t.position();
                        return t.css({
                                marginTop: t.css("marginTop"),
                                marginBottom: t.css("marginBottom"),
                                marginLeft: t.css("marginLeft"),
                                marginRight: t.css("marginRight")
                            }).outerWidth(t.outerWidth()).outerHeight(t.outerHeight()),
                            /^(static|relative)/.test(s) && (s = "absolute",
                                i = e("<" + t[0].nodeName + ">").insertAfter(t).css({
                                    display: /^(inline|ruby)/.test(t.css("display")) ? "inline-block" : "block",
                                    visibility: "hidden",
                                    marginTop: t.css("marginTop"),
                                    marginBottom: t.css("marginBottom"),
                                    marginLeft: t.css("marginLeft"),
                                    marginRight: t.css("marginRight"),
                                    float: t.css("float")
                                }).outerWidth(t.outerWidth()).outerHeight(t.outerHeight()).addClass("ui-effects-placeholder"),
                                t.data(l + "placeholder", i)),
                            t.css({
                                position: s,
                                left: n.left,
                                top: n.top
                            }),
                            i
                    },
                    removePlaceholder: function(e) {
                        var t = l + "placeholder",
                            i = e.data(t);
                        i && (i.remove(),
                            e.removeData(t))
                    },
                    cleanUp: function(t) {
                        e.effects.restoreStyle(t),
                            e.effects.removePlaceholder(t)
                    },
                    setTransition: function(t, i, s, n) {
                        return n = n || {},
                            e.each(i, function(e, i) {
                                var a = t.cssUnit(i);
                                a[0] > 0 && (n[i] = a[0] * s + a[1])
                            }),
                            n
                    }
                }),
                e.fn.extend({
                    effect: function() {
                        function i(t) {
                            function i() {
                                e.isFunction(l) && l.call(o[0]),
                                    e.isFunction(t) && t()
                            }
                            var o = e(this);
                            s.mode = c.shift(), !1 === e.uiBackCompat || a ? "none" === s.mode ? (o[h](),
                                i()) : n.call(o[0], s, function() {
                                o.removeData(u),
                                    e.effects.cleanUp(o),
                                    "hide" === s.mode && o.hide(),
                                    i()
                            }) : (o.is(":hidden") ? "hide" === h : "show" === h) ? (o[h](),
                                i()) : n.call(o[0], s, i)
                        }
                        var s = t.apply(this, arguments),
                            n = e.effects.effect[s.effect],
                            a = n.mode,
                            o = s.queue,
                            r = o || "fx",
                            l = s.complete,
                            h = s.mode,
                            c = [],
                            d = function(t) {
                                var i = e(this),
                                    s = e.effects.mode(i, h) || a;
                                i.data(u, !0),
                                    c.push(s),
                                    a && ("show" === s || s === a && "hide" === s) && i.show(),
                                    a && "none" === s || e.effects.saveStyle(i),
                                    e.isFunction(t) && t()
                            };
                        return e.fx.off || !n ? h ? this[h](s.duration, l) : this.each(function() {
                            l && l.call(this)
                        }) : !1 === o ? this.each(d).each(i) : this.queue(r, d).queue(r, i)
                    },
                    show: function(e) {
                        return function(s) {
                            if (i(s))
                                return e.apply(this, arguments);
                            var n = t.apply(this, arguments);
                            return n.mode = "show",
                                this.effect.call(this, n)
                        }
                    }(e.fn.show),
                    hide: function(e) {
                        return function(s) {
                            if (i(s))
                                return e.apply(this, arguments);
                            var n = t.apply(this, arguments);
                            return n.mode = "hide",
                                this.effect.call(this, n)
                        }
                    }(e.fn.hide),
                    toggle: function(e) {
                        return function(s) {
                            if (i(s) || "boolean" == typeof s)
                                return e.apply(this, arguments);
                            var n = t.apply(this, arguments);
                            return n.mode = "toggle",
                                this.effect.call(this, n)
                        }
                    }(e.fn.toggle),
                    cssUnit: function(t) {
                        var i = this.css(t),
                            s = [];
                        return e.each(["em", "px", "%", "pt"], function(e, t) {
                                i.indexOf(t) > 0 && (s = [parseFloat(i), t])
                            }),
                            s
                    },
                    cssClip: function(e) {
                        return e ? this.css("clip", "rect(" + e.top + "px " + e.right + "px " + e.bottom + "px " + e.left + "px)") : s(this.css("clip"), this)
                    },
                    transfer: function(t, i) {
                        var s = e(this),
                            n = e(t.to),
                            a = "fixed" === n.css("position"),
                            o = e("body"),
                            r = a ? o.scrollTop() : 0,
                            l = a ? o.scrollLeft() : 0,
                            h = n.offset(),
                            u = {
                                top: h.top - r,
                                left: h.left - l,
                                height: n.innerHeight(),
                                width: n.innerWidth()
                            },
                            c = s.offset(),
                            d = e("<div class='ui-effects-transfer'></div>").appendTo("body").addClass(t.className).css({
                                top: c.top - r,
                                left: c.left - l,
                                height: s.innerHeight(),
                                width: s.innerWidth(),
                                position: a ? "fixed" : "absolute"
                            }).animate(u, t.duration, t.easing, function() {
                                d.remove(),
                                    e.isFunction(i) && i()
                            })
                    }
                }),
                e.fx.step.clip = function(t) {
                    t.clipInit || (t.start = e(t.elem).cssClip(),
                            "string" == typeof t.end && (t.end = s(t.end, t.elem)),
                            t.clipInit = !0),
                        e(t.elem).cssClip({
                            top: t.pos * (t.end.top - t.start.top) + t.start.top,
                            right: t.pos * (t.end.right - t.start.right) + t.start.right,
                            bottom: t.pos * (t.end.bottom - t.start.bottom) + t.start.bottom,
                            left: t.pos * (t.end.left - t.start.left) + t.start.left
                        })
                }
        }(),
        function() {
            var t = {};
            e.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(e, i) {
                    t[i] = function(t) {
                        return Math.pow(t, e + 2)
                    }
                }),
                e.extend(t, {
                    Sine: function(e) {
                        return 1 - Math.cos(e * Math.PI / 2)
                    },
                    Circ: function(e) {
                        return 1 - Math.sqrt(1 - e * e)
                    },
                    Elastic: function(e) {
                        return 0 === e || 1 === e ? e : -Math.pow(2, 8 * (e - 1)) * Math.sin((80 * (e - 1) - 7.5) * Math.PI / 15)
                    },
                    Back: function(e) {
                        return e * e * (3 * e - 2)
                    },
                    Bounce: function(e) {
                        for (var t, i = 4;
                            ((t = Math.pow(2, --i)) - 1) / 11 > e;)
                        ;
                        return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * t - 2) / 22 - e, 2)
                    }
                }),
                e.each(t, function(t, i) {
                    e.easing["easeIn" + t] = i,
                        e.easing["easeOut" + t] = function(e) {
                            return 1 - i(1 - e)
                        },
                        e.easing["easeInOut" + t] = function(e) {
                            return .5 > e ? i(2 * e) / 2 : 1 - i(-2 * e + 2) / 2
                        }
                })
        }(),
        e.effects.define("blind", "hide", function(t, i) {
            var s = {
                    up: ["bottom", "top"],
                    vertical: ["bottom", "top"],
                    down: ["top", "bottom"],
                    left: ["right", "left"],
                    horizontal: ["right", "left"],
                    right: ["left", "right"]
                },
                n = e(this),
                a = t.direction || "up",
                o = n.cssClip(),
                r = {
                    clip: e.extend({}, o)
                },
                l = e.effects.createPlaceholder(n);
            r.clip[s[a][0]] = r.clip[s[a][1]],
                "show" === t.mode && (n.cssClip(r.clip),
                    l && l.css(e.effects.clipToBox(r)),
                    r.clip = o),
                l && l.animate(e.effects.clipToBox(r), t.duration, t.easing),
                n.animate(r, {
                    queue: !1,
                    duration: t.duration,
                    easing: t.easing,
                    complete: i
                })
        }),
        e.effects.define("bounce", function(t, i) {
            var s, n, a, o = e(this),
                r = t.mode,
                l = "hide" === r,
                h = "show" === r,
                u = t.direction || "up",
                c = t.distance,
                d = t.times || 5,
                p = 2 * d + (h || l ? 1 : 0),
                f = t.duration / p,
                m = t.easing,
                g = "up" === u || "down" === u ? "top" : "left",
                v = "up" === u || "left" === u,
                b = 0,
                y = o.queue().length;
            for (e.effects.createPlaceholder(o),
                a = o.css(g),
                c || (c = o["top" === g ? "outerHeight" : "outerWidth"]() / 3),
                h && ((n = {
                        opacity: 1
                    })[g] = a,
                    o.css("opacity", 0).css(g, v ? 2 * -c : 2 * c).animate(n, f, m)),
                l && (c /= Math.pow(2, d - 1)),
                (n = {})[g] = a; d > b; b++)
                (s = {})[g] = (v ? "-=" : "+=") + c,
                o.animate(s, f, m).animate(n, f, m),
                c = l ? 2 * c : c / 2;
            l && ((s = {
                        opacity: 0
                    })[g] = (v ? "-=" : "+=") + c,
                    o.animate(s, f, m)),
                o.queue(i),
                e.effects.unshift(o, y, p + 1)
        }),
        e.effects.define("clip", "hide", function(t, i) {
            var s, n = {},
                a = e(this),
                o = t.direction || "vertical",
                r = "both" === o,
                l = r || "horizontal" === o,
                h = r || "vertical" === o;
            s = a.cssClip(),
                n.clip = {
                    top: h ? (s.bottom - s.top) / 2 : s.top,
                    right: l ? (s.right - s.left) / 2 : s.right,
                    bottom: h ? (s.bottom - s.top) / 2 : s.bottom,
                    left: l ? (s.right - s.left) / 2 : s.left
                },
                e.effects.createPlaceholder(a),
                "show" === t.mode && (a.cssClip(n.clip),
                    n.clip = s),
                a.animate(n, {
                    queue: !1,
                    duration: t.duration,
                    easing: t.easing,
                    complete: i
                })
        }),
        e.effects.define("drop", "hide", function(t, i) {
            var s, n = e(this),
                a = "show" === t.mode,
                o = t.direction || "left",
                r = "up" === o || "down" === o ? "top" : "left",
                l = "up" === o || "left" === o ? "-=" : "+=",
                h = "+=" === l ? "-=" : "+=",
                u = {
                    opacity: 0
                };
            e.effects.createPlaceholder(n),
                s = t.distance || n["top" === r ? "outerHeight" : "outerWidth"](!0) / 2,
                u[r] = l + s,
                a && (n.css(u),
                    u[r] = h + s,
                    u.opacity = 1),
                n.animate(u, {
                    queue: !1,
                    duration: t.duration,
                    easing: t.easing,
                    complete: i
                })
        }),
        e.effects.define("explode", "hide", function(t, i) {
            function s() {
                v.push(this),
                    v.length === u * c && (d.css({
                            visibility: "visible"
                        }),
                        e(v).remove(),
                        i())
            }
            var n, a, o, r, l, h, u = t.pieces ? Math.round(Math.sqrt(t.pieces)) : 3,
                c = u,
                d = e(this),
                p = "show" === t.mode,
                f = d.show().css("visibility", "hidden").offset(),
                m = Math.ceil(d.outerWidth() / c),
                g = Math.ceil(d.outerHeight() / u),
                v = [];
            for (n = 0; u > n; n++)
                for (r = f.top + n * g,
                    h = n - (u - 1) / 2,
                    a = 0; c > a; a++)
                    o = f.left + a * m,
                    l = a - (c - 1) / 2,
                    d.clone().appendTo("body").wrap("<div></div>").css({
                        position: "absolute",
                        visibility: "visible",
                        left: -a * m,
                        top: -n * g
                    }).parent().addClass("ui-effects-explode").css({
                        position: "absolute",
                        overflow: "hidden",
                        width: m,
                        height: g,
                        left: o + (p ? l * m : 0),
                        top: r + (p ? h * g : 0),
                        opacity: p ? 0 : 1
                    }).animate({
                        left: o + (p ? 0 : l * m),
                        top: r + (p ? 0 : h * g),
                        opacity: p ? 1 : 0
                    }, t.duration || 500, t.easing, s)
        }),
        e.effects.define("fade", "toggle", function(t, i) {
            var s = "show" === t.mode;
            e(this).css("opacity", s ? 0 : 1).animate({
                opacity: s ? 1 : 0
            }, {
                queue: !1,
                duration: t.duration,
                easing: t.easing,
                complete: i
            })
        }),
        e.effects.define("fold", "hide", function(t, i) {
            var s = e(this),
                n = t.mode,
                a = "show" === n,
                o = "hide" === n,
                r = t.size || 15,
                l = /([0-9]+)%/.exec(r),
                h = t.horizFirst ? ["right", "bottom"] : ["bottom", "right"],
                u = t.duration / 2,
                c = e.effects.createPlaceholder(s),
                d = s.cssClip(),
                p = {
                    clip: e.extend({}, d)
                },
                f = {
                    clip: e.extend({}, d)
                },
                m = [d[h[0]], d[h[1]]],
                g = s.queue().length;
            l && (r = parseInt(l[1], 10) / 100 * m[o ? 0 : 1]),
                p.clip[h[0]] = r,
                f.clip[h[0]] = r,
                f.clip[h[1]] = 0,
                a && (s.cssClip(f.clip),
                    c && c.css(e.effects.clipToBox(f)),
                    f.clip = d),
                s.queue(function(i) {
                    c && c.animate(e.effects.clipToBox(p), u, t.easing).animate(e.effects.clipToBox(f), u, t.easing),
                        i()
                }).animate(p, u, t.easing).animate(f, u, t.easing).queue(i),
                e.effects.unshift(s, g, 4)
        }),
        e.effects.define("highlight", "show", function(t, i) {
            var s = e(this),
                n = {
                    backgroundColor: s.css("backgroundColor")
                };
            "hide" === t.mode && (n.opacity = 0),
                e.effects.saveStyle(s),
                s.css({
                    backgroundImage: "none",
                    backgroundColor: t.color || "#ffff99"
                }).animate(n, {
                    queue: !1,
                    duration: t.duration,
                    easing: t.easing,
                    complete: i
                })
        }),
        e.effects.define("size", function(t, i) {
            var s, n, a, o = e(this),
                r = ["fontSize"],
                l = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"],
                h = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
                u = t.mode,
                c = "effect" !== u,
                d = t.scale || "both",
                p = t.origin || ["middle", "center"],
                f = o.css("position"),
                m = o.position(),
                g = e.effects.scaledDimensions(o),
                v = t.from || g,
                b = t.to || e.effects.scaledDimensions(o, 0);
            e.effects.createPlaceholder(o),
                "show" === u && (a = v,
                    v = b,
                    b = a),
                n = {
                    from: {
                        y: v.height / g.height,
                        x: v.width / g.width
                    },
                    to: {
                        y: b.height / g.height,
                        x: b.width / g.width
                    }
                },
                ("box" === d || "both" === d) && (n.from.y !== n.to.y && (v = e.effects.setTransition(o, l, n.from.y, v),
                        b = e.effects.setTransition(o, l, n.to.y, b)),
                    n.from.x !== n.to.x && (v = e.effects.setTransition(o, h, n.from.x, v),
                        b = e.effects.setTransition(o, h, n.to.x, b))),
                ("content" === d || "both" === d) && n.from.y !== n.to.y && (v = e.effects.setTransition(o, r, n.from.y, v),
                    b = e.effects.setTransition(o, r, n.to.y, b)),
                p && (s = e.effects.getBaseline(p, g),
                    v.top = (g.outerHeight - v.outerHeight) * s.y + m.top,
                    v.left = (g.outerWidth - v.outerWidth) * s.x + m.left,
                    b.top = (g.outerHeight - b.outerHeight) * s.y + m.top,
                    b.left = (g.outerWidth - b.outerWidth) * s.x + m.left),
                o.css(v),
                ("content" === d || "both" === d) && (l = l.concat(["marginTop", "marginBottom"]).concat(r),
                    h = h.concat(["marginLeft", "marginRight"]),
                    o.find("*[width]").each(function() {
                        var i = e(this),
                            s = e.effects.scaledDimensions(i),
                            a = {
                                height: s.height * n.from.y,
                                width: s.width * n.from.x,
                                outerHeight: s.outerHeight * n.from.y,
                                outerWidth: s.outerWidth * n.from.x
                            },
                            o = {
                                height: s.height * n.to.y,
                                width: s.width * n.to.x,
                                outerHeight: s.height * n.to.y,
                                outerWidth: s.width * n.to.x
                            };
                        n.from.y !== n.to.y && (a = e.effects.setTransition(i, l, n.from.y, a),
                                o = e.effects.setTransition(i, l, n.to.y, o)),
                            n.from.x !== n.to.x && (a = e.effects.setTransition(i, h, n.from.x, a),
                                o = e.effects.setTransition(i, h, n.to.x, o)),
                            c && e.effects.saveStyle(i),
                            i.css(a),
                            i.animate(o, t.duration, t.easing, function() {
                                c && e.effects.restoreStyle(i)
                            })
                    })),
                o.animate(b, {
                    queue: !1,
                    duration: t.duration,
                    easing: t.easing,
                    complete: function() {
                        var t = o.offset();
                        0 === b.opacity && o.css("opacity", v.opacity),
                            c || (o.css("position", "static" === f ? "relative" : f).offset(t),
                                e.effects.saveStyle(o)),
                            i()
                    }
                })
        }),
        e.effects.define("scale", function(t, i) {
            var s = e(this),
                n = t.mode,
                a = parseInt(t.percent, 10) || (0 === parseInt(t.percent, 10) ? 0 : "effect" !== n ? 0 : 100),
                o = e.extend(!0, {
                    from: e.effects.scaledDimensions(s),
                    to: e.effects.scaledDimensions(s, a, t.direction || "both"),
                    origin: t.origin || ["middle", "center"]
                }, t);
            t.fade && (o.from.opacity = 1,
                    o.to.opacity = 0),
                e.effects.effect.size.call(this, o, i)
        }),
        e.effects.define("puff", "hide", function(t, i) {
            var s = e.extend(!0, {}, t, {
                fade: !0,
                percent: parseInt(t.percent, 10) || 150
            });
            e.effects.effect.scale.call(this, s, i)
        }),
        e.effects.define("pulsate", "show", function(t, i) {
            var s = e(this),
                n = t.mode,
                a = "show" === n,
                o = 2 * (t.times || 5) + (a || "hide" === n ? 1 : 0),
                r = t.duration / o,
                l = 0,
                h = 1,
                u = s.queue().length;
            for ((a || !s.is(":visible")) && (s.css("opacity", 0).show(),
                    l = 1); o > h; h++)
                s.animate({
                    opacity: l
                }, r, t.easing),
                l = 1 - l;
            s.animate({
                    opacity: l
                }, r, t.easing),
                s.queue(i),
                e.effects.unshift(s, u, o + 1)
        }),
        e.effects.define("shake", function(t, i) {
            var s = 1,
                n = e(this),
                a = t.direction || "left",
                o = t.distance || 20,
                r = t.times || 3,
                l = 2 * r + 1,
                h = Math.round(t.duration / l),
                u = "up" === a || "down" === a ? "top" : "left",
                c = "up" === a || "left" === a,
                d = {},
                p = {},
                f = {},
                m = n.queue().length;
            for (e.effects.createPlaceholder(n),
                d[u] = (c ? "-=" : "+=") + o,
                p[u] = (c ? "+=" : "-=") + 2 * o,
                f[u] = (c ? "-=" : "+=") + 2 * o,
                n.animate(d, h, t.easing); r > s; s++)
                n.animate(p, h, t.easing).animate(f, h, t.easing);
            n.animate(p, h, t.easing).animate(d, h / 2, t.easing).queue(i),
                e.effects.unshift(n, m, l + 1)
        }),
        e.effects.define("slide", "show", function(t, i) {
            var s, n, a = e(this),
                o = {
                    up: ["bottom", "top"],
                    down: ["top", "bottom"],
                    left: ["right", "left"],
                    right: ["left", "right"]
                },
                r = t.mode,
                l = t.direction || "left",
                h = "up" === l || "down" === l ? "top" : "left",
                u = "up" === l || "left" === l,
                c = t.distance || a["top" === h ? "outerHeight" : "outerWidth"](!0),
                d = {};
            e.effects.createPlaceholder(a),
                s = a.cssClip(),
                n = a.position()[h],
                d[h] = (u ? -1 : 1) * c + n,
                d.clip = a.cssClip(),
                d.clip[o[l][1]] = d.clip[o[l][0]],
                "show" === r && (a.cssClip(d.clip),
                    a.css(h, d[h]),
                    d.clip = s,
                    d[h] = n),
                a.animate(d, {
                    queue: !1,
                    duration: t.duration,
                    easing: t.easing,
                    complete: i
                })
        }), !1 !== e.uiBackCompat && e.effects.define("transfer", function(t, i) {
            e(this).transfer(t, i)
        }),
        e.ui.focusable = function(t, i) {
            var s, n, a, o, r, l = t.nodeName.toLowerCase();
            return "area" === l ? (n = (s = t.parentNode).name, !(!t.href || !n || "map" !== s.nodeName.toLowerCase()) && (a = e("img[usemap='#" + n + "']")).length > 0 && a.is(":visible")) : (/^(input|select|textarea|button|object)$/.test(l) ? (o = !t.disabled) && (r = e(t).closest("fieldset")[0]) && (o = !r.disabled) : o = "a" === l && t.href || i,
                o && e(t).is(":visible") && function(e) {
                    for (var t = e.css("visibility");
                        "inherit" === t;)
                        t = (e = e.parent()).css("visibility");
                    return "hidden" !== t
                }(e(t)))
        },
        e.extend(e.expr[":"], {
            focusable: function(t) {
                return e.ui.focusable(t, null != e.attr(t, "tabindex"))
            }
        }),
        e.fn.form = function() {
            return "string" == typeof this[0].form ? this.closest("form") : e(this[0].form)
        },
        e.ui.formResetMixin = {
            _formResetHandler: function() {
                var t = e(this);
                setTimeout(function() {
                    var i = t.data("ui-form-reset-instances");
                    e.each(i, function() {
                        this.refresh()
                    })
                })
            },
            _bindFormResetHandler: function() {
                if (this.form = this.element.form(),
                    this.form.length) {
                    var e = this.form.data("ui-form-reset-instances") || [];
                    e.length || this.form.on("reset.ui-form-reset", this._formResetHandler),
                        e.push(this),
                        this.form.data("ui-form-reset-instances", e)
                }
            },
            _unbindFormResetHandler: function() {
                if (this.form.length) {
                    var t = this.form.data("ui-form-reset-instances");
                    t.splice(e.inArray(this, t), 1),
                        t.length ? this.form.data("ui-form-reset-instances", t) : this.form.removeData("ui-form-reset-instances").off("reset.ui-form-reset")
                }
            }
        },
        "1.7" === e.fn.jquery.substring(0, 3) && (e.each(["Width", "Height"], function(t, i) {
                function s(t, i, s, a) {
                    return e.each(n, function() {
                            i -= parseFloat(e.css(t, "padding" + this)) || 0,
                                s && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0),
                                a && (i -= parseFloat(e.css(t, "margin" + this)) || 0)
                        }),
                        i
                }
                var n = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
                    a = i.toLowerCase(),
                    o = {
                        innerWidth: e.fn.innerWidth,
                        innerHeight: e.fn.innerHeight,
                        outerWidth: e.fn.outerWidth,
                        outerHeight: e.fn.outerHeight
                    };
                e.fn["inner" + i] = function(t) {
                        return void 0 === t ? o["inner" + i].call(this) : this.each(function() {
                            e(this).css(a, s(this, t) + "px")
                        })
                    },
                    e.fn["outer" + i] = function(t, n) {
                        return "number" != typeof t ? o["outer" + i].call(this, t) : this.each(function() {
                            e(this).css(a, s(this, t, !0, n) + "px")
                        })
                    }
            }),
            e.fn.addBack = function(e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        ),
        e.ui.keyCode = {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        },
        e.ui.escapeSelector = function() {
            var e = /([!"#$%&'()*+,.\/:;<=>?@[\]^`{|}~])/g;
            return function(t) {
                return t.replace(e, "\\$1")
            }
        }(),
        e.fn.labels = function() {
            var t, i, s, n, a;
            return this[0].labels && this[0].labels.length ? this.pushStack(this[0].labels) : (n = this.eq(0).parents("label"),
                (s = this.attr("id")) && (a = (t = this.eq(0).parents().last()).add(t.length ? t.siblings() : this.siblings()),
                    i = "label[for='" + e.ui.escapeSelector(s) + "']",
                    n = n.add(a.find(i).addBack(i))),
                this.pushStack(n))
        },
        e.fn.scrollParent = function(t) {
            var i = this.css("position"),
                s = "absolute" === i,
                n = t ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
                a = this.parents().filter(function() {
                    var t = e(this);
                    return (!s || "static" !== t.css("position")) && n.test(t.css("overflow") + t.css("overflow-y") + t.css("overflow-x"))
                }).eq(0);
            return "fixed" !== i && a.length ? a : e(this[0].ownerDocument || document)
        },
        e.extend(e.expr[":"], {
            tabbable: function(t) {
                var i = e.attr(t, "tabindex"),
                    s = null != i;
                return (!s || i >= 0) && e.ui.focusable(t, s)
            }
        }),
        e.fn.extend({
            uniqueId: function() {
                var e = 0;
                return function() {
                    return this.each(function() {
                        this.id || (this.id = "ui-id-" + ++e)
                    })
                }
            }(),
            removeUniqueId: function() {
                return this.each(function() {
                    /^ui-id-\d+$/.test(this.id) && e(this).removeAttr("id")
                })
            }
        }),
        e.widget("ui.accordion", {
            version: "1.12.1",
            options: {
                active: 0,
                animate: {},
                classes: {
                    "ui-accordion-header": "ui-corner-top",
                    "ui-accordion-header-collapsed": "ui-corner-all",
                    "ui-accordion-content": "ui-corner-bottom"
                },
                collapsible: !1,
                event: "click",
                header: "> li > :first-child, > :not(li):even",
                heightStyle: "auto",
                icons: {
                    activeHeader: "ui-icon-triangle-1-s",
                    header: "ui-icon-triangle-1-e"
                },
                activate: null,
                beforeActivate: null
            },
            hideProps: {
                borderTopWidth: "hide",
                borderBottomWidth: "hide",
                paddingTop: "hide",
                paddingBottom: "hide",
                height: "hide"
            },
            showProps: {
                borderTopWidth: "show",
                borderBottomWidth: "show",
                paddingTop: "show",
                paddingBottom: "show",
                height: "show"
            },
            _create: function() {
                var t = this.options;
                this.prevShow = this.prevHide = e(),
                    this._addClass("ui-accordion", "ui-widget ui-helper-reset"),
                    this.element.attr("role", "tablist"),
                    t.collapsible || !1 !== t.active && null != t.active || (t.active = 0),
                    this._processPanels(),
                    0 > t.active && (t.active += this.headers.length),
                    this._refresh()
            },
            _getCreateEventData: function() {
                return {
                    header: this.active,
                    panel: this.active.length ? this.active.next() : e()
                }
            },
            _createIcons: function() {
                var t, i, s = this.options.icons;
                s && (t = e("<span>"),
                    this._addClass(t, "ui-accordion-header-icon", "ui-icon " + s.header),
                    t.prependTo(this.headers),
                    i = this.active.children(".ui-accordion-header-icon"),
                    this._removeClass(i, s.header)._addClass(i, null, s.activeHeader)._addClass(this.headers, "ui-accordion-icons"))
            },
            _destroyIcons: function() {
                this._removeClass(this.headers, "ui-accordion-icons"),
                    this.headers.children(".ui-accordion-header-icon").remove()
            },
            _destroy: function() {
                var e;
                this.element.removeAttr("role"),
                    this.headers.removeAttr("role aria-expanded aria-selected aria-controls tabIndex").removeUniqueId(),
                    this._destroyIcons(),
                    e = this.headers.next().css("display", "").removeAttr("role aria-hidden aria-labelledby").removeUniqueId(),
                    "content" !== this.options.heightStyle && e.css("height", "")
            },
            _setOption: function(e, t) {
                return "active" === e ? void this._activate(t) : ("event" === e && (this.options.event && this._off(this.headers, this.options.event),
                        this._setupEvents(t)),
                    this._super(e, t),
                    "collapsible" !== e || t || !1 !== this.options.active || this._activate(0),
                    void("icons" === e && (this._destroyIcons(),
                        t && this._createIcons())))
            },
            _setOptionDisabled: function(e) {
                this._super(e),
                    this.element.attr("aria-disabled", e),
                    this._toggleClass(null, "ui-state-disabled", !!e),
                    this._toggleClass(this.headers.add(this.headers.next()), null, "ui-state-disabled", !!e)
            },
            _keydown: function(t) {
                if (!t.altKey && !t.ctrlKey) {
                    var i = e.ui.keyCode,
                        s = this.headers.length,
                        n = this.headers.index(t.target),
                        a = !1;
                    switch (t.keyCode) {
                        case i.RIGHT:
                        case i.DOWN:
                            a = this.headers[(n + 1) % s];
                            break;
                        case i.LEFT:
                        case i.UP:
                            a = this.headers[(n - 1 + s) % s];
                            break;
                        case i.SPACE:
                        case i.ENTER:
                            this._eventHandler(t);
                            break;
                        case i.HOME:
                            a = this.headers[0];
                            break;
                        case i.END:
                            a = this.headers[s - 1]
                    }
                    a && (e(t.target).attr("tabIndex", -1),
                        e(a).attr("tabIndex", 0),
                        e(a).trigger("focus"),
                        t.preventDefault())
                }
            },
            _panelKeyDown: function(t) {
                t.keyCode === e.ui.keyCode.UP && t.ctrlKey && e(t.currentTarget).prev().trigger("focus")
            },
            refresh: function() {
                var t = this.options;
                this._processPanels(), !1 === t.active && !0 === t.collapsible || !this.headers.length ? (t.active = !1,
                        this.active = e()) : !1 === t.active ? this._activate(0) : this.active.length && !e.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (t.active = !1,
                        this.active = e()) : this._activate(Math.max(0, t.active - 1)) : t.active = this.headers.index(this.active),
                    this._destroyIcons(),
                    this._refresh()
            },
            _processPanels: function() {
                var e = this.headers,
                    t = this.panels;
                this.headers = this.element.find(this.options.header),
                    this._addClass(this.headers, "ui-accordion-header ui-accordion-header-collapsed", "ui-state-default"),
                    this.panels = this.headers.next().filter(":not(.ui-accordion-content-active)").hide(),
                    this._addClass(this.panels, "ui-accordion-content", "ui-helper-reset ui-widget-content"),
                    t && (this._off(e.not(this.headers)),
                        this._off(t.not(this.panels)))
            },
            _refresh: function() {
                var t, i = this.options,
                    s = i.heightStyle,
                    n = this.element.parent();
                this.active = this._findActive(i.active),
                    this._addClass(this.active, "ui-accordion-header-active", "ui-state-active")._removeClass(this.active, "ui-accordion-header-collapsed"),
                    this._addClass(this.active.next(), "ui-accordion-content-active"),
                    this.active.next().show(),
                    this.headers.attr("role", "tab").each(function() {
                        var t = e(this),
                            i = t.uniqueId().attr("id"),
                            s = t.next(),
                            n = s.uniqueId().attr("id");
                        t.attr("aria-controls", n),
                            s.attr("aria-labelledby", i)
                    }).next().attr("role", "tabpanel"),
                    this.headers.not(this.active).attr({
                        "aria-selected": "false",
                        "aria-expanded": "false",
                        tabIndex: -1
                    }).next().attr({
                        "aria-hidden": "true"
                    }).hide(),
                    this.active.length ? this.active.attr({
                        "aria-selected": "true",
                        "aria-expanded": "true",
                        tabIndex: 0
                    }).next().attr({
                        "aria-hidden": "false"
                    }) : this.headers.eq(0).attr("tabIndex", 0),
                    this._createIcons(),
                    this._setupEvents(i.event),
                    "fill" === s ? (t = n.height(),
                        this.element.siblings(":visible").each(function() {
                            var i = e(this),
                                s = i.css("position");
                            "absolute" !== s && "fixed" !== s && (t -= i.outerHeight(!0))
                        }),
                        this.headers.each(function() {
                            t -= e(this).outerHeight(!0)
                        }),
                        this.headers.next().each(function() {
                            e(this).height(Math.max(0, t - e(this).innerHeight() + e(this).height()))
                        }).css("overflow", "auto")) : "auto" === s && (t = 0,
                        this.headers.next().each(function() {
                            var i = e(this).is(":visible");
                            i || e(this).show(),
                                t = Math.max(t, e(this).css("height", "").height()),
                                i || e(this).hide()
                        }).height(t))
            },
            _activate: function(t) {
                var i = this._findActive(t)[0];
                i !== this.active[0] && this._eventHandler({
                    target: i = i || this.active[0],
                    currentTarget: i,
                    preventDefault: e.noop
                })
            },
            _findActive: function(t) {
                return "number" == typeof t ? this.headers.eq(t) : e()
            },
            _setupEvents: function(t) {
                var i = {
                    keydown: "_keydown"
                };
                t && e.each(t.split(" "), function(e, t) {
                        i[t] = "_eventHandler"
                    }),
                    this._off(this.headers.add(this.headers.next())),
                    this._on(this.headers, i),
                    this._on(this.headers.next(), {
                        keydown: "_panelKeyDown"
                    }),
                    this._hoverable(this.headers),
                    this._focusable(this.headers)
            },
            _eventHandler: function(t) {
                var i, s, n = this.options,
                    a = this.active,
                    o = e(t.currentTarget),
                    r = o[0] === a[0],
                    l = r && n.collapsible,
                    h = l ? e() : o.next(),
                    u = a.next(),
                    c = {
                        oldHeader: a,
                        oldPanel: u,
                        newHeader: l ? e() : o,
                        newPanel: h
                    };
                t.preventDefault(),
                    r && !n.collapsible || !1 === this._trigger("beforeActivate", t, c) || (n.active = !l && this.headers.index(o),
                        this.active = r ? e() : o,
                        this._toggle(c),
                        this._removeClass(a, "ui-accordion-header-active", "ui-state-active"),
                        n.icons && (i = a.children(".ui-accordion-header-icon"),
                            this._removeClass(i, null, n.icons.activeHeader)._addClass(i, null, n.icons.header)),
                        r || (this._removeClass(o, "ui-accordion-header-collapsed")._addClass(o, "ui-accordion-header-active", "ui-state-active"),
                            n.icons && (s = o.children(".ui-accordion-header-icon"),
                                this._removeClass(s, null, n.icons.header)._addClass(s, null, n.icons.activeHeader)),
                            this._addClass(o.next(), "ui-accordion-content-active")))
            },
            _toggle: function(t) {
                var i = t.newPanel,
                    s = this.prevShow.length ? this.prevShow : t.oldPanel;
                this.prevShow.add(this.prevHide).stop(!0, !0),
                    this.prevShow = i,
                    this.prevHide = s,
                    this.options.animate ? this._animate(i, s, t) : (s.hide(),
                        i.show(),
                        this._toggleComplete(t)),
                    s.attr({
                        "aria-hidden": "true"
                    }),
                    s.prev().attr({
                        "aria-selected": "false",
                        "aria-expanded": "false"
                    }),
                    i.length && s.length ? s.prev().attr({
                        tabIndex: -1,
                        "aria-expanded": "false"
                    }) : i.length && this.headers.filter(function() {
                        return 0 === parseInt(e(this).attr("tabIndex"), 10)
                    }).attr("tabIndex", -1),
                    i.attr("aria-hidden", "false").prev().attr({
                        "aria-selected": "true",
                        "aria-expanded": "true",
                        tabIndex: 0
                    })
            },
            _animate: function(e, t, i) {
                var s, n, a, o = this,
                    r = 0,
                    l = e.css("box-sizing"),
                    h = e.length && (!t.length || e.index() < t.index()),
                    u = this.options.animate || {},
                    c = h && u.down || u,
                    d = function() {
                        o._toggleComplete(i)
                    };
                return "number" == typeof c && (a = c),
                    "string" == typeof c && (n = c),
                    n = n || c.easing || u.easing,
                    a = a || c.duration || u.duration,
                    t.length ? e.length ? (s = e.show().outerHeight(),
                        t.animate(this.hideProps, {
                            duration: a,
                            easing: n,
                            step: function(e, t) {
                                t.now = Math.round(e)
                            }
                        }),
                        void e.hide().animate(this.showProps, {
                            duration: a,
                            easing: n,
                            complete: d,
                            step: function(e, i) {
                                i.now = Math.round(e),
                                    "height" !== i.prop ? "content-box" === l && (r += i.now) : "content" !== o.options.heightStyle && (i.now = Math.round(s - t.outerHeight() - r),
                                        r = 0)
                            }
                        })) : t.animate(this.hideProps, a, n, d) : e.animate(this.showProps, a, n, d)
            },
            _toggleComplete: function(e) {
                var t = e.oldPanel,
                    i = t.prev();
                this._removeClass(t, "ui-accordion-content-active"),
                    this._removeClass(i, "ui-accordion-header-active")._addClass(i, "ui-accordion-header-collapsed"),
                    t.length && (t.parent()[0].className = t.parent()[0].className),
                    this._trigger("activate", null, e)
            }
        }),
        e.ui.safeActiveElement = function(e) {
            var t;
            try {
                t = e.activeElement
            } catch (i) {
                t = e.body
            }
            return t || (t = e.body),
                t.nodeName || (t = e.body),
                t
        },
        e.widget("ui.menu", {
            version: "1.12.1",
            defaultElement: "<ul>",
            delay: 300,
            options: {
                icons: {
                    submenu: "ui-icon-caret-1-e"
                },
                items: "> *",
                menus: "ul",
                position: {
                    my: "left top",
                    at: "right top"
                },
                role: "menu",
                blur: null,
                focus: null,
                select: null
            },
            _create: function() {
                this.activeMenu = this.element,
                    this.mouseHandled = !1,
                    this.element.uniqueId().attr({
                        role: this.options.role,
                        tabIndex: 0
                    }),
                    this._addClass("ui-menu", "ui-widget ui-widget-content"),
                    this._on({
                        "mousedown .ui-menu-item": function(e) {
                            e.preventDefault()
                        },
                        "click .ui-menu-item": function(t) {
                            var i = e(t.target),
                                s = e(e.ui.safeActiveElement(this.document[0]));
                            !this.mouseHandled && i.not(".ui-state-disabled").length && (this.select(t),
                                t.isPropagationStopped() || (this.mouseHandled = !0),
                                i.has(".ui-menu").length ? this.expand(t) : !this.element.is(":focus") && s.closest(".ui-menu").length && (this.element.trigger("focus", [!0]),
                                    this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
                        },
                        "mouseenter .ui-menu-item": function(t) {
                            if (!this.previousFilter) {
                                var i = e(t.target).closest(".ui-menu-item"),
                                    s = e(t.currentTarget);
                                i[0] === s[0] && (this._removeClass(s.siblings().children(".ui-state-active"), null, "ui-state-active"),
                                    this.focus(t, s))
                            }
                        },
                        mouseleave: "collapseAll",
                        "mouseleave .ui-menu": "collapseAll",
                        focus: function(e, t) {
                            var i = this.active || this.element.find(this.options.items).eq(0);
                            t || this.focus(e, i)
                        },
                        blur: function(t) {
                            this._delay(function() {
                                !e.contains(this.element[0], e.ui.safeActiveElement(this.document[0])) && this.collapseAll(t)
                            })
                        },
                        keydown: "_keydown"
                    }),
                    this.refresh(),
                    this._on(this.document, {
                        click: function(e) {
                            this._closeOnDocumentClick(e) && this.collapseAll(e),
                                this.mouseHandled = !1
                        }
                    })
            },
            _destroy: function() {
                var t = this.element.find(".ui-menu-item").removeAttr("role aria-disabled").children(".ui-menu-item-wrapper").removeUniqueId().removeAttr("tabIndex role aria-haspopup");
                this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeAttr("role aria-labelledby aria-expanded aria-hidden aria-disabled tabIndex").removeUniqueId().show(),
                    t.children().each(function() {
                        var t = e(this);
                        t.data("ui-menu-submenu-caret") && t.remove()
                    })
            },
            _keydown: function(t) {
                var i, s, n, a, o = !0;
                switch (t.keyCode) {
                    case e.ui.keyCode.PAGE_UP:
                        this.previousPage(t);
                        break;
                    case e.ui.keyCode.PAGE_DOWN:
                        this.nextPage(t);
                        break;
                    case e.ui.keyCode.HOME:
                        this._move("first", "first", t);
                        break;
                    case e.ui.keyCode.END:
                        this._move("last", "last", t);
                        break;
                    case e.ui.keyCode.UP:
                        this.previous(t);
                        break;
                    case e.ui.keyCode.DOWN:
                        this.next(t);
                        break;
                    case e.ui.keyCode.LEFT:
                        this.collapse(t);
                        break;
                    case e.ui.keyCode.RIGHT:
                        this.active && !this.active.is(".ui-state-disabled") && this.expand(t);
                        break;
                    case e.ui.keyCode.ENTER:
                    case e.ui.keyCode.SPACE:
                        this._activate(t);
                        break;
                    case e.ui.keyCode.ESCAPE:
                        this.collapse(t);
                        break;
                    default:
                        o = !1,
                            s = this.previousFilter || "",
                            a = !1,
                            n = t.keyCode >= 96 && 105 >= t.keyCode ? "" + (t.keyCode - 96) : String.fromCharCode(t.keyCode),
                            clearTimeout(this.filterTimer),
                            n === s ? a = !0 : n = s + n,
                            i = this._filterMenuItems(n),
                            (i = a && -1 !== i.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : i).length || (n = String.fromCharCode(t.keyCode),
                                i = this._filterMenuItems(n)),
                            i.length ? (this.focus(t, i),
                                this.previousFilter = n,
                                this.filterTimer = this._delay(function() {
                                    delete this.previousFilter
                                }, 1e3)) : delete this.previousFilter
                }
                o && t.preventDefault()
            },
            _activate: function(e) {
                this.active && !this.active.is(".ui-state-disabled") && (this.active.children("[aria-haspopup='true']").length ? this.expand(e) : this.select(e))
            },
            refresh: function() {
                var t, i, s, n, a = this,
                    o = this.options.icons.submenu,
                    r = this.element.find(this.options.menus);
                this._toggleClass("ui-menu-icons", null, !!this.element.find(".ui-icon").length),
                    i = r.filter(":not(.ui-menu)").hide().attr({
                        role: this.options.role,
                        "aria-hidden": "true",
                        "aria-expanded": "false"
                    }).each(function() {
                        var t = e(this),
                            i = t.prev(),
                            s = e("<span>").data("ui-menu-submenu-caret", !0);
                        a._addClass(s, "ui-menu-icon", "ui-icon " + o),
                            i.attr("aria-haspopup", "true").prepend(s),
                            t.attr("aria-labelledby", i.attr("id"))
                    }),
                    this._addClass(i, "ui-menu", "ui-widget ui-widget-content ui-front"),
                    (t = r.add(this.element).find(this.options.items)).not(".ui-menu-item").each(function() {
                        var t = e(this);
                        a._isDivider(t) && a._addClass(t, "ui-menu-divider", "ui-widget-content")
                    }),
                    n = (s = t.not(".ui-menu-item, .ui-menu-divider")).children().not(".ui-menu").uniqueId().attr({
                        tabIndex: -1,
                        role: this._itemRole()
                    }),
                    this._addClass(s, "ui-menu-item")._addClass(n, "ui-menu-item-wrapper"),
                    t.filter(".ui-state-disabled").attr("aria-disabled", "true"),
                    this.active && !e.contains(this.element[0], this.active[0]) && this.blur()
            },
            _itemRole: function() {
                return {
                    menu: "menuitem",
                    listbox: "option"
                }[this.options.role]
            },
            _setOption: function(e, t) {
                if ("icons" === e) {
                    var i = this.element.find(".ui-menu-icon");
                    this._removeClass(i, null, this.options.icons.submenu)._addClass(i, null, t.submenu)
                }
                this._super(e, t)
            },
            _setOptionDisabled: function(e) {
                this._super(e),
                    this.element.attr("aria-disabled", e + ""),
                    this._toggleClass(null, "ui-state-disabled", !!e)
            },
            focus: function(e, t) {
                var i, s, n;
                this.blur(e, e && "focus" === e.type),
                    this._scrollIntoView(t),
                    this.active = t.first(),
                    s = this.active.children(".ui-menu-item-wrapper"),
                    this._addClass(s, null, "ui-state-active"),
                    this.options.role && this.element.attr("aria-activedescendant", s.attr("id")),
                    n = this.active.parent().closest(".ui-menu-item").children(".ui-menu-item-wrapper"),
                    this._addClass(n, null, "ui-state-active"),
                    e && "keydown" === e.type ? this._close() : this.timer = this._delay(function() {
                        this._close()
                    }, this.delay),
                    (i = t.children(".ui-menu")).length && e && /^mouse/.test(e.type) && this._startOpening(i),
                    this.activeMenu = t.parent(),
                    this._trigger("focus", e, {
                        item: t
                    })
            },
            _scrollIntoView: function(t) {
                var i, s, n, a, o, r;
                this._hasScroll() && (i = parseFloat(e.css(this.activeMenu[0], "borderTopWidth")) || 0,
                    s = parseFloat(e.css(this.activeMenu[0], "paddingTop")) || 0,
                    n = t.offset().top - this.activeMenu.offset().top - i - s,
                    a = this.activeMenu.scrollTop(),
                    o = this.activeMenu.height(),
                    r = t.outerHeight(),
                    0 > n ? this.activeMenu.scrollTop(a + n) : n + r > o && this.activeMenu.scrollTop(a + n - o + r))
            },
            blur: function(e, t) {
                t || clearTimeout(this.timer),
                    this.active && (this._removeClass(this.active.children(".ui-menu-item-wrapper"), null, "ui-state-active"),
                        this._trigger("blur", e, {
                            item: this.active
                        }),
                        this.active = null)
            },
            _startOpening: function(e) {
                clearTimeout(this.timer),
                    "true" === e.attr("aria-hidden") && (this.timer = this._delay(function() {
                        this._close(),
                            this._open(e)
                    }, this.delay))
            },
            _open: function(t) {
                var i = e.extend({
                    of: this.active
                }, this.options.position);
                clearTimeout(this.timer),
                    this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden", "true"),
                    t.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i)
            },
            collapseAll: function(t, i) {
                clearTimeout(this.timer),
                    this.timer = this._delay(function() {
                        var s = i ? this.element : e(t && t.target).closest(this.element.find(".ui-menu"));
                        s.length || (s = this.element),
                            this._close(s),
                            this.blur(t),
                            this._removeClass(s.find(".ui-state-active"), null, "ui-state-active"),
                            this.activeMenu = s
                    }, this.delay)
            },
            _close: function(e) {
                e || (e = this.active ? this.active.parent() : this.element),
                    e.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false")
            },
            _closeOnDocumentClick: function(t) {
                return !e(t.target).closest(".ui-menu").length
            },
            _isDivider: function(e) {
                return !/[^\-\u2014\u2013\s]/.test(e.text())
            },
            collapse: function(e) {
                var t = this.active && this.active.parent().closest(".ui-menu-item", this.element);
                t && t.length && (this._close(),
                    this.focus(e, t))
            },
            expand: function(e) {
                var t = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
                t && t.length && (this._open(t.parent()),
                    this._delay(function() {
                        this.focus(e, t)
                    }))
            },
            next: function(e) {
                this._move("next", "first", e)
            },
            previous: function(e) {
                this._move("prev", "last", e)
            },
            isFirstItem: function() {
                return this.active && !this.active.prevAll(".ui-menu-item").length
            },
            isLastItem: function() {
                return this.active && !this.active.nextAll(".ui-menu-item").length
            },
            _move: function(e, t, i) {
                var s;
                this.active && (s = "first" === e || "last" === e ? this.active["first" === e ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[e + "All"](".ui-menu-item").eq(0)),
                    s && s.length && this.active || (s = this.activeMenu.find(this.options.items)[t]()),
                    this.focus(i, s)
            },
            nextPage: function(t) {
                var i, s, n;
                return this.active ? void(this.isLastItem() || (this._hasScroll() ? (s = this.active.offset().top,
                    n = this.element.height(),
                    this.active.nextAll(".ui-menu-item").each(function() {
                        return 0 > (i = e(this)).offset().top - s - n
                    }),
                    this.focus(t, i)) : this.focus(t, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]()))) : void this.next(t)
            },
            previousPage: function(t) {
                var i, s, n;
                return this.active ? void(this.isFirstItem() || (this._hasScroll() ? (s = this.active.offset().top,
                    n = this.element.height(),
                    this.active.prevAll(".ui-menu-item").each(function() {
                        return (i = e(this)).offset().top - s + n > 0
                    }),
                    this.focus(t, i)) : this.focus(t, this.activeMenu.find(this.options.items).first()))) : void this.next(t)
            },
            _hasScroll: function() {
                return this.element.outerHeight() < this.element.prop("scrollHeight")
            },
            select: function(t) {
                this.active = this.active || e(t.target).closest(".ui-menu-item");
                var i = {
                    item: this.active
                };
                this.active.has(".ui-menu").length || this.collapseAll(t, !0),
                    this._trigger("select", t, i)
            },
            _filterMenuItems: function(t) {
                var i = t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"),
                    s = RegExp("^" + i, "i");
                return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function() {
                    return s.test(e.trim(e(this).children(".ui-menu-item-wrapper").text()))
                })
            }
        }),
        e.widget("ui.autocomplete", {
            version: "1.12.1",
            defaultElement: "<input>",
            options: {
                appendTo: null,
                autoFocus: !1,
                delay: 300,
                minLength: 1,
                position: {
                    my: "left top",
                    at: "left bottom",
                    collision: "none"
                },
                source: null,
                change: null,
                close: null,
                focus: null,
                open: null,
                response: null,
                search: null,
                select: null
            },
            requestIndex: 0,
            pending: 0,
            _create: function() {
                var t, i, s, n = this.element[0].nodeName.toLowerCase(),
                    a = "textarea" === n,
                    o = "input" === n;
                this.isMultiLine = a || !o && this._isContentEditable(this.element),
                    this.valueMethod = this.element[a || o ? "val" : "text"],
                    this.isNewMenu = !0,
                    this._addClass("ui-autocomplete-input"),
                    this.element.attr("autocomplete", "off"),
                    this._on(this.element, {
                        keydown: function(n) {
                            if (this.element.prop("readOnly"))
                                return t = !0,
                                    s = !0,
                                    void(i = !0);
                            t = !1,
                                s = !1,
                                i = !1;
                            var a = e.ui.keyCode;
                            switch (n.keyCode) {
                                case a.PAGE_UP:
                                    t = !0,
                                        this._move("previousPage", n);
                                    break;
                                case a.PAGE_DOWN:
                                    t = !0,
                                        this._move("nextPage", n);
                                    break;
                                case a.UP:
                                    t = !0,
                                        this._keyEvent("previous", n);
                                    break;
                                case a.DOWN:
                                    t = !0,
                                        this._keyEvent("next", n);
                                    break;
                                case a.ENTER:
                                    this.menu.active && (t = !0,
                                        n.preventDefault(),
                                        this.menu.select(n));
                                    break;
                                case a.TAB:
                                    this.menu.active && this.menu.select(n);
                                    break;
                                case a.ESCAPE:
                                    this.menu.element.is(":visible") && (this.isMultiLine || this._value(this.term),
                                        this.close(n),
                                        n.preventDefault());
                                    break;
                                default:
                                    i = !0,
                                        this._searchTimeout(n)
                            }
                        },
                        keypress: function(s) {
                            if (t)
                                return t = !1,
                                    void((!this.isMultiLine || this.menu.element.is(":visible")) && s.preventDefault());
                            if (!i) {
                                var n = e.ui.keyCode;
                                switch (s.keyCode) {
                                    case n.PAGE_UP:
                                        this._move("previousPage", s);
                                        break;
                                    case n.PAGE_DOWN:
                                        this._move("nextPage", s);
                                        break;
                                    case n.UP:
                                        this._keyEvent("previous", s);
                                        break;
                                    case n.DOWN:
                                        this._keyEvent("next", s)
                                }
                            }
                        },
                        input: function(e) {
                            return s ? (s = !1,
                                void e.preventDefault()) : void this._searchTimeout(e)
                        },
                        focus: function() {
                            this.selectedItem = null,
                                this.previous = this._value()
                        },
                        blur: function(e) {
                            return this.cancelBlur ? void delete this.cancelBlur : (clearTimeout(this.searching),
                                this.close(e),
                                void this._change(e))
                        }
                    }),
                    this._initSource(),
                    this.menu = e("<ul>").appendTo(this._appendTo()).menu({
                        role: null
                    }).hide().menu("instance"),
                    this._addClass(this.menu.element, "ui-autocomplete", "ui-front"),
                    this._on(this.menu.element, {
                        mousedown: function(t) {
                            t.preventDefault(),
                                this.cancelBlur = !0,
                                this._delay(function() {
                                    delete this.cancelBlur,
                                        this.element[0] !== e.ui.safeActiveElement(this.document[0]) && this.element.trigger("focus")
                                })
                        },
                        menufocus: function(t, i) {
                            var s, n;
                            return this.isNewMenu && (this.isNewMenu = !1,
                                t.originalEvent && /^mouse/.test(t.originalEvent.type)) ? (this.menu.blur(),
                                void this.document.one("mousemove", function() {
                                    e(t.target).trigger(t.originalEvent)
                                })) : (n = i.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", t, {
                                    item: n
                                }) && t.originalEvent && /^key/.test(t.originalEvent.type) && this._value(n.value),
                                void((s = i.item.attr("aria-label") || n.value) && e.trim(s).length && (this.liveRegion.children().hide(),
                                    e("<div>").text(s).appendTo(this.liveRegion))))
                        },
                        menuselect: function(t, i) {
                            var s = i.item.data("ui-autocomplete-item"),
                                n = this.previous;
                            this.element[0] !== e.ui.safeActiveElement(this.document[0]) && (this.element.trigger("focus"),
                                    this.previous = n,
                                    this._delay(function() {
                                        this.previous = n,
                                            this.selectedItem = s
                                    })), !1 !== this._trigger("select", t, {
                                    item: s
                                }) && this._value(s.value),
                                this.term = this._value(),
                                this.close(t),
                                this.selectedItem = s
                        }
                    }),
                    this.liveRegion = e("<div>", {
                        role: "status",
                        "aria-live": "assertive",
                        "aria-relevant": "additions"
                    }).appendTo(this.document[0].body),
                    this._addClass(this.liveRegion, null, "ui-helper-hidden-accessible"),
                    this._on(this.window, {
                        beforeunload: function() {
                            this.element.removeAttr("autocomplete")
                        }
                    })
            },
            _destroy: function() {
                clearTimeout(this.searching),
                    this.element.removeAttr("autocomplete"),
                    this.menu.element.remove(),
                    this.liveRegion.remove()
            },
            _setOption: function(e, t) {
                this._super(e, t),
                    "source" === e && this._initSource(),
                    "appendTo" === e && this.menu.element.appendTo(this._appendTo()),
                    "disabled" === e && t && this.xhr && this.xhr.abort()
            },
            _isEventTargetInWidget: function(t) {
                var i = this.menu.element[0];
                return t.target === this.element[0] || t.target === i || e.contains(i, t.target)
            },
            _closeOnClickOutside: function(e) {
                this._isEventTargetInWidget(e) || this.close()
            },
            _appendTo: function() {
                var t = this.options.appendTo;
                return t && (t = t.jquery || t.nodeType ? e(t) : this.document.find(t).eq(0)),
                    t && t[0] || (t = this.element.closest(".ui-front, dialog")),
                    t.length || (t = this.document[0].body),
                    t
            },
            _initSource: function() {
                var t, i, s = this;
                e.isArray(this.options.source) ? (t = this.options.source,
                    this.source = function(i, s) {
                        s(e.ui.autocomplete.filter(t, i.term))
                    }
                ) : "string" == typeof this.options.source ? (i = this.options.source,
                    this.source = function(t, n) {
                        s.xhr && s.xhr.abort(),
                            s.xhr = e.ajax({
                                url: i,
                                data: t,
                                dataType: "json",
                                success: function(e) {
                                    n(e)
                                },
                                error: function() {
                                    n([])
                                }
                            })
                    }
                ) : this.source = this.options.source
            },
            _searchTimeout: function(e) {
                clearTimeout(this.searching),
                    this.searching = this._delay(function() {
                        var t = this.term === this._value(),
                            i = this.menu.element.is(":visible");
                        (!t || t && !i && !(e.altKey || e.ctrlKey || e.metaKey || e.shiftKey)) && (this.selectedItem = null,
                            this.search(null, e))
                    }, this.options.delay)
            },
            search: function(e, t) {
                return e = null != e ? e : this._value(),
                    this.term = this._value(),
                    e.length < this.options.minLength ? this.close(t) : !1 !== this._trigger("search", t) ? this._search(e) : void 0
            },
            _search: function(e) {
                this.pending++,
                    this._addClass("ui-autocomplete-loading"),
                    this.cancelSearch = !1,
                    this.source({
                        term: e
                    }, this._response())
            },
            _response: function() {
                var t = ++this.requestIndex;
                return e.proxy(function(e) {
                    t === this.requestIndex && this.__response(e),
                        this.pending--,
                        this.pending || this._removeClass("ui-autocomplete-loading")
                }, this)
            },
            __response: function(e) {
                e && (e = this._normalize(e)),
                    this._trigger("response", null, {
                        content: e
                    }), !this.options.disabled && e && e.length && !this.cancelSearch ? (this._suggest(e),
                        this._trigger("open")) : this._close()
            },
            close: function(e) {
                this.cancelSearch = !0,
                    this._close(e)
            },
            _close: function(e) {
                this._off(this.document, "mousedown"),
                    this.menu.element.is(":visible") && (this.menu.element.hide(),
                        this.menu.blur(),
                        this.isNewMenu = !0,
                        this._trigger("close", e))
            },
            _change: function(e) {
                this.previous !== this._value() && this._trigger("change", e, {
                    item: this.selectedItem
                })
            },
            _normalize: function(t) {
                return t.length && t[0].label && t[0].value ? t : e.map(t, function(t) {
                    return "string" == typeof t ? {
                        label: t,
                        value: t
                    } : e.extend({}, t, {
                        label: t.label || t.value,
                        value: t.value || t.label
                    })
                })
            },
            _suggest: function(t) {
                var i = this.menu.element.empty();
                this._renderMenu(i, t),
                    this.isNewMenu = !0,
                    this.menu.refresh(),
                    i.show(),
                    this._resizeMenu(),
                    i.position(e.extend({
                        of: this.element
                    }, this.options.position)),
                    this.options.autoFocus && this.menu.next(),
                    this._on(this.document, {
                        mousedown: "_closeOnClickOutside"
                    })
            },
            _resizeMenu: function() {
                var e = this.menu.element;
                e.outerWidth(Math.max(e.width("").outerWidth() + 1, this.element.outerWidth()))
            },
            _renderMenu: function(t, i) {
                var s = this;
                e.each(i, function(e, i) {
                    s._renderItemData(t, i)
                })
            },
            _renderItemData: function(e, t) {
                return this._renderItem(e, t).data("ui-autocomplete-item", t)
            },
            _renderItem: function(t, i) {
                return e("<li>").append(e("<div>").text(i.label)).appendTo(t)
            },
            _move: function(e, t) {
                return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(e) || this.menu.isLastItem() && /^next/.test(e) ? (this.isMultiLine || this._value(this.term),
                    void this.menu.blur()) : void this.menu[e](t) : void this.search(null, t)
            },
            widget: function() {
                return this.menu.element
            },
            _value: function() {
                return this.valueMethod.apply(this.element, arguments)
            },
            _keyEvent: function(e, t) {
                (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(e, t),
                    t.preventDefault())
            },
            _isContentEditable: function(e) {
                if (!e.length)
                    return !1;
                var t = e.prop("contentEditable");
                return "inherit" === t ? this._isContentEditable(e.parent()) : "true" === t
            }
        }),
        e.extend(e.ui.autocomplete, {
            escapeRegex: function(e) {
                return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
            },
            filter: function(t, i) {
                var s = RegExp(e.ui.autocomplete.escapeRegex(i), "i");
                return e.grep(t, function(e) {
                    return s.test(e.label || e.value || e)
                })
            }
        }),
        e.widget("ui.autocomplete", e.ui.autocomplete, {
            options: {
                messages: {
                    noResults: "No search results.",
                    results: function(e) {
                        return e + (e > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                    }
                }
            },
            __response: function(t) {
                var i;
                this._superApply(arguments),
                    this.options.disabled || this.cancelSearch || (i = t && t.length ? this.options.messages.results(t.length) : this.options.messages.noResults,
                        this.liveRegion.children().hide(),
                        e("<div>").text(i).appendTo(this.liveRegion))
            }
        });
    var d, p = /ui-corner-([a-z]){2,6}/g;
    e.widget("ui.controlgroup", {
            version: "1.12.1",
            defaultElement: "<div>",
            options: {
                direction: "horizontal",
                disabled: null,
                onlyVisible: !0,
                items: {
                    button: "input[type=button], input[type=submit], input[type=reset], button, a",
                    controlgroupLabel: ".ui-controlgroup-label",
                    checkboxradio: "input[type='checkbox'], input[type='radio']",
                    selectmenu: "select",
                    spinner: ".ui-spinner-input"
                }
            },
            _create: function() {
                this._enhance()
            },
            _enhance: function() {
                this.element.attr("role", "toolbar"),
                    this.refresh()
            },
            _destroy: function() {
                this._callChildMethod("destroy"),
                    this.childWidgets.removeData("ui-controlgroup-data"),
                    this.element.removeAttr("role"),
                    this.options.items.controlgroupLabel && this.element.find(this.options.items.controlgroupLabel).find(".ui-controlgroup-label-contents").contents().unwrap()
            },
            _initWidgets: function() {
                var t = this,
                    i = [];
                e.each(this.options.items, function(s, n) {
                        var a, o = {};
                        return n ? "controlgroupLabel" === s ? ((a = t.element.find(n)).each(function() {
                                var t = e(this);
                                t.children(".ui-controlgroup-label-contents").length || t.contents().wrapAll("<span class='ui-controlgroup-label-contents'></span>")
                            }),
                            t._addClass(a, null, "ui-widget ui-widget-content ui-state-default"),
                            void(i = i.concat(a.get()))) : void(e.fn[s] && (o = t["_" + s + "Options"] ? t["_" + s + "Options"]("middle") : {
                                classes: {}
                            },
                            t.element.find(n).each(function() {
                                var n = e(this),
                                    a = n[s]("instance"),
                                    r = e.widget.extend({}, o);
                                if ("button" !== s || !n.parent(".ui-spinner").length) {
                                    a || (a = n[s]()[s]("instance")),
                                        a && (r.classes = t._resolveClassesValues(r.classes, a)),
                                        n[s](r);
                                    var l = n[s]("widget");
                                    e.data(l[0], "ui-controlgroup-data", a || n[s]("instance")),
                                        i.push(l[0])
                                }
                            }))) : void 0
                    }),
                    this.childWidgets = e(e.unique(i)),
                    this._addClass(this.childWidgets, "ui-controlgroup-item")
            },
            _callChildMethod: function(t) {
                this.childWidgets.each(function() {
                    var i = e(this).data("ui-controlgroup-data");
                    i && i[t] && i[t]()
                })
            },
            _updateCornerClass: function(e, t) {
                var i = this._buildSimpleOptions(t, "label").classes.label;
                this._removeClass(e, null, "ui-corner-top ui-corner-bottom ui-corner-left ui-corner-right ui-corner-all"),
                    this._addClass(e, null, i)
            },
            _buildSimpleOptions: function(e, t) {
                var i = "vertical" === this.options.direction,
                    s = {
                        classes: {}
                    };
                return s.classes[t] = {
                        middle: "",
                        first: "ui-corner-" + (i ? "top" : "left"),
                        last: "ui-corner-" + (i ? "bottom" : "right"),
                        only: "ui-corner-all"
                    }[e],
                    s
            },
            _spinnerOptions: function(e) {
                var t = this._buildSimpleOptions(e, "ui-spinner");
                return t.classes["ui-spinner-up"] = "",
                    t.classes["ui-spinner-down"] = "",
                    t
            },
            _buttonOptions: function(e) {
                return this._buildSimpleOptions(e, "ui-button")
            },
            _checkboxradioOptions: function(e) {
                return this._buildSimpleOptions(e, "ui-checkboxradio-label")
            },
            _selectmenuOptions: function(e) {
                var t = "vertical" === this.options.direction;
                return {
                    width: !!t && "auto",
                    classes: {
                        middle: {
                            "ui-selectmenu-button-open": "",
                            "ui-selectmenu-button-closed": ""
                        },
                        first: {
                            "ui-selectmenu-button-open": "ui-corner-" + (t ? "top" : "tl"),
                            "ui-selectmenu-button-closed": "ui-corner-" + (t ? "top" : "left")
                        },
                        last: {
                            "ui-selectmenu-button-open": t ? "" : "ui-corner-tr",
                            "ui-selectmenu-button-closed": "ui-corner-" + (t ? "bottom" : "right")
                        },
                        only: {
                            "ui-selectmenu-button-open": "ui-corner-top",
                            "ui-selectmenu-button-closed": "ui-corner-all"
                        }
                    }[e]
                }
            },
            _resolveClassesValues: function(t, i) {
                var s = {};
                return e.each(t, function(n) {
                        var a = i.options.classes[n] || "";
                        a = e.trim(a.replace(p, "")),
                            s[n] = (a + " " + t[n]).replace(/\s+/g, " ")
                    }),
                    s
            },
            _setOption: function(e, t) {
                return "direction" === e && this._removeClass("ui-controlgroup-" + this.options.direction),
                    this._super(e, t),
                    "disabled" === e ? void this._callChildMethod(t ? "disable" : "enable") : void this.refresh()
            },
            refresh: function() {
                var t, i = this;
                this._addClass("ui-controlgroup ui-controlgroup-" + this.options.direction),
                    "horizontal" === this.options.direction && this._addClass(null, "ui-helper-clearfix"),
                    this._initWidgets(),
                    t = this.childWidgets,
                    this.options.onlyVisible && (t = t.filter(":visible")),
                    t.length && (e.each(["first", "last"], function(e, s) {
                            var n = t[s]().data("ui-controlgroup-data");
                            if (n && i["_" + n.widgetName + "Options"]) {
                                var a = i["_" + n.widgetName + "Options"](1 === t.length ? "only" : s);
                                a.classes = i._resolveClassesValues(a.classes, n),
                                    n.element[n.widgetName](a)
                            } else
                                i._updateCornerClass(t[s](), s)
                        }),
                        this._callChildMethod("refresh"))
            }
        }),
        e.widget("ui.checkboxradio", [e.ui.formResetMixin, {
            version: "1.12.1",
            options: {
                disabled: null,
                label: null,
                icon: !0,
                classes: {
                    "ui-checkboxradio-label": "ui-corner-all",
                    "ui-checkboxradio-icon": "ui-corner-all"
                }
            },
            _getCreateOptions: function() {
                var t, i, s = this,
                    n = this._super() || {};
                return this._readType(),
                    i = this.element.labels(),
                    this.label = e(i[i.length - 1]),
                    this.label.length || e.error("No label found for checkboxradio widget"),
                    this.originalLabel = "",
                    this.label.contents().not(this.element[0]).each(function() {
                        s.originalLabel += 3 === this.nodeType ? e(this).text() : this.outerHTML
                    }),
                    this.originalLabel && (n.label = this.originalLabel),
                    null != (t = this.element[0].disabled) && (n.disabled = t),
                    n
            },
            _create: function() {
                var e = this.element[0].checked;
                this._bindFormResetHandler(),
                    null == this.options.disabled && (this.options.disabled = this.element[0].disabled),
                    this._setOption("disabled", this.options.disabled),
                    this._addClass("ui-checkboxradio", "ui-helper-hidden-accessible"),
                    this._addClass(this.label, "ui-checkboxradio-label", "ui-button ui-widget"),
                    "radio" === this.type && this._addClass(this.label, "ui-checkboxradio-radio-label"),
                    this.options.label && this.options.label !== this.originalLabel ? this._updateLabel() : this.originalLabel && (this.options.label = this.originalLabel),
                    this._enhance(),
                    e && (this._addClass(this.label, "ui-checkboxradio-checked", "ui-state-active"),
                        this.icon && this._addClass(this.icon, null, "ui-state-hover")),
                    this._on({
                        change: "_toggleClasses",
                        focus: function() {
                            this._addClass(this.label, null, "ui-state-focus ui-visual-focus")
                        },
                        blur: function() {
                            this._removeClass(this.label, null, "ui-state-focus ui-visual-focus")
                        }
                    })
            },
            _readType: function() {
                var t = this.element[0].nodeName.toLowerCase();
                this.type = this.element[0].type,
                    "input" === t && /radio|checkbox/.test(this.type) || e.error("Can't create checkboxradio on element.nodeName=" + t + " and element.type=" + this.type)
            },
            _enhance: function() {
                this._updateIcon(this.element[0].checked)
            },
            widget: function() {
                return this.label
            },
            _getRadioGroup: function() {
                var t = this.element[0].name,
                    i = "input[name='" + e.ui.escapeSelector(t) + "']";
                return t ? (this.form.length ? e(this.form[0].elements).filter(i) : e(i).filter(function() {
                    return 0 === e(this).form().length
                })).not(this.element) : e([])
            },
            _toggleClasses: function() {
                var t = this.element[0].checked;
                this._toggleClass(this.label, "ui-checkboxradio-checked", "ui-state-active", t),
                    this.options.icon && "checkbox" === this.type && this._toggleClass(this.icon, null, "ui-icon-check ui-state-checked", t)._toggleClass(this.icon, null, "ui-icon-blank", !t),
                    "radio" === this.type && this._getRadioGroup().each(function() {
                        var t = e(this).checkboxradio("instance");
                        t && t._removeClass(t.label, "ui-checkboxradio-checked", "ui-state-active")
                    })
            },
            _destroy: function() {
                this._unbindFormResetHandler(),
                    this.icon && (this.icon.remove(),
                        this.iconSpace.remove())
            },
            _setOption: function(e, t) {
                return "label" !== e || t ? (this._super(e, t),
                    "disabled" === e ? (this._toggleClass(this.label, null, "ui-state-disabled", t),
                        void(this.element[0].disabled = t)) : void this.refresh()) : void 0
            },
            _updateIcon: function(t) {
                var i = "ui-icon ui-icon-background ";
                this.options.icon ? (this.icon || (this.icon = e("<span>"),
                        this.iconSpace = e("<span> </span>"),
                        this._addClass(this.iconSpace, "ui-checkboxradio-icon-space")),
                    "checkbox" === this.type ? (i += t ? "ui-icon-check ui-state-checked" : "ui-icon-blank",
                        this._removeClass(this.icon, null, t ? "ui-icon-blank" : "ui-icon-check")) : i += "ui-icon-blank",
                    this._addClass(this.icon, "ui-checkboxradio-icon", i),
                    t || this._removeClass(this.icon, null, "ui-icon-check ui-state-checked"),
                    this.icon.prependTo(this.label).after(this.iconSpace)) : void 0 !== this.icon && (this.icon.remove(),
                    this.iconSpace.remove(),
                    delete this.icon)
            },
            _updateLabel: function() {
                var e = this.label.contents().not(this.element[0]);
                this.icon && (e = e.not(this.icon[0])),
                    this.iconSpace && (e = e.not(this.iconSpace[0])),
                    e.remove(),
                    this.label.append(this.options.label)
            },
            refresh: function() {
                var e = this.element[0].checked,
                    t = this.element[0].disabled;
                this._updateIcon(e),
                    this._toggleClass(this.label, "ui-checkboxradio-checked", "ui-state-active", e),
                    null !== this.options.label && this._updateLabel(),
                    t !== this.options.disabled && this._setOptions({
                        disabled: t
                    })
            }
        }]),
        e.widget("ui.button", {
            version: "1.12.1",
            defaultElement: "<button>",
            options: {
                classes: {
                    "ui-button": "ui-corner-all"
                },
                disabled: null,
                icon: null,
                iconPosition: "beginning",
                label: null,
                showLabel: !0
            },
            _getCreateOptions: function() {
                var e, t = this._super() || {};
                return this.isInput = this.element.is("input"),
                    null != (e = this.element[0].disabled) && (t.disabled = e),
                    this.originalLabel = this.isInput ? this.element.val() : this.element.html(),
                    this.originalLabel && (t.label = this.originalLabel),
                    t
            },
            _create: function() {
                !this.option.showLabel & !this.options.icon && (this.options.showLabel = !0),
                    null == this.options.disabled && (this.options.disabled = this.element[0].disabled || !1),
                    this.hasTitle = !!this.element.attr("title"),
                    this.options.label && this.options.label !== this.originalLabel && (this.isInput ? this.element.val(this.options.label) : this.element.html(this.options.label)),
                    this._addClass("ui-button", "ui-widget"),
                    this._setOption("disabled", this.options.disabled),
                    this._enhance(),
                    this.element.is("a") && this._on({
                        keyup: function(t) {
                            t.keyCode === e.ui.keyCode.SPACE && (t.preventDefault(),
                                this.element[0].click ? this.element[0].click() : this.element.trigger("click"))
                        }
                    })
            },
            _enhance: function() {
                this.element.is("button") || this.element.attr("role", "button"),
                    this.options.icon && (this._updateIcon("icon", this.options.icon),
                        this._updateTooltip())
            },
            _updateTooltip: function() {
                this.title = this.element.attr("title"),
                    this.options.showLabel || this.title || this.element.attr("title", this.options.label)
            },
            _updateIcon: function(t, i) {
                var s = "iconPosition" !== t,
                    n = s ? this.options.iconPosition : i,
                    a = "top" === n || "bottom" === n;
                this.icon ? s && this._removeClass(this.icon, null, this.options.icon) : (this.icon = e("<span>"),
                        this._addClass(this.icon, "ui-button-icon", "ui-icon"),
                        this.options.showLabel || this._addClass("ui-button-icon-only")),
                    s && this._addClass(this.icon, null, i),
                    this._attachIcon(n),
                    a ? (this._addClass(this.icon, null, "ui-widget-icon-block"),
                        this.iconSpace && this.iconSpace.remove()) : (this.iconSpace || (this.iconSpace = e("<span> </span>"),
                            this._addClass(this.iconSpace, "ui-button-icon-space")),
                        this._removeClass(this.icon, null, "ui-wiget-icon-block"),
                        this._attachIconSpace(n))
            },
            _destroy: function() {
                this.element.removeAttr("role"),
                    this.icon && this.icon.remove(),
                    this.iconSpace && this.iconSpace.remove(),
                    this.hasTitle || this.element.removeAttr("title")
            },
            _attachIconSpace: function(e) {
                this.icon[/^(?:end|bottom)/.test(e) ? "before" : "after"](this.iconSpace)
            },
            _attachIcon: function(e) {
                this.element[/^(?:end|bottom)/.test(e) ? "append" : "prepend"](this.icon)
            },
            _setOptions: function(e) {
                (void 0 === e.showLabel ? this.options.showLabel : e.showLabel) || (void 0 === e.icon ? this.options.icon : e.icon) || (e.showLabel = !0),
                this._super(e)
            },
            _setOption: function(e, t) {
                "icon" === e && (t ? this._updateIcon(e, t) : this.icon && (this.icon.remove(),
                        this.iconSpace && this.iconSpace.remove())),
                    "iconPosition" === e && this._updateIcon(e, t),
                    "showLabel" === e && (this._toggleClass("ui-button-icon-only", null, !t),
                        this._updateTooltip()),
                    "label" === e && (this.isInput ? this.element.val(t) : (this.element.html(t),
                        this.icon && (this._attachIcon(this.options.iconPosition),
                            this._attachIconSpace(this.options.iconPosition)))),
                    this._super(e, t),
                    "disabled" === e && (this._toggleClass(null, "ui-state-disabled", t),
                        this.element[0].disabled = t,
                        t && this.element.blur())
            },
            refresh: function() {
                var e = this.element.is("input, button") ? this.element[0].disabled : this.element.hasClass("ui-button-disabled");
                e !== this.options.disabled && this._setOptions({
                        disabled: e
                    }),
                    this._updateTooltip()
            }
        }), !1 !== e.uiBackCompat && (e.widget("ui.button", e.ui.button, {
                options: {
                    text: !0,
                    icons: {
                        primary: null,
                        secondary: null
                    }
                },
                _create: function() {
                    this.options.showLabel && !this.options.text && (this.options.showLabel = this.options.text), !this.options.showLabel && this.options.text && (this.options.text = this.options.showLabel),
                        this.options.icon || !this.options.icons.primary && !this.options.icons.secondary ? this.options.icon && (this.options.icons.primary = this.options.icon) : this.options.icons.primary ? this.options.icon = this.options.icons.primary : (this.options.icon = this.options.icons.secondary,
                            this.options.iconPosition = "end"),
                        this._super()
                },
                _setOption: function(e, t) {
                    return "text" === e ? void this._super("showLabel", t) : ("showLabel" === e && (this.options.text = t),
                        "icon" === e && (this.options.icons.primary = t),
                        "icons" === e && (t.primary ? (this._super("icon", t.primary),
                            this._super("iconPosition", "beginning")) : t.secondary && (this._super("icon", t.secondary),
                            this._super("iconPosition", "end"))),
                        void this._superApply(arguments))
                }
            }),
            e.fn.button = function(t) {
                return function() {
                    return !this.length || this.length && "INPUT" !== this[0].tagName || this.length && "INPUT" === this[0].tagName && "checkbox" !== this.attr("type") && "radio" !== this.attr("type") ? t.apply(this, arguments) : (e.ui.checkboxradio || e.error("Checkboxradio widget missing"),
                        0 === arguments.length ? this.checkboxradio({
                            icon: !1
                        }) : this.checkboxradio.apply(this, arguments))
                }
            }(e.fn.button),
            e.fn.buttonset = function() {
                return e.ui.controlgroup || e.error("Controlgroup widget missing"),
                    "option" === arguments[0] && "items" === arguments[1] && arguments[2] ? this.controlgroup.apply(this, [arguments[0], "items.button", arguments[2]]) : "option" === arguments[0] && "items" === arguments[1] ? this.controlgroup.apply(this, [arguments[0], "items.button"]) : ("object" == typeof arguments[0] && arguments[0].items && (arguments[0].items = {
                            button: arguments[0].items
                        }),
                        this.controlgroup.apply(this, arguments))
            }
        ),
        e.extend(e.ui, {
            datepicker: {
                version: "1.12.1"
            }
        }),
        e.extend(t.prototype, {
            markerClassName: "hasDatepicker",
            maxRows: 4,
            _widgetDatepicker: function() {
                return this.dpDiv
            },
            setDefaults: function(e) {
                return n(this._defaults, e || {}),
                    this
            },
            _attachDatepicker: function(t, i) {
                var s, n, a;
                n = "div" === (s = t.nodeName.toLowerCase()) || "span" === s,
                    t.id || (this.uuid += 1,
                        t.id = "dp" + this.uuid),
                    (a = this._newInst(e(t), n)).settings = e.extend({}, i || {}),
                    "input" === s ? this._connectDatepicker(t, a) : n && this._inlineDatepicker(t, a)
            },
            _newInst: function(t, s) {
                return {
                    id: t[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"),
                    input: t,
                    selectedDay: 0,
                    selectedMonth: 0,
                    selectedYear: 0,
                    drawMonth: 0,
                    drawYear: 0,
                    inline: s,
                    dpDiv: s ? i(e("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
                }
            },
            _connectDatepicker: function(t, i) {
                var s = e(t);
                i.append = e([]),
                    i.trigger = e([]),
                    s.hasClass(this.markerClassName) || (this._attachments(s, i),
                        s.addClass(this.markerClassName).on("keydown", this._doKeyDown).on("keypress", this._doKeyPress).on("keyup", this._doKeyUp),
                        this._autoSize(i),
                        e.data(t, "datepicker", i),
                        i.settings.disabled && this._disableDatepicker(t))
            },
            _attachments: function(t, i) {
                var s, n, a, o = this._get(i, "appendText"),
                    r = this._get(i, "isRTL");
                i.append && i.append.remove(),
                    o && (i.append = e("<span class='" + this._appendClass + "'>" + o + "</span>"),
                        t[r ? "before" : "after"](i.append)),
                    t.off("focus", this._showDatepicker),
                    i.trigger && i.trigger.remove(),
                    ("focus" === (s = this._get(i, "showOn")) || "both" === s) && t.on("focus", this._showDatepicker),
                    ("button" === s || "both" === s) && (n = this._get(i, "buttonText"),
                        a = this._get(i, "buttonImage"),
                        i.trigger = e(this._get(i, "buttonImageOnly") ? e("<img/>").addClass(this._triggerClass).attr({
                            src: a,
                            alt: n,
                            title: n
                        }) : e("<button type='button'></button>").addClass(this._triggerClass).html(a ? e("<img/>").attr({
                            src: a,
                            alt: n,
                            title: n
                        }) : n)),
                        t[r ? "before" : "after"](i.trigger),
                        i.trigger.on("click", function() {
                            return e.datepicker._datepickerShowing && e.datepicker._lastInput === t[0] ? e.datepicker._hideDatepicker() : e.datepicker._datepickerShowing && e.datepicker._lastInput !== t[0] ? (e.datepicker._hideDatepicker(),
                                e.datepicker._showDatepicker(t[0])) : e.datepicker._showDatepicker(t[0]), !1
                        }))
            },
            _autoSize: function(e) {
                if (this._get(e, "autoSize") && !e.inline) {
                    var t, i, s, n, a = new Date(2009, 11, 20),
                        o = this._get(e, "dateFormat");
                    o.match(/[DM]/) && (a.setMonth((t = function(e) {
                                for (i = 0,
                                    s = 0,
                                    n = 0; e.length > n; n++)
                                    e[n].length > i && (i = e[n].length,
                                        s = n);
                                return s
                            })(this._get(e, o.match(/MM/) ? "monthNames" : "monthNamesShort"))),
                            a.setDate(t(this._get(e, o.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - a.getDay())),
                        e.input.attr("size", this._formatDate(e, a).length)
                }
            },
            _inlineDatepicker: function(t, i) {
                var s = e(t);
                s.hasClass(this.markerClassName) || (s.addClass(this.markerClassName).append(i.dpDiv),
                    e.data(t, "datepicker", i),
                    this._setDate(i, this._getDefaultDate(i), !0),
                    this._updateDatepicker(i),
                    this._updateAlternate(i),
                    i.settings.disabled && this._disableDatepicker(t),
                    i.dpDiv.css("display", "block"))
            },
            _dialogDatepicker: function(t, i, s, a, o) {
                var r, l, h, u, c = this._dialogInst;
                return c || (this.uuid += 1,
                        this._dialogInput = e("<input type='text' id='dp" + this.uuid + "' style='position: absolute; top: -100px; width: 0px;'/>"),
                        this._dialogInput.on("keydown", this._doKeyDown),
                        e("body").append(this._dialogInput),
                        (c = this._dialogInst = this._newInst(this._dialogInput, !1)).settings = {},
                        e.data(this._dialogInput[0], "datepicker", c)),
                    n(c.settings, a || {}),
                    i = i && i.constructor === Date ? this._formatDate(c, i) : i,
                    this._dialogInput.val(i),
                    this._pos = o ? o.length ? o : [o.pageX, o.pageY] : null,
                    this._pos || (r = document.documentElement.clientWidth,
                        l = document.documentElement.clientHeight,
                        h = document.documentElement.scrollLeft || document.body.scrollLeft,
                        u = document.documentElement.scrollTop || document.body.scrollTop,
                        this._pos = [r / 2 - 100 + h, l / 2 - 150 + u]),
                    this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"),
                    c.settings.onSelect = s,
                    this._inDialog = !0,
                    this.dpDiv.addClass(this._dialogClass),
                    this._showDatepicker(this._dialogInput[0]),
                    e.blockUI && e.blockUI(this.dpDiv),
                    e.data(this._dialogInput[0], "datepicker", c),
                    this
            },
            _destroyDatepicker: function(t) {
                var i, s = e(t),
                    n = e.data(t, "datepicker");
                s.hasClass(this.markerClassName) && (i = t.nodeName.toLowerCase(),
                    e.removeData(t, "datepicker"),
                    "input" === i ? (n.append.remove(),
                        n.trigger.remove(),
                        s.removeClass(this.markerClassName).off("focus", this._showDatepicker).off("keydown", this._doKeyDown).off("keypress", this._doKeyPress).off("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && s.removeClass(this.markerClassName).empty(),
                    d === n && (d = null))
            },
            _enableDatepicker: function(t) {
                var i, s, n = e(t),
                    a = e.data(t, "datepicker");
                n.hasClass(this.markerClassName) && ("input" === (i = t.nodeName.toLowerCase()) ? (t.disabled = !1,
                        a.trigger.filter("button").each(function() {
                            this.disabled = !1
                        }).end().filter("img").css({
                            opacity: "1.0",
                            cursor: ""
                        })) : ("div" === i || "span" === i) && ((s = n.children("." + this._inlineClass)).children().removeClass("ui-state-disabled"),
                        s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)),
                    this._disabledInputs = e.map(this._disabledInputs, function(e) {
                        return e === t ? null : e
                    }))
            },
            _disableDatepicker: function(t) {
                var i, s, n = e(t),
                    a = e.data(t, "datepicker");
                n.hasClass(this.markerClassName) && ("input" === (i = t.nodeName.toLowerCase()) ? (t.disabled = !0,
                        a.trigger.filter("button").each(function() {
                            this.disabled = !0
                        }).end().filter("img").css({
                            opacity: "0.5",
                            cursor: "default"
                        })) : ("div" === i || "span" === i) && ((s = n.children("." + this._inlineClass)).children().addClass("ui-state-disabled"),
                        s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)),
                    this._disabledInputs = e.map(this._disabledInputs, function(e) {
                        return e === t ? null : e
                    }),
                    this._disabledInputs[this._disabledInputs.length] = t)
            },
            _isDisabledDatepicker: function(e) {
                if (!e)
                    return !1;
                for (var t = 0; this._disabledInputs.length > t; t++)
                    if (this._disabledInputs[t] === e)
                        return !0;
                return !1
            },
            _getInst: function(t) {
                try {
                    return e.data(t, "datepicker")
                } catch (i) {
                    throw "Missing instance data for this datepicker"
                }
            },
            _optionDatepicker: function(t, i, s) {
                var a, o, r, l, h = this._getInst(t);
                return 2 === arguments.length && "string" == typeof i ? "defaults" === i ? e.extend({}, e.datepicker._defaults) : h ? "all" === i ? e.extend({}, h.settings) : this._get(h, i) : null : (a = i || {},
                    "string" == typeof i && ((a = {})[i] = s),
                    void(h && (this._curInst === h && this._hideDatepicker(),
                        o = this._getDateDatepicker(t, !0),
                        r = this._getMinMaxDate(h, "min"),
                        l = this._getMinMaxDate(h, "max"),
                        n(h.settings, a),
                        null !== r && void 0 !== a.dateFormat && void 0 === a.minDate && (h.settings.minDate = this._formatDate(h, r)),
                        null !== l && void 0 !== a.dateFormat && void 0 === a.maxDate && (h.settings.maxDate = this._formatDate(h, l)),
                        "disabled" in a && (a.disabled ? this._disableDatepicker(t) : this._enableDatepicker(t)),
                        this._attachments(e(t), h),
                        this._autoSize(h),
                        this._setDate(h, o),
                        this._updateAlternate(h),
                        this._updateDatepicker(h))))
            },
            _changeDatepicker: function(e, t, i) {
                this._optionDatepicker(e, t, i)
            },
            _refreshDatepicker: function(e) {
                var t = this._getInst(e);
                t && this._updateDatepicker(t)
            },
            _setDateDatepicker: function(e, t) {
                var i = this._getInst(e);
                i && (this._setDate(i, t),
                    this._updateDatepicker(i),
                    this._updateAlternate(i))
            },
            _getDateDatepicker: function(e, t) {
                var i = this._getInst(e);
                return i && !i.inline && this._setDateFromField(i, t),
                    i ? this._getDate(i) : null
            },
            _doKeyDown: function(t) {
                var i, s, n, a = e.datepicker._getInst(t.target),
                    o = !0,
                    r = a.dpDiv.is(".ui-datepicker-rtl");
                if (a._keyEvent = !0,
                    e.datepicker._datepickerShowing)
                    switch (t.keyCode) {
                        case 9:
                            e.datepicker._hideDatepicker(),
                                o = !1;
                            break;
                        case 13:
                            return (n = e("td." + e.datepicker._dayOverClass + ":not(." + e.datepicker._currentClass + ")", a.dpDiv))[0] && e.datepicker._selectDay(t.target, a.selectedMonth, a.selectedYear, n[0]),
                                (i = e.datepicker._get(a, "onSelect")) ? (s = e.datepicker._formatDate(a),
                                    i.apply(a.input ? a.input[0] : null, [s, a])) : e.datepicker._hideDatepicker(), !1;
                        case 27:
                            e.datepicker._hideDatepicker();
                            break;
                        case 33:
                            e.datepicker._adjustDate(t.target, t.ctrlKey ? -e.datepicker._get(a, "stepBigMonths") : -e.datepicker._get(a, "stepMonths"), "M");
                            break;
                        case 34:
                            e.datepicker._adjustDate(t.target, t.ctrlKey ? +e.datepicker._get(a, "stepBigMonths") : +e.datepicker._get(a, "stepMonths"), "M");
                            break;
                        case 35:
                            (t.ctrlKey || t.metaKey) && e.datepicker._clearDate(t.target),
                                o = t.ctrlKey || t.metaKey;
                            break;
                        case 36:
                            (t.ctrlKey || t.metaKey) && e.datepicker._gotoToday(t.target),
                                o = t.ctrlKey || t.metaKey;
                            break;
                        case 37:
                            (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, r ? 1 : -1, "D"),
                                o = t.ctrlKey || t.metaKey,
                                t.originalEvent.altKey && e.datepicker._adjustDate(t.target, t.ctrlKey ? -e.datepicker._get(a, "stepBigMonths") : -e.datepicker._get(a, "stepMonths"), "M");
                            break;
                        case 38:
                            (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, -7, "D"),
                                o = t.ctrlKey || t.metaKey;
                            break;
                        case 39:
                            (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, r ? -1 : 1, "D"),
                                o = t.ctrlKey || t.metaKey,
                                t.originalEvent.altKey && e.datepicker._adjustDate(t.target, t.ctrlKey ? +e.datepicker._get(a, "stepBigMonths") : +e.datepicker._get(a, "stepMonths"), "M");
                            break;
                        case 40:
                            (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, 7, "D"),
                                o = t.ctrlKey || t.metaKey;
                            break;
                        default:
                            o = !1
                    }
                else
                    36 === t.keyCode && t.ctrlKey ? e.datepicker._showDatepicker(this) : o = !1;
                o && (t.preventDefault(),
                    t.stopPropagation())
            },
            _doKeyPress: function(t) {
                var i, s, n = e.datepicker._getInst(t.target);
                return e.datepicker._get(n, "constrainInput") ? (i = e.datepicker._possibleChars(e.datepicker._get(n, "dateFormat")),
                    s = String.fromCharCode(null == t.charCode ? t.keyCode : t.charCode),
                    t.ctrlKey || t.metaKey || " " > s || !i || i.indexOf(s) > -1) : void 0
            },
            _doKeyUp: function(t) {
                var s = e.datepicker._getInst(t.target);
                if (s.input.val() !== s.lastVal)
                    try {
                        e.datepicker.parseDate(e.datepicker._get(s, "dateFormat"), s.input ? s.input.val() : null, e.datepicker._getFormatConfig(s)) && (e.datepicker._setDateFromField(s),
                            e.datepicker._updateAlternate(s),
                            e.datepicker._updateDatepicker(s))
                    } catch (i) {}
                return !0
            },
            _showDatepicker: function(t) {
                var i, s, a, o, r, l, h;
                "input" !== (t = t.target || t).nodeName.toLowerCase() && (t = e("input", t.parentNode)[0]),
                    e.datepicker._isDisabledDatepicker(t) || e.datepicker._lastInput === t || (i = e.datepicker._getInst(t),
                        e.datepicker._curInst && e.datepicker._curInst !== i && (e.datepicker._curInst.dpDiv.stop(!0, !0),
                            i && e.datepicker._datepickerShowing && e.datepicker._hideDatepicker(e.datepicker._curInst.input[0])), !1 !== (a = (s = e.datepicker._get(i, "beforeShow")) ? s.apply(t, [t, i]) : {}) && (n(i.settings, a),
                            i.lastVal = null,
                            e.datepicker._lastInput = t,
                            e.datepicker._setDateFromField(i),
                            e.datepicker._inDialog && (t.value = ""),
                            e.datepicker._pos || (e.datepicker._pos = e.datepicker._findPos(t),
                                e.datepicker._pos[1] += t.offsetHeight),
                            o = !1,
                            e(t).parents().each(function() {
                                return !(o |= "fixed" === e(this).css("position"))
                            }),
                            r = {
                                left: e.datepicker._pos[0],
                                top: e.datepicker._pos[1]
                            },
                            e.datepicker._pos = null,
                            i.dpDiv.empty(),
                            i.dpDiv.css({
                                position: "absolute",
                                display: "block",
                                top: "-1000px"
                            }),
                            e.datepicker._updateDatepicker(i),
                            r = e.datepicker._checkOffset(i, r, o),
                            i.dpDiv.css({
                                position: e.datepicker._inDialog && e.blockUI ? "static" : o ? "fixed" : "absolute",
                                display: "none",
                                left: r.left + "px",
                                top: r.top + "px"
                            }),
                            i.inline || (l = e.datepicker._get(i, "showAnim"),
                                h = e.datepicker._get(i, "duration"),
                                i.dpDiv.css("z-index", function(e) {
                                    for (var t, i; e.length && e[0] !== document;) {
                                        if (("absolute" === (t = e.css("position")) || "relative" === t || "fixed" === t) && (i = parseInt(e.css("zIndex"), 10), !isNaN(i) && 0 !== i))
                                            return i;
                                        e = e.parent()
                                    }
                                    return 0
                                }(e(t)) + 1),
                                e.datepicker._datepickerShowing = !0,
                                e.effects && e.effects.effect[l] ? i.dpDiv.show(l, e.datepicker._get(i, "showOptions"), h) : i.dpDiv[l || "show"](l ? h : null),
                                e.datepicker._shouldFocusInput(i) && i.input.trigger("focus"),
                                e.datepicker._curInst = i)))
            },
            _updateDatepicker: function(t) {
                this.maxRows = 4,
                    d = t,
                    t.dpDiv.empty().append(this._generateHTML(t)),
                    this._attachHandlers(t);
                var i, n = this._getNumberOfMonths(t),
                    a = n[1],
                    o = t.dpDiv.find("." + this._dayOverClass + " a");
                o.length > 0 && s.apply(o.get(0)),
                    t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),
                    a > 1 && t.dpDiv.addClass("ui-datepicker-multi-" + a).css("width", 17 * a + "em"),
                    t.dpDiv[(1 !== n[0] || 1 !== n[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"),
                    t.dpDiv[(this._get(t, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"),
                    t === e.datepicker._curInst && e.datepicker._datepickerShowing && e.datepicker._shouldFocusInput(t) && t.input.trigger("focus"),
                    t.yearshtml && (i = t.yearshtml,
                        setTimeout(function() {
                            i === t.yearshtml && t.yearshtml && t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml),
                                i = t.yearshtml = null
                        }, 0))
            },
            _shouldFocusInput: function(e) {
                return e.input && e.input.is(":visible") && !e.input.is(":disabled") && !e.input.is(":focus")
            },
            _checkOffset: function(t, i, s) {
                var n = t.dpDiv.outerWidth(),
                    a = t.dpDiv.outerHeight(),
                    o = t.input ? t.input.outerWidth() : 0,
                    r = t.input ? t.input.outerHeight() : 0,
                    l = document.documentElement.clientWidth + (s ? 0 : e(document).scrollLeft()),
                    h = document.documentElement.clientHeight + (s ? 0 : e(document).scrollTop());
                return i.left -= this._get(t, "isRTL") ? n - o : 0,
                    i.left -= s && i.left === t.input.offset().left ? e(document).scrollLeft() : 0,
                    i.top -= s && i.top === t.input.offset().top + r ? e(document).scrollTop() : 0,
                    i.left -= Math.min(i.left, i.left + n > l && l > n ? Math.abs(i.left + n - l) : 0),
                    i.top -= Math.min(i.top, i.top + a > h && h > a ? Math.abs(a + r) : 0),
                    i
            },
            _findPos: function(t) {
                for (var i, s = this._getInst(t), n = this._get(s, "isRTL"); t && ("hidden" === t.type || 1 !== t.nodeType || e.expr.filters.hidden(t));)
                    t = t[n ? "previousSibling" : "nextSibling"];
                return [(i = e(t).offset()).left, i.top]
            },
            _hideDatepicker: function(t) {
                var i, s, n, a, o = this._curInst;
                !o || t && o !== e.data(t, "datepicker") || this._datepickerShowing && (i = this._get(o, "showAnim"),
                    s = this._get(o, "duration"),
                    n = function() {
                        e.datepicker._tidyDialog(o)
                    },
                    e.effects && (e.effects.effect[i] || e.effects[i]) ? o.dpDiv.hide(i, e.datepicker._get(o, "showOptions"), s, n) : o.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? s : null, n),
                    i || n(),
                    this._datepickerShowing = !1,
                    (a = this._get(o, "onClose")) && a.apply(o.input ? o.input[0] : null, [o.input ? o.input.val() : "", o]),
                    this._lastInput = null,
                    this._inDialog && (this._dialogInput.css({
                            position: "absolute",
                            left: "0",
                            top: "-100px"
                        }),
                        e.blockUI && (e.unblockUI(),
                            e("body").append(this.dpDiv))),
                    this._inDialog = !1)
            },
            _tidyDialog: function(e) {
                e.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar")
            },
            _checkExternalClick: function(t) {
                if (e.datepicker._curInst) {
                    var i = e(t.target),
                        s = e.datepicker._getInst(i[0]);
                    (i[0].id !== e.datepicker._mainDivId && 0 === i.parents("#" + e.datepicker._mainDivId).length && !i.hasClass(e.datepicker.markerClassName) && !i.closest("." + e.datepicker._triggerClass).length && e.datepicker._datepickerShowing && (!e.datepicker._inDialog || !e.blockUI) || i.hasClass(e.datepicker.markerClassName) && e.datepicker._curInst !== s) && e.datepicker._hideDatepicker()
                }
            },
            _adjustDate: function(t, i, s) {
                var n = e(t),
                    a = this._getInst(n[0]);
                this._isDisabledDatepicker(n[0]) || (this._adjustInstDate(a, i + ("M" === s ? this._get(a, "showCurrentAtPos") : 0), s),
                    this._updateDatepicker(a))
            },
            _gotoToday: function(t) {
                var i, s = e(t),
                    n = this._getInst(s[0]);
                this._get(n, "gotoCurrent") && n.currentDay ? (n.selectedDay = n.currentDay,
                        n.drawMonth = n.selectedMonth = n.currentMonth,
                        n.drawYear = n.selectedYear = n.currentYear) : (i = new Date,
                        n.selectedDay = i.getDate(),
                        n.drawMonth = n.selectedMonth = i.getMonth(),
                        n.drawYear = n.selectedYear = i.getFullYear()),
                    this._notifyChange(n),
                    this._adjustDate(s)
            },
            _selectMonthYear: function(t, i, s) {
                var n = e(t),
                    a = this._getInst(n[0]);
                a["selected" + ("M" === s ? "Month" : "Year")] = a["draw" + ("M" === s ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10),
                    this._notifyChange(a),
                    this._adjustDate(n)
            },
            _selectDay: function(t, i, s, n) {
                var a, o = e(t);
                e(n).hasClass(this._unselectableClass) || this._isDisabledDatepicker(o[0]) || ((a = this._getInst(o[0])).selectedDay = a.currentDay = e("a", n).html(),
                    a.selectedMonth = a.currentMonth = i,
                    a.selectedYear = a.currentYear = s,
                    this._selectDate(t, this._formatDate(a, a.currentDay, a.currentMonth, a.currentYear)))
            },
            _clearDate: function(t) {
                var i = e(t);
                this._selectDate(i, "")
            },
            _selectDate: function(t, i) {
                var s, n = e(t),
                    a = this._getInst(n[0]);
                i = null != i ? i : this._formatDate(a),
                    a.input && a.input.val(i),
                    this._updateAlternate(a),
                    (s = this._get(a, "onSelect")) ? s.apply(a.input ? a.input[0] : null, [i, a]) : a.input && a.input.trigger("change"),
                    a.inline ? this._updateDatepicker(a) : (this._hideDatepicker(),
                        this._lastInput = a.input[0],
                        "object" != typeof a.input[0] && a.input.trigger("focus"),
                        this._lastInput = null)
            },
            _updateAlternate: function(t) {
                var i, s, n, a = this._get(t, "altField");
                a && (i = this._get(t, "altFormat") || this._get(t, "dateFormat"),
                    s = this._getDate(t),
                    n = this.formatDate(i, s, this._getFormatConfig(t)),
                    e(a).val(n))
            },
            noWeekends: function(e) {
                var t = e.getDay();
                return [t > 0 && 6 > t, ""]
            },
            iso8601Week: function(e) {
                var t, i = new Date(e.getTime());
                return i.setDate(i.getDate() + 4 - (i.getDay() || 7)),
                    t = i.getTime(),
                    i.setMonth(0),
                    i.setDate(1),
                    Math.floor(Math.round((t - i) / 864e5) / 7) + 1
            },
            parseDate: function(t, i, s) {
                if (null == t || null == i)
                    throw "Invalid arguments";
                if ("" == (i = "object" == typeof i ? "" + i : i + ""))
                    return null;
                var n, a, o, r, l = 0,
                    h = (s ? s.shortYearCutoff : null) || this._defaults.shortYearCutoff,
                    u = "string" != typeof h ? h : (new Date).getFullYear() % 100 + parseInt(h, 10),
                    c = (s ? s.dayNamesShort : null) || this._defaults.dayNamesShort,
                    d = (s ? s.dayNames : null) || this._defaults.dayNames,
                    p = (s ? s.monthNamesShort : null) || this._defaults.monthNamesShort,
                    f = (s ? s.monthNames : null) || this._defaults.monthNames,
                    m = -1,
                    g = -1,
                    v = -1,
                    b = -1,
                    y = !1,
                    _ = function(e) {
                        var i = t.length > n + 1 && t.charAt(n + 1) === e;
                        return i && n++,
                            i
                    },
                    w = function(e) {
                        var t = _(e),
                            s = "@" === e ? 14 : "!" === e ? 20 : "y" === e && t ? 4 : "o" === e ? 3 : 2,
                            n = RegExp("^\\d{" + ("y" === e ? s : 1) + "," + s + "}"),
                            a = i.substring(l).match(n);
                        if (!a)
                            throw "Missing number at position " + l;
                        return l += a[0].length,
                            parseInt(a[0], 10)
                    },
                    x = function(t, s, n) {
                        var a = -1,
                            o = e.map(_(t) ? n : s, function(e, t) {
                                return [
                                    [t, e]
                                ]
                            }).sort(function(e, t) {
                                return -(e[1].length - t[1].length)
                            });
                        if (e.each(o, function(e, t) {
                                var s = t[1];
                                return i.substr(l, s.length).toLowerCase() === s.toLowerCase() ? (a = t[0],
                                    l += s.length, !1) : void 0
                            }), -1 !== a)
                            return a + 1;
                        throw "Unknown name at position " + l
                    },
                    C = function() {
                        if (i.charAt(l) !== t.charAt(n))
                            throw "Unexpected literal at position " + l;
                        l++
                    };
                for (n = 0; t.length > n; n++)
                    if (y)
                        "'" !== t.charAt(n) || _("'") ? C() : y = !1;
                    else
                        switch (t.charAt(n)) {
                            case "d":
                                v = w("d");
                                break;
                            case "D":
                                x("D", c, d);
                                break;
                            case "o":
                                b = w("o");
                                break;
                            case "m":
                                g = w("m");
                                break;
                            case "M":
                                g = x("M", p, f);
                                break;
                            case "y":
                                m = w("y");
                                break;
                            case "@":
                                m = (r = new Date(w("@"))).getFullYear(),
                                    g = r.getMonth() + 1,
                                    v = r.getDate();
                                break;
                            case "!":
                                m = (r = new Date((w("!") - this._ticksTo1970) / 1e4)).getFullYear(),
                                    g = r.getMonth() + 1,
                                    v = r.getDate();
                                break;
                            case "'":
                                _("'") ? C() : y = !0;
                                break;
                            default:
                                C()
                        }
                if (i.length > l && (o = i.substr(l), !/^\s+/.test(o)))
                    throw "Extra/unparsed characters found in date: " + o;
                if (-1 === m ? m = (new Date).getFullYear() : 100 > m && (m += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (u >= m ? 0 : -100)),
                    b > -1)
                    for (g = 1,
                        v = b; !((a = this._getDaysInMonth(m, g - 1)) >= v);)
                        g++,
                        v -= a;
                if ((r = this._daylightSavingAdjust(new Date(m, g - 1, v))).getFullYear() !== m || r.getMonth() + 1 !== g || r.getDate() !== v)
                    throw "Invalid date";
                return r
            },
            ATOM: "yy-mm-dd",
            COOKIE: "D, dd M yy",
            ISO_8601: "yy-mm-dd",
            RFC_822: "D, d M y",
            RFC_850: "DD, dd-M-y",
            RFC_1036: "D, d M y",
            RFC_1123: "D, d M yy",
            RFC_2822: "D, d M yy",
            RSS: "D, d M y",
            TICKS: "!",
            TIMESTAMP: "@",
            W3C: "yy-mm-dd",
            _ticksTo1970: 864e9 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
            formatDate: function(e, t, i) {
                if (!t)
                    return "";
                var s, n = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
                    a = (i ? i.dayNames : null) || this._defaults.dayNames,
                    o = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
                    r = (i ? i.monthNames : null) || this._defaults.monthNames,
                    l = function(t) {
                        var i = e.length > s + 1 && e.charAt(s + 1) === t;
                        return i && s++,
                            i
                    },
                    h = function(e, t, i) {
                        var s = "" + t;
                        if (l(e))
                            for (; i > s.length;)
                                s = "0" + s;
                        return s
                    },
                    u = function(e, t, i, s) {
                        return l(e) ? s[t] : i[t]
                    },
                    c = "",
                    d = !1;
                if (t)
                    for (s = 0; e.length > s; s++)
                        if (d)
                            "'" !== e.charAt(s) || l("'") ? c += e.charAt(s) : d = !1;
                        else
                            switch (e.charAt(s)) {
                                case "d":
                                    c += h("d", t.getDate(), 2);
                                    break;
                                case "D":
                                    c += u("D", t.getDay(), n, a);
                                    break;
                                case "o":
                                    c += h("o", Math.round((new Date(t.getFullYear(), t.getMonth(), t.getDate()).getTime() - new Date(t.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                                    break;
                                case "m":
                                    c += h("m", t.getMonth() + 1, 2);
                                    break;
                                case "M":
                                    c += u("M", t.getMonth(), o, r);
                                    break;
                                case "y":
                                    c += l("y") ? t.getFullYear() : (10 > t.getFullYear() % 100 ? "0" : "") + t.getFullYear() % 100;
                                    break;
                                case "@":
                                    c += t.getTime();
                                    break;
                                case "!":
                                    c += 1e4 * t.getTime() + this._ticksTo1970;
                                    break;
                                case "'":
                                    l("'") ? c += "'" : d = !0;
                                    break;
                                default:
                                    c += e.charAt(s)
                            }
                return c
            },
            _possibleChars: function(e) {
                var t, i = "",
                    s = !1,
                    n = function(i) {
                        var s = e.length > t + 1 && e.charAt(t + 1) === i;
                        return s && t++,
                            s
                    };
                for (t = 0; e.length > t; t++)
                    if (s)
                        "'" !== e.charAt(t) || n("'") ? i += e.charAt(t) : s = !1;
                    else
                        switch (e.charAt(t)) {
                            case "d":
                            case "m":
                            case "y":
                            case "@":
                                i += "0123456789";
                                break;
                            case "D":
                            case "M":
                                return null;
                            case "'":
                                n("'") ? i += "'" : s = !0;
                                break;
                            default:
                                i += e.charAt(t)
                        }
                return i
            },
            _get: function(e, t) {
                return void 0 !== e.settings[t] ? e.settings[t] : this._defaults[t]
            },
            _setDateFromField: function(e, t) {
                if (e.input.val() !== e.lastVal) {
                    var i = this._get(e, "dateFormat"),
                        s = e.lastVal = e.input ? e.input.val() : null,
                        n = this._getDefaultDate(e),
                        o = n,
                        r = this._getFormatConfig(e);
                    try {
                        o = this.parseDate(i, s, r) || n
                    } catch (a) {
                        s = t ? "" : s
                    }
                    e.selectedDay = o.getDate(),
                        e.drawMonth = e.selectedMonth = o.getMonth(),
                        e.drawYear = e.selectedYear = o.getFullYear(),
                        e.currentDay = s ? o.getDate() : 0,
                        e.currentMonth = s ? o.getMonth() : 0,
                        e.currentYear = s ? o.getFullYear() : 0,
                        this._adjustInstDate(e)
                }
            },
            _getDefaultDate: function(e) {
                return this._restrictMinMax(e, this._determineDate(e, this._get(e, "defaultDate"), new Date))
            },
            _determineDate: function(t, i, s) {
                var n = null == i || "" === i ? s : "string" == typeof i ? function(i) {
                    try {
                        return e.datepicker.parseDate(e.datepicker._get(t, "dateFormat"), i, e.datepicker._getFormatConfig(t))
                    } catch (s) {}
                    for (var n = (i.toLowerCase().match(/^c/) ? e.datepicker._getDate(t) : null) || new Date, a = n.getFullYear(), o = n.getMonth(), r = n.getDate(), l = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, h = l.exec(i); h;) {
                        switch (h[2] || "d") {
                            case "d":
                            case "D":
                                r += parseInt(h[1], 10);
                                break;
                            case "w":
                            case "W":
                                r += 7 * parseInt(h[1], 10);
                                break;
                            case "m":
                            case "M":
                                o += parseInt(h[1], 10),
                                    r = Math.min(r, e.datepicker._getDaysInMonth(a, o));
                                break;
                            case "y":
                            case "Y":
                                a += parseInt(h[1], 10),
                                    r = Math.min(r, e.datepicker._getDaysInMonth(a, o))
                        }
                        h = l.exec(i)
                    }
                    return new Date(a, o, r)
                }(i) : "number" == typeof i ? isNaN(i) ? s : function(e) {
                    var t = new Date;
                    return t.setDate(t.getDate() + e),
                        t
                }(i) : new Date(i.getTime());
                return (n = n && "Invalid Date" == "" + n ? s : n) && (n.setHours(0),
                        n.setMinutes(0),
                        n.setSeconds(0),
                        n.setMilliseconds(0)),
                    this._daylightSavingAdjust(n)
            },
            _daylightSavingAdjust: function(e) {
                return e ? (e.setHours(e.getHours() > 12 ? e.getHours() + 2 : 0),
                    e) : null
            },
            _setDate: function(e, t, i) {
                var s = !t,
                    n = e.selectedMonth,
                    a = e.selectedYear,
                    o = this._restrictMinMax(e, this._determineDate(e, t, new Date));
                e.selectedDay = e.currentDay = o.getDate(),
                    e.drawMonth = e.selectedMonth = e.currentMonth = o.getMonth(),
                    e.drawYear = e.selectedYear = e.currentYear = o.getFullYear(),
                    n === e.selectedMonth && a === e.selectedYear || i || this._notifyChange(e),
                    this._adjustInstDate(e),
                    e.input && e.input.val(s ? "" : this._formatDate(e))
            },
            _getDate: function(e) {
                return !e.currentYear || e.input && "" === e.input.val() ? null : this._daylightSavingAdjust(new Date(e.currentYear, e.currentMonth, e.currentDay))
            },
            _attachHandlers: function(t) {
                var i = this._get(t, "stepMonths"),
                    s = "#" + t.id.replace(/\\\\/g, "\\");
                t.dpDiv.find("[data-handler]").map(function() {
                    var t = {
                        prev: function() {
                            e.datepicker._adjustDate(s, -i, "M")
                        },
                        next: function() {
                            e.datepicker._adjustDate(s, +i, "M")
                        },
                        hide: function() {
                            e.datepicker._hideDatepicker()
                        },
                        today: function() {
                            e.datepicker._gotoToday(s)
                        },
                        selectDay: function() {
                            return e.datepicker._selectDay(s, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                        },
                        selectMonth: function() {
                            return e.datepicker._selectMonthYear(s, this, "M"), !1
                        },
                        selectYear: function() {
                            return e.datepicker._selectMonthYear(s, this, "Y"), !1
                        }
                    };
                    e(this).on(this.getAttribute("data-event"), t[this.getAttribute("data-handler")])
                })
            },
            _generateHTML: function(e) {
                var t, i, s, n, a, o, r, l, h, u, c, d, p, f, m, g, v, b, y, _, w, x, C, T, k, S, E, D, M, P, I, z, A, O, H, N, L, W, $, F = new Date,
                    R = this._daylightSavingAdjust(new Date(F.getFullYear(), F.getMonth(), F.getDate())),
                    B = this._get(e, "isRTL"),
                    j = this._get(e, "showButtonPanel"),
                    q = this._get(e, "hideIfNoPrevNext"),
                    Y = this._get(e, "navigationAsDateFormat"),
                    V = this._getNumberOfMonths(e),
                    X = this._get(e, "showCurrentAtPos"),
                    G = this._get(e, "stepMonths"),
                    U = 1 !== V[0] || 1 !== V[1],
                    K = this._daylightSavingAdjust(e.currentDay ? new Date(e.currentYear, e.currentMonth, e.currentDay) : new Date(9999, 9, 9)),
                    Q = this._getMinMaxDate(e, "min"),
                    J = this._getMinMaxDate(e, "max"),
                    Z = e.drawMonth - X,
                    ee = e.drawYear;
                if (0 > Z && (Z += 12,
                        ee--),
                    J)
                    for (t = this._daylightSavingAdjust(new Date(J.getFullYear(), J.getMonth() - V[0] * V[1] + 1, J.getDate())),
                        t = Q && Q > t ? Q : t; this._daylightSavingAdjust(new Date(ee, Z, 1)) > t;)
                        0 > --Z && (Z = 11,
                            ee--);
                for (e.drawMonth = Z,
                    e.drawYear = ee,
                    i = this._get(e, "prevText"),
                    i = Y ? this.formatDate(i, this._daylightSavingAdjust(new Date(ee, Z - G, 1)), this._getFormatConfig(e)) : i,
                    s = this._canAdjustMonth(e, -1, ee, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "e" : "w") + "'>" + i + "</span></a>" : q ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "e" : "w") + "'>" + i + "</span></a>",
                    n = this._get(e, "nextText"),
                    n = Y ? this.formatDate(n, this._daylightSavingAdjust(new Date(ee, Z + G, 1)), this._getFormatConfig(e)) : n,
                    a = this._canAdjustMonth(e, 1, ee, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "w" : "e") + "'>" + n + "</span></a>" : q ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "w" : "e") + "'>" + n + "</span></a>",
                    o = this._get(e, "currentText"),
                    r = this._get(e, "gotoCurrent") && e.currentDay ? K : R,
                    o = Y ? this.formatDate(o, r, this._getFormatConfig(e)) : o,
                    l = e.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(e, "closeText") + "</button>",
                    h = j ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (B ? l : "") + (this._isInRange(e, r) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + o + "</button>" : "") + (B ? "" : l) + "</div>" : "",
                    u = parseInt(this._get(e, "firstDay"), 10),
                    u = isNaN(u) ? 0 : u,
                    c = this._get(e, "showWeek"),
                    d = this._get(e, "dayNames"),
                    p = this._get(e, "dayNamesMin"),
                    f = this._get(e, "monthNames"),
                    m = this._get(e, "monthNamesShort"),
                    g = this._get(e, "beforeShowDay"),
                    v = this._get(e, "showOtherMonths"),
                    b = this._get(e, "selectOtherMonths"),
                    y = this._getDefaultDate(e),
                    _ = "",
                    x = 0; V[0] > x; x++) {
                    for (C = "",
                        this.maxRows = 4,
                        T = 0; V[1] > T; T++) {
                        if (k = this._daylightSavingAdjust(new Date(ee, Z, e.selectedDay)),
                            S = " ui-corner-all",
                            E = "",
                            U) {
                            if (E += "<div class='ui-datepicker-group",
                                V[1] > 1)
                                switch (T) {
                                    case 0:
                                        E += " ui-datepicker-group-first",
                                            S = " ui-corner-" + (B ? "right" : "left");
                                        break;
                                    case V[1] - 1:
                                        E += " ui-datepicker-group-last",
                                            S = " ui-corner-" + (B ? "left" : "right");
                                        break;
                                    default:
                                        E += " ui-datepicker-group-middle",
                                            S = ""
                                }
                            E += "'>"
                        }
                        for (E += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + S + "'>" + (/all|left/.test(S) && 0 === x ? B ? a : s : "") + (/all|right/.test(S) && 0 === x ? B ? s : a : "") + this._generateMonthYearHeader(e, Z, ee, Q, J, x > 0 || T > 0, f, m) + "</div><table class='ui-datepicker-calendar'><thead><tr>",
                            D = c ? "<th class='ui-datepicker-week-col'>" + this._get(e, "weekHeader") + "</th>" : "",
                            w = 0; 7 > w; w++)
                            D += "<th scope='col'" + ((w + u + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + "><span title='" + d[M = (w + u) % 7] + "'>" + p[M] + "</span></th>";
                        for (E += D + "</tr></thead><tbody>",
                            P = this._getDaysInMonth(ee, Z),
                            ee === e.selectedYear && Z === e.selectedMonth && (e.selectedDay = Math.min(e.selectedDay, P)),
                            I = (this._getFirstDayOfMonth(ee, Z) - u + 7) % 7,
                            z = Math.ceil((I + P) / 7),
                            this.maxRows = A = U && this.maxRows > z ? this.maxRows : z,
                            O = this._daylightSavingAdjust(new Date(ee, Z, 1 - I)),
                            H = 0; A > H; H++) {
                            for (E += "<tr>",
                                N = c ? "<td class='ui-datepicker-week-col'>" + this._get(e, "calculateWeek")(O) + "</td>" : "",
                                w = 0; 7 > w; w++)
                                L = g ? g.apply(e.input ? e.input[0] : null, [O]) : [!0, ""],
                                $ = (W = O.getMonth() !== Z) && !b || !L[0] || Q && Q > O || J && O > J,
                                N += "<td class='" + ((w + u + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (W ? " ui-datepicker-other-month" : "") + (O.getTime() === k.getTime() && Z === e.selectedMonth && e._keyEvent || y.getTime() === O.getTime() && y.getTime() === k.getTime() ? " " + this._dayOverClass : "") + ($ ? " " + this._unselectableClass + " ui-state-disabled" : "") + (W && !v ? "" : " " + L[1] + (O.getTime() === K.getTime() ? " " + this._currentClass : "") + (O.getTime() === R.getTime() ? " ui-datepicker-today" : "")) + "'" + (W && !v || !L[2] ? "" : " title='" + L[2].replace(/'/g, "&#39;") + "'") + ($ ? "" : " data-handler='selectDay' data-event='click' data-month='" + O.getMonth() + "' data-year='" + O.getFullYear() + "'") + ">" + (W && !v ? "&#xa0;" : $ ? "<span class='ui-state-default'>" + O.getDate() + "</span>" : "<a class='ui-state-default" + (O.getTime() === R.getTime() ? " ui-state-highlight" : "") + (O.getTime() === K.getTime() ? " ui-state-active" : "") + (W ? " ui-priority-secondary" : "") + "' href='#'>" + O.getDate() + "</a>") + "</td>",
                                O.setDate(O.getDate() + 1),
                                O = this._daylightSavingAdjust(O);
                            E += N + "</tr>"
                        }
                        ++Z > 11 && (Z = 0,
                                ee++),
                            C += E += "</tbody></table>" + (U ? "</div>" + (V[0] > 0 && T === V[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : "")
                    }
                    _ += C
                }
                return _ += h,
                    e._keyEvent = !1,
                    _
            },
            _generateMonthYearHeader: function(e, t, i, s, n, a, o, r) {
                var l, h, u, c, d, p, f, m, g = this._get(e, "changeMonth"),
                    v = this._get(e, "changeYear"),
                    b = this._get(e, "showMonthAfterYear"),
                    y = "<div class='ui-datepicker-title'>",
                    _ = "";
                if (a || !g)
                    _ += "<span class='ui-datepicker-month'>" + o[t] + "</span>";
                else {
                    for (l = s && s.getFullYear() === i,
                        h = n && n.getFullYear() === i,
                        _ += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>",
                        u = 0; 12 > u; u++)
                        (!l || u >= s.getMonth()) && (!h || n.getMonth() >= u) && (_ += "<option value='" + u + "'" + (u === t ? " selected='selected'" : "") + ">" + r[u] + "</option>");
                    _ += "</select>"
                }
                if (b || (y += _ + (!a && g && v ? "" : "&#xa0;")), !e.yearshtml)
                    if (e.yearshtml = "",
                        a || !v)
                        y += "<span class='ui-datepicker-year'>" + i + "</span>";
                    else {
                        for (c = this._get(e, "yearRange").split(":"),
                            d = (new Date).getFullYear(),
                            f = (p = function(e) {
                                var t = e.match(/c[+\-].*/) ? i + parseInt(e.substring(1), 10) : e.match(/[+\-].*/) ? d + parseInt(e, 10) : parseInt(e, 10);
                                return isNaN(t) ? d : t
                            })(c[0]),
                            m = Math.max(f, p(c[1] || "")),
                            f = s ? Math.max(f, s.getFullYear()) : f,
                            m = n ? Math.min(m, n.getFullYear()) : m,
                            e.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; m >= f; f++)
                            e.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>";
                        e.yearshtml += "</select>",
                            y += e.yearshtml,
                            e.yearshtml = null
                    }
                return y += this._get(e, "yearSuffix"),
                    b && (y += (!a && g && v ? "" : "&#xa0;") + _),
                    y + "</div>"
            },
            _adjustInstDate: function(e, t, i) {
                var s = e.selectedYear + ("Y" === i ? t : 0),
                    n = e.selectedMonth + ("M" === i ? t : 0),
                    a = Math.min(e.selectedDay, this._getDaysInMonth(s, n)) + ("D" === i ? t : 0),
                    o = this._restrictMinMax(e, this._daylightSavingAdjust(new Date(s, n, a)));
                e.selectedDay = o.getDate(),
                    e.drawMonth = e.selectedMonth = o.getMonth(),
                    e.drawYear = e.selectedYear = o.getFullYear(),
                    ("M" === i || "Y" === i) && this._notifyChange(e)
            },
            _restrictMinMax: function(e, t) {
                var i = this._getMinMaxDate(e, "min"),
                    s = this._getMinMaxDate(e, "max"),
                    n = i && i > t ? i : t;
                return s && n > s ? s : n
            },
            _notifyChange: function(e) {
                var t = this._get(e, "onChangeMonthYear");
                t && t.apply(e.input ? e.input[0] : null, [e.selectedYear, e.selectedMonth + 1, e])
            },
            _getNumberOfMonths: function(e) {
                var t = this._get(e, "numberOfMonths");
                return null == t ? [1, 1] : "number" == typeof t ? [1, t] : t
            },
            _getMinMaxDate: function(e, t) {
                return this._determineDate(e, this._get(e, t + "Date"), null)
            },
            _getDaysInMonth: function(e, t) {
                return 32 - this._daylightSavingAdjust(new Date(e, t, 32)).getDate()
            },
            _getFirstDayOfMonth: function(e, t) {
                return new Date(e, t, 1).getDay()
            },
            _canAdjustMonth: function(e, t, i, s) {
                var n = this._getNumberOfMonths(e),
                    a = this._daylightSavingAdjust(new Date(i, s + (0 > t ? t : n[0] * n[1]), 1));
                return 0 > t && a.setDate(this._getDaysInMonth(a.getFullYear(), a.getMonth())),
                    this._isInRange(e, a)
            },
            _isInRange: function(e, t) {
                var i, s, n = this._getMinMaxDate(e, "min"),
                    a = this._getMinMaxDate(e, "max"),
                    o = null,
                    r = null,
                    l = this._get(e, "yearRange");
                return l && (i = l.split(":"),
                        s = (new Date).getFullYear(),
                        o = parseInt(i[0], 10),
                        r = parseInt(i[1], 10),
                        i[0].match(/[+\-].*/) && (o += s),
                        i[1].match(/[+\-].*/) && (r += s)),
                    (!n || t.getTime() >= n.getTime()) && (!a || t.getTime() <= a.getTime()) && (!o || t.getFullYear() >= o) && (!r || r >= t.getFullYear())
            },
            _getFormatConfig: function(e) {
                var t = this._get(e, "shortYearCutoff");
                return {
                    shortYearCutoff: t = "string" != typeof t ? t : (new Date).getFullYear() % 100 + parseInt(t, 10),
                    dayNamesShort: this._get(e, "dayNamesShort"),
                    dayNames: this._get(e, "dayNames"),
                    monthNamesShort: this._get(e, "monthNamesShort"),
                    monthNames: this._get(e, "monthNames")
                }
            },
            _formatDate: function(e, t, i, s) {
                t || (e.currentDay = e.selectedDay,
                    e.currentMonth = e.selectedMonth,
                    e.currentYear = e.selectedYear);
                var n = t ? "object" == typeof t ? t : this._daylightSavingAdjust(new Date(s, i, t)) : this._daylightSavingAdjust(new Date(e.currentYear, e.currentMonth, e.currentDay));
                return this.formatDate(this._get(e, "dateFormat"), n, this._getFormatConfig(e))
            }
        }),
        e.fn.datepicker = function(t) {
            if (!this.length)
                return this;
            e.datepicker.initialized || (e(document).on("mousedown", e.datepicker._checkExternalClick),
                    e.datepicker.initialized = !0),
                0 === e("#" + e.datepicker._mainDivId).length && e("body").append(e.datepicker.dpDiv);
            var i = Array.prototype.slice.call(arguments, 1);
            return "string" != typeof t || "isDisabled" !== t && "getDate" !== t && "widget" !== t ? "option" === t && 2 === arguments.length && "string" == typeof arguments[1] ? e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this[0]].concat(i)) : this.each(function() {
                "string" == typeof t ? e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this].concat(i)) : e.datepicker._attachDatepicker(this, t)
            }) : e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this[0]].concat(i))
        },
        e.datepicker = new t,
        e.datepicker.initialized = !1,
        e.datepicker.uuid = (new Date).getTime(),
        e.datepicker.version = "1.12.1",
        e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());
    var f = !1;
    e(document).on("mouseup", function() {
            f = !1
        }),
        e.widget("ui.mouse", {
            version: "1.12.1",
            options: {
                cancel: "input, textarea, button, select, option",
                distance: 1,
                delay: 0
            },
            _mouseInit: function() {
                var t = this;
                this.element.on("mousedown." + this.widgetName, function(e) {
                        return t._mouseDown(e)
                    }).on("click." + this.widgetName, function(i) {
                        return !0 === e.data(i.target, t.widgetName + ".preventClickEvent") ? (e.removeData(i.target, t.widgetName + ".preventClickEvent"),
                            i.stopImmediatePropagation(), !1) : void 0
                    }),
                    this.started = !1
            },
            _mouseDestroy: function() {
                this.element.off("." + this.widgetName),
                    this._mouseMoveDelegate && this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate)
            },
            _mouseDown: function(t) {
                if (!f) {
                    this._mouseMoved = !1,
                        this._mouseStarted && this._mouseUp(t),
                        this._mouseDownEvent = t;
                    var i = this,
                        s = 1 === t.which,
                        n = !("string" != typeof this.options.cancel || !t.target.nodeName) && e(t.target).closest(this.options.cancel).length;
                    return !(s && !n && this._mouseCapture(t) && (this.mouseDelayMet = !this.options.delay,
                        this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                            i.mouseDelayMet = !0
                        }, this.options.delay)),
                        this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = !1 !== this._mouseStart(t), !this._mouseStarted) ? (t.preventDefault(),
                            0) : (!0 === e.data(t.target, this.widgetName + ".preventClickEvent") && e.removeData(t.target, this.widgetName + ".preventClickEvent"),
                            this._mouseMoveDelegate = function(e) {
                                return i._mouseMove(e)
                            },
                            this._mouseUpDelegate = function(e) {
                                return i._mouseUp(e)
                            },
                            this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate),
                            t.preventDefault(),
                            f = !0,
                            0)))
                }
            },
            _mouseMove: function(t) {
                if (this._mouseMoved) {
                    if (e.ui.ie && (!document.documentMode || 9 > document.documentMode) && !t.button)
                        return this._mouseUp(t);
                    if (!t.which)
                        if (t.originalEvent.altKey || t.originalEvent.ctrlKey || t.originalEvent.metaKey || t.originalEvent.shiftKey)
                            this.ignoreMissingWhich = !0;
                        else if (!this.ignoreMissingWhich)
                        return this._mouseUp(t)
                }
                return (t.which || t.button) && (this._mouseMoved = !0),
                    this._mouseStarted ? (this._mouseDrag(t),
                        t.preventDefault()) : (this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = !1 !== this._mouseStart(this._mouseDownEvent, t),
                        this._mouseStarted ? this._mouseDrag(t) : this._mouseUp(t)), !this._mouseStarted)
            },
            _mouseUp: function(t) {
                this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate),
                    this._mouseStarted && (this._mouseStarted = !1,
                        t.target === this._mouseDownEvent.target && e.data(t.target, this.widgetName + ".preventClickEvent", !0),
                        this._mouseStop(t)),
                    this._mouseDelayTimer && (clearTimeout(this._mouseDelayTimer),
                        delete this._mouseDelayTimer),
                    this.ignoreMissingWhich = !1,
                    f = !1,
                    t.preventDefault()
            },
            _mouseDistanceMet: function(e) {
                return Math.max(Math.abs(this._mouseDownEvent.pageX - e.pageX), Math.abs(this._mouseDownEvent.pageY - e.pageY)) >= this.options.distance
            },
            _mouseDelayMet: function() {
                return this.mouseDelayMet
            },
            _mouseStart: function() {},
            _mouseDrag: function() {},
            _mouseStop: function() {},
            _mouseCapture: function() {
                return !0
            }
        }),
        e.ui.plugin = {
            add: function(t, i, s) {
                var n, a = e.ui[t].prototype;
                for (n in s)
                    a.plugins[n] = a.plugins[n] || [],
                    a.plugins[n].push([i, s[n]])
            },
            call: function(e, t, i, s) {
                var n, a = e.plugins[t];
                if (a && (s || e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType))
                    for (n = 0; a.length > n; n++)
                        e.options[a[n][0]] && a[n][1].apply(e.element, i)
            }
        },
        e.ui.safeBlur = function(t) {
            t && "body" !== t.nodeName.toLowerCase() && e(t).trigger("blur")
        },
        e.widget("ui.draggable", e.ui.mouse, {
            version: "1.12.1",
            widgetEventPrefix: "drag",
            options: {
                addClasses: !0,
                appendTo: "parent",
                axis: !1,
                connectToSortable: !1,
                containment: !1,
                cursor: "auto",
                cursorAt: !1,
                grid: !1,
                handle: !1,
                helper: "original",
                iframeFix: !1,
                opacity: !1,
                refreshPositions: !1,
                revert: !1,
                revertDuration: 500,
                scope: "default",
                scroll: !0,
                scrollSensitivity: 20,
                scrollSpeed: 20,
                snap: !1,
                snapMode: "both",
                snapTolerance: 20,
                stack: !1,
                zIndex: !1,
                drag: null,
                start: null,
                stop: null
            },
            _create: function() {
                "original" === this.options.helper && this._setPositionRelative(),
                    this.options.addClasses && this._addClass("ui-draggable"),
                    this._setHandleClassName(),
                    this._mouseInit()
            },
            _setOption: function(e, t) {
                this._super(e, t),
                    "handle" === e && (this._removeHandleClassName(),
                        this._setHandleClassName())
            },
            _destroy: function() {
                return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this._removeHandleClassName(),
                    void this._mouseDestroy())
            },
            _mouseCapture: function(t) {
                var i = this.options;
                return !(this.helper || i.disabled || e(t.target).closest(".ui-resizable-handle").length > 0 || (this.handle = this._getHandle(t), !this.handle || (this._blurActiveElement(t),
                    this._blockFrames(!0 === i.iframeFix ? "iframe" : i.iframeFix),
                    0)))
            },
            _blockFrames: function(t) {
                this.iframeBlocks = this.document.find(t).map(function() {
                    var t = e(this);
                    return e("<div>").css("position", "absolute").appendTo(t.parent()).outerWidth(t.outerWidth()).outerHeight(t.outerHeight()).offset(t.offset())[0]
                })
            },
            _unblockFrames: function() {
                this.iframeBlocks && (this.iframeBlocks.remove(),
                    delete this.iframeBlocks)
            },
            _blurActiveElement: function(t) {
                var i = e.ui.safeActiveElement(this.document[0]);
                e(t.target).closest(i).length || e.ui.safeBlur(i)
            },
            _mouseStart: function(t) {
                var i = this.options;
                return this.helper = this._createHelper(t),
                    this._addClass(this.helper, "ui-draggable-dragging"),
                    this._cacheHelperProportions(),
                    e.ui.ddmanager && (e.ui.ddmanager.current = this),
                    this._cacheMargins(),
                    this.cssPosition = this.helper.css("position"),
                    this.scrollParent = this.helper.scrollParent(!0),
                    this.offsetParent = this.helper.offsetParent(),
                    this.hasFixedAncestor = this.helper.parents().filter(function() {
                        return "fixed" === e(this).css("position")
                    }).length > 0,
                    this.positionAbs = this.element.offset(),
                    this._refreshOffsets(t),
                    this.originalPosition = this.position = this._generatePosition(t, !1),
                    this.originalPageX = t.pageX,
                    this.originalPageY = t.pageY,
                    i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt),
                    this._setContainment(), !1 === this._trigger("start", t) ? (this._clear(), !1) : (this._cacheHelperProportions(),
                        e.ui.ddmanager && !i.dropBehaviour && e.ui.ddmanager.prepareOffsets(this, t),
                        this._mouseDrag(t, !0),
                        e.ui.ddmanager && e.ui.ddmanager.dragStart(this, t), !0)
            },
            _refreshOffsets: function(e) {
                this.offset = {
                        top: this.positionAbs.top - this.margins.top,
                        left: this.positionAbs.left - this.margins.left,
                        scroll: !1,
                        parent: this._getParentOffset(),
                        relative: this._getRelativeOffset()
                    },
                    this.offset.click = {
                        left: e.pageX - this.offset.left,
                        top: e.pageY - this.offset.top
                    }
            },
            _mouseDrag: function(t, i) {
                if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()),
                    this.position = this._generatePosition(t, !0),
                    this.positionAbs = this._convertPositionTo("absolute"), !i) {
                    var s = this._uiHash();
                    if (!1 === this._trigger("drag", t, s))
                        return this._mouseUp(new e.Event("mouseup", t)), !1;
                    this.position = s.position
                }
                return this.helper[0].style.left = this.position.left + "px",
                    this.helper[0].style.top = this.position.top + "px",
                    e.ui.ddmanager && e.ui.ddmanager.drag(this, t), !1
            },
            _mouseStop: function(t) {
                var i = this,
                    s = !1;
                return e.ui.ddmanager && !this.options.dropBehaviour && (s = e.ui.ddmanager.drop(this, t)),
                    this.dropped && (s = this.dropped,
                        this.dropped = !1),
                    "invalid" === this.options.revert && !s || "valid" === this.options.revert && s || !0 === this.options.revert || e.isFunction(this.options.revert) && this.options.revert.call(this.element, s) ? e(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                        !1 !== i._trigger("stop", t) && i._clear()
                    }) : !1 !== this._trigger("stop", t) && this._clear(), !1
            },
            _mouseUp: function(t) {
                return this._unblockFrames(),
                    e.ui.ddmanager && e.ui.ddmanager.dragStop(this, t),
                    this.handleElement.is(t.target) && this.element.trigger("focus"),
                    e.ui.mouse.prototype._mouseUp.call(this, t)
            },
            cancel: function() {
                return this.helper.is(".ui-draggable-dragging") ? this._mouseUp(new e.Event("mouseup", {
                        target: this.element[0]
                    })) : this._clear(),
                    this
            },
            _getHandle: function(t) {
                return !this.options.handle || !!e(t.target).closest(this.element.find(this.options.handle)).length
            },
            _setHandleClassName: function() {
                this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element,
                    this._addClass(this.handleElement, "ui-draggable-handle")
            },
            _removeHandleClassName: function() {
                this._removeClass(this.handleElement, "ui-draggable-handle")
            },
            _createHelper: function(t) {
                var i = this.options,
                    s = e.isFunction(i.helper),
                    n = s ? e(i.helper.apply(this.element[0], [t])) : "clone" === i.helper ? this.element.clone().removeAttr("id") : this.element;
                return n.parents("body").length || n.appendTo("parent" === i.appendTo ? this.element[0].parentNode : i.appendTo),
                    s && n[0] === this.element[0] && this._setPositionRelative(),
                    n[0] === this.element[0] || /(fixed|absolute)/.test(n.css("position")) || n.css("position", "absolute"),
                    n
            },
            _setPositionRelative: function() {
                /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative")
            },
            _adjustOffsetFromHelper: function(t) {
                "string" == typeof t && (t = t.split(" ")),
                    e.isArray(t) && (t = {
                        left: +t[0],
                        top: +t[1] || 0
                    }),
                    "left" in t && (this.offset.click.left = t.left + this.margins.left),
                    "right" in t && (this.offset.click.left = this.helperProportions.width - t.right + this.margins.left),
                    "top" in t && (this.offset.click.top = t.top + this.margins.top),
                    "bottom" in t && (this.offset.click.top = this.helperProportions.height - t.bottom + this.margins.top)
            },
            _isRootNode: function(e) {
                return /(html|body)/i.test(e.tagName) || e === this.document[0]
            },
            _getParentOffset: function() {
                var t = this.offsetParent.offset();
                return "absolute" === this.cssPosition && this.scrollParent[0] !== this.document[0] && e.contains(this.scrollParent[0], this.offsetParent[0]) && (t.left += this.scrollParent.scrollLeft(),
                        t.top += this.scrollParent.scrollTop()),
                    this._isRootNode(this.offsetParent[0]) && (t = {
                        top: 0,
                        left: 0
                    }), {
                        top: t.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                        left: t.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
                    }
            },
            _getRelativeOffset: function() {
                if ("relative" !== this.cssPosition)
                    return {
                        top: 0,
                        left: 0
                    };
                var e = this.element.position(),
                    t = this._isRootNode(this.scrollParent[0]);
                return {
                    top: e.top - (parseInt(this.helper.css("top"), 10) || 0) + (t ? 0 : this.scrollParent.scrollTop()),
                    left: e.left - (parseInt(this.helper.css("left"), 10) || 0) + (t ? 0 : this.scrollParent.scrollLeft())
                }
            },
            _cacheMargins: function() {
                this.margins = {
                    left: parseInt(this.element.css("marginLeft"), 10) || 0,
                    top: parseInt(this.element.css("marginTop"), 10) || 0,
                    right: parseInt(this.element.css("marginRight"), 10) || 0,
                    bottom: parseInt(this.element.css("marginBottom"), 10) || 0
                }
            },
            _cacheHelperProportions: function() {
                this.helperProportions = {
                    width: this.helper.outerWidth(),
                    height: this.helper.outerHeight()
                }
            },
            _setContainment: function() {
                var t, i, s, n = this.options,
                    a = this.document[0];
                return this.relativeContainer = null,
                    n.containment ? "window" === n.containment ? void(this.containment = [e(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, e(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, e(window).scrollLeft() + e(window).width() - this.helperProportions.width - this.margins.left, e(window).scrollTop() + (e(window).height() || a.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === n.containment ? void(this.containment = [0, 0, e(a).width() - this.helperProportions.width - this.margins.left, (e(a).height() || a.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : n.containment.constructor === Array ? void(this.containment = n.containment) : ("parent" === n.containment && (n.containment = this.helper[0].parentNode),
                        void((s = (i = e(n.containment))[0]) && (t = /(scroll|auto)/.test(i.css("overflow")),
                            this.containment = [(parseInt(i.css("borderLeftWidth"), 10) || 0) + (parseInt(i.css("paddingLeft"), 10) || 0), (parseInt(i.css("borderTopWidth"), 10) || 0) + (parseInt(i.css("paddingTop"), 10) || 0), (t ? Math.max(s.scrollWidth, s.offsetWidth) : s.offsetWidth) - (parseInt(i.css("borderRightWidth"), 10) || 0) - (parseInt(i.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (t ? Math.max(s.scrollHeight, s.offsetHeight) : s.offsetHeight) - (parseInt(i.css("borderBottomWidth"), 10) || 0) - (parseInt(i.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom],
                            this.relativeContainer = i))) : void(this.containment = null)
            },
            _convertPositionTo: function(e, t) {
                t || (t = this.position);
                var i = "absolute" === e ? 1 : -1,
                    s = this._isRootNode(this.scrollParent[0]);
                return {
                    top: t.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.offset.scroll.top : s ? 0 : this.offset.scroll.top) * i,
                    left: t.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.offset.scroll.left : s ? 0 : this.offset.scroll.left) * i
                }
            },
            _generatePosition: function(e, t) {
                var i, s, n, a, o = this.options,
                    r = this._isRootNode(this.scrollParent[0]),
                    l = e.pageX,
                    h = e.pageY;
                return r && this.offset.scroll || (this.offset.scroll = {
                        top: this.scrollParent.scrollTop(),
                        left: this.scrollParent.scrollLeft()
                    }),
                    t && (this.containment && (this.relativeContainer ? (s = this.relativeContainer.offset(),
                                i = [this.containment[0] + s.left, this.containment[1] + s.top, this.containment[2] + s.left, this.containment[3] + s.top]) : i = this.containment,
                            e.pageX - this.offset.click.left < i[0] && (l = i[0] + this.offset.click.left),
                            e.pageY - this.offset.click.top < i[1] && (h = i[1] + this.offset.click.top),
                            e.pageX - this.offset.click.left > i[2] && (l = i[2] + this.offset.click.left),
                            e.pageY - this.offset.click.top > i[3] && (h = i[3] + this.offset.click.top)),
                        o.grid && (n = o.grid[1] ? this.originalPageY + Math.round((h - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY,
                            h = i ? n - this.offset.click.top >= i[1] || n - this.offset.click.top > i[3] ? n : n - this.offset.click.top >= i[1] ? n - o.grid[1] : n + o.grid[1] : n,
                            a = o.grid[0] ? this.originalPageX + Math.round((l - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX,
                            l = i ? a - this.offset.click.left >= i[0] || a - this.offset.click.left > i[2] ? a : a - this.offset.click.left >= i[0] ? a - o.grid[0] : a + o.grid[0] : a),
                        "y" === o.axis && (l = this.originalPageX),
                        "x" === o.axis && (h = this.originalPageY)), {
                        top: h - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : r ? 0 : this.offset.scroll.top),
                        left: l - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : r ? 0 : this.offset.scroll.left)
                    }
            },
            _clear: function() {
                this._removeClass(this.helper, "ui-draggable-dragging"),
                    this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(),
                    this.helper = null,
                    this.cancelHelperRemoval = !1,
                    this.destroyOnClear && this.destroy()
            },
            _trigger: function(t, i, s) {
                return s = s || this._uiHash(),
                    e.ui.plugin.call(this, t, [i, s, this], !0),
                    /^(drag|start|stop)/.test(t) && (this.positionAbs = this._convertPositionTo("absolute"),
                        s.offset = this.positionAbs),
                    e.Widget.prototype._trigger.call(this, t, i, s)
            },
            plugins: {},
            _uiHash: function() {
                return {
                    helper: this.helper,
                    position: this.position,
                    originalPosition: this.originalPosition,
                    offset: this.positionAbs
                }
            }
        }),
        e.ui.plugin.add("draggable", "connectToSortable", {
            start: function(t, i, s) {
                var n = e.extend({}, i, {
                    item: s.element
                });
                s.sortables = [],
                    e(s.options.connectToSortable).each(function() {
                        var i = e(this).sortable("instance");
                        i && !i.options.disabled && (s.sortables.push(i),
                            i.refreshPositions(),
                            i._trigger("activate", t, n))
                    })
            },
            stop: function(t, i, s) {
                var n = e.extend({}, i, {
                    item: s.element
                });
                s.cancelHelperRemoval = !1,
                    e.each(s.sortables, function() {
                        var e = this;
                        e.isOver ? (e.isOver = 0,
                            s.cancelHelperRemoval = !0,
                            e.cancelHelperRemoval = !1,
                            e._storedCSS = {
                                position: e.placeholder.css("position"),
                                top: e.placeholder.css("top"),
                                left: e.placeholder.css("left")
                            },
                            e._mouseStop(t),
                            e.options.helper = e.options._helper) : (e.cancelHelperRemoval = !0,
                            e._trigger("deactivate", t, n))
                    })
            },
            drag: function(t, i, s) {
                e.each(s.sortables, function() {
                    var n = !1,
                        a = this;
                    a.positionAbs = s.positionAbs,
                        a.helperProportions = s.helperProportions,
                        a.offset.click = s.offset.click,
                        a._intersectsWith(a.containerCache) && (n = !0,
                            e.each(s.sortables, function() {
                                return this.positionAbs = s.positionAbs,
                                    this.helperProportions = s.helperProportions,
                                    this.offset.click = s.offset.click,
                                    this !== a && this._intersectsWith(this.containerCache) && e.contains(a.element[0], this.element[0]) && (n = !1),
                                    n
                            })),
                        n ? (a.isOver || (a.isOver = 1,
                                s._parent = i.helper.parent(),
                                a.currentItem = i.helper.appendTo(a.element).data("ui-sortable-item", !0),
                                a.options._helper = a.options.helper,
                                a.options.helper = function() {
                                    return i.helper[0]
                                },
                                t.target = a.currentItem[0],
                                a._mouseCapture(t, !0),
                                a._mouseStart(t, !0, !0),
                                a.offset.click.top = s.offset.click.top,
                                a.offset.click.left = s.offset.click.left,
                                a.offset.parent.left -= s.offset.parent.left - a.offset.parent.left,
                                a.offset.parent.top -= s.offset.parent.top - a.offset.parent.top,
                                s._trigger("toSortable", t),
                                s.dropped = a.element,
                                e.each(s.sortables, function() {
                                    this.refreshPositions()
                                }),
                                s.currentItem = s.element,
                                a.fromOutside = s),
                            a.currentItem && (a._mouseDrag(t),
                                i.position = a.position)) : a.isOver && (a.isOver = 0,
                            a.cancelHelperRemoval = !0,
                            a.options._revert = a.options.revert,
                            a.options.revert = !1,
                            a._trigger("out", t, a._uiHash(a)),
                            a._mouseStop(t, !0),
                            a.options.revert = a.options._revert,
                            a.options.helper = a.options._helper,
                            a.placeholder && a.placeholder.remove(),
                            i.helper.appendTo(s._parent),
                            s._refreshOffsets(t),
                            i.position = s._generatePosition(t, !0),
                            s._trigger("fromSortable", t),
                            s.dropped = !1,
                            e.each(s.sortables, function() {
                                this.refreshPositions()
                            }))
                })
            }
        }),
        e.ui.plugin.add("draggable", "cursor", {
            start: function(t, i, s) {
                var n = e("body"),
                    a = s.options;
                n.css("cursor") && (a._cursor = n.css("cursor")),
                    n.css("cursor", a.cursor)
            },
            stop: function(t, i, s) {
                var n = s.options;
                n._cursor && e("body").css("cursor", n._cursor)
            }
        }),
        e.ui.plugin.add("draggable", "opacity", {
            start: function(t, i, s) {
                var n = e(i.helper),
                    a = s.options;
                n.css("opacity") && (a._opacity = n.css("opacity")),
                    n.css("opacity", a.opacity)
            },
            stop: function(t, i, s) {
                var n = s.options;
                n._opacity && e(i.helper).css("opacity", n._opacity)
            }
        }),
        e.ui.plugin.add("draggable", "scroll", {
            start: function(e, t, i) {
                i.scrollParentNotHidden || (i.scrollParentNotHidden = i.helper.scrollParent(!1)),
                    i.scrollParentNotHidden[0] !== i.document[0] && "HTML" !== i.scrollParentNotHidden[0].tagName && (i.overflowOffset = i.scrollParentNotHidden.offset())
            },
            drag: function(t, i, s) {
                var n = s.options,
                    a = !1,
                    o = s.scrollParentNotHidden[0],
                    r = s.document[0];
                o !== r && "HTML" !== o.tagName ? (n.axis && "x" === n.axis || (s.overflowOffset.top + o.offsetHeight - t.pageY < n.scrollSensitivity ? o.scrollTop = a = o.scrollTop + n.scrollSpeed : t.pageY - s.overflowOffset.top < n.scrollSensitivity && (o.scrollTop = a = o.scrollTop - n.scrollSpeed)),
                    n.axis && "y" === n.axis || (s.overflowOffset.left + o.offsetWidth - t.pageX < n.scrollSensitivity ? o.scrollLeft = a = o.scrollLeft + n.scrollSpeed : t.pageX - s.overflowOffset.left < n.scrollSensitivity && (o.scrollLeft = a = o.scrollLeft - n.scrollSpeed))) : (n.axis && "x" === n.axis || (t.pageY - e(r).scrollTop() < n.scrollSensitivity ? a = e(r).scrollTop(e(r).scrollTop() - n.scrollSpeed) : e(window).height() - (t.pageY - e(r).scrollTop()) < n.scrollSensitivity && (a = e(r).scrollTop(e(r).scrollTop() + n.scrollSpeed))),
                    n.axis && "y" === n.axis || (t.pageX - e(r).scrollLeft() < n.scrollSensitivity ? a = e(r).scrollLeft(e(r).scrollLeft() - n.scrollSpeed) : e(window).width() - (t.pageX - e(r).scrollLeft()) < n.scrollSensitivity && (a = e(r).scrollLeft(e(r).scrollLeft() + n.scrollSpeed)))), !1 !== a && e.ui.ddmanager && !n.dropBehaviour && e.ui.ddmanager.prepareOffsets(s, t)
            }
        }),
        e.ui.plugin.add("draggable", "snap", {
            start: function(t, i, s) {
                var n = s.options;
                s.snapElements = [],
                    e(n.snap.constructor !== String ? n.snap.items || ":data(ui-draggable)" : n.snap).each(function() {
                        var t = e(this),
                            i = t.offset();
                        this !== s.element[0] && s.snapElements.push({
                            item: this,
                            width: t.outerWidth(),
                            height: t.outerHeight(),
                            top: i.top,
                            left: i.left
                        })
                    })
            },
            drag: function(t, i, s) {
                var n, a, o, r, l, h, u, c, d, p, f = s.options,
                    m = f.snapTolerance,
                    g = i.offset.left,
                    v = g + s.helperProportions.width,
                    b = i.offset.top,
                    y = b + s.helperProportions.height;
                for (d = s.snapElements.length - 1; d >= 0; d--)
                    h = (l = s.snapElements[d].left - s.margins.left) + s.snapElements[d].width,
                    c = (u = s.snapElements[d].top - s.margins.top) + s.snapElements[d].height,
                    l - m > v || g > h + m || u - m > y || b > c + m || !e.contains(s.snapElements[d].item.ownerDocument, s.snapElements[d].item) ? (s.snapElements[d].snapping && s.options.snap.release && s.options.snap.release.call(s.element, t, e.extend(s._uiHash(), {
                            snapItem: s.snapElements[d].item
                        })),
                        s.snapElements[d].snapping = !1) : ("inner" !== f.snapMode && (n = m >= Math.abs(u - y),
                            a = m >= Math.abs(c - b),
                            o = m >= Math.abs(l - v),
                            r = m >= Math.abs(h - g),
                            n && (i.position.top = s._convertPositionTo("relative", {
                                top: u - s.helperProportions.height,
                                left: 0
                            }).top),
                            a && (i.position.top = s._convertPositionTo("relative", {
                                top: c,
                                left: 0
                            }).top),
                            o && (i.position.left = s._convertPositionTo("relative", {
                                top: 0,
                                left: l - s.helperProportions.width
                            }).left),
                            r && (i.position.left = s._convertPositionTo("relative", {
                                top: 0,
                                left: h
                            }).left)),
                        p = n || a || o || r,
                        "outer" !== f.snapMode && (n = m >= Math.abs(u - b),
                            a = m >= Math.abs(c - y),
                            o = m >= Math.abs(l - g),
                            r = m >= Math.abs(h - v),
                            n && (i.position.top = s._convertPositionTo("relative", {
                                top: u,
                                left: 0
                            }).top),
                            a && (i.position.top = s._convertPositionTo("relative", {
                                top: c - s.helperProportions.height,
                                left: 0
                            }).top),
                            o && (i.position.left = s._convertPositionTo("relative", {
                                top: 0,
                                left: l
                            }).left),
                            r && (i.position.left = s._convertPositionTo("relative", {
                                top: 0,
                                left: h - s.helperProportions.width
                            }).left)), !s.snapElements[d].snapping && (n || a || o || r || p) && s.options.snap.snap && s.options.snap.snap.call(s.element, t, e.extend(s._uiHash(), {
                            snapItem: s.snapElements[d].item
                        })),
                        s.snapElements[d].snapping = n || a || o || r || p)
            }
        }),
        e.ui.plugin.add("draggable", "stack", {
            start: function(t, i, s) {
                var n, a = e.makeArray(e(s.options.stack)).sort(function(t, i) {
                    return (parseInt(e(t).css("zIndex"), 10) || 0) - (parseInt(e(i).css("zIndex"), 10) || 0)
                });
                a.length && (n = parseInt(e(a[0]).css("zIndex"), 10) || 0,
                    e(a).each(function(t) {
                        e(this).css("zIndex", n + t)
                    }),
                    this.css("zIndex", n + a.length))
            }
        }),
        e.ui.plugin.add("draggable", "zIndex", {
            start: function(t, i, s) {
                var n = e(i.helper),
                    a = s.options;
                n.css("zIndex") && (a._zIndex = n.css("zIndex")),
                    n.css("zIndex", a.zIndex)
            },
            stop: function(t, i, s) {
                var n = s.options;
                n._zIndex && e(i.helper).css("zIndex", n._zIndex)
            }
        }),
        e.widget("ui.resizable", e.ui.mouse, {
            version: "1.12.1",
            widgetEventPrefix: "resize",
            options: {
                alsoResize: !1,
                animate: !1,
                animateDuration: "slow",
                animateEasing: "swing",
                aspectRatio: !1,
                autoHide: !1,
                classes: {
                    "ui-resizable-se": "ui-icon ui-icon-gripsmall-diagonal-se"
                },
                containment: !1,
                ghost: !1,
                grid: !1,
                handles: "e,s,se",
                helper: !1,
                maxHeight: null,
                maxWidth: null,
                minHeight: 10,
                minWidth: 10,
                zIndex: 90,
                resize: null,
                start: null,
                stop: null
            },
            _num: function(e) {
                return parseFloat(e) || 0
            },
            _isNumber: function(e) {
                return !isNaN(parseFloat(e))
            },
            _hasScroll: function(t, i) {
                if ("hidden" === e(t).css("overflow"))
                    return !1;
                var s = i && "left" === i ? "scrollLeft" : "scrollTop",
                    n = !1;
                return t[s] > 0 || (t[s] = 1,
                    n = t[s] > 0,
                    t[s] = 0,
                    n)
            },
            _create: function() {
                var t, i = this.options,
                    s = this;
                this._addClass("ui-resizable"),
                    e.extend(this, {
                        _aspectRatio: !!i.aspectRatio,
                        aspectRatio: i.aspectRatio,
                        originalElement: this.element,
                        _proportionallyResizeElements: [],
                        _helper: i.helper || i.ghost || i.animate ? i.helper || "ui-resizable-helper" : null
                    }),
                    this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i) && (this.element.wrap(e("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                            position: this.element.css("position"),
                            width: this.element.outerWidth(),
                            height: this.element.outerHeight(),
                            top: this.element.css("top"),
                            left: this.element.css("left")
                        })),
                        this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")),
                        this.elementIsWrapper = !0,
                        t = {
                            marginTop: this.originalElement.css("marginTop"),
                            marginRight: this.originalElement.css("marginRight"),
                            marginBottom: this.originalElement.css("marginBottom"),
                            marginLeft: this.originalElement.css("marginLeft")
                        },
                        this.element.css(t),
                        this.originalElement.css("margin", 0),
                        this.originalResizeStyle = this.originalElement.css("resize"),
                        this.originalElement.css("resize", "none"),
                        this._proportionallyResizeElements.push(this.originalElement.css({
                            position: "static",
                            zoom: 1,
                            display: "block"
                        })),
                        this.originalElement.css(t),
                        this._proportionallyResize()),
                    this._setupHandles(),
                    i.autoHide && e(this.element).on("mouseenter", function() {
                        i.disabled || (s._removeClass("ui-resizable-autohide"),
                            s._handles.show())
                    }).on("mouseleave", function() {
                        i.disabled || s.resizing || (s._addClass("ui-resizable-autohide"),
                            s._handles.hide())
                    }),
                    this._mouseInit()
            },
            _destroy: function() {
                this._mouseDestroy();
                var t, i = function(t) {
                    e(t).removeData("resizable").removeData("ui-resizable").off(".resizable").find(".ui-resizable-handle").remove()
                };
                return this.elementIsWrapper && (i(this.element),
                        this.originalElement.css({
                            position: (t = this.element).css("position"),
                            width: t.outerWidth(),
                            height: t.outerHeight(),
                            top: t.css("top"),
                            left: t.css("left")
                        }).insertAfter(t),
                        t.remove()),
                    this.originalElement.css("resize", this.originalResizeStyle),
                    i(this.originalElement),
                    this
            },
            _setOption: function(e, t) {
                switch (this._super(e, t),
                    e) {
                    case "handles":
                        this._removeHandles(),
                            this._setupHandles()
                }
            },
            _setupHandles: function() {
                var t, i, s, n, a, o = this.options,
                    r = this;
                if (this.handles = o.handles || (e(".ui-resizable-handle", this.element).length ? {
                        n: ".ui-resizable-n",
                        e: ".ui-resizable-e",
                        s: ".ui-resizable-s",
                        w: ".ui-resizable-w",
                        se: ".ui-resizable-se",
                        sw: ".ui-resizable-sw",
                        ne: ".ui-resizable-ne",
                        nw: ".ui-resizable-nw"
                    } : "e,s,se"),
                    this._handles = e(),
                    this.handles.constructor === String)
                    for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"),
                        s = this.handles.split(","),
                        this.handles = {},
                        i = 0; s.length > i; i++)
                        n = "ui-resizable-" + (t = e.trim(s[i])),
                        a = e("<div>"),
                        this._addClass(a, "ui-resizable-handle " + n),
                        a.css({
                            zIndex: o.zIndex
                        }),
                        this.handles[t] = ".ui-resizable-" + t,
                        this.element.append(a);
                this._renderAxis = function(t) {
                        var i, s, n, a;
                        for (i in t = t || this.element,
                            this.handles)
                            this.handles[i].constructor === String ? this.handles[i] = this.element.children(this.handles[i]).first().show() : (this.handles[i].jquery || this.handles[i].nodeType) && (this.handles[i] = e(this.handles[i]),
                                this._on(this.handles[i], {
                                    mousedown: r._mouseDown
                                })),
                            this.elementIsWrapper && this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i) && (s = e(this.handles[i], this.element),
                                a = /sw|ne|nw|se|n|s/.test(i) ? s.outerHeight() : s.outerWidth(),
                                n = ["padding", /ne|nw|n/.test(i) ? "Top" : /se|sw|s/.test(i) ? "Bottom" : /^e$/.test(i) ? "Right" : "Left"].join(""),
                                t.css(n, a),
                                this._proportionallyResize()),
                            this._handles = this._handles.add(this.handles[i])
                    },
                    this._renderAxis(this.element),
                    this._handles = this._handles.add(this.element.find(".ui-resizable-handle")),
                    this._handles.disableSelection(),
                    this._handles.on("mouseover", function() {
                        r.resizing || (this.className && (a = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)),
                            r.axis = a && a[1] ? a[1] : "se")
                    }),
                    o.autoHide && (this._handles.hide(),
                        this._addClass("ui-resizable-autohide"))
            },
            _removeHandles: function() {
                this._handles.remove()
            },
            _mouseCapture: function(t) {
                var i, s, n = !1;
                for (i in this.handles)
                    ((s = e(this.handles[i])[0]) === t.target || e.contains(s, t.target)) && (n = !0);
                return !this.options.disabled && n
            },
            _mouseStart: function(t) {
                var i, s, n, a = this.options,
                    o = this.element;
                return this.resizing = !0,
                    this._renderProxy(),
                    i = this._num(this.helper.css("left")),
                    s = this._num(this.helper.css("top")),
                    a.containment && (i += e(a.containment).scrollLeft() || 0,
                        s += e(a.containment).scrollTop() || 0),
                    this.offset = this.helper.offset(),
                    this.position = {
                        left: i,
                        top: s
                    },
                    this.size = this._helper ? {
                        width: this.helper.width(),
                        height: this.helper.height()
                    } : {
                        width: o.width(),
                        height: o.height()
                    },
                    this.originalSize = this._helper ? {
                        width: o.outerWidth(),
                        height: o.outerHeight()
                    } : {
                        width: o.width(),
                        height: o.height()
                    },
                    this.sizeDiff = {
                        width: o.outerWidth() - o.width(),
                        height: o.outerHeight() - o.height()
                    },
                    this.originalPosition = {
                        left: i,
                        top: s
                    },
                    this.originalMousePosition = {
                        left: t.pageX,
                        top: t.pageY
                    },
                    this.aspectRatio = "number" == typeof a.aspectRatio ? a.aspectRatio : this.originalSize.width / this.originalSize.height || 1,
                    n = e(".ui-resizable-" + this.axis).css("cursor"),
                    e("body").css("cursor", "auto" === n ? this.axis + "-resize" : n),
                    this._addClass("ui-resizable-resizing"),
                    this._propagate("start", t), !0
            },
            _mouseDrag: function(t) {
                var i, s, n = this.originalMousePosition,
                    a = t.pageX - n.left || 0,
                    o = t.pageY - n.top || 0,
                    r = this._change[this.axis];
                return this._updatePrevProperties(), !!r && (i = r.apply(this, [t, a, o]),
                    this._updateVirtualBoundaries(t.shiftKey),
                    (this._aspectRatio || t.shiftKey) && (i = this._updateRatio(i, t)),
                    i = this._respectSize(i, t),
                    this._updateCache(i),
                    this._propagate("resize", t),
                    s = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(),
                    e.isEmptyObject(s) || (this._updatePrevProperties(),
                        this._trigger("resize", t, this.ui()),
                        this._applyChanges()), !1)
            },
            _mouseStop: function(t) {
                this.resizing = !1;
                var i, s, n, a, o, r, l, h = this.options,
                    u = this;
                return this._helper && (n = (s = (i = this._proportionallyResizeElements).length && /textarea/i.test(i[0].nodeName)) && this._hasScroll(i[0], "left") ? 0 : u.sizeDiff.height,
                        a = s ? 0 : u.sizeDiff.width,
                        o = {
                            width: u.helper.width() - a,
                            height: u.helper.height() - n
                        },
                        r = parseFloat(u.element.css("left")) + (u.position.left - u.originalPosition.left) || null,
                        l = parseFloat(u.element.css("top")) + (u.position.top - u.originalPosition.top) || null,
                        h.animate || this.element.css(e.extend(o, {
                            top: l,
                            left: r
                        })),
                        u.helper.height(u.size.height),
                        u.helper.width(u.size.width),
                        this._helper && !h.animate && this._proportionallyResize()),
                    e("body").css("cursor", "auto"),
                    this._removeClass("ui-resizable-resizing"),
                    this._propagate("stop", t),
                    this._helper && this.helper.remove(), !1
            },
            _updatePrevProperties: function() {
                this.prevPosition = {
                        top: this.position.top,
                        left: this.position.left
                    },
                    this.prevSize = {
                        width: this.size.width,
                        height: this.size.height
                    }
            },
            _applyChanges: function() {
                var e = {};
                return this.position.top !== this.prevPosition.top && (e.top = this.position.top + "px"),
                    this.position.left !== this.prevPosition.left && (e.left = this.position.left + "px"),
                    this.size.width !== this.prevSize.width && (e.width = this.size.width + "px"),
                    this.size.height !== this.prevSize.height && (e.height = this.size.height + "px"),
                    this.helper.css(e),
                    e
            },
            _updateVirtualBoundaries: function(e) {
                var t, i, s, n, a, o = this.options;
                a = {
                        minWidth: this._isNumber(o.minWidth) ? o.minWidth : 0,
                        maxWidth: this._isNumber(o.maxWidth) ? o.maxWidth : 1 / 0,
                        minHeight: this._isNumber(o.minHeight) ? o.minHeight : 0,
                        maxHeight: this._isNumber(o.maxHeight) ? o.maxHeight : 1 / 0
                    },
                    (this._aspectRatio || e) && (s = a.minWidth / this.aspectRatio,
                        i = a.maxHeight * this.aspectRatio,
                        n = a.maxWidth / this.aspectRatio,
                        (t = a.minHeight * this.aspectRatio) > a.minWidth && (a.minWidth = t),
                        s > a.minHeight && (a.minHeight = s),
                        a.maxWidth > i && (a.maxWidth = i),
                        a.maxHeight > n && (a.maxHeight = n)),
                    this._vBoundaries = a
            },
            _updateCache: function(e) {
                this.offset = this.helper.offset(),
                    this._isNumber(e.left) && (this.position.left = e.left),
                    this._isNumber(e.top) && (this.position.top = e.top),
                    this._isNumber(e.height) && (this.size.height = e.height),
                    this._isNumber(e.width) && (this.size.width = e.width)
            },
            _updateRatio: function(e) {
                var t = this.position,
                    i = this.size,
                    s = this.axis;
                return this._isNumber(e.height) ? e.width = e.height * this.aspectRatio : this._isNumber(e.width) && (e.height = e.width / this.aspectRatio),
                    "sw" === s && (e.left = t.left + (i.width - e.width),
                        e.top = null),
                    "nw" === s && (e.top = t.top + (i.height - e.height),
                        e.left = t.left + (i.width - e.width)),
                    e
            },
            _respectSize: function(e) {
                var t = this._vBoundaries,
                    i = this.axis,
                    s = this._isNumber(e.width) && t.maxWidth && t.maxWidth < e.width,
                    n = this._isNumber(e.height) && t.maxHeight && t.maxHeight < e.height,
                    a = this._isNumber(e.width) && t.minWidth && t.minWidth > e.width,
                    o = this._isNumber(e.height) && t.minHeight && t.minHeight > e.height,
                    r = this.originalPosition.left + this.originalSize.width,
                    l = this.originalPosition.top + this.originalSize.height,
                    h = /sw|nw|w/.test(i),
                    u = /nw|ne|n/.test(i);
                return a && (e.width = t.minWidth),
                    o && (e.height = t.minHeight),
                    s && (e.width = t.maxWidth),
                    n && (e.height = t.maxHeight),
                    a && h && (e.left = r - t.minWidth),
                    s && h && (e.left = r - t.maxWidth),
                    o && u && (e.top = l - t.minHeight),
                    n && u && (e.top = l - t.maxHeight),
                    e.width || e.height || e.left || !e.top ? e.width || e.height || e.top || !e.left || (e.left = null) : e.top = null,
                    e
            },
            _getPaddingPlusBorderDimensions: function(e) {
                for (var t = 0, i = [], s = [e.css("borderTopWidth"), e.css("borderRightWidth"), e.css("borderBottomWidth"), e.css("borderLeftWidth")], n = [e.css("paddingTop"), e.css("paddingRight"), e.css("paddingBottom"), e.css("paddingLeft")]; 4 > t; t++)
                    i[t] = parseFloat(s[t]) || 0,
                    i[t] += parseFloat(n[t]) || 0;
                return {
                    height: i[0] + i[2],
                    width: i[1] + i[3]
                }
            },
            _proportionallyResize: function() {
                if (this._proportionallyResizeElements.length)
                    for (var e, t = 0, i = this.helper || this.element; this._proportionallyResizeElements.length > t; t++)
                        e = this._proportionallyResizeElements[t],
                        this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(e)),
                        e.css({
                            height: i.height() - this.outerDimensions.height || 0,
                            width: i.width() - this.outerDimensions.width || 0
                        })
            },
            _renderProxy: function() {
                var t = this.options;
                this.elementOffset = this.element.offset(),
                    this._helper ? (this.helper = this.helper || e("<div style='overflow:hidden;'></div>"),
                        this._addClass(this.helper, this._helper),
                        this.helper.css({
                            width: this.element.outerWidth(),
                            height: this.element.outerHeight(),
                            position: "absolute",
                            left: this.elementOffset.left + "px",
                            top: this.elementOffset.top + "px",
                            zIndex: ++t.zIndex
                        }),
                        this.helper.appendTo("body").disableSelection()) : this.helper = this.element
            },
            _change: {
                e: function(e, t) {
                    return {
                        width: this.originalSize.width + t
                    }
                },
                w: function(e, t) {
                    return {
                        left: this.originalPosition.left + t,
                        width: this.originalSize.width - t
                    }
                },
                n: function(e, t, i) {
                    return {
                        top: this.originalPosition.top + i,
                        height: this.originalSize.height - i
                    }
                },
                s: function(e, t, i) {
                    return {
                        height: this.originalSize.height + i
                    }
                },
                se: function(t, i, s) {
                    return e.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [t, i, s]))
                },
                sw: function(t, i, s) {
                    return e.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [t, i, s]))
                },
                ne: function(t, i, s) {
                    return e.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [t, i, s]))
                },
                nw: function(t, i, s) {
                    return e.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [t, i, s]))
                }
            },
            _propagate: function(t, i) {
                e.ui.plugin.call(this, t, [i, this.ui()]),
                    "resize" !== t && this._trigger(t, i, this.ui())
            },
            plugins: {},
            ui: function() {
                return {
                    originalElement: this.originalElement,
                    element: this.element,
                    helper: this.helper,
                    position: this.position,
                    size: this.size,
                    originalSize: this.originalSize,
                    originalPosition: this.originalPosition
                }
            }
        }),
        e.ui.plugin.add("resizable", "animate", {
            stop: function(t) {
                var i = e(this).resizable("instance"),
                    s = i.options,
                    n = i._proportionallyResizeElements,
                    a = n.length && /textarea/i.test(n[0].nodeName),
                    o = a && i._hasScroll(n[0], "left") ? 0 : i.sizeDiff.height,
                    r = {
                        width: i.size.width - (a ? 0 : i.sizeDiff.width),
                        height: i.size.height - o
                    },
                    l = parseFloat(i.element.css("left")) + (i.position.left - i.originalPosition.left) || null,
                    h = parseFloat(i.element.css("top")) + (i.position.top - i.originalPosition.top) || null;
                i.element.animate(e.extend(r, h && l ? {
                    top: h,
                    left: l
                } : {}), {
                    duration: s.animateDuration,
                    easing: s.animateEasing,
                    step: function() {
                        var s = {
                            width: parseFloat(i.element.css("width")),
                            height: parseFloat(i.element.css("height")),
                            top: parseFloat(i.element.css("top")),
                            left: parseFloat(i.element.css("left"))
                        };
                        n && n.length && e(n[0]).css({
                                width: s.width,
                                height: s.height
                            }),
                            i._updateCache(s),
                            i._propagate("resize", t)
                    }
                })
            }
        }),
        e.ui.plugin.add("resizable", "containment", {
            start: function() {
                var t, i, s, n, a, o, r, l = e(this).resizable("instance"),
                    h = l.element,
                    u = l.options.containment,
                    c = u instanceof e ? u.get(0) : /parent/.test(u) ? h.parent().get(0) : u;
                c && (l.containerElement = e(c),
                    /document/.test(u) || u === document ? (l.containerOffset = {
                            left: 0,
                            top: 0
                        },
                        l.containerPosition = {
                            left: 0,
                            top: 0
                        },
                        l.parentData = {
                            element: e(document),
                            left: 0,
                            top: 0,
                            width: e(document).width(),
                            height: e(document).height() || document.body.parentNode.scrollHeight
                        }) : (t = e(c),
                        i = [],
                        e(["Top", "Right", "Left", "Bottom"]).each(function(e, s) {
                            i[e] = l._num(t.css("padding" + s))
                        }),
                        l.containerOffset = t.offset(),
                        l.containerPosition = t.position(),
                        l.containerSize = {
                            height: t.innerHeight() - i[3],
                            width: t.innerWidth() - i[1]
                        },
                        s = l.containerOffset,
                        n = l.containerSize.height,
                        a = l.containerSize.width,
                        o = l._hasScroll(c, "left") ? c.scrollWidth : a,
                        r = l._hasScroll(c) ? c.scrollHeight : n,
                        l.parentData = {
                            element: c,
                            left: s.left,
                            top: s.top,
                            width: o,
                            height: r
                        }))
            },
            resize: function(t) {
                var i, s, n, a, o = e(this).resizable("instance"),
                    r = o.options,
                    l = o.containerOffset,
                    h = o.position,
                    u = o._aspectRatio || t.shiftKey,
                    c = {
                        top: 0,
                        left: 0
                    },
                    d = o.containerElement,
                    p = !0;
                d[0] !== document && /static/.test(d.css("position")) && (c = l),
                    h.left < (o._helper ? l.left : 0) && (o.size.width = o.size.width + (o._helper ? o.position.left - l.left : o.position.left - c.left),
                        u && (o.size.height = o.size.width / o.aspectRatio,
                            p = !1),
                        o.position.left = r.helper ? l.left : 0),
                    h.top < (o._helper ? l.top : 0) && (o.size.height = o.size.height + (o._helper ? o.position.top - l.top : o.position.top),
                        u && (o.size.width = o.size.height * o.aspectRatio,
                            p = !1),
                        o.position.top = o._helper ? l.top : 0),
                    n = o.containerElement.get(0) === o.element.parent().get(0),
                    a = /relative|absolute/.test(o.containerElement.css("position")),
                    n && a ? (o.offset.left = o.parentData.left + o.position.left,
                        o.offset.top = o.parentData.top + o.position.top) : (o.offset.left = o.element.offset().left,
                        o.offset.top = o.element.offset().top),
                    i = Math.abs(o.sizeDiff.width + (o._helper ? o.offset.left - c.left : o.offset.left - l.left)),
                    s = Math.abs(o.sizeDiff.height + (o._helper ? o.offset.top - c.top : o.offset.top - l.top)),
                    i + o.size.width >= o.parentData.width && (o.size.width = o.parentData.width - i,
                        u && (o.size.height = o.size.width / o.aspectRatio,
                            p = !1)),
                    s + o.size.height >= o.parentData.height && (o.size.height = o.parentData.height - s,
                        u && (o.size.width = o.size.height * o.aspectRatio,
                            p = !1)),
                    p || (o.position.left = o.prevPosition.left,
                        o.position.top = o.prevPosition.top,
                        o.size.width = o.prevSize.width,
                        o.size.height = o.prevSize.height)
            },
            stop: function() {
                var t = e(this).resizable("instance"),
                    i = t.options,
                    s = t.containerOffset,
                    n = t.containerPosition,
                    a = t.containerElement,
                    o = e(t.helper),
                    r = o.offset(),
                    l = o.outerWidth() - t.sizeDiff.width,
                    h = o.outerHeight() - t.sizeDiff.height;
                t._helper && !i.animate && /relative/.test(a.css("position")) && e(this).css({
                        left: r.left - n.left - s.left,
                        width: l,
                        height: h
                    }),
                    t._helper && !i.animate && /static/.test(a.css("position")) && e(this).css({
                        left: r.left - n.left - s.left,
                        width: l,
                        height: h
                    })
            }
        }),
        e.ui.plugin.add("resizable", "alsoResize", {
            start: function() {
                var t = e(this).resizable("instance");
                e(t.options.alsoResize).each(function() {
                    var t = e(this);
                    t.data("ui-resizable-alsoresize", {
                        width: parseFloat(t.width()),
                        height: parseFloat(t.height()),
                        left: parseFloat(t.css("left")),
                        top: parseFloat(t.css("top"))
                    })
                })
            },
            resize: function(t, i) {
                var s = e(this).resizable("instance"),
                    n = s.originalSize,
                    a = s.originalPosition,
                    o = {
                        height: s.size.height - n.height || 0,
                        width: s.size.width - n.width || 0,
                        top: s.position.top - a.top || 0,
                        left: s.position.left - a.left || 0
                    };
                e(s.options.alsoResize).each(function() {
                    var t = e(this),
                        s = e(this).data("ui-resizable-alsoresize"),
                        n = {},
                        a = t.parents(i.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                    e.each(a, function(e, t) {
                            var i = (s[t] || 0) + (o[t] || 0);
                            i && i >= 0 && (n[t] = i || null)
                        }),
                        t.css(n)
                })
            },
            stop: function() {
                e(this).removeData("ui-resizable-alsoresize")
            }
        }),
        e.ui.plugin.add("resizable", "ghost", {
            start: function() {
                var t = e(this).resizable("instance"),
                    i = t.size;
                t.ghost = t.originalElement.clone(),
                    t.ghost.css({
                        opacity: .25,
                        display: "block",
                        position: "relative",
                        height: i.height,
                        width: i.width,
                        margin: 0,
                        left: 0,
                        top: 0
                    }),
                    t._addClass(t.ghost, "ui-resizable-ghost"), !1 !== e.uiBackCompat && "string" == typeof t.options.ghost && t.ghost.addClass(this.options.ghost),
                    t.ghost.appendTo(t.helper)
            },
            resize: function() {
                var t = e(this).resizable("instance");
                t.ghost && t.ghost.css({
                    position: "relative",
                    height: t.size.height,
                    width: t.size.width
                })
            },
            stop: function() {
                var t = e(this).resizable("instance");
                t.ghost && t.helper && t.helper.get(0).removeChild(t.ghost.get(0))
            }
        }),
        e.ui.plugin.add("resizable", "grid", {
            resize: function() {
                var t, i = e(this).resizable("instance"),
                    s = i.options,
                    n = i.size,
                    a = i.originalSize,
                    o = i.originalPosition,
                    r = i.axis,
                    l = "number" == typeof s.grid ? [s.grid, s.grid] : s.grid,
                    h = l[0] || 1,
                    u = l[1] || 1,
                    c = Math.round((n.width - a.width) / h) * h,
                    d = Math.round((n.height - a.height) / u) * u,
                    p = a.width + c,
                    f = a.height + d,
                    m = s.maxWidth && p > s.maxWidth,
                    g = s.maxHeight && f > s.maxHeight,
                    v = s.minWidth && s.minWidth > p,
                    b = s.minHeight && s.minHeight > f;
                s.grid = l,
                    v && (p += h),
                    b && (f += u),
                    m && (p -= h),
                    g && (f -= u),
                    /^(se|s|e)$/.test(r) ? (i.size.width = p,
                        i.size.height = f) : /^(ne)$/.test(r) ? (i.size.width = p,
                        i.size.height = f,
                        i.position.top = o.top - d) : /^(sw)$/.test(r) ? (i.size.width = p,
                        i.size.height = f,
                        i.position.left = o.left - c) : ((0 >= f - u || 0 >= p - h) && (t = i._getPaddingPlusBorderDimensions(this)),
                        f - u > 0 ? (i.size.height = f,
                            i.position.top = o.top - d) : (i.size.height = f = u - t.height,
                            i.position.top = o.top + a.height - f),
                        p - h > 0 ? (i.size.width = p,
                            i.position.left = o.left - c) : (i.size.width = p = h - t.width,
                            i.position.left = o.left + a.width - p))
            }
        }),
        e.widget("ui.dialog", {
            version: "1.12.1",
            options: {
                appendTo: "body",
                autoOpen: !0,
                buttons: [],
                classes: {
                    "ui-dialog": "ui-corner-all",
                    "ui-dialog-titlebar": "ui-corner-all"
                },
                closeOnEscape: !0,
                closeText: "Close",
                draggable: !0,
                hide: null,
                height: "auto",
                maxHeight: null,
                maxWidth: null,
                minHeight: 150,
                minWidth: 150,
                modal: !1,
                position: {
                    my: "center",
                    at: "center",
                    of: window,
                    collision: "fit",
                    using: function(t) {
                        var i = e(this).css(t).offset().top;
                        0 > i && e(this).css("top", t.top - i)
                    }
                },
                resizable: !0,
                show: null,
                title: null,
                width: 300,
                beforeClose: null,
                close: null,
                drag: null,
                dragStart: null,
                dragStop: null,
                focus: null,
                open: null,
                resize: null,
                resizeStart: null,
                resizeStop: null
            },
            sizeRelatedOptions: {
                buttons: !0,
                height: !0,
                maxHeight: !0,
                maxWidth: !0,
                minHeight: !0,
                minWidth: !0,
                width: !0
            },
            resizableRelatedOptions: {
                maxHeight: !0,
                maxWidth: !0,
                minHeight: !0,
                minWidth: !0
            },
            _create: function() {
                this.originalCss = {
                        display: this.element[0].style.display,
                        width: this.element[0].style.width,
                        minHeight: this.element[0].style.minHeight,
                        maxHeight: this.element[0].style.maxHeight,
                        height: this.element[0].style.height
                    },
                    this.originalPosition = {
                        parent: this.element.parent(),
                        index: this.element.parent().children().index(this.element)
                    },
                    this.originalTitle = this.element.attr("title"),
                    null == this.options.title && null != this.originalTitle && (this.options.title = this.originalTitle),
                    this.options.disabled && (this.options.disabled = !1),
                    this._createWrapper(),
                    this.element.show().removeAttr("title").appendTo(this.uiDialog),
                    this._addClass("ui-dialog-content", "ui-widget-content"),
                    this._createTitlebar(),
                    this._createButtonPane(),
                    this.options.draggable && e.fn.draggable && this._makeDraggable(),
                    this.options.resizable && e.fn.resizable && this._makeResizable(),
                    this._isOpen = !1,
                    this._trackFocus()
            },
            _init: function() {
                this.options.autoOpen && this.open()
            },
            _appendTo: function() {
                var t = this.options.appendTo;
                return t && (t.jquery || t.nodeType) ? e(t) : this.document.find(t || "body").eq(0)
            },
            _destroy: function() {
                var e, t = this.originalPosition;
                this._untrackInstance(),
                    this._destroyOverlay(),
                    this.element.removeUniqueId().css(this.originalCss).detach(),
                    this.uiDialog.remove(),
                    this.originalTitle && this.element.attr("title", this.originalTitle),
                    (e = t.parent.children().eq(t.index)).length && e[0] !== this.element[0] ? e.before(this.element) : t.parent.append(this.element)
            },
            widget: function() {
                return this.uiDialog
            },
            disable: e.noop,
            enable: e.noop,
            close: function(t) {
                var i = this;
                this._isOpen && !1 !== this._trigger("beforeClose", t) && (this._isOpen = !1,
                    this._focusedElement = null,
                    this._destroyOverlay(),
                    this._untrackInstance(),
                    this.opener.filter(":focusable").trigger("focus").length || e.ui.safeBlur(e.ui.safeActiveElement(this.document[0])),
                    this._hide(this.uiDialog, this.options.hide, function() {
                        i._trigger("close", t)
                    }))
            },
            isOpen: function() {
                return this._isOpen
            },
            moveToTop: function() {
                this._moveToTop()
            },
            _moveToTop: function(t, i) {
                var s = !1,
                    n = this.uiDialog.siblings(".ui-front:visible").map(function() {
                        return +e(this).css("z-index")
                    }).get(),
                    a = Math.max.apply(null, n);
                return a >= +this.uiDialog.css("z-index") && (this.uiDialog.css("z-index", a + 1),
                        s = !0),
                    s && !i && this._trigger("focus", t),
                    s
            },
            open: function() {
                var t = this;
                return this._isOpen ? void(this._moveToTop() && this._focusTabbable()) : (this._isOpen = !0,
                    this.opener = e(e.ui.safeActiveElement(this.document[0])),
                    this._size(),
                    this._position(),
                    this._createOverlay(),
                    this._moveToTop(null, !0),
                    this.overlay && this.overlay.css("z-index", this.uiDialog.css("z-index") - 1),
                    this._show(this.uiDialog, this.options.show, function() {
                        t._focusTabbable(),
                            t._trigger("focus")
                    }),
                    this._makeFocusTarget(),
                    void this._trigger("open"))
            },
            _focusTabbable: function() {
                var e = this._focusedElement;
                e || (e = this.element.find("[autofocus]")),
                    e.length || (e = this.element.find(":tabbable")),
                    e.length || (e = this.uiDialogButtonPane.find(":tabbable")),
                    e.length || (e = this.uiDialogTitlebarClose.filter(":tabbable")),
                    e.length || (e = this.uiDialog),
                    e.eq(0).trigger("focus")
            },
            _keepFocus: function(t) {
                function i() {
                    var t = e.ui.safeActiveElement(this.document[0]);
                    this.uiDialog[0] === t || e.contains(this.uiDialog[0], t) || this._focusTabbable()
                }
                t.preventDefault(),
                    i.call(this),
                    this._delay(i)
            },
            _createWrapper: function() {
                this.uiDialog = e("<div>").hide().attr({
                        tabIndex: -1,
                        role: "dialog"
                    }).appendTo(this._appendTo()),
                    this._addClass(this.uiDialog, "ui-dialog", "ui-widget ui-widget-content ui-front"),
                    this._on(this.uiDialog, {
                        keydown: function(t) {
                            if (this.options.closeOnEscape && !t.isDefaultPrevented() && t.keyCode && t.keyCode === e.ui.keyCode.ESCAPE)
                                return t.preventDefault(),
                                    void this.close(t);
                            if (t.keyCode === e.ui.keyCode.TAB && !t.isDefaultPrevented()) {
                                var i = this.uiDialog.find(":tabbable"),
                                    s = i.filter(":first"),
                                    n = i.filter(":last");
                                t.target !== n[0] && t.target !== this.uiDialog[0] || t.shiftKey ? t.target !== s[0] && t.target !== this.uiDialog[0] || !t.shiftKey || (this._delay(function() {
                                        n.trigger("focus")
                                    }),
                                    t.preventDefault()) : (this._delay(function() {
                                        s.trigger("focus")
                                    }),
                                    t.preventDefault())
                            }
                        },
                        mousedown: function(e) {
                            this._moveToTop(e) && this._focusTabbable()
                        }
                    }),
                    this.element.find("[aria-describedby]").length || this.uiDialog.attr({
                        "aria-describedby": this.element.uniqueId().attr("id")
                    })
            },
            _createTitlebar: function() {
                var t;
                this.uiDialogTitlebar = e("<div>"),
                    this._addClass(this.uiDialogTitlebar, "ui-dialog-titlebar", "ui-widget-header ui-helper-clearfix"),
                    this._on(this.uiDialogTitlebar, {
                        mousedown: function(t) {
                            e(t.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.trigger("focus")
                        }
                    }),
                    this.uiDialogTitlebarClose = e("<button type='button'></button>").button({
                        label: e("<a>").text(this.options.closeText).html(),
                        icon: "ui-icon-closethick",
                        showLabel: !1
                    }).appendTo(this.uiDialogTitlebar),
                    this._addClass(this.uiDialogTitlebarClose, "ui-dialog-titlebar-close"),
                    this._on(this.uiDialogTitlebarClose, {
                        click: function(e) {
                            e.preventDefault(),
                                this.close(e)
                        }
                    }),
                    t = e("<span>").uniqueId().prependTo(this.uiDialogTitlebar),
                    this._addClass(t, "ui-dialog-title"),
                    this._title(t),
                    this.uiDialogTitlebar.prependTo(this.uiDialog),
                    this.uiDialog.attr({
                        "aria-labelledby": t.attr("id")
                    })
            },
            _title: function(e) {
                this.options.title ? e.text(this.options.title) : e.html("&#160;")
            },
            _createButtonPane: function() {
                this.uiDialogButtonPane = e("<div>"),
                    this._addClass(this.uiDialogButtonPane, "ui-dialog-buttonpane", "ui-widget-content ui-helper-clearfix"),
                    this.uiButtonSet = e("<div>").appendTo(this.uiDialogButtonPane),
                    this._addClass(this.uiButtonSet, "ui-dialog-buttonset"),
                    this._createButtons()
            },
            _createButtons: function() {
                var t = this,
                    i = this.options.buttons;
                return this.uiDialogButtonPane.remove(),
                    this.uiButtonSet.empty(),
                    e.isEmptyObject(i) || e.isArray(i) && !i.length ? void this._removeClass(this.uiDialog, "ui-dialog-buttons") : (e.each(i, function(i, s) {
                            var n, a;
                            s = e.isFunction(s) ? {
                                    click: s,
                                    text: i
                                } : s,
                                s = e.extend({
                                    type: "button"
                                }, s),
                                n = s.click,
                                a = {
                                    icon: s.icon,
                                    iconPosition: s.iconPosition,
                                    showLabel: s.showLabel,
                                    icons: s.icons,
                                    text: s.text
                                },
                                delete s.click,
                                delete s.icon,
                                delete s.iconPosition,
                                delete s.showLabel,
                                delete s.icons,
                                "boolean" == typeof s.text && delete s.text,
                                e("<button></button>", s).button(a).appendTo(t.uiButtonSet).on("click", function() {
                                    n.apply(t.element[0], arguments)
                                })
                        }),
                        this._addClass(this.uiDialog, "ui-dialog-buttons"),
                        void this.uiDialogButtonPane.appendTo(this.uiDialog))
            },
            _makeDraggable: function() {
                function t(e) {
                    return {
                        position: e.position,
                        offset: e.offset
                    }
                }
                var i = this,
                    s = this.options;
                this.uiDialog.draggable({
                    cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                    handle: ".ui-dialog-titlebar",
                    containment: "document",
                    start: function(s, n) {
                        i._addClass(e(this), "ui-dialog-dragging"),
                            i._blockFrames(),
                            i._trigger("dragStart", s, t(n))
                    },
                    drag: function(e, s) {
                        i._trigger("drag", e, t(s))
                    },
                    stop: function(n, a) {
                        var o = a.offset.left - i.document.scrollLeft(),
                            r = a.offset.top - i.document.scrollTop();
                        s.position = {
                                my: "left top",
                                at: "left" + (o >= 0 ? "+" : "") + o + " top" + (r >= 0 ? "+" : "") + r,
                                of: i.window
                            },
                            i._removeClass(e(this), "ui-dialog-dragging"),
                            i._unblockFrames(),
                            i._trigger("dragStop", n, t(a))
                    }
                })
            },
            _makeResizable: function() {
                function t(e) {
                    return {
                        originalPosition: e.originalPosition,
                        originalSize: e.originalSize,
                        position: e.position,
                        size: e.size
                    }
                }
                var i = this,
                    s = this.options,
                    n = s.resizable,
                    a = this.uiDialog.css("position"),
                    o = "string" == typeof n ? n : "n,e,s,w,se,sw,ne,nw";
                this.uiDialog.resizable({
                    cancel: ".ui-dialog-content",
                    containment: "document",
                    alsoResize: this.element,
                    maxWidth: s.maxWidth,
                    maxHeight: s.maxHeight,
                    minWidth: s.minWidth,
                    minHeight: this._minHeight(),
                    handles: o,
                    start: function(s, n) {
                        i._addClass(e(this), "ui-dialog-resizing"),
                            i._blockFrames(),
                            i._trigger("resizeStart", s, t(n))
                    },
                    resize: function(e, s) {
                        i._trigger("resize", e, t(s))
                    },
                    stop: function(n, a) {
                        var o = i.uiDialog.offset(),
                            r = o.left - i.document.scrollLeft(),
                            l = o.top - i.document.scrollTop();
                        s.height = i.uiDialog.height(),
                            s.width = i.uiDialog.width(),
                            s.position = {
                                my: "left top",
                                at: "left" + (r >= 0 ? "+" : "") + r + " top" + (l >= 0 ? "+" : "") + l,
                                of: i.window
                            },
                            i._removeClass(e(this), "ui-dialog-resizing"),
                            i._unblockFrames(),
                            i._trigger("resizeStop", n, t(a))
                    }
                }).css("position", a)
            },
            _trackFocus: function() {
                this._on(this.widget(), {
                    focusin: function(t) {
                        this._makeFocusTarget(),
                            this._focusedElement = e(t.target)
                    }
                })
            },
            _makeFocusTarget: function() {
                this._untrackInstance(),
                    this._trackingInstances().unshift(this)
            },
            _untrackInstance: function() {
                var t = this._trackingInstances(),
                    i = e.inArray(this, t); -
                1 !== i && t.splice(i, 1)
            },
            _trackingInstances: function() {
                var e = this.document.data("ui-dialog-instances");
                return e || this.document.data("ui-dialog-instances", e = []),
                    e
            },
            _minHeight: function() {
                var e = this.options;
                return "auto" === e.height ? e.minHeight : Math.min(e.minHeight, e.height)
            },
            _position: function() {
                var e = this.uiDialog.is(":visible");
                e || this.uiDialog.show(),
                    this.uiDialog.position(this.options.position),
                    e || this.uiDialog.hide()
            },
            _setOptions: function(t) {
                var i = this,
                    s = !1,
                    n = {};
                e.each(t, function(e, t) {
                        i._setOption(e, t),
                            e in i.sizeRelatedOptions && (s = !0),
                            e in i.resizableRelatedOptions && (n[e] = t)
                    }),
                    s && (this._size(),
                        this._position()),
                    this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", n)
            },
            _setOption: function(t, i) {
                var s, n, a = this.uiDialog;
                "disabled" !== t && (this._super(t, i),
                    "appendTo" === t && this.uiDialog.appendTo(this._appendTo()),
                    "buttons" === t && this._createButtons(),
                    "closeText" === t && this.uiDialogTitlebarClose.button({
                        label: e("<a>").text("" + this.options.closeText).html()
                    }),
                    "draggable" === t && ((s = a.is(":data(ui-draggable)")) && !i && a.draggable("destroy"), !s && i && this._makeDraggable()),
                    "position" === t && this._position(),
                    "resizable" === t && ((n = a.is(":data(ui-resizable)")) && !i && a.resizable("destroy"),
                        n && "string" == typeof i && a.resizable("option", "handles", i),
                        n || !1 === i || this._makeResizable()),
                    "title" === t && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")))
            },
            _size: function() {
                var e, t, i, s = this.options;
                this.element.show().css({
                        width: "auto",
                        minHeight: 0,
                        maxHeight: "none",
                        height: 0
                    }),
                    s.minWidth > s.width && (s.width = s.minWidth),
                    e = this.uiDialog.css({
                        height: "auto",
                        width: s.width
                    }).outerHeight(),
                    t = Math.max(0, s.minHeight - e),
                    i = "number" == typeof s.maxHeight ? Math.max(0, s.maxHeight - e) : "none",
                    "auto" === s.height ? this.element.css({
                        minHeight: t,
                        maxHeight: i,
                        height: "auto"
                    }) : this.element.height(Math.max(0, s.height - e)),
                    this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
            },
            _blockFrames: function() {
                this.iframeBlocks = this.document.find("iframe").map(function() {
                    var t = e(this);
                    return e("<div>").css({
                        position: "absolute",
                        width: t.outerWidth(),
                        height: t.outerHeight()
                    }).appendTo(t.parent()).offset(t.offset())[0]
                })
            },
            _unblockFrames: function() {
                this.iframeBlocks && (this.iframeBlocks.remove(),
                    delete this.iframeBlocks)
            },
            _allowInteraction: function(t) {
                return !!e(t.target).closest(".ui-dialog").length || !!e(t.target).closest(".ui-datepicker").length
            },
            _createOverlay: function() {
                if (this.options.modal) {
                    var t = !0;
                    this._delay(function() {
                            t = !1
                        }),
                        this.document.data("ui-dialog-overlays") || this._on(this.document, {
                            focusin: function(e) {
                                t || this._allowInteraction(e) || (e.preventDefault(),
                                    this._trackingInstances()[0]._focusTabbable())
                            }
                        }),
                        this.overlay = e("<div>").appendTo(this._appendTo()),
                        this._addClass(this.overlay, null, "ui-widget-overlay ui-front"),
                        this._on(this.overlay, {
                            mousedown: "_keepFocus"
                        }),
                        this.document.data("ui-dialog-overlays", (this.document.data("ui-dialog-overlays") || 0) + 1)
                }
            },
            _destroyOverlay: function() {
                if (this.options.modal && this.overlay) {
                    var e = this.document.data("ui-dialog-overlays") - 1;
                    e ? this.document.data("ui-dialog-overlays", e) : (this._off(this.document, "focusin"),
                            this.document.removeData("ui-dialog-overlays")),
                        this.overlay.remove(),
                        this.overlay = null
                }
            }
        }), !1 !== e.uiBackCompat && e.widget("ui.dialog", e.ui.dialog, {
            options: {
                dialogClass: ""
            },
            _createWrapper: function() {
                this._super(),
                    this.uiDialog.addClass(this.options.dialogClass)
            },
            _setOption: function(e, t) {
                "dialogClass" === e && this.uiDialog.removeClass(this.options.dialogClass).addClass(t),
                    this._superApply(arguments)
            }
        }),
        e.widget("ui.droppable", {
            version: "1.12.1",
            widgetEventPrefix: "drop",
            options: {
                accept: "*",
                addClasses: !0,
                greedy: !1,
                scope: "default",
                tolerance: "intersect",
                activate: null,
                deactivate: null,
                drop: null,
                out: null,
                over: null
            },
            _create: function() {
                var t, i = this.options,
                    s = i.accept;
                this.isover = !1,
                    this.isout = !0,
                    this.accept = e.isFunction(s) ? s : function(e) {
                        return e.is(s)
                    },
                    this.proportions = function() {
                        return arguments.length ? void(t = arguments[0]) : t || (t = {
                            width: this.element[0].offsetWidth,
                            height: this.element[0].offsetHeight
                        })
                    },
                    this._addToManager(i.scope),
                    i.addClasses && this._addClass("ui-droppable")
            },
            _addToManager: function(t) {
                e.ui.ddmanager.droppables[t] = e.ui.ddmanager.droppables[t] || [],
                    e.ui.ddmanager.droppables[t].push(this)
            },
            _splice: function(e) {
                for (var t = 0; e.length > t; t++)
                    e[t] === this && e.splice(t, 1)
            },
            _destroy: function() {
                this._splice(e.ui.ddmanager.droppables[this.options.scope])
            },
            _setOption: function(t, i) {
                "accept" === t ? this.accept = e.isFunction(i) ? i : function(e) {
                        return e.is(i)
                    } :
                    "scope" === t && (this._splice(e.ui.ddmanager.droppables[this.options.scope]),
                        this._addToManager(i)),
                    this._super(t, i)
            },
            _activate: function(t) {
                var i = e.ui.ddmanager.current;
                this._addActiveClass(),
                    i && this._trigger("activate", t, this.ui(i))
            },
            _deactivate: function(t) {
                var i = e.ui.ddmanager.current;
                this._removeActiveClass(),
                    i && this._trigger("deactivate", t, this.ui(i))
            },
            _over: function(t) {
                var i = e.ui.ddmanager.current;
                i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this._addHoverClass(),
                    this._trigger("over", t, this.ui(i)))
            },
            _out: function(t) {
                var i = e.ui.ddmanager.current;
                i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this._removeHoverClass(),
                    this._trigger("out", t, this.ui(i)))
            },
            _drop: function(t, i) {
                var s = i || e.ui.ddmanager.current,
                    n = !1;
                return !(!s || (s.currentItem || s.element)[0] === this.element[0]) && (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
                    var i = e(this).droppable("instance");
                    return i.options.greedy && !i.options.disabled && i.options.scope === s.options.scope && i.accept.call(i.element[0], s.currentItem || s.element) && m(s, e.extend(i, {
                        offset: i.element.offset()
                    }), i.options.tolerance, t) ? (n = !0, !1) : void 0
                }), !n && !!this.accept.call(this.element[0], s.currentItem || s.element) && (this._removeActiveClass(),
                    this._removeHoverClass(),
                    this._trigger("drop", t, this.ui(s)),
                    this.element))
            },
            ui: function(e) {
                return {
                    draggable: e.currentItem || e.element,
                    helper: e.helper,
                    position: e.position,
                    offset: e.positionAbs
                }
            },
            _addHoverClass: function() {
                this._addClass("ui-droppable-hover")
            },
            _removeHoverClass: function() {
                this._removeClass("ui-droppable-hover")
            },
            _addActiveClass: function() {
                this._addClass("ui-droppable-active")
            },
            _removeActiveClass: function() {
                this._removeClass("ui-droppable-active")
            }
        });
    var m = e.ui.intersect = function() {
        function e(e, t, i) {
            return e >= t && t + i > e
        }
        return function(t, i, s, n) {
            if (!i.offset)
                return !1;
            var a = (t.positionAbs || t.position.absolute).left + t.margins.left,
                o = (t.positionAbs || t.position.absolute).top + t.margins.top,
                r = a + t.helperProportions.width,
                l = o + t.helperProportions.height,
                h = i.offset.left,
                u = i.offset.top,
                c = h + i.proportions().width,
                d = u + i.proportions().height;
            switch (s) {
                case "fit":
                    return a >= h && c >= r && o >= u && d >= l;
                case "intersect":
                    return a + t.helperProportions.width / 2 > h && c > r - t.helperProportions.width / 2 && o + t.helperProportions.height / 2 > u && d > l - t.helperProportions.height / 2;
                case "pointer":
                    return e(n.pageY, u, i.proportions().height) && e(n.pageX, h, i.proportions().width);
                case "touch":
                    return (o >= u && d >= o || l >= u && d >= l || u > o && l > d) && (a >= h && c >= a || r >= h && c >= r || h > a && r > c);
                default:
                    return !1
            }
        }
    }();
    e.ui.ddmanager = {
            current: null,
            droppables: {
                default: []
            },
            prepareOffsets: function(t, i) {
                var s, n, a = e.ui.ddmanager.droppables[t.options.scope] || [],
                    o = i ? i.type : null,
                    r = (t.currentItem || t.element).find(":data(ui-droppable)").addBack();
                e: for (s = 0; a.length > s; s++)
                    if (!(a[s].options.disabled || t && !a[s].accept.call(a[s].element[0], t.currentItem || t.element))) {
                        for (n = 0; r.length > n; n++)
                            if (r[n] === a[s].element[0]) {
                                a[s].proportions().height = 0;
                                continue e
                            }
                        a[s].visible = "none" !== a[s].element.css("display"),
                            a[s].visible && ("mousedown" === o && a[s]._activate.call(a[s], i),
                                a[s].offset = a[s].element.offset(),
                                a[s].proportions({
                                    width: a[s].element[0].offsetWidth,
                                    height: a[s].element[0].offsetHeight
                                }))
                    }
            },
            drop: function(t, i) {
                var s = !1;
                return e.each((e.ui.ddmanager.droppables[t.options.scope] || []).slice(), function() {
                        this.options && (!this.options.disabled && this.visible && m(t, this, this.options.tolerance, i) && (s = this._drop.call(this, i) || s), !this.options.disabled && this.visible && this.accept.call(this.element[0], t.currentItem || t.element) && (this.isout = !0,
                            this.isover = !1,
                            this._deactivate.call(this, i)))
                    }),
                    s
            },
            dragStart: function(t, i) {
                t.element.parentsUntil("body").on("scroll.droppable", function() {
                    t.options.refreshPositions || e.ui.ddmanager.prepareOffsets(t, i)
                })
            },
            drag: function(t, i) {
                t.options.refreshPositions && e.ui.ddmanager.prepareOffsets(t, i),
                    e.each(e.ui.ddmanager.droppables[t.options.scope] || [], function() {
                        if (!this.options.disabled && !this.greedyChild && this.visible) {
                            var s, n, a, o = m(t, this, this.options.tolerance, i),
                                r = !o && this.isover ? "isout" : o && !this.isover ? "isover" : null;
                            r && (this.options.greedy && (n = this.options.scope,
                                    (a = this.element.parents(":data(ui-droppable)").filter(function() {
                                        return e(this).droppable("instance").options.scope === n
                                    })).length && ((s = e(a[0]).droppable("instance")).greedyChild = "isover" === r)),
                                s && "isover" === r && (s.isover = !1,
                                    s.isout = !0,
                                    s._out.call(s, i)),
                                this[r] = !0,
                                this["isout" === r ? "isover" : "isout"] = !1,
                                this["isover" === r ? "_over" : "_out"].call(this, i),
                                s && "isout" === r && (s.isout = !1,
                                    s.isover = !0,
                                    s._over.call(s, i)))
                        }
                    })
            },
            dragStop: function(t, i) {
                t.element.parentsUntil("body").off("scroll.droppable"),
                    t.options.refreshPositions || e.ui.ddmanager.prepareOffsets(t, i)
            }
        }, !1 !== e.uiBackCompat && e.widget("ui.droppable", e.ui.droppable, {
            options: {
                hoverClass: !1,
                activeClass: !1
            },
            _addActiveClass: function() {
                this._super(),
                    this.options.activeClass && this.element.addClass(this.options.activeClass)
            },
            _removeActiveClass: function() {
                this._super(),
                    this.options.activeClass && this.element.removeClass(this.options.activeClass)
            },
            _addHoverClass: function() {
                this._super(),
                    this.options.hoverClass && this.element.addClass(this.options.hoverClass)
            },
            _removeHoverClass: function() {
                this._super(),
                    this.options.hoverClass && this.element.removeClass(this.options.hoverClass)
            }
        }),
        e.widget("ui.progressbar", {
            version: "1.12.1",
            options: {
                classes: {
                    "ui-progressbar": "ui-corner-all",
                    "ui-progressbar-value": "ui-corner-left",
                    "ui-progressbar-complete": "ui-corner-right"
                },
                max: 100,
                value: 0,
                change: null,
                complete: null
            },
            min: 0,
            _create: function() {
                this.oldValue = this.options.value = this._constrainedValue(),
                    this.element.attr({
                        role: "progressbar",
                        "aria-valuemin": this.min
                    }),
                    this._addClass("ui-progressbar", "ui-widget ui-widget-content"),
                    this.valueDiv = e("<div>").appendTo(this.element),
                    this._addClass(this.valueDiv, "ui-progressbar-value", "ui-widget-header"),
                    this._refreshValue()
            },
            _destroy: function() {
                this.element.removeAttr("role aria-valuemin aria-valuemax aria-valuenow"),
                    this.valueDiv.remove()
            },
            value: function(e) {
                return void 0 === e ? this.options.value : (this.options.value = this._constrainedValue(e),
                    void this._refreshValue())
            },
            _constrainedValue: function(e) {
                return void 0 === e && (e = this.options.value),
                    this.indeterminate = !1 === e,
                    "number" != typeof e && (e = 0), !this.indeterminate && Math.min(this.options.max, Math.max(this.min, e))
            },
            _setOptions: function(e) {
                var t = e.value;
                delete e.value,
                    this._super(e),
                    this.options.value = this._constrainedValue(t),
                    this._refreshValue()
            },
            _setOption: function(e, t) {
                "max" === e && (t = Math.max(this.min, t)),
                    this._super(e, t)
            },
            _setOptionDisabled: function(e) {
                this._super(e),
                    this.element.attr("aria-disabled", e),
                    this._toggleClass(null, "ui-state-disabled", !!e)
            },
            _percentage: function() {
                return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min)
            },
            _refreshValue: function() {
                var t = this.options.value,
                    i = this._percentage();
                this.valueDiv.toggle(this.indeterminate || t > this.min).width(i.toFixed(0) + "%"),
                    this._toggleClass(this.valueDiv, "ui-progressbar-complete", null, t === this.options.max)._toggleClass("ui-progressbar-indeterminate", null, this.indeterminate),
                    this.indeterminate ? (this.element.removeAttr("aria-valuenow"),
                        this.overlayDiv || (this.overlayDiv = e("<div>").appendTo(this.valueDiv),
                            this._addClass(this.overlayDiv, "ui-progressbar-overlay"))) : (this.element.attr({
                            "aria-valuemax": this.options.max,
                            "aria-valuenow": t
                        }),
                        this.overlayDiv && (this.overlayDiv.remove(),
                            this.overlayDiv = null)),
                    this.oldValue !== t && (this.oldValue = t,
                        this._trigger("change")),
                    t === this.options.max && this._trigger("complete")
            }
        }),
        e.widget("ui.selectable", e.ui.mouse, {
            version: "1.12.1",
            options: {
                appendTo: "body",
                autoRefresh: !0,
                distance: 0,
                filter: "*",
                tolerance: "touch",
                selected: null,
                selecting: null,
                start: null,
                stop: null,
                unselected: null,
                unselecting: null
            },
            _create: function() {
                var t = this;
                this._addClass("ui-selectable"),
                    this.dragged = !1,
                    this.refresh = function() {
                        t.elementPos = e(t.element[0]).offset(),
                            t.selectees = e(t.options.filter, t.element[0]),
                            t._addClass(t.selectees, "ui-selectee"),
                            t.selectees.each(function() {
                                var i = e(this),
                                    s = i.offset(),
                                    n = {
                                        left: s.left - t.elementPos.left,
                                        top: s.top - t.elementPos.top
                                    };
                                e.data(this, "selectable-item", {
                                    element: this,
                                    $element: i,
                                    left: n.left,
                                    top: n.top,
                                    right: n.left + i.outerWidth(),
                                    bottom: n.top + i.outerHeight(),
                                    startselected: !1,
                                    selected: i.hasClass("ui-selected"),
                                    selecting: i.hasClass("ui-selecting"),
                                    unselecting: i.hasClass("ui-unselecting")
                                })
                            })
                    },
                    this.refresh(),
                    this._mouseInit(),
                    this.helper = e("<div>"),
                    this._addClass(this.helper, "ui-selectable-helper")
            },
            _destroy: function() {
                this.selectees.removeData("selectable-item"),
                    this._mouseDestroy()
            },
            _mouseStart: function(t) {
                var i = this,
                    s = this.options;
                this.opos = [t.pageX, t.pageY],
                    this.elementPos = e(this.element[0]).offset(),
                    this.options.disabled || (this.selectees = e(s.filter, this.element[0]),
                        this._trigger("start", t),
                        e(s.appendTo).append(this.helper),
                        this.helper.css({
                            left: t.pageX,
                            top: t.pageY,
                            width: 0,
                            height: 0
                        }),
                        s.autoRefresh && this.refresh(),
                        this.selectees.filter(".ui-selected").each(function() {
                            var s = e.data(this, "selectable-item");
                            s.startselected = !0,
                                t.metaKey || t.ctrlKey || (i._removeClass(s.$element, "ui-selected"),
                                    s.selected = !1,
                                    i._addClass(s.$element, "ui-unselecting"),
                                    s.unselecting = !0,
                                    i._trigger("unselecting", t, {
                                        unselecting: s.element
                                    }))
                        }),
                        e(t.target).parents().addBack().each(function() {
                            var s, n = e.data(this, "selectable-item");
                            return n ? (s = !t.metaKey && !t.ctrlKey || !n.$element.hasClass("ui-selected"),
                                i._removeClass(n.$element, s ? "ui-unselecting" : "ui-selected")._addClass(n.$element, s ? "ui-selecting" : "ui-unselecting"),
                                n.unselecting = !s,
                                n.selecting = s,
                                n.selected = s,
                                s ? i._trigger("selecting", t, {
                                    selecting: n.element
                                }) : i._trigger("unselecting", t, {
                                    unselecting: n.element
                                }), !1) : void 0
                        }))
            },
            _mouseDrag: function(t) {
                if (this.dragged = !0, !this.options.disabled) {
                    var i, s = this,
                        n = this.options,
                        a = this.opos[0],
                        o = this.opos[1],
                        r = t.pageX,
                        l = t.pageY;
                    return a > r && (i = r,
                            r = a,
                            a = i),
                        o > l && (i = l,
                            l = o,
                            o = i),
                        this.helper.css({
                            left: a,
                            top: o,
                            width: r - a,
                            height: l - o
                        }),
                        this.selectees.each(function() {
                            var i = e.data(this, "selectable-item"),
                                h = !1,
                                u = {};
                            i && i.element !== s.element[0] && (u.left = i.left + s.elementPos.left,
                                u.right = i.right + s.elementPos.left,
                                u.top = i.top + s.elementPos.top,
                                u.bottom = i.bottom + s.elementPos.top,
                                "touch" === n.tolerance ? h = !(u.left > r || a > u.right || u.top > l || o > u.bottom) : "fit" === n.tolerance && (h = u.left > a && r > u.right && u.top > o && l > u.bottom),
                                h ? (i.selected && (s._removeClass(i.$element, "ui-selected"),
                                        i.selected = !1),
                                    i.unselecting && (s._removeClass(i.$element, "ui-unselecting"),
                                        i.unselecting = !1),
                                    i.selecting || (s._addClass(i.$element, "ui-selecting"),
                                        i.selecting = !0,
                                        s._trigger("selecting", t, {
                                            selecting: i.element
                                        }))) : (i.selecting && ((t.metaKey || t.ctrlKey) && i.startselected ? (s._removeClass(i.$element, "ui-selecting"),
                                        i.selecting = !1,
                                        s._addClass(i.$element, "ui-selected"),
                                        i.selected = !0) : (s._removeClass(i.$element, "ui-selecting"),
                                        i.selecting = !1,
                                        i.startselected && (s._addClass(i.$element, "ui-unselecting"),
                                            i.unselecting = !0),
                                        s._trigger("unselecting", t, {
                                            unselecting: i.element
                                        }))),
                                    i.selected && (t.metaKey || t.ctrlKey || i.startselected || (s._removeClass(i.$element, "ui-selected"),
                                        i.selected = !1,
                                        s._addClass(i.$element, "ui-unselecting"),
                                        i.unselecting = !0,
                                        s._trigger("unselecting", t, {
                                            unselecting: i.element
                                        })))))
                        }), !1
                }
            },
            _mouseStop: function(t) {
                var i = this;
                return this.dragged = !1,
                    e(".ui-unselecting", this.element[0]).each(function() {
                        var s = e.data(this, "selectable-item");
                        i._removeClass(s.$element, "ui-unselecting"),
                            s.unselecting = !1,
                            s.startselected = !1,
                            i._trigger("unselected", t, {
                                unselected: s.element
                            })
                    }),
                    e(".ui-selecting", this.element[0]).each(function() {
                        var s = e.data(this, "selectable-item");
                        i._removeClass(s.$element, "ui-selecting")._addClass(s.$element, "ui-selected"),
                            s.selecting = !1,
                            s.selected = !0,
                            s.startselected = !0,
                            i._trigger("selected", t, {
                                selected: s.element
                            })
                    }),
                    this._trigger("stop", t),
                    this.helper.remove(), !1
            }
        }),
        e.widget("ui.selectmenu", [e.ui.formResetMixin, {
            version: "1.12.1",
            defaultElement: "<select>",
            options: {
                appendTo: null,
                classes: {
                    "ui-selectmenu-button-open": "ui-corner-top",
                    "ui-selectmenu-button-closed": "ui-corner-all"
                },
                disabled: null,
                icons: {
                    button: "ui-icon-triangle-1-s"
                },
                position: {
                    my: "left top",
                    at: "left bottom",
                    collision: "none"
                },
                width: !1,
                change: null,
                close: null,
                focus: null,
                open: null,
                select: null
            },
            _create: function() {
                var t = this.element.uniqueId().attr("id");
                this.ids = {
                        element: t,
                        button: t + "-button",
                        menu: t + "-menu"
                    },
                    this._drawButton(),
                    this._drawMenu(),
                    this._bindFormResetHandler(),
                    this._rendered = !1,
                    this.menuItems = e()
            },
            _drawButton: function() {
                var t, i = this,
                    s = this._parseOption(this.element.find("option:selected"), this.element[0].selectedIndex);
                this.labels = this.element.labels().attr("for", this.ids.button),
                    this._on(this.labels, {
                        click: function(e) {
                            this.button.focus(),
                                e.preventDefault()
                        }
                    }),
                    this.element.hide(),
                    this.button = e("<span>", {
                        tabindex: this.options.disabled ? -1 : 0,
                        id: this.ids.button,
                        role: "combobox",
                        "aria-expanded": "false",
                        "aria-autocomplete": "list",
                        "aria-owns": this.ids.menu,
                        "aria-haspopup": "true",
                        title: this.element.attr("title")
                    }).insertAfter(this.element),
                    this._addClass(this.button, "ui-selectmenu-button ui-selectmenu-button-closed", "ui-button ui-widget"),
                    t = e("<span>").appendTo(this.button),
                    this._addClass(t, "ui-selectmenu-icon", "ui-icon " + this.options.icons.button),
                    this.buttonItem = this._renderButtonItem(s).appendTo(this.button), !1 !== this.options.width && this._resizeButton(),
                    this._on(this.button, this._buttonEvents),
                    this.button.one("focusin", function() {
                        i._rendered || i._refreshMenu()
                    })
            },
            _drawMenu: function() {
                var t = this;
                this.menu = e("<ul>", {
                        "aria-hidden": "true",
                        "aria-labelledby": this.ids.button,
                        id: this.ids.menu
                    }),
                    this.menuWrap = e("<div>").append(this.menu),
                    this._addClass(this.menuWrap, "ui-selectmenu-menu", "ui-front"),
                    this.menuWrap.appendTo(this._appendTo()),
                    this.menuInstance = this.menu.menu({
                        classes: {
                            "ui-menu": "ui-corner-bottom"
                        },
                        role: "listbox",
                        select: function(e, i) {
                            e.preventDefault(),
                                t._setSelection(),
                                t._select(i.item.data("ui-selectmenu-item"), e)
                        },
                        focus: function(e, i) {
                            var s = i.item.data("ui-selectmenu-item");
                            null != t.focusIndex && s.index !== t.focusIndex && (t._trigger("focus", e, {
                                        item: s
                                    }),
                                    t.isOpen || t._select(s, e)),
                                t.focusIndex = s.index,
                                t.button.attr("aria-activedescendant", t.menuItems.eq(s.index).attr("id"))
                        }
                    }).menu("instance"),
                    this.menuInstance._off(this.menu, "mouseleave"),
                    this.menuInstance._closeOnDocumentClick = function() {
                        return !1
                    },
                    this.menuInstance._isDivider = function() {
                        return !1
                    }
            },
            refresh: function() {
                this._refreshMenu(),
                    this.buttonItem.replaceWith(this.buttonItem = this._renderButtonItem(this._getSelectedItem().data("ui-selectmenu-item") || {})),
                    null === this.options.width && this._resizeButton()
            },
            _refreshMenu: function() {
                var e, t = this.element.find("option");
                this.menu.empty(),
                    this._parseOptions(t),
                    this._renderMenu(this.menu, this.items),
                    this.menuInstance.refresh(),
                    this.menuItems = this.menu.find("li").not(".ui-selectmenu-optgroup").find(".ui-menu-item-wrapper"),
                    this._rendered = !0,
                    t.length && (e = this._getSelectedItem(),
                        this.menuInstance.focus(null, e),
                        this._setAria(e.data("ui-selectmenu-item")),
                        this._setOption("disabled", this.element.prop("disabled")))
            },
            open: function(e) {
                this.options.disabled || (this._rendered ? (this._removeClass(this.menu.find(".ui-state-active"), null, "ui-state-active"),
                        this.menuInstance.focus(null, this._getSelectedItem())) : this._refreshMenu(),
                    this.menuItems.length && (this.isOpen = !0,
                        this._toggleAttr(),
                        this._resizeMenu(),
                        this._position(),
                        this._on(this.document, this._documentClick),
                        this._trigger("open", e)))
            },
            _position: function() {
                this.menuWrap.position(e.extend({
                    of: this.button
                }, this.options.position))
            },
            close: function(e) {
                this.isOpen && (this.isOpen = !1,
                    this._toggleAttr(),
                    this.range = null,
                    this._off(this.document),
                    this._trigger("close", e))
            },
            widget: function() {
                return this.button
            },
            menuWidget: function() {
                return this.menu
            },
            _renderButtonItem: function(t) {
                var i = e("<span>");
                return this._setText(i, t.label),
                    this._addClass(i, "ui-selectmenu-text"),
                    i
            },
            _renderMenu: function(t, i) {
                var s = this,
                    n = "";
                e.each(i, function(i, a) {
                    var o;
                    a.optgroup !== n && (o = e("<li>", {
                                text: a.optgroup
                            }),
                            s._addClass(o, "ui-selectmenu-optgroup", "ui-menu-divider" + (a.element.parent("optgroup").prop("disabled") ? " ui-state-disabled" : "")),
                            o.appendTo(t),
                            n = a.optgroup),
                        s._renderItemData(t, a)
                })
            },
            _renderItemData: function(e, t) {
                return this._renderItem(e, t).data("ui-selectmenu-item", t)
            },
            _renderItem: function(t, i) {
                var s = e("<li>"),
                    n = e("<div>", {
                        title: i.element.attr("title")
                    });
                return i.disabled && this._addClass(s, null, "ui-state-disabled"),
                    this._setText(n, i.label),
                    s.append(n).appendTo(t)
            },
            _setText: function(e, t) {
                t ? e.text(t) : e.html("&#160;")
            },
            _move: function(e, t) {
                var i, s, n = ".ui-menu-item";
                this.isOpen ? i = this.menuItems.eq(this.focusIndex).parent("li") : (i = this.menuItems.eq(this.element[0].selectedIndex).parent("li"),
                        n += ":not(.ui-state-disabled)"),
                    (s = "first" === e || "last" === e ? i["first" === e ? "prevAll" : "nextAll"](n).eq(-1) : i[e + "All"](n).eq(0)).length && this.menuInstance.focus(t, s)
            },
            _getSelectedItem: function() {
                return this.menuItems.eq(this.element[0].selectedIndex).parent("li")
            },
            _toggle: function(e) {
                this[this.isOpen ? "close" : "open"](e)
            },
            _setSelection: function() {
                var e;
                this.range && (window.getSelection ? ((e = window.getSelection()).removeAllRanges(),
                        e.addRange(this.range)) : this.range.select(),
                    this.button.focus())
            },
            _documentClick: {
                mousedown: function(t) {
                    this.isOpen && (e(t.target).closest(".ui-selectmenu-menu, #" + e.ui.escapeSelector(this.ids.button)).length || this.close(t))
                }
            },
            _buttonEvents: {
                mousedown: function() {
                    var e;
                    window.getSelection ? (e = window.getSelection()).rangeCount && (this.range = e.getRangeAt(0)) : this.range = document.selection.createRange()
                },
                click: function(e) {
                    this._setSelection(),
                        this._toggle(e)
                },
                keydown: function(t) {
                    var i = !0;
                    switch (t.keyCode) {
                        case e.ui.keyCode.TAB:
                        case e.ui.keyCode.ESCAPE:
                            this.close(t),
                                i = !1;
                            break;
                        case e.ui.keyCode.ENTER:
                            this.isOpen && this._selectFocusedItem(t);
                            break;
                        case e.ui.keyCode.UP:
                            t.altKey ? this._toggle(t) : this._move("prev", t);
                            break;
                        case e.ui.keyCode.DOWN:
                            t.altKey ? this._toggle(t) : this._move("next", t);
                            break;
                        case e.ui.keyCode.SPACE:
                            this.isOpen ? this._selectFocusedItem(t) : this._toggle(t);
                            break;
                        case e.ui.keyCode.LEFT:
                            this._move("prev", t);
                            break;
                        case e.ui.keyCode.RIGHT:
                            this._move("next", t);
                            break;
                        case e.ui.keyCode.HOME:
                        case e.ui.keyCode.PAGE_UP:
                            this._move("first", t);
                            break;
                        case e.ui.keyCode.END:
                        case e.ui.keyCode.PAGE_DOWN:
                            this._move("last", t);
                            break;
                        default:
                            this.menu.trigger(t),
                                i = !1
                    }
                    i && t.preventDefault()
                }
            },
            _selectFocusedItem: function(e) {
                var t = this.menuItems.eq(this.focusIndex).parent("li");
                t.hasClass("ui-state-disabled") || this._select(t.data("ui-selectmenu-item"), e)
            },
            _select: function(e, t) {
                var i = this.element[0].selectedIndex;
                this.element[0].selectedIndex = e.index,
                    this.buttonItem.replaceWith(this.buttonItem = this._renderButtonItem(e)),
                    this._setAria(e),
                    this._trigger("select", t, {
                        item: e
                    }),
                    e.index !== i && this._trigger("change", t, {
                        item: e
                    }),
                    this.close(t)
            },
            _setAria: function(e) {
                var t = this.menuItems.eq(e.index).attr("id");
                this.button.attr({
                        "aria-labelledby": t,
                        "aria-activedescendant": t
                    }),
                    this.menu.attr("aria-activedescendant", t)
            },
            _setOption: function(e, t) {
                if ("icons" === e) {
                    var i = this.button.find("span.ui-icon");
                    this._removeClass(i, null, this.options.icons.button)._addClass(i, null, t.button)
                }
                this._super(e, t),
                    "appendTo" === e && this.menuWrap.appendTo(this._appendTo()),
                    "width" === e && this._resizeButton()
            },
            _setOptionDisabled: function(e) {
                this._super(e),
                    this.menuInstance.option("disabled", e),
                    this.button.attr("aria-disabled", e),
                    this._toggleClass(this.button, null, "ui-state-disabled", e),
                    this.element.prop("disabled", e),
                    e ? (this.button.attr("tabindex", -1),
                        this.close()) : this.button.attr("tabindex", 0)
            },
            _appendTo: function() {
                var t = this.options.appendTo;
                return t && (t = t.jquery || t.nodeType ? e(t) : this.document.find(t).eq(0)),
                    t && t[0] || (t = this.element.closest(".ui-front, dialog")),
                    t.length || (t = this.document[0].body),
                    t
            },
            _toggleAttr: function() {
                this.button.attr("aria-expanded", this.isOpen),
                    this._removeClass(this.button, "ui-selectmenu-button-" + (this.isOpen ? "closed" : "open"))._addClass(this.button, "ui-selectmenu-button-" + (this.isOpen ? "open" : "closed"))._toggleClass(this.menuWrap, "ui-selectmenu-open", null, this.isOpen),
                    this.menu.attr("aria-hidden", !this.isOpen)
            },
            _resizeButton: function() {
                var e = this.options.width;
                return !1 === e ? void this.button.css("width", "") : (null === e && (e = this.element.show().outerWidth(),
                        this.element.hide()),
                    void this.button.outerWidth(e))
            },
            _resizeMenu: function() {
                this.menu.outerWidth(Math.max(this.button.outerWidth(), this.menu.width("").outerWidth() + 1))
            },
            _getCreateOptions: function() {
                var e = this._super();
                return e.disabled = this.element.prop("disabled"),
                    e
            },
            _parseOptions: function(t) {
                var i = this,
                    s = [];
                t.each(function(t, n) {
                        s.push(i._parseOption(e(n), t))
                    }),
                    this.items = s
            },
            _parseOption: function(e, t) {
                var i = e.parent("optgroup");
                return {
                    element: e,
                    index: t,
                    value: e.val(),
                    label: e.text(),
                    optgroup: i.attr("label") || "",
                    disabled: i.prop("disabled") || e.prop("disabled")
                }
            },
            _destroy: function() {
                this._unbindFormResetHandler(),
                    this.menuWrap.remove(),
                    this.button.remove(),
                    this.element.show(),
                    this.element.removeUniqueId(),
                    this.labels.attr("for", this.ids.element)
            }
        }]),
        e.widget("ui.slider", e.ui.mouse, {
            version: "1.12.1",
            widgetEventPrefix: "slide",
            options: {
                animate: !1,
                classes: {
                    "ui-slider": "ui-corner-all",
                    "ui-slider-handle": "ui-corner-all",
                    "ui-slider-range": "ui-corner-all ui-widget-header"
                },
                distance: 0,
                max: 100,
                min: 0,
                orientation: "horizontal",
                range: !1,
                step: 1,
                value: 0,
                values: null,
                change: null,
                slide: null,
                start: null,
                stop: null
            },
            numPages: 5,
            _create: function() {
                this._keySliding = !1,
                    this._mouseSliding = !1,
                    this._animateOff = !0,
                    this._handleIndex = null,
                    this._detectOrientation(),
                    this._mouseInit(),
                    this._calculateNewMax(),
                    this._addClass("ui-slider ui-slider-" + this.orientation, "ui-widget ui-widget-content"),
                    this._refresh(),
                    this._animateOff = !1
            },
            _refresh: function() {
                this._createRange(),
                    this._createHandles(),
                    this._setupEvents(),
                    this._refreshValue()
            },
            _createHandles: function() {
                var t, i, s = this.options,
                    n = this.element.find(".ui-slider-handle"),
                    a = [];
                for (n.length > (i = s.values && s.values.length || 1) && (n.slice(i).remove(),
                        n = n.slice(0, i)),
                    t = n.length; i > t; t++)
                    a.push("<span tabindex='0'></span>");
                this.handles = n.add(e(a.join("")).appendTo(this.element)),
                    this._addClass(this.handles, "ui-slider-handle", "ui-state-default"),
                    this.handle = this.handles.eq(0),
                    this.handles.each(function(t) {
                        e(this).data("ui-slider-handle-index", t).attr("tabIndex", 0)
                    })
            },
            _createRange: function() {
                var t = this.options;
                t.range ? (!0 === t.range && (t.values ? t.values.length && 2 !== t.values.length ? t.values = [t.values[0], t.values[0]] : e.isArray(t.values) && (t.values = t.values.slice(0)) : t.values = [this._valueMin(), this._valueMin()]),
                    this.range && this.range.length ? (this._removeClass(this.range, "ui-slider-range-min ui-slider-range-max"),
                        this.range.css({
                            left: "",
                            bottom: ""
                        })) : (this.range = e("<div>").appendTo(this.element),
                        this._addClass(this.range, "ui-slider-range")),
                    ("min" === t.range || "max" === t.range) && this._addClass(this.range, "ui-slider-range-" + t.range)) : (this.range && this.range.remove(),
                    this.range = null)
            },
            _setupEvents: function() {
                this._off(this.handles),
                    this._on(this.handles, this._handleEvents),
                    this._hoverable(this.handles),
                    this._focusable(this.handles)
            },
            _destroy: function() {
                this.handles.remove(),
                    this.range && this.range.remove(),
                    this._mouseDestroy()
            },
            _mouseCapture: function(t) {
                var i, s, n, a, o, r, l = this,
                    h = this.options;
                return !h.disabled && (this.elementSize = {
                        width: this.element.outerWidth(),
                        height: this.element.outerHeight()
                    },
                    this.elementOffset = this.element.offset(),
                    i = this._normValueFromMouse({
                        x: t.pageX,
                        y: t.pageY
                    }),
                    s = this._valueMax() - this._valueMin() + 1,
                    this.handles.each(function(t) {
                        var o = Math.abs(i - l.values(t));
                        (s > o || s === o && (t === l._lastChangedValue || l.values(t) === h.min)) && (s = o,
                            n = e(this),
                            a = t)
                    }), !1 !== this._start(t, a) && (this._mouseSliding = !0,
                        this._handleIndex = a,
                        this._addClass(n, null, "ui-state-active"),
                        n.trigger("focus"),
                        o = n.offset(),
                        r = !e(t.target).parents().addBack().is(".ui-slider-handle"),
                        this._clickOffset = r ? {
                            left: 0,
                            top: 0
                        } : {
                            left: t.pageX - o.left - n.width() / 2,
                            top: t.pageY - o.top - n.height() / 2 - (parseInt(n.css("borderTopWidth"), 10) || 0) - (parseInt(n.css("borderBottomWidth"), 10) || 0) + (parseInt(n.css("marginTop"), 10) || 0)
                        },
                        this.handles.hasClass("ui-state-hover") || this._slide(t, a, i),
                        this._animateOff = !0, !0))
            },
            _mouseStart: function() {
                return !0
            },
            _mouseDrag: function(e) {
                var t = this._normValueFromMouse({
                    x: e.pageX,
                    y: e.pageY
                });
                return this._slide(e, this._handleIndex, t), !1
            },
            _mouseStop: function(e) {
                return this._removeClass(this.handles, null, "ui-state-active"),
                    this._mouseSliding = !1,
                    this._stop(e, this._handleIndex),
                    this._change(e, this._handleIndex),
                    this._handleIndex = null,
                    this._clickOffset = null,
                    this._animateOff = !1, !1
            },
            _detectOrientation: function() {
                this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
            },
            _normValueFromMouse: function(e) {
                var t, i, s, n, a;
                return "horizontal" === this.orientation ? (t = this.elementSize.width,
                        i = e.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (t = this.elementSize.height,
                        i = e.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)),
                    (s = i / t) > 1 && (s = 1),
                    0 > s && (s = 0),
                    "vertical" === this.orientation && (s = 1 - s),
                    n = this._valueMax() - this._valueMin(),
                    a = this._valueMin() + s * n,
                    this._trimAlignValue(a)
            },
            _uiHash: function(e, t, i) {
                var s = {
                    handle: this.handles[e],
                    handleIndex: e,
                    value: void 0 !== t ? t : this.value()
                };
                return this._hasMultipleValues() && (s.value = void 0 !== t ? t : this.values(e),
                        s.values = i || this.values()),
                    s
            },
            _hasMultipleValues: function() {
                return this.options.values && this.options.values.length
            },
            _start: function(e, t) {
                return this._trigger("start", e, this._uiHash(t))
            },
            _slide: function(e, t, i) {
                var s, n = this.value(),
                    a = this.values();
                this._hasMultipleValues() && (s = this.values(t ? 0 : 1),
                        n = this.values(t),
                        2 === this.options.values.length && !0 === this.options.range && (i = 0 === t ? Math.min(s, i) : Math.max(s, i)),
                        a[t] = i),
                    i !== n && !1 !== this._trigger("slide", e, this._uiHash(t, i, a)) && (this._hasMultipleValues() ? this.values(t, i) : this.value(i))
            },
            _stop: function(e, t) {
                this._trigger("stop", e, this._uiHash(t))
            },
            _change: function(e, t) {
                this._keySliding || this._mouseSliding || (this._lastChangedValue = t,
                    this._trigger("change", e, this._uiHash(t)))
            },
            value: function(e) {
                return arguments.length ? (this.options.value = this._trimAlignValue(e),
                    this._refreshValue(),
                    void this._change(null, 0)) : this._value()
            },
            values: function(t, i) {
                var s, n, a;
                if (arguments.length > 1)
                    return this.options.values[t] = this._trimAlignValue(i),
                        this._refreshValue(),
                        void this._change(null, t);
                if (!arguments.length)
                    return this._values();
                if (!e.isArray(arguments[0]))
                    return this._hasMultipleValues() ? this._values(t) : this.value();
                for (s = this.options.values,
                    n = arguments[0],
                    a = 0; s.length > a; a += 1)
                    s[a] = this._trimAlignValue(n[a]),
                    this._change(null, a);
                this._refreshValue()
            },
            _setOption: function(t, i) {
                var s, n = 0;
                switch ("range" === t && !0 === this.options.range && ("min" === i ? (this.options.value = this._values(0),
                        this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1),
                        this.options.values = null)),
                    e.isArray(this.options.values) && (n = this.options.values.length),
                    this._super(t, i),
                    t) {
                    case "orientation":
                        this._detectOrientation(),
                            this._removeClass("ui-slider-horizontal ui-slider-vertical")._addClass("ui-slider-" + this.orientation),
                            this._refreshValue(),
                            this.options.range && this._refreshRange(i),
                            this.handles.css("horizontal" === i ? "bottom" : "left", "");
                        break;
                    case "value":
                        this._animateOff = !0,
                            this._refreshValue(),
                            this._change(null, 0),
                            this._animateOff = !1;
                        break;
                    case "values":
                        for (this._animateOff = !0,
                            this._refreshValue(),
                            s = n - 1; s >= 0; s--)
                            this._change(null, s);
                        this._animateOff = !1;
                        break;
                    case "step":
                    case "min":
                    case "max":
                        this._animateOff = !0,
                            this._calculateNewMax(),
                            this._refreshValue(),
                            this._animateOff = !1;
                        break;
                    case "range":
                        this._animateOff = !0,
                            this._refresh(),
                            this._animateOff = !1
                }
            },
            _setOptionDisabled: function(e) {
                this._super(e),
                    this._toggleClass(null, "ui-state-disabled", !!e)
            },
            _value: function() {
                return this._trimAlignValue(this.options.value)
            },
            _values: function(e) {
                var t, i;
                if (arguments.length)
                    return this._trimAlignValue(this.options.values[e]);
                if (this._hasMultipleValues()) {
                    for (t = this.options.values.slice(),
                        i = 0; t.length > i; i += 1)
                        t[i] = this._trimAlignValue(t[i]);
                    return t
                }
                return []
            },
            _trimAlignValue: function(e) {
                if (this._valueMin() >= e)
                    return this._valueMin();
                if (e >= this._valueMax())
                    return this._valueMax();
                var t = this.options.step > 0 ? this.options.step : 1,
                    i = (e - this._valueMin()) % t,
                    s = e - i;
                return 2 * Math.abs(i) >= t && (s += i > 0 ? t : -t),
                    parseFloat(s.toFixed(5))
            },
            _calculateNewMax: function() {
                var e = this.options.max,
                    t = this._valueMin(),
                    i = this.options.step;
                (e = Math.round((e - t) / i) * i + t) > this.options.max && (e -= i),
                    this.max = parseFloat(e.toFixed(this._precision()))
            },
            _precision: function() {
                var e = this._precisionOf(this.options.step);
                return null !== this.options.min && (e = Math.max(e, this._precisionOf(this.options.min))),
                    e
            },
            _precisionOf: function(e) {
                var t = "" + e,
                    i = t.indexOf(".");
                return -1 === i ? 0 : t.length - i - 1
            },
            _valueMin: function() {
                return this.options.min
            },
            _valueMax: function() {
                return this.max
            },
            _refreshRange: function(e) {
                "vertical" === e && this.range.css({
                        width: "",
                        left: ""
                    }),
                    "horizontal" === e && this.range.css({
                        height: "",
                        bottom: ""
                    })
            },
            _refreshValue: function() {
                var t, i, s, n, a, o = this.options.range,
                    r = this.options,
                    l = this,
                    h = !this._animateOff && r.animate,
                    u = {};
                this._hasMultipleValues() ? this.handles.each(function(s) {
                    i = (l.values(s) - l._valueMin()) / (l._valueMax() - l._valueMin()) * 100,
                        u["horizontal" === l.orientation ? "left" : "bottom"] = i + "%",
                        e(this).stop(1, 1)[h ? "animate" : "css"](u, r.animate), !0 === l.options.range && ("horizontal" === l.orientation ? (0 === s && l.range.stop(1, 1)[h ? "animate" : "css"]({
                                left: i + "%"
                            }, r.animate),
                            1 === s && l.range[h ? "animate" : "css"]({
                                width: i - t + "%"
                            }, {
                                queue: !1,
                                duration: r.animate
                            })) : (0 === s && l.range.stop(1, 1)[h ? "animate" : "css"]({
                                bottom: i + "%"
                            }, r.animate),
                            1 === s && l.range[h ? "animate" : "css"]({
                                height: i - t + "%"
                            }, {
                                queue: !1,
                                duration: r.animate
                            }))),
                        t = i
                }) : (s = this.value(),
                    n = this._valueMin(),
                    a = this._valueMax(),
                    u["horizontal" === this.orientation ? "left" : "bottom"] = (i = a !== n ? (s - n) / (a - n) * 100 : 0) + "%",
                    this.handle.stop(1, 1)[h ? "animate" : "css"](u, r.animate),
                    "min" === o && "horizontal" === this.orientation && this.range.stop(1, 1)[h ? "animate" : "css"]({
                        width: i + "%"
                    }, r.animate),
                    "max" === o && "horizontal" === this.orientation && this.range.stop(1, 1)[h ? "animate" : "css"]({
                        width: 100 - i + "%"
                    }, r.animate),
                    "min" === o && "vertical" === this.orientation && this.range.stop(1, 1)[h ? "animate" : "css"]({
                        height: i + "%"
                    }, r.animate),
                    "max" === o && "vertical" === this.orientation && this.range.stop(1, 1)[h ? "animate" : "css"]({
                        height: 100 - i + "%"
                    }, r.animate))
            },
            _handleEvents: {
                keydown: function(t) {
                    var i, s, n, a = e(t.target).data("ui-slider-handle-index");
                    switch (t.keyCode) {
                        case e.ui.keyCode.HOME:
                        case e.ui.keyCode.END:
                        case e.ui.keyCode.PAGE_UP:
                        case e.ui.keyCode.PAGE_DOWN:
                        case e.ui.keyCode.UP:
                        case e.ui.keyCode.RIGHT:
                        case e.ui.keyCode.DOWN:
                        case e.ui.keyCode.LEFT:
                            if (t.preventDefault(), !this._keySliding && (this._keySliding = !0,
                                    this._addClass(e(t.target), null, "ui-state-active"), !1 === this._start(t, a)))
                                return
                    }
                    switch (n = this.options.step,
                        i = s = this._hasMultipleValues() ? this.values(a) : this.value(),
                        t.keyCode) {
                        case e.ui.keyCode.HOME:
                            s = this._valueMin();
                            break;
                        case e.ui.keyCode.END:
                            s = this._valueMax();
                            break;
                        case e.ui.keyCode.PAGE_UP:
                            s = this._trimAlignValue(i + (this._valueMax() - this._valueMin()) / this.numPages);
                            break;
                        case e.ui.keyCode.PAGE_DOWN:
                            s = this._trimAlignValue(i - (this._valueMax() - this._valueMin()) / this.numPages);
                            break;
                        case e.ui.keyCode.UP:
                        case e.ui.keyCode.RIGHT:
                            if (i === this._valueMax())
                                return;
                            s = this._trimAlignValue(i + n);
                            break;
                        case e.ui.keyCode.DOWN:
                        case e.ui.keyCode.LEFT:
                            if (i === this._valueMin())
                                return;
                            s = this._trimAlignValue(i - n)
                    }
                    this._slide(t, a, s)
                },
                keyup: function(t) {
                    var i = e(t.target).data("ui-slider-handle-index");
                    this._keySliding && (this._keySliding = !1,
                        this._stop(t, i),
                        this._change(t, i),
                        this._removeClass(e(t.target), null, "ui-state-active"))
                }
            }
        }),
        e.widget("ui.sortable", e.ui.mouse, {
            version: "1.12.1",
            widgetEventPrefix: "sort",
            ready: !1,
            options: {
                appendTo: "parent",
                axis: !1,
                connectWith: !1,
                containment: !1,
                cursor: "auto",
                cursorAt: !1,
                dropOnEmpty: !0,
                forcePlaceholderSize: !1,
                forceHelperSize: !1,
                grid: !1,
                handle: !1,
                helper: "original",
                items: "> *",
                opacity: !1,
                placeholder: !1,
                revert: !1,
                scroll: !0,
                scrollSensitivity: 20,
                scrollSpeed: 20,
                scope: "default",
                tolerance: "intersect",
                zIndex: 1e3,
                activate: null,
                beforeStop: null,
                change: null,
                deactivate: null,
                out: null,
                over: null,
                receive: null,
                remove: null,
                sort: null,
                start: null,
                stop: null,
                update: null
            },
            _isOverAxis: function(e, t, i) {
                return e >= t && t + i > e
            },
            _isFloating: function(e) {
                return /left|right/.test(e.css("float")) || /inline|table-cell/.test(e.css("display"))
            },
            _create: function() {
                this.containerCache = {},
                    this._addClass("ui-sortable"),
                    this.refresh(),
                    this.offset = this.element.offset(),
                    this._mouseInit(),
                    this._setHandleClassName(),
                    this.ready = !0
            },
            _setOption: function(e, t) {
                this._super(e, t),
                    "handle" === e && this._setHandleClassName()
            },
            _setHandleClassName: function() {
                var t = this;
                this._removeClass(this.element.find(".ui-sortable-handle"), "ui-sortable-handle"),
                    e.each(this.items, function() {
                        t._addClass(this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item, "ui-sortable-handle")
                    })
            },
            _destroy: function() {
                this._mouseDestroy();
                for (var e = this.items.length - 1; e >= 0; e--)
                    this.items[e].item.removeData(this.widgetName + "-item");
                return this
            },
            _mouseCapture: function(t, i) {
                var s = null,
                    n = !1,
                    a = this;
                return !(this.reverting || this.options.disabled || "static" === this.options.type || (this._refreshItems(t),
                    e(t.target).parents().each(function() {
                        return e.data(this, a.widgetName + "-item") === a ? (s = e(this), !1) : void 0
                    }),
                    e.data(t.target, a.widgetName + "-item") === a && (s = e(t.target)), !s || this.options.handle && !i && (e(this.options.handle, s).find("*").addBack().each(function() {
                        this === t.target && (n = !0)
                    }), !n) || (this.currentItem = s,
                        this._removeCurrentsFromItems(),
                        0)))
            },
            _mouseStart: function(t, i, s) {
                var n, a, o = this.options;
                if (this.currentContainer = this,
                    this.refreshPositions(),
                    this.helper = this._createHelper(t),
                    this._cacheHelperProportions(),
                    this._cacheMargins(),
                    this.scrollParent = this.helper.scrollParent(),
                    this.offset = this.currentItem.offset(),
                    this.offset = {
                        top: this.offset.top - this.margins.top,
                        left: this.offset.left - this.margins.left
                    },
                    e.extend(this.offset, {
                        click: {
                            left: t.pageX - this.offset.left,
                            top: t.pageY - this.offset.top
                        },
                        parent: this._getParentOffset(),
                        relative: this._getRelativeOffset()
                    }),
                    this.helper.css("position", "absolute"),
                    this.cssPosition = this.helper.css("position"),
                    this.originalPosition = this._generatePosition(t),
                    this.originalPageX = t.pageX,
                    this.originalPageY = t.pageY,
                    o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt),
                    this.domPosition = {
                        prev: this.currentItem.prev()[0],
                        parent: this.currentItem.parent()[0]
                    },
                    this.helper[0] !== this.currentItem[0] && this.currentItem.hide(),
                    this._createPlaceholder(),
                    o.containment && this._setContainment(),
                    o.cursor && "auto" !== o.cursor && (a = this.document.find("body"),
                        this.storedCursor = a.css("cursor"),
                        a.css("cursor", o.cursor),
                        this.storedStylesheet = e("<style>*{ cursor: " + o.cursor + " !important; }</style>").appendTo(a)),
                    o.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")),
                        this.helper.css("opacity", o.opacity)),
                    o.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")),
                        this.helper.css("zIndex", o.zIndex)),
                    this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()),
                    this._trigger("start", t, this._uiHash()),
                    this._preserveHelperProportions || this._cacheHelperProportions(), !s)
                    for (n = this.containers.length - 1; n >= 0; n--)
                        this.containers[n]._trigger("activate", t, this._uiHash(this));
                return e.ui.ddmanager && (e.ui.ddmanager.current = this),
                    e.ui.ddmanager && !o.dropBehaviour && e.ui.ddmanager.prepareOffsets(this, t),
                    this.dragging = !0,
                    this._addClass(this.helper, "ui-sortable-helper"),
                    this._mouseDrag(t), !0
            },
            _mouseDrag: function(t) {
                var i, s, n, a, o = this.options,
                    r = !1;
                for (this.position = this._generatePosition(t),
                    this.positionAbs = this._convertPositionTo("absolute"),
                    this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs),
                    this.options.scroll && (this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - t.pageY < o.scrollSensitivity ? this.scrollParent[0].scrollTop = r = this.scrollParent[0].scrollTop + o.scrollSpeed : t.pageY - this.overflowOffset.top < o.scrollSensitivity && (this.scrollParent[0].scrollTop = r = this.scrollParent[0].scrollTop - o.scrollSpeed),
                        this.overflowOffset.left + this.scrollParent[0].offsetWidth - t.pageX < o.scrollSensitivity ? this.scrollParent[0].scrollLeft = r = this.scrollParent[0].scrollLeft + o.scrollSpeed : t.pageX - this.overflowOffset.left < o.scrollSensitivity && (this.scrollParent[0].scrollLeft = r = this.scrollParent[0].scrollLeft - o.scrollSpeed)) : (t.pageY - this.document.scrollTop() < o.scrollSensitivity ? r = this.document.scrollTop(this.document.scrollTop() - o.scrollSpeed) : this.window.height() - (t.pageY - this.document.scrollTop()) < o.scrollSensitivity && (r = this.document.scrollTop(this.document.scrollTop() + o.scrollSpeed)),
                        t.pageX - this.document.scrollLeft() < o.scrollSensitivity ? r = this.document.scrollLeft(this.document.scrollLeft() - o.scrollSpeed) : this.window.width() - (t.pageX - this.document.scrollLeft()) < o.scrollSensitivity && (r = this.document.scrollLeft(this.document.scrollLeft() + o.scrollSpeed))), !1 !== r && e.ui.ddmanager && !o.dropBehaviour && e.ui.ddmanager.prepareOffsets(this, t)),
                    this.positionAbs = this._convertPositionTo("absolute"),
                    this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"),
                    this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"),
                    i = this.items.length - 1; i >= 0; i--)
                    if (n = (s = this.items[i]).item[0],
                        (a = this._intersectsWithPointer(s)) && s.instance === this.currentContainer && n !== this.currentItem[0] && this.placeholder[1 === a ? "next" : "prev"]()[0] !== n && !e.contains(this.placeholder[0], n) && ("semi-dynamic" !== this.options.type || !e.contains(this.element[0], n))) {
                        if (this.direction = 1 === a ? "down" : "up",
                            "pointer" !== this.options.tolerance && !this._intersectsWithSides(s))
                            break;
                        this._rearrange(t, s),
                            this._trigger("change", t, this._uiHash());
                        break
                    }
                return this._contactContainers(t),
                    e.ui.ddmanager && e.ui.ddmanager.drag(this, t),
                    this._trigger("sort", t, this._uiHash()),
                    this.lastPositionAbs = this.positionAbs, !1
            },
            _mouseStop: function(t, i) {
                if (t) {
                    if (e.ui.ddmanager && !this.options.dropBehaviour && e.ui.ddmanager.drop(this, t),
                        this.options.revert) {
                        var s = this,
                            n = this.placeholder.offset(),
                            a = this.options.axis,
                            o = {};
                        a && "x" !== a || (o.left = n.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollLeft)),
                            a && "y" !== a || (o.top = n.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollTop)),
                            this.reverting = !0,
                            e(this.helper).animate(o, parseInt(this.options.revert, 10) || 500, function() {
                                s._clear(t)
                            })
                    } else
                        this._clear(t, i);
                    return !1
                }
            },
            cancel: function() {
                if (this.dragging) {
                    this._mouseUp(new e.Event("mouseup", {
                            target: null
                        })),
                        "original" === this.options.helper ? (this.currentItem.css(this._storedCSS),
                            this._removeClass(this.currentItem, "ui-sortable-helper")) : this.currentItem.show();
                    for (var t = this.containers.length - 1; t >= 0; t--)
                        this.containers[t]._trigger("deactivate", null, this._uiHash(this)),
                        this.containers[t].containerCache.over && (this.containers[t]._trigger("out", null, this._uiHash(this)),
                            this.containers[t].containerCache.over = 0)
                }
                return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]),
                        "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(),
                        e.extend(this, {
                            helper: null,
                            dragging: !1,
                            reverting: !1,
                            _noFinalSort: null
                        }),
                        this.domPosition.prev ? e(this.domPosition.prev).after(this.currentItem) : e(this.domPosition.parent).prepend(this.currentItem)),
                    this
            },
            serialize: function(t) {
                var i = this._getItemsAsjQuery(t && t.connected),
                    s = [];
                return t = t || {},
                    e(i).each(function() {
                        var i = (e(t.item || this).attr(t.attribute || "id") || "").match(t.expression || /(.+)[\-=_](.+)/);
                        i && s.push((t.key || i[1] + "[]") + "=" + (t.key && t.expression ? i[1] : i[2]))
                    }), !s.length && t.key && s.push(t.key + "="),
                    s.join("&")
            },
            toArray: function(t) {
                var i = this._getItemsAsjQuery(t && t.connected),
                    s = [];
                return t = t || {},
                    i.each(function() {
                        s.push(e(t.item || this).attr(t.attribute || "id") || "")
                    }),
                    s
            },
            _intersectsWith: function(e) {
                var t = this.positionAbs.left,
                    i = this.positionAbs.top,
                    s = e.left,
                    n = s + e.width,
                    a = e.top,
                    o = a + e.height,
                    r = this.offset.click.top,
                    l = this.offset.click.left;
                return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > e[this.floating ? "width" : "height"] ? ("x" === this.options.axis || i + r > a && o > i + r) && ("y" === this.options.axis || t + l > s && n > t + l) : t + this.helperProportions.width / 2 > s && n > t + this.helperProportions.width - this.helperProportions.width / 2 && i + this.helperProportions.height / 2 > a && o > i + this.helperProportions.height - this.helperProportions.height / 2
            },
            _intersectsWithPointer: function(e) {
                var t, i, s = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, e.top, e.height),
                    n = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, e.left, e.width);
                return !(!s || !n) && (t = this._getDragVerticalDirection(),
                    i = this._getDragHorizontalDirection(),
                    this.floating ? "right" === i || "down" === t ? 2 : 1 : t && ("down" === t ? 2 : 1))
            },
            _intersectsWithSides: function(e) {
                var t = this._isOverAxis(this.positionAbs.top + this.offset.click.top, e.top + e.height / 2, e.height),
                    i = this._isOverAxis(this.positionAbs.left + this.offset.click.left, e.left + e.width / 2, e.width),
                    s = this._getDragVerticalDirection(),
                    n = this._getDragHorizontalDirection();
                return this.floating && n ? "right" === n && i || "left" === n && !i : s && ("down" === s && t || "up" === s && !t)
            },
            _getDragVerticalDirection: function() {
                var e = this.positionAbs.top - this.lastPositionAbs.top;
                return 0 !== e && (e > 0 ? "down" : "up")
            },
            _getDragHorizontalDirection: function() {
                var e = this.positionAbs.left - this.lastPositionAbs.left;
                return 0 !== e && (e > 0 ? "right" : "left")
            },
            refresh: function(e) {
                return this._refreshItems(e),
                    this._setHandleClassName(),
                    this.refreshPositions(),
                    this
            },
            _connectWith: function() {
                var e = this.options;
                return e.connectWith.constructor === String ? [e.connectWith] : e.connectWith
            },
            _getItemsAsjQuery: function(t) {
                function i() {
                    r.push(this)
                }
                var s, n, a, o, r = [],
                    l = [],
                    h = this._connectWith();
                if (h && t)
                    for (s = h.length - 1; s >= 0; s--)
                        for (n = (a = e(h[s], this.document[0])).length - 1; n >= 0; n--)
                            (o = e.data(a[n], this.widgetFullName)) && o !== this && !o.options.disabled && l.push([e.isFunction(o.options.items) ? o.options.items.call(o.element) : e(o.options.items, o.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), o]);
                for (l.push([e.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                        options: this.options,
                        item: this.currentItem
                    }) : e(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]),
                    s = l.length - 1; s >= 0; s--)
                    l[s][0].each(i);
                return e(r)
            },
            _removeCurrentsFromItems: function() {
                var t = this.currentItem.find(":data(" + this.widgetName + "-item)");
                this.items = e.grep(this.items, function(e) {
                    for (var i = 0; t.length > i; i++)
                        if (t[i] === e.item[0])
                            return !1;
                    return !0
                })
            },
            _refreshItems: function(t) {
                this.items = [],
                    this.containers = [this];
                var i, s, n, a, o, r, l, h, u = this.items,
                    c = [
                        [e.isFunction(this.options.items) ? this.options.items.call(this.element[0], t, {
                            item: this.currentItem
                        }) : e(this.options.items, this.element), this]
                    ],
                    d = this._connectWith();
                if (d && this.ready)
                    for (i = d.length - 1; i >= 0; i--)
                        for (s = (n = e(d[i], this.document[0])).length - 1; s >= 0; s--)
                            (a = e.data(n[s], this.widgetFullName)) && a !== this && !a.options.disabled && (c.push([e.isFunction(a.options.items) ? a.options.items.call(a.element[0], t, {
                                    item: this.currentItem
                                }) : e(a.options.items, a.element), a]),
                                this.containers.push(a));
                for (i = c.length - 1; i >= 0; i--)
                    for (o = c[i][1],
                        s = 0,
                        h = (r = c[i][0]).length; h > s; s++)
                        (l = e(r[s])).data(this.widgetName + "-item", o),
                        u.push({
                            item: l,
                            instance: o,
                            width: 0,
                            height: 0,
                            left: 0,
                            top: 0
                        })
            },
            refreshPositions: function(t) {
                var i, s, n, a;
                for (this.floating = !!this.items.length && ("x" === this.options.axis || this._isFloating(this.items[0].item)),
                    this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset()),
                    i = this.items.length - 1; i >= 0; i--)
                    (s = this.items[i]).instance !== this.currentContainer && this.currentContainer && s.item[0] !== this.currentItem[0] || (n = this.options.toleranceElement ? e(this.options.toleranceElement, s.item) : s.item,
                        t || (s.width = n.outerWidth(),
                            s.height = n.outerHeight()),
                        a = n.offset(),
                        s.left = a.left,
                        s.top = a.top);
                if (this.options.custom && this.options.custom.refreshContainers)
                    this.options.custom.refreshContainers.call(this);
                else
                    for (i = this.containers.length - 1; i >= 0; i--)
                        a = this.containers[i].element.offset(),
                        this.containers[i].containerCache.left = a.left,
                        this.containers[i].containerCache.top = a.top,
                        this.containers[i].containerCache.width = this.containers[i].element.outerWidth(),
                        this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
                return this
            },
            _createPlaceholder: function(t) {
                var i, s = (t = t || this).options;
                s.placeholder && s.placeholder.constructor !== String || (i = s.placeholder,
                        s.placeholder = {
                            element: function() {
                                var s = t.currentItem[0].nodeName.toLowerCase(),
                                    n = e("<" + s + ">", t.document[0]);
                                return t._addClass(n, "ui-sortable-placeholder", i || t.currentItem[0].className)._removeClass(n, "ui-sortable-helper"),
                                    "tbody" === s ? t._createTrPlaceholder(t.currentItem.find("tr").eq(0), e("<tr>", t.document[0]).appendTo(n)) : "tr" === s ? t._createTrPlaceholder(t.currentItem, n) : "img" === s && n.attr("src", t.currentItem.attr("src")),
                                    i || n.css("visibility", "hidden"),
                                    n
                            },
                            update: function(e, n) {
                                (!i || s.forcePlaceholderSize) && (n.height() || n.height(t.currentItem.innerHeight() - parseInt(t.currentItem.css("paddingTop") || 0, 10) - parseInt(t.currentItem.css("paddingBottom") || 0, 10)),
                                    n.width() || n.width(t.currentItem.innerWidth() - parseInt(t.currentItem.css("paddingLeft") || 0, 10) - parseInt(t.currentItem.css("paddingRight") || 0, 10)))
                            }
                        }),
                    t.placeholder = e(s.placeholder.element.call(t.element, t.currentItem)),
                    t.currentItem.after(t.placeholder),
                    s.placeholder.update(t, t.placeholder)
            },
            _createTrPlaceholder: function(t, i) {
                var s = this;
                t.children().each(function() {
                    e("<td>&#160;</td>", s.document[0]).attr("colspan", e(this).attr("colspan") || 1).appendTo(i)
                })
            },
            _contactContainers: function(t) {
                var i, s, n, a, o, r, l, h, u, c, d = null,
                    p = null;
                for (i = this.containers.length - 1; i >= 0; i--)
                    if (!e.contains(this.currentItem[0], this.containers[i].element[0]))
                        if (this._intersectsWith(this.containers[i].containerCache)) {
                            if (d && e.contains(this.containers[i].element[0], d.element[0]))
                                continue;
                            d = this.containers[i],
                                p = i
                        } else
                            this.containers[i].containerCache.over && (this.containers[i]._trigger("out", t, this._uiHash(this)),
                                this.containers[i].containerCache.over = 0);
                if (d)
                    if (1 === this.containers.length)
                        this.containers[p].containerCache.over || (this.containers[p]._trigger("over", t, this._uiHash(this)),
                            this.containers[p].containerCache.over = 1);
                    else {
                        for (n = 1e4,
                            a = null,
                            o = (u = d.floating || this._isFloating(this.currentItem)) ? "left" : "top",
                            r = u ? "width" : "height",
                            c = u ? "pageX" : "pageY",
                            s = this.items.length - 1; s >= 0; s--)
                            e.contains(this.containers[p].element[0], this.items[s].item[0]) && this.items[s].item[0] !== this.currentItem[0] && (l = this.items[s].item.offset()[o],
                                h = !1,
                                t[c] - l > this.items[s][r] / 2 && (h = !0),
                                n > Math.abs(t[c] - l) && (n = Math.abs(t[c] - l),
                                    a = this.items[s],
                                    this.direction = h ? "up" : "down"));
                        if (!a && !this.options.dropOnEmpty)
                            return;
                        if (this.currentContainer === this.containers[p])
                            return void(this.currentContainer.containerCache.over || (this.containers[p]._trigger("over", t, this._uiHash()),
                                this.currentContainer.containerCache.over = 1));
                        a ? this._rearrange(t, a, null, !0) : this._rearrange(t, null, this.containers[p].element, !0),
                            this._trigger("change", t, this._uiHash()),
                            this.containers[p]._trigger("change", t, this._uiHash(this)),
                            this.currentContainer = this.containers[p],
                            this.options.placeholder.update(this.currentContainer, this.placeholder),
                            this.containers[p]._trigger("over", t, this._uiHash(this)),
                            this.containers[p].containerCache.over = 1
                    }
            },
            _createHelper: function(t) {
                var i = this.options,
                    s = e.isFunction(i.helper) ? e(i.helper.apply(this.element[0], [t, this.currentItem])) : "clone" === i.helper ? this.currentItem.clone() : this.currentItem;
                return s.parents("body").length || e("parent" !== i.appendTo ? i.appendTo : this.currentItem[0].parentNode)[0].appendChild(s[0]),
                    s[0] === this.currentItem[0] && (this._storedCSS = {
                        width: this.currentItem[0].style.width,
                        height: this.currentItem[0].style.height,
                        position: this.currentItem.css("position"),
                        top: this.currentItem.css("top"),
                        left: this.currentItem.css("left")
                    }),
                    (!s[0].style.width || i.forceHelperSize) && s.width(this.currentItem.width()),
                    (!s[0].style.height || i.forceHelperSize) && s.height(this.currentItem.height()),
                    s
            },
            _adjustOffsetFromHelper: function(t) {
                "string" == typeof t && (t = t.split(" ")),
                    e.isArray(t) && (t = {
                        left: +t[0],
                        top: +t[1] || 0
                    }),
                    "left" in t && (this.offset.click.left = t.left + this.margins.left),
                    "right" in t && (this.offset.click.left = this.helperProportions.width - t.right + this.margins.left),
                    "top" in t && (this.offset.click.top = t.top + this.margins.top),
                    "bottom" in t && (this.offset.click.top = this.helperProportions.height - t.bottom + this.margins.top)
            },
            _getParentOffset: function() {
                this.offsetParent = this.helper.offsetParent();
                var t = this.offsetParent.offset();
                return "absolute" === this.cssPosition && this.scrollParent[0] !== this.document[0] && e.contains(this.scrollParent[0], this.offsetParent[0]) && (t.left += this.scrollParent.scrollLeft(),
                        t.top += this.scrollParent.scrollTop()),
                    (this.offsetParent[0] === this.document[0].body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && e.ui.ie) && (t = {
                        top: 0,
                        left: 0
                    }), {
                        top: t.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                        left: t.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
                    }
            },
            _getRelativeOffset: function() {
                if ("relative" === this.cssPosition) {
                    var e = this.currentItem.position();
                    return {
                        top: e.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                        left: e.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                    }
                }
                return {
                    top: 0,
                    left: 0
                }
            },
            _cacheMargins: function() {
                this.margins = {
                    left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                    top: parseInt(this.currentItem.css("marginTop"), 10) || 0
                }
            },
            _cacheHelperProportions: function() {
                this.helperProportions = {
                    width: this.helper.outerWidth(),
                    height: this.helper.outerHeight()
                }
            },
            _setContainment: function() {
                var t, i, s, n = this.options;
                "parent" === n.containment && (n.containment = this.helper[0].parentNode),
                    ("document" === n.containment || "window" === n.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, "document" === n.containment ? this.document.width() : this.window.width() - this.helperProportions.width - this.margins.left, ("document" === n.containment ? this.document.height() || document.body.parentNode.scrollHeight : this.window.height() || this.document[0].body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]),
                    /^(document|window|parent)$/.test(n.containment) || (t = e(n.containment)[0],
                        i = e(n.containment).offset(),
                        s = "hidden" !== e(t).css("overflow"),
                        this.containment = [i.left + (parseInt(e(t).css("borderLeftWidth"), 10) || 0) + (parseInt(e(t).css("paddingLeft"), 10) || 0) - this.margins.left, i.top + (parseInt(e(t).css("borderTopWidth"), 10) || 0) + (parseInt(e(t).css("paddingTop"), 10) || 0) - this.margins.top, i.left + (s ? Math.max(t.scrollWidth, t.offsetWidth) : t.offsetWidth) - (parseInt(e(t).css("borderLeftWidth"), 10) || 0) - (parseInt(e(t).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, i.top + (s ? Math.max(t.scrollHeight, t.offsetHeight) : t.offsetHeight) - (parseInt(e(t).css("borderTopWidth"), 10) || 0) - (parseInt(e(t).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
            },
            _convertPositionTo: function(t, i) {
                i || (i = this.position);
                var s = "absolute" === t ? 1 : -1,
                    n = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && e.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                    a = /(html|body)/i.test(n[0].tagName);
                return {
                    top: i.top + this.offset.relative.top * s + this.offset.parent.top * s - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : a ? 0 : n.scrollTop()) * s,
                    left: i.left + this.offset.relative.left * s + this.offset.parent.left * s - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : a ? 0 : n.scrollLeft()) * s
                }
            },
            _generatePosition: function(t) {
                var i, s, n = this.options,
                    a = t.pageX,
                    o = t.pageY,
                    r = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && e.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                    l = /(html|body)/i.test(r[0].tagName);
                return "relative" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()),
                    this.originalPosition && (this.containment && (t.pageX - this.offset.click.left < this.containment[0] && (a = this.containment[0] + this.offset.click.left),
                            t.pageY - this.offset.click.top < this.containment[1] && (o = this.containment[1] + this.offset.click.top),
                            t.pageX - this.offset.click.left > this.containment[2] && (a = this.containment[2] + this.offset.click.left),
                            t.pageY - this.offset.click.top > this.containment[3] && (o = this.containment[3] + this.offset.click.top)),
                        n.grid && (i = this.originalPageY + Math.round((o - this.originalPageY) / n.grid[1]) * n.grid[1],
                            o = this.containment ? i - this.offset.click.top >= this.containment[1] && i - this.offset.click.top <= this.containment[3] ? i : i - this.offset.click.top >= this.containment[1] ? i - n.grid[1] : i + n.grid[1] : i,
                            s = this.originalPageX + Math.round((a - this.originalPageX) / n.grid[0]) * n.grid[0],
                            a = this.containment ? s - this.offset.click.left >= this.containment[0] && s - this.offset.click.left <= this.containment[2] ? s : s - this.offset.click.left >= this.containment[0] ? s - n.grid[0] : s + n.grid[0] : s)), {
                        top: o - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : l ? 0 : r.scrollTop()),
                        left: a - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : l ? 0 : r.scrollLeft())
                    }
            },
            _rearrange: function(e, t, i, s) {
                i ? i[0].appendChild(this.placeholder[0]) : t.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? t.item[0] : t.item[0].nextSibling),
                    this.counter = this.counter ? ++this.counter : 1;
                var n = this.counter;
                this._delay(function() {
                    n === this.counter && this.refreshPositions(!s)
                })
            },
            _clear: function(e, t) {
                function i(e, t, i) {
                    return function(s) {
                        i._trigger(e, s, t._uiHash(t))
                    }
                }
                this.reverting = !1;
                var s, n = [];
                if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem),
                    this._noFinalSort = null,
                    this.helper[0] === this.currentItem[0]) {
                    for (s in this._storedCSS)
                        ("auto" === this._storedCSS[s] || "static" === this._storedCSS[s]) && (this._storedCSS[s] = "");
                    this.currentItem.css(this._storedCSS),
                        this._removeClass(this.currentItem, "ui-sortable-helper")
                } else
                    this.currentItem.show();
                for (this.fromOutside && !t && n.push(function(e) {
                        this._trigger("receive", e, this._uiHash(this.fromOutside))
                    }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || t || n.push(function(e) {
                        this._trigger("update", e, this._uiHash())
                    }),
                    this !== this.currentContainer && (t || (n.push(function(e) {
                            this._trigger("remove", e, this._uiHash())
                        }),
                        n.push((function(e) {
                            return function(t) {
                                e._trigger("receive", t, this._uiHash(this))
                            }
                        }).call(this, this.currentContainer)),
                        n.push((function(e) {
                            return function(t) {
                                e._trigger("update", t, this._uiHash(this))
                            }
                        }).call(this, this.currentContainer)))),
                    s = this.containers.length - 1; s >= 0; s--)
                    t || n.push(i("deactivate", this, this.containers[s])),
                    this.containers[s].containerCache.over && (n.push(i("out", this, this.containers[s])),
                        this.containers[s].containerCache.over = 0);
                if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor),
                        this.storedStylesheet.remove()),
                    this._storedOpacity && this.helper.css("opacity", this._storedOpacity),
                    this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex),
                    this.dragging = !1,
                    t || this._trigger("beforeStop", e, this._uiHash()),
                    this.placeholder[0].parentNode.removeChild(this.placeholder[0]),
                    this.cancelHelperRemoval || (this.helper[0] !== this.currentItem[0] && this.helper.remove(),
                        this.helper = null), !t) {
                    for (s = 0; n.length > s; s++)
                        n[s].call(this, e);
                    this._trigger("stop", e, this._uiHash())
                }
                return this.fromOutside = !1, !this.cancelHelperRemoval
            },
            _trigger: function() {
                !1 === e.Widget.prototype._trigger.apply(this, arguments) && this.cancel()
            },
            _uiHash: function(t) {
                var i = t || this;
                return {
                    helper: i.helper,
                    placeholder: i.placeholder || e([]),
                    position: i.position,
                    originalPosition: i.originalPosition,
                    offset: i.positionAbs,
                    item: i.currentItem,
                    sender: t ? t.element : null
                }
            }
        }),
        e.widget("ui.spinner", {
            version: "1.12.1",
            defaultElement: "<input>",
            widgetEventPrefix: "spin",
            options: {
                classes: {
                    "ui-spinner": "ui-corner-all",
                    "ui-spinner-down": "ui-corner-br",
                    "ui-spinner-up": "ui-corner-tr"
                },
                culture: null,
                icons: {
                    down: "ui-icon-triangle-1-s",
                    up: "ui-icon-triangle-1-n"
                },
                incremental: !0,
                max: null,
                min: null,
                numberFormat: null,
                page: 10,
                step: 1,
                change: null,
                spin: null,
                start: null,
                stop: null
            },
            _create: function() {
                this._setOption("max", this.options.max),
                    this._setOption("min", this.options.min),
                    this._setOption("step", this.options.step),
                    "" !== this.value() && this._value(this.element.val(), !0),
                    this._draw(),
                    this._on(this._events),
                    this._refresh(),
                    this._on(this.window, {
                        beforeunload: function() {
                            this.element.removeAttr("autocomplete")
                        }
                    })
            },
            _getCreateOptions: function() {
                var t = this._super(),
                    i = this.element;
                return e.each(["min", "max", "step"], function(e, s) {
                        var n = i.attr(s);
                        null != n && n.length && (t[s] = n)
                    }),
                    t
            },
            _events: {
                keydown: function(e) {
                    this._start(e) && this._keydown(e) && e.preventDefault()
                },
                keyup: "_stop",
                focus: function() {
                    this.previous = this.element.val()
                },
                blur: function(e) {
                    return this.cancelBlur ? void delete this.cancelBlur : (this._stop(),
                        this._refresh(),
                        void(this.previous !== this.element.val() && this._trigger("change", e)))
                },
                mousewheel: function(e, t) {
                    if (t) {
                        if (!this.spinning && !this._start(e))
                            return !1;
                        this._spin((t > 0 ? 1 : -1) * this.options.step, e),
                            clearTimeout(this.mousewheelTimer),
                            this.mousewheelTimer = this._delay(function() {
                                this.spinning && this._stop(e)
                            }, 100),
                            e.preventDefault()
                    }
                },
                "mousedown .ui-spinner-button": function(t) {
                    function i() {
                        this.element[0] === e.ui.safeActiveElement(this.document[0]) || (this.element.trigger("focus"),
                            this.previous = s,
                            this._delay(function() {
                                this.previous = s
                            }))
                    }
                    var s;
                    s = this.element[0] === e.ui.safeActiveElement(this.document[0]) ? this.previous : this.element.val(),
                        t.preventDefault(),
                        i.call(this),
                        this.cancelBlur = !0,
                        this._delay(function() {
                            delete this.cancelBlur,
                                i.call(this)
                        }), !1 !== this._start(t) && this._repeat(null, e(t.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, t)
                },
                "mouseup .ui-spinner-button": "_stop",
                "mouseenter .ui-spinner-button": function(t) {
                    return e(t.currentTarget).hasClass("ui-state-active") ? !1 !== this._start(t) && void this._repeat(null, e(t.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, t) : void 0
                },
                "mouseleave .ui-spinner-button": "_stop"
            },
            _enhance: function() {
                this.uiSpinner = this.element.attr("autocomplete", "off").wrap("<span>").parent().append("<a></a><a></a>")
            },
            _draw: function() {
                this._enhance(),
                    this._addClass(this.uiSpinner, "ui-spinner", "ui-widget ui-widget-content"),
                    this._addClass("ui-spinner-input"),
                    this.element.attr("role", "spinbutton"),
                    this.buttons = this.uiSpinner.children("a").attr("tabIndex", -1).attr("aria-hidden", !0).button({
                        classes: {
                            "ui-button": ""
                        }
                    }),
                    this._removeClass(this.buttons, "ui-corner-all"),
                    this._addClass(this.buttons.first(), "ui-spinner-button ui-spinner-up"),
                    this._addClass(this.buttons.last(), "ui-spinner-button ui-spinner-down"),
                    this.buttons.first().button({
                        icon: this.options.icons.up,
                        showLabel: !1
                    }),
                    this.buttons.last().button({
                        icon: this.options.icons.down,
                        showLabel: !1
                    }),
                    this.buttons.height() > Math.ceil(.5 * this.uiSpinner.height()) && this.uiSpinner.height() > 0 && this.uiSpinner.height(this.uiSpinner.height())
            },
            _keydown: function(t) {
                var i = this.options,
                    s = e.ui.keyCode;
                switch (t.keyCode) {
                    case s.UP:
                        return this._repeat(null, 1, t), !0;
                    case s.DOWN:
                        return this._repeat(null, -1, t), !0;
                    case s.PAGE_UP:
                        return this._repeat(null, i.page, t), !0;
                    case s.PAGE_DOWN:
                        return this._repeat(null, -i.page, t), !0
                }
                return !1
            },
            _start: function(e) {
                return !(!this.spinning && !1 === this._trigger("start", e) || (this.counter || (this.counter = 1),
                    this.spinning = !0,
                    0))
            },
            _repeat: function(e, t, i) {
                e = e || 500,
                    clearTimeout(this.timer),
                    this.timer = this._delay(function() {
                        this._repeat(40, t, i)
                    }, e),
                    this._spin(t * this.options.step, i)
            },
            _spin: function(e, t) {
                var i = this.value() || 0;
                this.counter || (this.counter = 1),
                    i = this._adjustValue(i + e * this._increment(this.counter)),
                    this.spinning && !1 === this._trigger("spin", t, {
                        value: i
                    }) || (this._value(i),
                        this.counter++)
            },
            _increment: function(t) {
                var i = this.options.incremental;
                return i ? e.isFunction(i) ? i(t) : Math.floor(t * t * t / 5e4 - t * t / 500 + 17 * t / 200 + 1) : 1
            },
            _precision: function() {
                var e = this._precisionOf(this.options.step);
                return null !== this.options.min && (e = Math.max(e, this._precisionOf(this.options.min))),
                    e
            },
            _precisionOf: function(e) {
                var t = "" + e,
                    i = t.indexOf(".");
                return -1 === i ? 0 : t.length - i - 1
            },
            _adjustValue: function(e) {
                var t, i, s = this.options;
                return i = e - (t = null !== s.min ? s.min : 0),
                    e = t + (i = Math.round(i / s.step) * s.step),
                    e = parseFloat(e.toFixed(this._precision())),
                    null !== s.max && e > s.max ? s.max : null !== s.min && s.min > e ? s.min : e
            },
            _stop: function(e) {
                this.spinning && (clearTimeout(this.timer),
                    clearTimeout(this.mousewheelTimer),
                    this.counter = 0,
                    this.spinning = !1,
                    this._trigger("stop", e))
            },
            _setOption: function(e, t) {
                var i, s, n;
                return "culture" === e || "numberFormat" === e ? (i = this._parse(this.element.val()),
                    this.options[e] = t,
                    void this.element.val(this._format(i))) : (("max" === e || "min" === e || "step" === e) && "string" == typeof t && (t = this._parse(t)),
                    "icons" === e && (s = this.buttons.first().find(".ui-icon"),
                        this._removeClass(s, null, this.options.icons.up),
                        this._addClass(s, null, t.up),
                        n = this.buttons.last().find(".ui-icon"),
                        this._removeClass(n, null, this.options.icons.down),
                        this._addClass(n, null, t.down)),
                    void this._super(e, t))
            },
            _setOptionDisabled: function(e) {
                this._super(e),
                    this._toggleClass(this.uiSpinner, null, "ui-state-disabled", !!e),
                    this.element.prop("disabled", !!e),
                    this.buttons.button(e ? "disable" : "enable")
            },
            _setOptions: a(function(e) {
                this._super(e)
            }),
            _parse: function(e) {
                return "string" == typeof e && "" !== e && (e = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(e, 10, this.options.culture) : +e),
                    "" === e || isNaN(e) ? null : e
            },
            _format: function(e) {
                return "" === e ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(e, this.options.numberFormat, this.options.culture) : e
            },
            _refresh: function() {
                this.element.attr({
                    "aria-valuemin": this.options.min,
                    "aria-valuemax": this.options.max,
                    "aria-valuenow": this._parse(this.element.val())
                })
            },
            isValid: function() {
                var e = this.value();
                return null !== e && e === this._adjustValue(e)
            },
            _value: function(e, t) {
                var i;
                "" !== e && null !== (i = this._parse(e)) && (t || (i = this._adjustValue(i)),
                        e = this._format(i)),
                    this.element.val(e),
                    this._refresh()
            },
            _destroy: function() {
                this.element.prop("disabled", !1).removeAttr("autocomplete role aria-valuemin aria-valuemax aria-valuenow"),
                    this.uiSpinner.replaceWith(this.element)
            },
            stepUp: a(function(e) {
                this._stepUp(e)
            }),
            _stepUp: function(e) {
                this._start() && (this._spin((e || 1) * this.options.step),
                    this._stop())
            },
            stepDown: a(function(e) {
                this._stepDown(e)
            }),
            _stepDown: function(e) {
                this._start() && (this._spin((e || 1) * -this.options.step),
                    this._stop())
            },
            pageUp: a(function(e) {
                this._stepUp((e || 1) * this.options.page)
            }),
            pageDown: a(function(e) {
                this._stepDown((e || 1) * this.options.page)
            }),
            value: function(e) {
                return arguments.length ? void a(this._value).call(this, e) : this._parse(this.element.val())
            },
            widget: function() {
                return this.uiSpinner
            }
        }), !1 !== e.uiBackCompat && e.widget("ui.spinner", e.ui.spinner, {
            _enhance: function() {
                this.uiSpinner = this.element.attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml())
            },
            _uiSpinnerHtml: function() {
                return "<span>"
            },
            _buttonHtml: function() {
                return "<a></a><a></a>"
            }
        }),
        e.widget("ui.tabs", {
            version: "1.12.1",
            delay: 300,
            options: {
                active: null,
                classes: {
                    "ui-tabs": "ui-corner-all",
                    "ui-tabs-nav": "ui-corner-all",
                    "ui-tabs-panel": "ui-corner-bottom",
                    "ui-tabs-tab": "ui-corner-top"
                },
                collapsible: !1,
                event: "click",
                heightStyle: "content",
                hide: null,
                show: null,
                activate: null,
                beforeActivate: null,
                beforeLoad: null,
                load: null
            },
            _isLocal: function() {
                var e = /#.*$/;
                return function(t) {
                    var s, n;
                    s = t.href.replace(e, ""),
                        n = location.href.replace(e, "");
                    try {
                        s = decodeURIComponent(s)
                    } catch (i) {}
                    try {
                        n = decodeURIComponent(n)
                    } catch (i) {}
                    return t.hash.length > 1 && s === n
                }
            }(),
            _create: function() {
                var t = this,
                    i = this.options;
                this.running = !1,
                    this._addClass("ui-tabs", "ui-widget ui-widget-content"),
                    this._toggleClass("ui-tabs-collapsible", null, i.collapsible),
                    this._processTabs(),
                    i.active = this._initialActive(),
                    e.isArray(i.disabled) && (i.disabled = e.unique(i.disabled.concat(e.map(this.tabs.filter(".ui-state-disabled"), function(e) {
                        return t.tabs.index(e)
                    }))).sort()),
                    this.active = !1 !== this.options.active && this.anchors.length ? this._findActive(i.active) : e(),
                    this._refresh(),
                    this.active.length && this.load(i.active)
            },
            _initialActive: function() {
                var t = this.options.active,
                    i = this.options.collapsible,
                    s = location.hash.substring(1);
                return null === t && (s && this.tabs.each(function(i, n) {
                            return e(n).attr("aria-controls") === s ? (t = i, !1) : void 0
                        }),
                        null === t && (t = this.tabs.index(this.tabs.filter(".ui-tabs-active"))),
                        (null === t || -1 === t) && (t = !!this.tabs.length && 0)), !1 !== t && -1 === (t = this.tabs.index(this.tabs.eq(t))) && (t = !i && 0), !i && !1 === t && this.anchors.length && (t = 0),
                    t
            },
            _getCreateEventData: function() {
                return {
                    tab: this.active,
                    panel: this.active.length ? this._getPanelForTab(this.active) : e()
                }
            },
            _tabKeydown: function(t) {
                var i = e(e.ui.safeActiveElement(this.document[0])).closest("li"),
                    s = this.tabs.index(i),
                    n = !0;
                if (!this._handlePageNav(t)) {
                    switch (t.keyCode) {
                        case e.ui.keyCode.RIGHT:
                        case e.ui.keyCode.DOWN:
                            s++;
                            break;
                        case e.ui.keyCode.UP:
                        case e.ui.keyCode.LEFT:
                            n = !1,
                                s--;
                            break;
                        case e.ui.keyCode.END:
                            s = this.anchors.length - 1;
                            break;
                        case e.ui.keyCode.HOME:
                            s = 0;
                            break;
                        case e.ui.keyCode.SPACE:
                            return t.preventDefault(),
                                clearTimeout(this.activating),
                                void this._activate(s);
                        case e.ui.keyCode.ENTER:
                            return t.preventDefault(),
                                clearTimeout(this.activating),
                                void this._activate(s !== this.options.active && s);
                        default:
                            return
                    }
                    t.preventDefault(),
                        clearTimeout(this.activating),
                        s = this._focusNextTab(s, n),
                        t.ctrlKey || t.metaKey || (i.attr("aria-selected", "false"),
                            this.tabs.eq(s).attr("aria-selected", "true"),
                            this.activating = this._delay(function() {
                                this.option("active", s)
                            }, this.delay))
                }
            },
            _panelKeydown: function(t) {
                this._handlePageNav(t) || t.ctrlKey && t.keyCode === e.ui.keyCode.UP && (t.preventDefault(),
                    this.active.trigger("focus"))
            },
            _handlePageNav: function(t) {
                return t.altKey && t.keyCode === e.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), !0) : t.altKey && t.keyCode === e.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), !0) : void 0
            },
            _findNextTab: function(t, i) {
                for (var s = this.tabs.length - 1; - 1 !== e.inArray((t > s && (t = 0),
                        0 > t && (t = s),
                        t), this.options.disabled);)
                    t = i ? t + 1 : t - 1;
                return t
            },
            _focusNextTab: function(e, t) {
                return e = this._findNextTab(e, t),
                    this.tabs.eq(e).trigger("focus"),
                    e
            },
            _setOption: function(e, t) {
                return "active" === e ? void this._activate(t) : (this._super(e, t),
                    "collapsible" === e && (this._toggleClass("ui-tabs-collapsible", null, t),
                        t || !1 !== this.options.active || this._activate(0)),
                    "event" === e && this._setupEvents(t),
                    void("heightStyle" === e && this._setupHeightStyle(t)))
            },
            _sanitizeSelector: function(e) {
                return e ? e.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : ""
            },
            refresh: function() {
                var t = this.options,
                    i = this.tablist.children(":has(a[href])");
                t.disabled = e.map(i.filter(".ui-state-disabled"), function(e) {
                        return i.index(e)
                    }),
                    this._processTabs(), !1 !== t.active && this.anchors.length ? this.active.length && !e.contains(this.tablist[0], this.active[0]) ? this.tabs.length === t.disabled.length ? (t.active = !1,
                        this.active = e()) : this._activate(this._findNextTab(Math.max(0, t.active - 1), !1)) : t.active = this.tabs.index(this.active) : (t.active = !1,
                        this.active = e()),
                    this._refresh()
            },
            _refresh: function() {
                this._setOptionDisabled(this.options.disabled),
                    this._setupEvents(this.options.event),
                    this._setupHeightStyle(this.options.heightStyle),
                    this.tabs.not(this.active).attr({
                        "aria-selected": "false",
                        "aria-expanded": "false",
                        tabIndex: -1
                    }),
                    this.panels.not(this._getPanelForTab(this.active)).hide().attr({
                        "aria-hidden": "true"
                    }),
                    this.active.length ? (this.active.attr({
                            "aria-selected": "true",
                            "aria-expanded": "true",
                            tabIndex: 0
                        }),
                        this._addClass(this.active, "ui-tabs-active", "ui-state-active"),
                        this._getPanelForTab(this.active).show().attr({
                            "aria-hidden": "false"
                        })) : this.tabs.eq(0).attr("tabIndex", 0)
            },
            _processTabs: function() {
                var t = this,
                    i = this.tabs,
                    s = this.anchors,
                    n = this.panels;
                this.tablist = this._getList().attr("role", "tablist"),
                    this._addClass(this.tablist, "ui-tabs-nav", "ui-helper-reset ui-helper-clearfix ui-widget-header"),
                    this.tablist.on("mousedown" + this.eventNamespace, "> li", function(t) {
                        e(this).is(".ui-state-disabled") && t.preventDefault()
                    }).on("focus" + this.eventNamespace, ".ui-tabs-anchor", function() {
                        e(this).closest("li").is(".ui-state-disabled") && this.blur()
                    }),
                    this.tabs = this.tablist.find("> li:has(a[href])").attr({
                        role: "tab",
                        tabIndex: -1
                    }),
                    this._addClass(this.tabs, "ui-tabs-tab", "ui-state-default"),
                    this.anchors = this.tabs.map(function() {
                        return e("a", this)[0]
                    }).attr({
                        role: "presentation",
                        tabIndex: -1
                    }),
                    this._addClass(this.anchors, "ui-tabs-anchor"),
                    this.panels = e(),
                    this.anchors.each(function(i, s) {
                        var n, a, o, r = e(s).uniqueId().attr("id"),
                            l = e(s).closest("li"),
                            h = l.attr("aria-controls");
                        t._isLocal(s) ? (o = (n = s.hash).substring(1),
                                a = t.element.find(t._sanitizeSelector(n))) : (o = l.attr("aria-controls") || e({}).uniqueId()[0].id,
                                (a = t.element.find(n = "#" + o)).length || (a = t._createPanel(o)).insertAfter(t.panels[i - 1] || t.tablist),
                                a.attr("aria-live", "polite")),
                            a.length && (t.panels = t.panels.add(a)),
                            h && l.data("ui-tabs-aria-controls", h),
                            l.attr({
                                "aria-controls": o,
                                "aria-labelledby": r
                            }),
                            a.attr("aria-labelledby", r)
                    }),
                    this.panels.attr("role", "tabpanel"),
                    this._addClass(this.panels, "ui-tabs-panel", "ui-widget-content"),
                    i && (this._off(i.not(this.tabs)),
                        this._off(s.not(this.anchors)),
                        this._off(n.not(this.panels)))
            },
            _getList: function() {
                return this.tablist || this.element.find("ol, ul").eq(0)
            },
            _createPanel: function(t) {
                return e("<div>").attr("id", t).data("ui-tabs-destroy", !0)
            },
            _setOptionDisabled: function(t) {
                var i, s, n;
                for (e.isArray(t) && (t.length ? t.length === this.anchors.length && (t = !0) : t = !1),
                    n = 0; s = this.tabs[n]; n++)
                    i = e(s), !0 === t || -1 !== e.inArray(n, t) ? (i.attr("aria-disabled", "true"),
                        this._addClass(i, null, "ui-state-disabled")) : (i.removeAttr("aria-disabled"),
                        this._removeClass(i, null, "ui-state-disabled"));
                this.options.disabled = t,
                    this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !0 === t)
            },
            _setupEvents: function(t) {
                var i = {};
                t && e.each(t.split(" "), function(e, t) {
                        i[t] = "_eventHandler"
                    }),
                    this._off(this.anchors.add(this.tabs).add(this.panels)),
                    this._on(!0, this.anchors, {
                        click: function(e) {
                            e.preventDefault()
                        }
                    }),
                    this._on(this.anchors, i),
                    this._on(this.tabs, {
                        keydown: "_tabKeydown"
                    }),
                    this._on(this.panels, {
                        keydown: "_panelKeydown"
                    }),
                    this._focusable(this.tabs),
                    this._hoverable(this.tabs)
            },
            _setupHeightStyle: function(t) {
                var i, s = this.element.parent();
                "fill" === t ? (i = s.height(),
                    i -= this.element.outerHeight() - this.element.height(),
                    this.element.siblings(":visible").each(function() {
                        var t = e(this),
                            s = t.css("position");
                        "absolute" !== s && "fixed" !== s && (i -= t.outerHeight(!0))
                    }),
                    this.element.children().not(this.panels).each(function() {
                        i -= e(this).outerHeight(!0)
                    }),
                    this.panels.each(function() {
                        e(this).height(Math.max(0, i - e(this).innerHeight() + e(this).height()))
                    }).css("overflow", "auto")) : "auto" === t && (i = 0,
                    this.panels.each(function() {
                        i = Math.max(i, e(this).height("").height())
                    }).height(i))
            },
            _eventHandler: function(t) {
                var i = this.options,
                    s = this.active,
                    n = e(t.currentTarget).closest("li"),
                    a = n[0] === s[0],
                    o = a && i.collapsible,
                    r = o ? e() : this._getPanelForTab(n),
                    l = s.length ? this._getPanelForTab(s) : e(),
                    h = {
                        oldTab: s,
                        oldPanel: l,
                        newTab: o ? e() : n,
                        newPanel: r
                    };
                t.preventDefault(),
                    n.hasClass("ui-state-disabled") || n.hasClass("ui-tabs-loading") || this.running || a && !i.collapsible || !1 === this._trigger("beforeActivate", t, h) || (i.active = !o && this.tabs.index(n),
                        this.active = a ? e() : n,
                        this.xhr && this.xhr.abort(),
                        l.length || r.length || e.error("jQuery UI Tabs: Mismatching fragment identifier."),
                        r.length && this.load(this.tabs.index(n), t),
                        this._toggle(t, h))
            },
            _toggle: function(t, i) {
                function s() {
                    a.running = !1,
                        a._trigger("activate", t, i)
                }

                function n() {
                    a._addClass(i.newTab.closest("li"), "ui-tabs-active", "ui-state-active"),
                        o.length && a.options.show ? a._show(o, a.options.show, s) : (o.show(),
                            s())
                }
                var a = this,
                    o = i.newPanel,
                    r = i.oldPanel;
                this.running = !0,
                    r.length && this.options.hide ? this._hide(r, this.options.hide, function() {
                        a._removeClass(i.oldTab.closest("li"), "ui-tabs-active", "ui-state-active"),
                            n()
                    }) : (this._removeClass(i.oldTab.closest("li"), "ui-tabs-active", "ui-state-active"),
                        r.hide(),
                        n()),
                    r.attr("aria-hidden", "true"),
                    i.oldTab.attr({
                        "aria-selected": "false",
                        "aria-expanded": "false"
                    }),
                    o.length && r.length ? i.oldTab.attr("tabIndex", -1) : o.length && this.tabs.filter(function() {
                        return 0 === e(this).attr("tabIndex")
                    }).attr("tabIndex", -1),
                    o.attr("aria-hidden", "false"),
                    i.newTab.attr({
                        "aria-selected": "true",
                        "aria-expanded": "true",
                        tabIndex: 0
                    })
            },
            _activate: function(t) {
                var i, s = this._findActive(t);
                s[0] !== this.active[0] && (s.length || (s = this.active),
                    i = s.find(".ui-tabs-anchor")[0],
                    this._eventHandler({
                        target: i,
                        currentTarget: i,
                        preventDefault: e.noop
                    }))
            },
            _findActive: function(t) {
                return !1 === t ? e() : this.tabs.eq(t)
            },
            _getIndex: function(t) {
                return "string" == typeof t && (t = this.anchors.index(this.anchors.filter("[href$='" + e.ui.escapeSelector(t) + "']"))),
                    t
            },
            _destroy: function() {
                this.xhr && this.xhr.abort(),
                    this.tablist.removeAttr("role").off(this.eventNamespace),
                    this.anchors.removeAttr("role tabIndex").removeUniqueId(),
                    this.tabs.add(this.panels).each(function() {
                        e.data(this, "ui-tabs-destroy") ? e(this).remove() : e(this).removeAttr("role tabIndex aria-live aria-busy aria-selected aria-labelledby aria-hidden aria-expanded")
                    }),
                    this.tabs.each(function() {
                        var t = e(this),
                            i = t.data("ui-tabs-aria-controls");
                        i ? t.attr("aria-controls", i).removeData("ui-tabs-aria-controls") : t.removeAttr("aria-controls")
                    }),
                    this.panels.show(),
                    "content" !== this.options.heightStyle && this.panels.css("height", "")
            },
            enable: function(t) {
                var i = this.options.disabled;
                !1 !== i && (void 0 === t ? i = !1 : (t = this._getIndex(t),
                        i = e.isArray(i) ? e.map(i, function(e) {
                            return e !== t ? e : null
                        }) : e.map(this.tabs, function(e, i) {
                            return i !== t ? i : null
                        })),
                    this._setOptionDisabled(i))
            },
            disable: function(t) {
                var i = this.options.disabled;
                if (!0 !== i) {
                    if (void 0 === t)
                        i = !0;
                    else {
                        if (t = this._getIndex(t), -1 !== e.inArray(t, i))
                            return;
                        i = e.isArray(i) ? e.merge([t], i).sort() : [t]
                    }
                    this._setOptionDisabled(i)
                }
            },
            load: function(t, i) {
                t = this._getIndex(t);
                var s = this,
                    n = this.tabs.eq(t),
                    a = n.find(".ui-tabs-anchor"),
                    o = this._getPanelForTab(n),
                    r = {
                        tab: n,
                        panel: o
                    },
                    l = function(e, t) {
                        "abort" === t && s.panels.stop(!1, !0),
                            s._removeClass(n, "ui-tabs-loading"),
                            o.removeAttr("aria-busy"),
                            e === s.xhr && delete s.xhr
                    };
                this._isLocal(a[0]) || (this.xhr = e.ajax(this._ajaxSettings(a, i, r)),
                    this.xhr && "canceled" !== this.xhr.statusText && (this._addClass(n, "ui-tabs-loading"),
                        o.attr("aria-busy", "true"),
                        this.xhr.done(function(e, t, n) {
                            setTimeout(function() {
                                o.html(e),
                                    s._trigger("load", i, r),
                                    l(n, t)
                            }, 1)
                        }).fail(function(e, t) {
                            setTimeout(function() {
                                l(e, t)
                            }, 1)
                        })))
            },
            _ajaxSettings: function(t, i, s) {
                var n = this;
                return {
                    url: t.attr("href").replace(/#.*$/, ""),
                    beforeSend: function(t, a) {
                        return n._trigger("beforeLoad", i, e.extend({
                            jqXHR: t,
                            ajaxSettings: a
                        }, s))
                    }
                }
            },
            _getPanelForTab: function(t) {
                var i = e(t).attr("aria-controls");
                return this.element.find(this._sanitizeSelector("#" + i))
            }
        }), !1 !== e.uiBackCompat && e.widget("ui.tabs", e.ui.tabs, {
            _processTabs: function() {
                this._superApply(arguments),
                    this._addClass(this.tabs, "ui-tab")
            }
        }),
        e.widget("ui.tooltip", {
            version: "1.12.1",
            options: {
                classes: {
                    "ui-tooltip": "ui-corner-all ui-widget-shadow"
                },
                content: function() {
                    var t = e(this).attr("title") || "";
                    return e("<a>").text(t).html()
                },
                hide: !0,
                items: "[title]:not([disabled])",
                position: {
                    my: "left top+15",
                    at: "left bottom",
                    collision: "flipfit flip"
                },
                show: !0,
                track: !1,
                close: null,
                open: null
            },
            _addDescribedBy: function(t, i) {
                var s = (t.attr("aria-describedby") || "").split(/\s+/);
                s.push(i),
                    t.data("ui-tooltip-id", i).attr("aria-describedby", e.trim(s.join(" ")))
            },
            _removeDescribedBy: function(t) {
                var i = t.data("ui-tooltip-id"),
                    s = (t.attr("aria-describedby") || "").split(/\s+/),
                    n = e.inArray(i, s); -
                1 !== n && s.splice(n, 1),
                    t.removeData("ui-tooltip-id"),
                    (s = e.trim(s.join(" "))) ? t.attr("aria-describedby", s) : t.removeAttr("aria-describedby")
            },
            _create: function() {
                this._on({
                        mouseover: "open",
                        focusin: "open"
                    }),
                    this.tooltips = {},
                    this.parents = {},
                    this.liveRegion = e("<div>").attr({
                        role: "log",
                        "aria-live": "assertive",
                        "aria-relevant": "additions"
                    }).appendTo(this.document[0].body),
                    this._addClass(this.liveRegion, null, "ui-helper-hidden-accessible"),
                    this.disabledTitles = e([])
            },
            _setOption: function(t, i) {
                var s = this;
                this._super(t, i),
                    "content" === t && e.each(this.tooltips, function(e, t) {
                        s._updateContent(t.element)
                    })
            },
            _setOptionDisabled: function(e) {
                this[e ? "_disable" : "_enable"]()
            },
            _disable: function() {
                var t = this;
                e.each(this.tooltips, function(i, s) {
                        var n = e.Event("blur");
                        n.target = n.currentTarget = s.element[0],
                            t.close(n, !0)
                    }),
                    this.disabledTitles = this.disabledTitles.add(this.element.find(this.options.items).addBack().filter(function() {
                        var t = e(this);
                        return t.is("[title]") ? t.data("ui-tooltip-title", t.attr("title")).removeAttr("title") : void 0
                    }))
            },
            _enable: function() {
                this.disabledTitles.each(function() {
                        var t = e(this);
                        t.data("ui-tooltip-title") && t.attr("title", t.data("ui-tooltip-title"))
                    }),
                    this.disabledTitles = e([])
            },
            open: function(t) {
                var i = this,
                    s = e(t ? t.target : this.element).closest(this.options.items);
                s.length && !s.data("ui-tooltip-id") && (s.attr("title") && s.data("ui-tooltip-title", s.attr("title")),
                    s.data("ui-tooltip-open", !0),
                    t && "mouseover" === t.type && s.parents().each(function() {
                        var t, s = e(this);
                        s.data("ui-tooltip-open") && ((t = e.Event("blur")).target = t.currentTarget = this,
                                i.close(t, !0)),
                            s.attr("title") && (s.uniqueId(),
                                i.parents[this.id] = {
                                    element: this,
                                    title: s.attr("title")
                                },
                                s.attr("title", ""))
                    }),
                    this._registerCloseHandlers(t, s),
                    this._updateContent(s, t))
            },
            _updateContent: function(e, t) {
                var i, s = this.options.content,
                    n = this,
                    a = t ? t.type : null;
                return "string" == typeof s || s.nodeType || s.jquery ? this._open(t, e, s) : void((i = s.call(e[0], function(i) {
                    n._delay(function() {
                        e.data("ui-tooltip-open") && (t && (t.type = a),
                            this._open(t, e, i))
                    })
                })) && this._open(t, e, i))
            },
            _open: function(t, i, s) {
                function n(e) {
                    h.of = e,
                        o.is(":hidden") || o.position(h)
                }
                var a, o, r, l, h = e.extend({}, this.options.position);
                if (s) {
                    if (a = this._find(i))
                        return void a.tooltip.find(".ui-tooltip-content").html(s);
                    i.is("[title]") && (t && "mouseover" === t.type ? i.attr("title", "") : i.removeAttr("title")),
                        a = this._tooltip(i),
                        this._addDescribedBy(i, (o = a.tooltip).attr("id")),
                        o.find(".ui-tooltip-content").html(s),
                        this.liveRegion.children().hide(),
                        (l = e("<div>").html(o.find(".ui-tooltip-content").html())).removeAttr("name").find("[name]").removeAttr("name"),
                        l.removeAttr("id").find("[id]").removeAttr("id"),
                        l.appendTo(this.liveRegion),
                        this.options.track && t && /^mouse/.test(t.type) ? (this._on(this.document, {
                                mousemove: n
                            }),
                            n(t)) : o.position(e.extend({
                            of: i
                        }, this.options.position)),
                        o.hide(),
                        this._show(o, this.options.show),
                        this.options.track && this.options.show && this.options.show.delay && (r = this.delayedShow = setInterval(function() {
                            o.is(":visible") && (n(h.of),
                                clearInterval(r))
                        }, e.fx.interval)),
                        this._trigger("open", t, {
                            tooltip: o
                        })
                }
            },
            _registerCloseHandlers: function(t, i) {
                var s = {
                    keyup: function(t) {
                        if (t.keyCode === e.ui.keyCode.ESCAPE) {
                            var s = e.Event(t);
                            s.currentTarget = i[0],
                                this.close(s, !0)
                        }
                    }
                };
                i[0] !== this.element[0] && (s.remove = function() {
                        this._removeTooltip(this._find(i).tooltip)
                    }),
                    t && "mouseover" !== t.type || (s.mouseleave = "close"),
                    t && "focusin" !== t.type || (s.focusout = "close"),
                    this._on(!0, i, s)
            },
            close: function(t) {
                var i, s = this,
                    n = e(t ? t.currentTarget : this.element),
                    a = this._find(n);
                return a ? (i = a.tooltip,
                    void(a.closing || (clearInterval(this.delayedShow),
                        n.data("ui-tooltip-title") && !n.attr("title") && n.attr("title", n.data("ui-tooltip-title")),
                        this._removeDescribedBy(n),
                        a.hiding = !0,
                        i.stop(!0),
                        this._hide(i, this.options.hide, function() {
                            s._removeTooltip(e(this))
                        }),
                        n.removeData("ui-tooltip-open"),
                        this._off(n, "mouseleave focusout keyup"),
                        n[0] !== this.element[0] && this._off(n, "remove"),
                        this._off(this.document, "mousemove"),
                        t && "mouseleave" === t.type && e.each(this.parents, function(t, i) {
                            e(i.element).attr("title", i.title),
                                delete s.parents[t]
                        }),
                        a.closing = !0,
                        this._trigger("close", t, {
                            tooltip: i
                        }),
                        a.hiding || (a.closing = !1)))) : void n.removeData("ui-tooltip-open")
            },
            _tooltip: function(t) {
                var i = e("<div>").attr("role", "tooltip"),
                    s = e("<div>").appendTo(i),
                    n = i.uniqueId().attr("id");
                return this._addClass(s, "ui-tooltip-content"),
                    this._addClass(i, "ui-tooltip", "ui-widget ui-widget-content"),
                    i.appendTo(this._appendTo(t)),
                    this.tooltips[n] = {
                        element: t,
                        tooltip: i
                    }
            },
            _find: function(e) {
                var t = e.data("ui-tooltip-id");
                return t ? this.tooltips[t] : null
            },
            _removeTooltip: function(e) {
                e.remove(),
                    delete this.tooltips[e.attr("id")]
            },
            _appendTo: function(e) {
                var t = e.closest(".ui-front, dialog");
                return t.length || (t = this.document[0].body),
                    t
            },
            _destroy: function() {
                var t = this;
                e.each(this.tooltips, function(i, s) {
                        var n = e.Event("blur"),
                            a = s.element;
                        n.target = n.currentTarget = a[0],
                            t.close(n, !0),
                            e("#" + i).remove(),
                            a.data("ui-tooltip-title") && (a.attr("title") || a.attr("title", a.data("ui-tooltip-title")),
                                a.removeData("ui-tooltip-title"))
                    }),
                    this.liveRegion.remove()
            }
        }), !1 !== e.uiBackCompat && e.widget("ui.tooltip", e.ui.tooltip, {
            options: {
                tooltipClass: null
            },
            _tooltip: function() {
                var e = this._superApply(arguments);
                return this.options.tooltipClass && e.tooltip.addClass(this.options.tooltipClass),
                    e
            }
        })
}),
$(document).ready(function() {
    $(window).off("scroll").on("scroll", scrollEvent)
});
var goTopFade = function() {
        $(window).scrollTop() < 800 ? $("#pageTop").fadeOut(300) : $("#pageTop").fadeIn(300);
        var e = $(document).height() - $(window).height() - $(window).scrollTop();
        e >= $("#footer").height() ? $("#pageTop").css("bottom", "0") : $("#pageTop").css("bottom", $("#footer").height() - e)
    },
    scrollEvent = function() {
        $(window).scrollTop() <= 73 ? $(".breadcrumbs").removeClass("fixed") : $(".breadcrumbs").addClass("fixed"),
            goTopFade()
    },
    selectUI = function() {
        $(".select_ui").each(function() {
                var e = $(".select_ui"),
                    t = $(this).find(".selectedValue"),
                    i = $(this).find(".opt");
                t.click(function() {
                        var t = $(this).parent();
                        t.hasClass("disabled") || (t.hasClass("open") ? e.removeClass("open") : (e.removeClass("open"),
                            t.addClass("open")))
                    }),
                    i.click(function() {
                        $(this).text(),
                            $(this).hasClass("disabled") || e.removeClass("open")
                    })
            }),
            $("body").click(function(e) {
                $(".select_ui").hasClass("open") && ($(".select_ui").has(e.target).length || $(".select_ui").removeClass("open"))
            })
    },
    listToggle = function() {
        $(".toggleWrap").each(function() {
            var e = $(this).find(".toggleLi");
            $(this).find(".toggleBtn").click(function() {
                $(this).closest(".toggleLi").hasClass("active") ? e.removeClass("active") : (e.removeClass("active"),
                    $(this).closest(".toggleLi").addClass("active"))
            })
        })
    },
    layerPopup = function() {
        $(".btnPopOpen").click(function() {
                var e = $(this).data("pop");
                $(".layerpopup." + e).show()
            }),
            $(".pop_close").click(function() {
                $(this).closest(".layerpopup").hide()
            })
    };
! function(e, t) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : (e = e || self).Swiper = t()
}(this, function() {
    "use strict";
    var e = "undefined" == typeof document ? {
            body: {},
            addEventListener: function() {},
            removeEventListener: function() {},
            activeElement: {
                blur: function() {},
                nodeName: ""
            },
            querySelector: function() {
                return null
            },
            querySelectorAll: function() {
                return []
            },
            getElementById: function() {
                return null
            },
            createEvent: function() {
                return {
                    initEvent: function() {}
                }
            },
            createElement: function() {
                return {
                    children: [],
                    childNodes: [],
                    style: {},
                    setAttribute: function() {},
                    getElementsByTagName: function() {
                        return []
                    }
                }
            },
            location: {
                hash: ""
            }
        } : document,
        t = "undefined" == typeof window ? {
            document: e,
            navigator: {
                userAgent: ""
            },
            location: {},
            history: {},
            CustomEvent: function() {
                return this
            },
            addEventListener: function() {},
            removeEventListener: function() {},
            getComputedStyle: function() {
                return {
                    getPropertyValue: function() {
                        return ""
                    }
                }
            },
            Image: function() {},
            Date: function() {},
            screen: {},
            setTimeout: function() {},
            clearTimeout: function() {}
        } : window,
        i = function(e) {
            for (var t = 0; t < e.length; t += 1)
                this[t] = e[t];
            return this.length = e.length,
                this
        };

    function s(s, n) {
        var a = [],
            o = 0;
        if (s && !n && s instanceof i)
            return s;
        if (s)
            if ("string" == typeof s) {
                var r, l, h = s.trim();
                if (0 <= h.indexOf("<") && 0 <= h.indexOf(">")) {
                    var u = "div";
                    for (0 === h.indexOf("<li") && (u = "ul"),
                        0 === h.indexOf("<tr") && (u = "tbody"),
                        0 !== h.indexOf("<td") && 0 !== h.indexOf("<th") || (u = "tr"),
                        0 === h.indexOf("<tbody") && (u = "table"),
                        0 === h.indexOf("<option") && (u = "select"),
                        (l = e.createElement(u)).innerHTML = h,
                        o = 0; o < l.childNodes.length; o += 1)
                        a.push(l.childNodes[o])
                } else
                    for (r = n || "#" !== s[0] || s.match(/[ .<>:~]/) ? (n || e).querySelectorAll(s.trim()) : [e.getElementById(s.trim().split("#")[1])],
                        o = 0; o < r.length; o += 1)
                        r[o] && a.push(r[o])
            } else if (s.nodeType || s === t || s === e)
            a.push(s);
        else if (0 < s.length && s[0].nodeType)
            for (o = 0; o < s.length; o += 1)
                a.push(s[o]);
        return new i(a)
    }

    function n(e) {
        for (var t = [], i = 0; i < e.length; i += 1)
            -
            1 === t.indexOf(e[i]) && t.push(e[i]);
        return t
    }
    s.fn = i.prototype,
        s.Class = i,
        s.Dom7 = i;
    var a = {
        addClass: function(e) {
            if (void 0 === e)
                return this;
            for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                for (var s = 0; s < this.length; s += 1)
                    void 0 !== this[s] && void 0 !== this[s].classList && this[s].classList.add(t[i]);
            return this
        },
        removeClass: function(e) {
            for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                for (var s = 0; s < this.length; s += 1)
                    void 0 !== this[s] && void 0 !== this[s].classList && this[s].classList.remove(t[i]);
            return this
        },
        hasClass: function(e) {
            return !!this[0] && this[0].classList.contains(e)
        },
        toggleClass: function(e) {
            for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                for (var s = 0; s < this.length; s += 1)
                    void 0 !== this[s] && void 0 !== this[s].classList && this[s].classList.toggle(t[i]);
            return this
        },
        attr: function(e, t) {
            var i = arguments;
            if (1 === arguments.length && "string" == typeof e)
                return this[0] ? this[0].getAttribute(e) : void 0;
            for (var s = 0; s < this.length; s += 1)
                if (2 === i.length)
                    this[s].setAttribute(e, t);
                else
                    for (var n in e)
                        this[s][n] = e[n],
                        this[s].setAttribute(n, e[n]);
            return this
        },
        removeAttr: function(e) {
            for (var t = 0; t < this.length; t += 1)
                this[t].removeAttribute(e);
            return this
        },
        data: function(e, t) {
            var i;
            if (void 0 !== t) {
                for (var s = 0; s < this.length; s += 1)
                    (i = this[s]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}),
                    i.dom7ElementDataStorage[e] = t;
                return this
            }
            if (i = this[0])
                return i.dom7ElementDataStorage && e in i.dom7ElementDataStorage ? i.dom7ElementDataStorage[e] : i.getAttribute("data-" + e) || void 0
        },
        transform: function(e) {
            for (var t = 0; t < this.length; t += 1) {
                var i = this[t].style;
                i.webkitTransform = e,
                    i.transform = e
            }
            return this
        },
        transition: function(e) {
            "string" != typeof e && (e += "ms");
            for (var t = 0; t < this.length; t += 1) {
                var i = this[t].style;
                i.webkitTransitionDuration = e,
                    i.transitionDuration = e
            }
            return this
        },
        on: function() {
            for (var e, t = [], i = arguments.length; i--;)
                t[i] = arguments[i];
            var n = t[0],
                a = t[1],
                o = t[2],
                r = t[3];

            function l(e) {
                var t = e.target;
                if (t) {
                    var i = e.target.dom7EventData || [];
                    if (i.indexOf(e) < 0 && i.unshift(e),
                        s(t).is(a))
                        o.apply(t, i);
                    else
                        for (var n = s(t).parents(), r = 0; r < n.length; r += 1)
                            s(n[r]).is(a) && o.apply(n[r], i)
                }
            }

            function h(e) {
                var t = e && e.target && e.target.dom7EventData || [];
                t.indexOf(e) < 0 && t.unshift(e),
                    o.apply(this, t)
            }
            "function" == typeof t[1] && (n = (e = t)[0],
                    o = e[1],
                    r = e[2],
                    a = void 0),
                r || (r = !1);
            for (var u, c = n.split(" "), d = 0; d < this.length; d += 1) {
                var p = this[d];
                if (a)
                    for (u = 0; u < c.length; u += 1) {
                        var f = c[u];
                        p.dom7LiveListeners || (p.dom7LiveListeners = {}),
                            p.dom7LiveListeners[f] || (p.dom7LiveListeners[f] = []),
                            p.dom7LiveListeners[f].push({
                                listener: o,
                                proxyListener: l
                            }),
                            p.addEventListener(f, l, r)
                    }
                else
                    for (u = 0; u < c.length; u += 1) {
                        var m = c[u];
                        p.dom7Listeners || (p.dom7Listeners = {}),
                            p.dom7Listeners[m] || (p.dom7Listeners[m] = []),
                            p.dom7Listeners[m].push({
                                listener: o,
                                proxyListener: h
                            }),
                            p.addEventListener(m, h, r)
                    }
            }
            return this
        },
        off: function() {
            for (var e, t = [], i = arguments.length; i--;)
                t[i] = arguments[i];
            var s = t[0],
                n = t[1],
                a = t[2],
                o = t[3];
            "function" == typeof t[1] && (s = (e = t)[0],
                    a = e[1],
                    o = e[2],
                    n = void 0),
                o || (o = !1);
            for (var r = s.split(" "), l = 0; l < r.length; l += 1)
                for (var h = r[l], u = 0; u < this.length; u += 1) {
                    var c = this[u],
                        d = void 0;
                    if (!n && c.dom7Listeners ? d = c.dom7Listeners[h] : n && c.dom7LiveListeners && (d = c.dom7LiveListeners[h]),
                        d && d.length)
                        for (var p = d.length - 1; 0 <= p; p -= 1) {
                            var f = d[p];
                            a && f.listener === a ? (c.removeEventListener(h, f.proxyListener, o),
                                d.splice(p, 1)) : a && f.listener && f.listener.dom7proxy && f.listener.dom7proxy === a ? (c.removeEventListener(h, f.proxyListener, o),
                                d.splice(p, 1)) : a || (c.removeEventListener(h, f.proxyListener, o),
                                d.splice(p, 1))
                        }
                }
            return this
        },
        trigger: function() {
            for (var i = [], s = arguments.length; s--;)
                i[s] = arguments[s];
            for (var n = i[0].split(" "), a = i[1], o = 0; o < n.length; o += 1)
                for (var r = n[o], l = 0; l < this.length; l += 1) {
                    var h = this[l],
                        u = void 0;
                    try {
                        u = new t.CustomEvent(r, {
                            detail: a,
                            bubbles: !0,
                            cancelable: !0
                        })
                    } catch (i) {
                        (u = e.createEvent("Event")).initEvent(r, !0, !0),
                            u.detail = a
                    }
                    h.dom7EventData = i.filter(function(e, t) {
                            return 0 < t
                        }),
                        h.dispatchEvent(u),
                        h.dom7EventData = [],
                        delete h.dom7EventData
                }
            return this
        },
        transitionEnd: function(e) {
            var t, i = ["webkitTransitionEnd", "transitionend"],
                s = this;

            function n(a) {
                if (a.target === this)
                    for (e.call(this, a),
                        t = 0; t < i.length; t += 1)
                        s.off(i[t], n)
            }
            if (e)
                for (t = 0; t < i.length; t += 1)
                    s.on(i[t], n);
            return this
        },
        outerWidth: function(e) {
            if (0 < this.length) {
                if (e) {
                    var t = this.styles();
                    return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
                }
                return this[0].offsetWidth
            }
            return null
        },
        outerHeight: function(e) {
            if (0 < this.length) {
                if (e) {
                    var t = this.styles();
                    return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
                }
                return this[0].offsetHeight
            }
            return null
        },
        offset: function() {
            if (0 < this.length) {
                var i = this[0],
                    s = i.getBoundingClientRect(),
                    n = e.body;
                return {
                    top: s.top + (i === t ? t.scrollY : i.scrollTop) - (i.clientTop || n.clientTop || 0),
                    left: s.left + (i === t ? t.scrollX : i.scrollLeft) - (i.clientLeft || n.clientLeft || 0)
                }
            }
            return null
        },
        css: function(e, i) {
            var s;
            if (1 === arguments.length) {
                if ("string" != typeof e) {
                    for (s = 0; s < this.length; s += 1)
                        for (var n in e)
                            this[s].style[n] = e[n];
                    return this
                }
                if (this[0])
                    return t.getComputedStyle(this[0], null).getPropertyValue(e)
            }
            if (2 === arguments.length && "string" == typeof e) {
                for (s = 0; s < this.length; s += 1)
                    this[s].style[e] = i;
                return this
            }
            return this
        },
        each: function(e) {
            if (!e)
                return this;
            for (var t = 0; t < this.length; t += 1)
                if (!1 === e.call(this[t], t, this[t]))
                    return this;
            return this
        },
        html: function(e) {
            if (void 0 === e)
                return this[0] ? this[0].innerHTML : void 0;
            for (var t = 0; t < this.length; t += 1)
                this[t].innerHTML = e;
            return this
        },
        text: function(e) {
            if (void 0 === e)
                return this[0] ? this[0].textContent.trim() : null;
            for (var t = 0; t < this.length; t += 1)
                this[t].textContent = e;
            return this
        },
        is: function(n) {
            var a, o, r = this[0];
            if (!r || void 0 === n)
                return !1;
            if ("string" == typeof n) {
                if (r.matches)
                    return r.matches(n);
                if (r.webkitMatchesSelector)
                    return r.webkitMatchesSelector(n);
                if (r.msMatchesSelector)
                    return r.msMatchesSelector(n);
                for (a = s(n),
                    o = 0; o < a.length; o += 1)
                    if (a[o] === r)
                        return !0;
                return !1
            }
            if (n === e)
                return r === e;
            if (n === t)
                return r === t;
            if (n.nodeType || n instanceof i) {
                for (a = n.nodeType ? [n] : n,
                    o = 0; o < a.length; o += 1)
                    if (a[o] === r)
                        return !0;
                return !1
            }
            return !1
        },
        index: function() {
            var e, t = this[0];
            if (t) {
                for (e = 0; null !== (t = t.previousSibling);)
                    1 === t.nodeType && (e += 1);
                return e
            }
        },
        eq: function(e) {
            if (void 0 === e)
                return this;
            var t, s = this.length;
            return new i(s - 1 < e ? [] : e < 0 ? (t = s + e) < 0 ? [] : [this[t]] : [this[e]])
        },
        append: function() {
            for (var t, s = [], n = arguments.length; n--;)
                s[n] = arguments[n];
            for (var a = 0; a < s.length; a += 1) {
                t = s[a];
                for (var o = 0; o < this.length; o += 1)
                    if ("string" == typeof t) {
                        var r = e.createElement("div");
                        for (r.innerHTML = t; r.firstChild;)
                            this[o].appendChild(r.firstChild)
                    } else if (t instanceof i)
                    for (var l = 0; l < t.length; l += 1)
                        this[o].appendChild(t[l]);
                else
                    this[o].appendChild(t)
            }
            return this
        },
        prepend: function(t) {
            var s, n;
            for (s = 0; s < this.length; s += 1)
                if ("string" == typeof t) {
                    var a = e.createElement("div");
                    for (a.innerHTML = t,
                        n = a.childNodes.length - 1; 0 <= n; n -= 1)
                        this[s].insertBefore(a.childNodes[n], this[s].childNodes[0])
                } else if (t instanceof i)
                for (n = 0; n < t.length; n += 1)
                    this[s].insertBefore(t[n], this[s].childNodes[0]);
            else
                this[s].insertBefore(t, this[s].childNodes[0]);
            return this
        },
        next: function(e) {
            return 0 < this.length ? e ? this[0].nextElementSibling && s(this[0].nextElementSibling).is(e) ? new i([this[0].nextElementSibling]) : new i([]) : new i(this[0].nextElementSibling ? [this[0].nextElementSibling] : []) : new i([])
        },
        nextAll: function(e) {
            var t = [],
                n = this[0];
            if (!n)
                return new i([]);
            for (; n.nextElementSibling;) {
                var a = n.nextElementSibling;
                e ? s(a).is(e) && t.push(a) : t.push(a),
                    n = a
            }
            return new i(t)
        },
        prev: function(e) {
            if (0 < this.length) {
                var t = this[0];
                return e ? t.previousElementSibling && s(t.previousElementSibling).is(e) ? new i([t.previousElementSibling]) : new i([]) : new i(t.previousElementSibling ? [t.previousElementSibling] : [])
            }
            return new i([])
        },
        prevAll: function(e) {
            var t = [],
                n = this[0];
            if (!n)
                return new i([]);
            for (; n.previousElementSibling;) {
                var a = n.previousElementSibling;
                e ? s(a).is(e) && t.push(a) : t.push(a),
                    n = a
            }
            return new i(t)
        },
        parent: function(e) {
            for (var t = [], i = 0; i < this.length; i += 1)
                null !== this[i].parentNode && (e ? s(this[i].parentNode).is(e) && t.push(this[i].parentNode) : t.push(this[i].parentNode));
            return s(n(t))
        },
        parents: function(e) {
            for (var t = [], i = 0; i < this.length; i += 1)
                for (var a = this[i].parentNode; a;)
                    e ? s(a).is(e) && t.push(a) : t.push(a),
                    a = a.parentNode;
            return s(n(t))
        },
        closest: function(e) {
            var t = this;
            return void 0 === e ? new i([]) : (t.is(e) || (t = t.parents(e).eq(0)),
                t)
        },
        find: function(e) {
            for (var t = [], s = 0; s < this.length; s += 1)
                for (var n = this[s].querySelectorAll(e), a = 0; a < n.length; a += 1)
                    t.push(n[a]);
            return new i(t)
        },
        children: function(e) {
            for (var t = [], a = 0; a < this.length; a += 1)
                for (var o = this[a].childNodes, r = 0; r < o.length; r += 1)
                    e ? 1 === o[r].nodeType && s(o[r]).is(e) && t.push(o[r]) : 1 === o[r].nodeType && t.push(o[r]);
            return new i(n(t))
        },
        remove: function() {
            for (var e = 0; e < this.length; e += 1)
                this[e].parentNode && this[e].parentNode.removeChild(this[e]);
            return this
        },
        add: function() {
            for (var e = [], t = arguments.length; t--;)
                e[t] = arguments[t];
            var i, n;
            for (i = 0; i < e.length; i += 1) {
                var a = s(e[i]);
                for (n = 0; n < a.length; n += 1)
                    this[this.length] = a[n],
                    this.length += 1
            }
            return this
        },
        styles: function() {
            return this[0] ? t.getComputedStyle(this[0], null) : {}
        }
    };
    Object.keys(a).forEach(function(e) {
        s.fn[e] = a[e]
    });
    var o, r, l, h, u = {
            deleteProps: function(e) {
                var t = e;
                Object.keys(t).forEach(function(e) {
                    try {
                        t[e] = null
                    } catch (e) {}
                    try {
                        delete t[e]
                    } catch (e) {}
                })
            },
            nextTick: function(e, t) {
                return void 0 === t && (t = 0),
                    setTimeout(e, t)
            },
            now: function() {
                return Date.now()
            },
            getTranslate: function(e, i) {
                var s, n, a;
                void 0 === i && (i = "x");
                var o = t.getComputedStyle(e, null);
                return t.WebKitCSSMatrix ? (6 < (n = o.transform || o.webkitTransform).split(",").length && (n = n.split(", ").map(function(e) {
                            return e.replace(",", ".")
                        }).join(", ")),
                        a = new t.WebKitCSSMatrix("none" === n ? "" : n)) : s = (a = o.MozTransform || o.OTransform || o.MsTransform || o.msTransform || o.transform || o.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","),
                    "x" === i && (n = t.WebKitCSSMatrix ? a.m41 : 16 === s.length ? parseFloat(s[12]) : parseFloat(s[4])),
                    "y" === i && (n = t.WebKitCSSMatrix ? a.m42 : 16 === s.length ? parseFloat(s[13]) : parseFloat(s[5])),
                    n || 0
            },
            parseUrlQuery: function(e) {
                var i, s, n, a, o = {},
                    r = e || t.location.href;
                if ("string" == typeof r && r.length)
                    for (a = (s = (r = -1 < r.indexOf("?") ? r.replace(/\S*\?/, "") : "").split("&").filter(function(e) {
                            return "" !== e
                        })).length,
                        i = 0; i < a; i += 1)
                        n = s[i].replace(/#\S+/g, "").split("="),
                        o[decodeURIComponent(n[0])] = void 0 === n[1] ? void 0 : decodeURIComponent(n[1]) || "";
                return o
            },
            isObject: function(e) {
                return "object" == typeof e && null !== e && e.constructor && e.constructor === Object
            },
            extend: function() {
                for (var e = [], t = arguments.length; t--;)
                    e[t] = arguments[t];
                for (var i = Object(e[0]), s = 1; s < e.length; s += 1) {
                    var n = e[s];
                    if (null != n)
                        for (var a = Object.keys(Object(n)), o = 0, r = a.length; o < r; o += 1) {
                            var l = a[o],
                                h = Object.getOwnPropertyDescriptor(n, l);
                            void 0 !== h && h.enumerable && (u.isObject(i[l]) && u.isObject(n[l]) ? u.extend(i[l], n[l]) : !u.isObject(i[l]) && u.isObject(n[l]) ? (i[l] = {},
                                u.extend(i[l], n[l])) : i[l] = n[l])
                        }
                }
                return i
            }
        },
        c = (l = e.createElement("div"), {
            touch: t.Modernizr && !0 === t.Modernizr.touch || !!(0 < t.navigator.maxTouchPoints || "ontouchstart" in t || t.DocumentTouch && e instanceof t.DocumentTouch),
            pointerEvents: !!(t.navigator.pointerEnabled || t.PointerEvent || "maxTouchPoints" in t.navigator && 0 < t.navigator.maxTouchPoints),
            prefixedPointerEvents: !!t.navigator.msPointerEnabled,
            transition: (r = l.style,
                "transition" in r || "webkitTransition" in r || "MozTransition" in r),
            transforms3d: t.Modernizr && !0 === t.Modernizr.csstransforms3d || (o = l.style,
                "webkitPerspective" in o || "MozPerspective" in o || "OPerspective" in o || "MsPerspective" in o || "perspective" in o),
            flexbox: function() {
                for (var e = l.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), i = 0; i < t.length; i += 1)
                    if (t[i] in e)
                        return !0;
                return !1
            }(),
            observer: "MutationObserver" in t || "WebkitMutationObserver" in t,
            passiveListener: function() {
                var e = !1;
                try {
                    var i = Object.defineProperty({}, "passive", {
                        get: function() {
                            e = !0
                        }
                    });
                    t.addEventListener("testPassiveListener", null, i)
                } catch (e) {}
                return e
            }(),
            gestures: "ongesturestart" in t
        }),
        d = {
            isIE: !!t.navigator.userAgent.match(/Trident/g) || !!t.navigator.userAgent.match(/MSIE/g),
            isEdge: !!t.navigator.userAgent.match(/Edge/g),
            isSafari: (h = t.navigator.userAgent.toLowerCase(),
                0 <= h.indexOf("safari") && h.indexOf("chrome") < 0 && h.indexOf("android") < 0),
            isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(t.navigator.userAgent)
        },
        p = function(e) {
            void 0 === e && (e = {});
            var t = this;
            t.params = e,
                t.eventsListeners = {},
                t.params && t.params.on && Object.keys(t.params.on).forEach(function(e) {
                    t.on(e, t.params.on[e])
                })
        },
        f = {
            components: {
                configurable: !0
            }
        };
    p.prototype.on = function(e, t, i) {
            var s = this;
            if ("function" != typeof t)
                return s;
            var n = i ? "unshift" : "push";
            return e.split(" ").forEach(function(e) {
                    s.eventsListeners[e] || (s.eventsListeners[e] = []),
                        s.eventsListeners[e][n](t)
                }),
                s
        },
        p.prototype.once = function(e, t, i) {
            var s = this;
            if ("function" != typeof t)
                return s;

            function n() {
                for (var i = [], a = arguments.length; a--;)
                    i[a] = arguments[a];
                t.apply(s, i),
                    s.off(e, n),
                    n.f7proxy && delete n.f7proxy
            }
            return n.f7proxy = t,
                s.on(e, n, i)
        },
        p.prototype.off = function(e, t) {
            var i = this;
            return i.eventsListeners && e.split(" ").forEach(function(e) {
                    void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e] && i.eventsListeners[e].length && i.eventsListeners[e].forEach(function(s, n) {
                        (s === t || s.f7proxy && s.f7proxy === t) && i.eventsListeners[e].splice(n, 1)
                    })
                }),
                i
        },
        p.prototype.emit = function() {
            for (var e = [], t = arguments.length; t--;)
                e[t] = arguments[t];
            var i, s, n, a = this;
            return a.eventsListeners && ("string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0],
                        s = e.slice(1, e.length),
                        n = a) : (i = e[0].events,
                        s = e[0].data,
                        n = e[0].context || a),
                    (Array.isArray(i) ? i : i.split(" ")).forEach(function(e) {
                        if (a.eventsListeners && a.eventsListeners[e]) {
                            var t = [];
                            a.eventsListeners[e].forEach(function(e) {
                                    t.push(e)
                                }),
                                t.forEach(function(e) {
                                    e.apply(n, s)
                                })
                        }
                    })),
                a
        },
        p.prototype.useModulesParams = function(e) {
            var t = this;
            t.modules && Object.keys(t.modules).forEach(function(i) {
                var s = t.modules[i];
                s.params && u.extend(e, s.params)
            })
        },
        p.prototype.useModules = function(e) {
            void 0 === e && (e = {});
            var t = this;
            t.modules && Object.keys(t.modules).forEach(function(i) {
                var s = t.modules[i],
                    n = e[i] || {};
                s.instance && Object.keys(s.instance).forEach(function(e) {
                        var i = s.instance[e];
                        t[e] = "function" == typeof i ? i.bind(t) : i
                    }),
                    s.on && t.on && Object.keys(s.on).forEach(function(e) {
                        t.on(e, s.on[e])
                    }),
                    s.create && s.create.bind(t)(n)
            })
        },
        f.components.set = function(e) {
            this.use && this.use(e)
        },
        p.installModule = function(e) {
            for (var t = [], i = arguments.length - 1; 0 < i--;)
                t[i] = arguments[i + 1];
            var s = this;
            s.prototype.modules || (s.prototype.modules = {});
            var n = e.name || Object.keys(s.prototype.modules).length + "_" + u.now();
            return (s.prototype.modules[n] = e).proto && Object.keys(e.proto).forEach(function(t) {
                    s.prototype[t] = e.proto[t]
                }),
                e.static && Object.keys(e.static).forEach(function(t) {
                    s[t] = e.static[t]
                }),
                e.install && e.install.apply(s, t),
                s
        },
        p.use = function(e) {
            for (var t = [], i = arguments.length - 1; 0 < i--;)
                t[i] = arguments[i + 1];
            var s = this;
            return Array.isArray(e) ? (e.forEach(function(e) {
                    return s.installModule(e)
                }),
                s) : s.installModule.apply(s, [e].concat(t))
        },
        Object.defineProperties(p, f);
    var m = {
            updateSize: function() {
                var e, t, i = this,
                    s = i.$el;
                t = void 0 !== i.params.height ? i.params.height : s[0].clientHeight,
                    0 === (e = void 0 !== i.params.width ? i.params.width : s[0].clientWidth) && i.isHorizontal() || 0 === t && i.isVertical() || (e = e - parseInt(s.css("padding-left"), 10) - parseInt(s.css("padding-right"), 10),
                        t = t - parseInt(s.css("padding-top"), 10) - parseInt(s.css("padding-bottom"), 10),
                        u.extend(i, {
                            width: e,
                            height: t,
                            size: i.isHorizontal() ? e : t
                        }))
            },
            updateSlides: function() {
                var e = this,
                    i = e.params,
                    s = e.$wrapperEl,
                    n = e.size,
                    a = e.rtlTranslate,
                    o = e.wrongRTL,
                    r = e.virtual && i.virtual.enabled,
                    l = r ? e.virtual.slides.length : e.slides.length,
                    h = s.children("." + e.params.slideClass),
                    d = r ? e.virtual.slides.length : h.length,
                    p = [],
                    f = [],
                    m = [],
                    g = i.slidesOffsetBefore;
                "function" == typeof g && (g = i.slidesOffsetBefore.call(e));
                var v = i.slidesOffsetAfter;
                "function" == typeof v && (v = i.slidesOffsetAfter.call(e));
                var b = e.snapGrid.length,
                    y = e.snapGrid.length,
                    _ = i.spaceBetween,
                    w = -g,
                    x = 0,
                    C = 0;
                if (void 0 !== n) {
                    var T, k;
                    "string" == typeof _ && 0 <= _.indexOf("%") && (_ = parseFloat(_.replace("%", "")) / 100 * n),
                        e.virtualSize = -_,
                        h.css(a ? {
                            marginLeft: "",
                            marginTop: ""
                        } : {
                            marginRight: "",
                            marginBottom: ""
                        }),
                        1 < i.slidesPerColumn && (T = Math.floor(d / i.slidesPerColumn) === d / e.params.slidesPerColumn ? d : Math.ceil(d / i.slidesPerColumn) * i.slidesPerColumn,
                            "auto" !== i.slidesPerView && "row" === i.slidesPerColumnFill && (T = Math.max(T, i.slidesPerView * i.slidesPerColumn)));
                    for (var S, E = i.slidesPerColumn, D = T / E, M = Math.floor(d / i.slidesPerColumn), P = 0; P < d; P += 1) {
                        k = 0;
                        var I = h.eq(P);
                        if (1 < i.slidesPerColumn) {
                            var z = void 0,
                                A = void 0,
                                O = void 0;
                            "column" === i.slidesPerColumnFill ? (O = P - (A = Math.floor(P / E)) * E,
                                    (M < A || A === M && O === E - 1) && E <= (O += 1) && (O = 0,
                                        A += 1),
                                    I.css({
                                        "-webkit-box-ordinal-group": z = A + O * T / E,
                                        "-moz-box-ordinal-group": z,
                                        "-ms-flex-order": z,
                                        "-webkit-order": z,
                                        order: z
                                    })) : A = P - (O = Math.floor(P / D)) * D,
                                I.css("margin-" + (e.isHorizontal() ? "top" : "left"), 0 !== O && i.spaceBetween && i.spaceBetween + "px").attr("data-swiper-column", A).attr("data-swiper-row", O)
                        }
                        if ("none" !== I.css("display")) {
                            if ("auto" === i.slidesPerView) {
                                var H = t.getComputedStyle(I[0], null),
                                    N = I[0].style.transform,
                                    L = I[0].style.webkitTransform;
                                if (N && (I[0].style.transform = "none"),
                                    L && (I[0].style.webkitTransform = "none"),
                                    i.roundLengths)
                                    k = e.isHorizontal() ? I.outerWidth(!0) : I.outerHeight(!0);
                                else if (e.isHorizontal()) {
                                    var W = parseFloat(H.getPropertyValue("width")),
                                        $ = parseFloat(H.getPropertyValue("padding-left")),
                                        F = parseFloat(H.getPropertyValue("padding-right")),
                                        R = parseFloat(H.getPropertyValue("margin-left")),
                                        B = parseFloat(H.getPropertyValue("margin-right")),
                                        j = H.getPropertyValue("box-sizing");
                                    k = j && "border-box" === j ? W + R + B : W + $ + F + R + B
                                } else {
                                    var q = parseFloat(H.getPropertyValue("height")),
                                        Y = parseFloat(H.getPropertyValue("padding-top")),
                                        V = parseFloat(H.getPropertyValue("padding-bottom")),
                                        X = parseFloat(H.getPropertyValue("margin-top")),
                                        G = parseFloat(H.getPropertyValue("margin-bottom")),
                                        U = H.getPropertyValue("box-sizing");
                                    k = U && "border-box" === U ? q + X + G : q + Y + V + X + G
                                }
                                N && (I[0].style.transform = N),
                                    L && (I[0].style.webkitTransform = L),
                                    i.roundLengths && (k = Math.floor(k))
                            } else
                                k = (n - (i.slidesPerView - 1) * _) / i.slidesPerView,
                                i.roundLengths && (k = Math.floor(k)),
                                h[P] && (e.isHorizontal() ? h[P].style.width = k + "px" : h[P].style.height = k + "px");
                            h[P] && (h[P].swiperSlideSize = k),
                                m.push(k),
                                i.centeredSlides ? (w = w + k / 2 + x / 2 + _,
                                    0 === x && 0 !== P && (w = w - n / 2 - _),
                                    0 === P && (w = w - n / 2 - _),
                                    Math.abs(w) < .001 && (w = 0),
                                    i.roundLengths && (w = Math.floor(w)),
                                    C % i.slidesPerGroup == 0 && p.push(w),
                                    f.push(w)) : (i.roundLengths && (w = Math.floor(w)),
                                    C % i.slidesPerGroup == 0 && p.push(w),
                                    f.push(w),
                                    w = w + k + _),
                                e.virtualSize += k + _,
                                x = k,
                                C += 1
                        }
                    }
                    if (e.virtualSize = Math.max(e.virtualSize, n) + v,
                        a && o && ("slide" === i.effect || "coverflow" === i.effect) && s.css({
                            width: e.virtualSize + i.spaceBetween + "px"
                        }),
                        c.flexbox && !i.setWrapperSize || (e.isHorizontal() ? s.css({
                            width: e.virtualSize + i.spaceBetween + "px"
                        }) : s.css({
                            height: e.virtualSize + i.spaceBetween + "px"
                        })),
                        1 < i.slidesPerColumn && (e.virtualSize = (k + i.spaceBetween) * T,
                            e.virtualSize = Math.ceil(e.virtualSize / i.slidesPerColumn) - i.spaceBetween,
                            e.isHorizontal() ? s.css({
                                width: e.virtualSize + i.spaceBetween + "px"
                            }) : s.css({
                                height: e.virtualSize + i.spaceBetween + "px"
                            }),
                            i.centeredSlides)) {
                        S = [];
                        for (var K = 0; K < p.length; K += 1) {
                            var Q = p[K];
                            i.roundLengths && (Q = Math.floor(Q)),
                                p[K] < e.virtualSize + p[0] && S.push(Q)
                        }
                        p = S
                    }
                    if (!i.centeredSlides) {
                        S = [];
                        for (var J = 0; J < p.length; J += 1) {
                            var Z = p[J];
                            i.roundLengths && (Z = Math.floor(Z)),
                                p[J] <= e.virtualSize - n && S.push(Z)
                        }
                        p = S,
                            1 < Math.floor(e.virtualSize - n) - Math.floor(p[p.length - 1]) && p.push(e.virtualSize - n)
                    }
                    if (0 === p.length && (p = [0]),
                        0 !== i.spaceBetween && (e.isHorizontal() ? h.css(a ? {
                            marginLeft: _ + "px"
                        } : {
                            marginRight: _ + "px"
                        }) : h.css({
                            marginBottom: _ + "px"
                        })),
                        i.centerInsufficientSlides) {
                        var ee = 0;
                        if (m.forEach(function(e) {
                                ee += e + (i.spaceBetween ? i.spaceBetween : 0)
                            }),
                            (ee -= i.spaceBetween) < n) {
                            var te = (n - ee) / 2;
                            p.forEach(function(e, t) {
                                    p[t] = e - te
                                }),
                                f.forEach(function(e, t) {
                                    f[t] = e + te
                                })
                        }
                    }
                    u.extend(e, {
                            slides: h,
                            snapGrid: p,
                            slidesGrid: f,
                            slidesSizesGrid: m
                        }),
                        d !== l && e.emit("slidesLengthChange"),
                        p.length !== b && (e.params.watchOverflow && e.checkOverflow(),
                            e.emit("snapGridLengthChange")),
                        f.length !== y && e.emit("slidesGridLengthChange"),
                        (i.watchSlidesProgress || i.watchSlidesVisibility) && e.updateSlidesOffset()
                }
            },
            updateAutoHeight: function(e) {
                var t, i = this,
                    s = [],
                    n = 0;
                if ("number" == typeof e ? i.setTransition(e) : !0 === e && i.setTransition(i.params.speed),
                    "auto" !== i.params.slidesPerView && 1 < i.params.slidesPerView)
                    for (t = 0; t < Math.ceil(i.params.slidesPerView); t += 1) {
                        var a = i.activeIndex + t;
                        if (a > i.slides.length)
                            break;
                        s.push(i.slides.eq(a)[0])
                    }
                else
                    s.push(i.slides.eq(i.activeIndex)[0]);
                for (t = 0; t < s.length; t += 1)
                    if (void 0 !== s[t]) {
                        var o = s[t].offsetHeight;
                        n = n < o ? o : n
                    }
                n && i.$wrapperEl.css("height", n + "px")
            },
            updateSlidesOffset: function() {
                for (var e = this.slides, t = 0; t < e.length; t += 1)
                    e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop
            },
            updateSlidesProgress: function(e) {
                void 0 === e && (e = this && this.translate || 0);
                var t = this,
                    i = t.params,
                    n = t.slides,
                    a = t.rtlTranslate;
                if (0 !== n.length) {
                    void 0 === n[0].swiperSlideOffset && t.updateSlidesOffset();
                    var o = -e;
                    a && (o = e),
                        n.removeClass(i.slideVisibleClass),
                        t.visibleSlidesIndexes = [],
                        t.visibleSlides = [];
                    for (var r = 0; r < n.length; r += 1) {
                        var l = n[r],
                            h = (o + (i.centeredSlides ? t.minTranslate() : 0) - l.swiperSlideOffset) / (l.swiperSlideSize + i.spaceBetween);
                        if (i.watchSlidesVisibility) {
                            var u = -(o - l.swiperSlideOffset),
                                c = u + t.slidesSizesGrid[r];
                            (0 <= u && u < t.size || 0 < c && c <= t.size || u <= 0 && c >= t.size) && (t.visibleSlides.push(l),
                                t.visibleSlidesIndexes.push(r),
                                n.eq(r).addClass(i.slideVisibleClass))
                        }
                        l.progress = a ? -h : h
                    }
                    t.visibleSlides = s(t.visibleSlides)
                }
            },
            updateProgress: function(e) {
                void 0 === e && (e = this && this.translate || 0);
                var t = this,
                    i = t.params,
                    s = t.maxTranslate() - t.minTranslate(),
                    n = t.progress,
                    a = t.isBeginning,
                    o = t.isEnd,
                    r = a,
                    l = o;
                0 === s ? o = a = !(n = 0) : (a = (n = (e - t.minTranslate()) / s) <= 0,
                        o = 1 <= n),
                    u.extend(t, {
                        progress: n,
                        isBeginning: a,
                        isEnd: o
                    }),
                    (i.watchSlidesProgress || i.watchSlidesVisibility) && t.updateSlidesProgress(e),
                    a && !r && t.emit("reachBeginning toEdge"),
                    o && !l && t.emit("reachEnd toEdge"),
                    (r && !a || l && !o) && t.emit("fromEdge"),
                    t.emit("progress", n)
            },
            updateSlidesClasses: function() {
                var e, t = this,
                    i = t.slides,
                    s = t.params,
                    n = t.$wrapperEl,
                    a = t.activeIndex,
                    o = t.realIndex,
                    r = t.virtual && s.virtual.enabled;
                i.removeClass(s.slideActiveClass + " " + s.slideNextClass + " " + s.slidePrevClass + " " + s.slideDuplicateActiveClass + " " + s.slideDuplicateNextClass + " " + s.slideDuplicatePrevClass),
                    (e = r ? t.$wrapperEl.find("." + s.slideClass + '[data-swiper-slide-index="' + a + '"]') : i.eq(a)).addClass(s.slideActiveClass),
                    s.loop && (e.hasClass(s.slideDuplicateClass) ? n.children("." + s.slideClass + ":not(." + s.slideDuplicateClass + ')[data-swiper-slide-index="' + o + '"]').addClass(s.slideDuplicateActiveClass) : n.children("." + s.slideClass + "." + s.slideDuplicateClass + '[data-swiper-slide-index="' + o + '"]').addClass(s.slideDuplicateActiveClass));
                var l = e.nextAll("." + s.slideClass).eq(0).addClass(s.slideNextClass);
                s.loop && 0 === l.length && (l = i.eq(0)).addClass(s.slideNextClass);
                var h = e.prevAll("." + s.slideClass).eq(0).addClass(s.slidePrevClass);
                s.loop && 0 === h.length && (h = i.eq(-1)).addClass(s.slidePrevClass),
                    s.loop && (l.hasClass(s.slideDuplicateClass) ? n.children("." + s.slideClass + ":not(." + s.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(s.slideDuplicateNextClass) : n.children("." + s.slideClass + "." + s.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(s.slideDuplicateNextClass),
                        h.hasClass(s.slideDuplicateClass) ? n.children("." + s.slideClass + ":not(." + s.slideDuplicateClass + ')[data-swiper-slide-index="' + h.attr("data-swiper-slide-index") + '"]').addClass(s.slideDuplicatePrevClass) : n.children("." + s.slideClass + "." + s.slideDuplicateClass + '[data-swiper-slide-index="' + h.attr("data-swiper-slide-index") + '"]').addClass(s.slideDuplicatePrevClass))
            },
            updateActiveIndex: function(e) {
                var t, i = this,
                    s = i.rtlTranslate ? i.translate : -i.translate,
                    n = i.slidesGrid,
                    a = i.snapGrid,
                    o = i.params,
                    r = i.activeIndex,
                    l = i.realIndex,
                    h = i.snapIndex,
                    c = e;
                if (void 0 === c) {
                    for (var d = 0; d < n.length; d += 1)
                        void 0 !== n[d + 1] ? s >= n[d] && s < n[d + 1] - (n[d + 1] - n[d]) / 2 ? c = d : s >= n[d] && s < n[d + 1] && (c = d + 1) : s >= n[d] && (c = d);
                    o.normalizeSlideIndex && (c < 0 || void 0 === c) && (c = 0)
                }
                if ((t = 0 <= a.indexOf(s) ? a.indexOf(s) : Math.floor(c / o.slidesPerGroup)) >= a.length && (t = a.length - 1),
                    c !== r) {
                    var p = parseInt(i.slides.eq(c).attr("data-swiper-slide-index") || c, 10);
                    u.extend(i, {
                            snapIndex: t,
                            realIndex: p,
                            previousIndex: r,
                            activeIndex: c
                        }),
                        i.emit("activeIndexChange"),
                        i.emit("snapIndexChange"),
                        l !== p && i.emit("realIndexChange"),
                        i.emit("slideChange")
                } else
                    t !== h && (i.snapIndex = t,
                        i.emit("snapIndexChange"))
            },
            updateClickedSlide: function(e) {
                var t = this,
                    i = t.params,
                    n = s(e.target).closest("." + i.slideClass)[0],
                    a = !1;
                if (n)
                    for (var o = 0; o < t.slides.length; o += 1)
                        t.slides[o] === n && (a = !0);
                if (!n || !a)
                    return t.clickedSlide = void 0,
                        void(t.clickedIndex = void 0);
                t.clickedSlide = n,
                    t.clickedIndex = t.virtual && t.params.virtual.enabled ? parseInt(s(n).attr("data-swiper-slide-index"), 10) : s(n).index(),
                    i.slideToClickedSlide && void 0 !== t.clickedIndex && t.clickedIndex !== t.activeIndex && t.slideToClickedSlide()
            }
        },
        g = {
            getTranslate: function(e) {
                void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                var t = this.rtlTranslate,
                    i = this.translate;
                if (this.params.virtualTranslate)
                    return t ? -i : i;
                var s = u.getTranslate(this.$wrapperEl[0], e);
                return t && (s = -s),
                    s || 0
            },
            setTranslate: function(e, t) {
                var i = this,
                    s = i.rtlTranslate,
                    n = i.params,
                    a = i.$wrapperEl,
                    o = i.progress,
                    r = 0,
                    l = 0;
                i.isHorizontal() ? r = s ? -e : e : l = e,
                    n.roundLengths && (r = Math.floor(r),
                        l = Math.floor(l)),
                    n.virtualTranslate || a.transform(c.transforms3d ? "translate3d(" + r + "px, " + l + "px, 0px)" : "translate(" + r + "px, " + l + "px)"),
                    i.previousTranslate = i.translate,
                    i.translate = i.isHorizontal() ? r : l;
                var h = i.maxTranslate() - i.minTranslate();
                (0 === h ? 0 : (e - i.minTranslate()) / h) !== o && i.updateProgress(e),
                    i.emit("setTranslate", i.translate, t)
            },
            minTranslate: function() {
                return -this.snapGrid[0]
            },
            maxTranslate: function() {
                return -this.snapGrid[this.snapGrid.length - 1]
            }
        },
        v = {
            slideTo: function(e, t, i, s) {
                void 0 === e && (e = 0),
                    void 0 === t && (t = this.params.speed),
                    void 0 === i && (i = !0);
                var n = this,
                    a = e;
                a < 0 && (a = 0);
                var o = n.params,
                    r = n.snapGrid,
                    l = n.slidesGrid,
                    h = n.previousIndex,
                    u = n.activeIndex,
                    d = n.rtlTranslate;
                if (n.animating && o.preventInteractionOnTransition)
                    return !1;
                var p = Math.floor(a / o.slidesPerGroup);
                p >= r.length && (p = r.length - 1),
                    (u || o.initialSlide || 0) === (h || 0) && i && n.emit("beforeSlideChangeStart");
                var f, m = -r[p];
                if (n.updateProgress(m),
                    o.normalizeSlideIndex)
                    for (var g = 0; g < l.length; g += 1)
                        -
                        Math.floor(100 * m) >= Math.floor(100 * l[g]) && (a = g);
                if (n.initialized && a !== u) {
                    if (!n.allowSlideNext && m < n.translate && m < n.minTranslate())
                        return !1;
                    if (!n.allowSlidePrev && m > n.translate && m > n.maxTranslate() && (u || 0) !== a)
                        return !1
                }
                return f = u < a ? "next" : a < u ? "prev" : "reset",
                    d && -m === n.translate || !d && m === n.translate ? (n.updateActiveIndex(a),
                        o.autoHeight && n.updateAutoHeight(),
                        n.updateSlidesClasses(),
                        "slide" !== o.effect && n.setTranslate(m),
                        "reset" !== f && (n.transitionStart(i, f),
                            n.transitionEnd(i, f)), !1) : (0 !== t && c.transition ? (n.setTransition(t),
                        n.setTranslate(m),
                        n.updateActiveIndex(a),
                        n.updateSlidesClasses(),
                        n.emit("beforeTransitionStart", t, s),
                        n.transitionStart(i, f),
                        n.animating || (n.animating = !0,
                            n.onSlideToWrapperTransitionEnd || (n.onSlideToWrapperTransitionEnd = function(e) {
                                n && !n.destroyed && e.target === this && (n.$wrapperEl[0].removeEventListener("transitionend", n.onSlideToWrapperTransitionEnd),
                                    n.$wrapperEl[0].removeEventListener("webkitTransitionEnd", n.onSlideToWrapperTransitionEnd),
                                    n.onSlideToWrapperTransitionEnd = null,
                                    delete n.onSlideToWrapperTransitionEnd,
                                    n.transitionEnd(i, f))
                            }),
                            n.$wrapperEl[0].addEventListener("transitionend", n.onSlideToWrapperTransitionEnd),
                            n.$wrapperEl[0].addEventListener("webkitTransitionEnd", n.onSlideToWrapperTransitionEnd))) : (n.setTransition(0),
                        n.setTranslate(m),
                        n.updateActiveIndex(a),
                        n.updateSlidesClasses(),
                        n.emit("beforeTransitionStart", t, s),
                        n.transitionStart(i, f),
                        n.transitionEnd(i, f)), !0)
            },
            slideToLoop: function(e, t, i, s) {
                void 0 === e && (e = 0),
                    void 0 === t && (t = this.params.speed),
                    void 0 === i && (i = !0);
                var n = e;
                return this.params.loop && (n += this.loopedSlides),
                    this.slideTo(n, t, i, s)
            },
            slideNext: function(e, t, i) {
                void 0 === e && (e = this.params.speed),
                    void 0 === t && (t = !0);
                var s = this,
                    n = s.params;
                return n.loop ? !s.animating && (s.loopFix(),
                    s._clientLeft = s.$wrapperEl[0].clientLeft,
                    s.slideTo(s.activeIndex + n.slidesPerGroup, e, t, i)) : s.slideTo(s.activeIndex + n.slidesPerGroup, e, t, i)
            },
            slidePrev: function(e, t, i) {
                void 0 === e && (e = this.params.speed),
                    void 0 === t && (t = !0);
                var s = this,
                    n = s.snapGrid,
                    a = s.slidesGrid,
                    o = s.rtlTranslate;
                if (s.params.loop) {
                    if (s.animating)
                        return !1;
                    s.loopFix(),
                        s._clientLeft = s.$wrapperEl[0].clientLeft
                }

                function r(e) {
                    return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e)
                }
                var l, h = r(o ? s.translate : -s.translate),
                    u = n.map(function(e) {
                        return r(e)
                    }),
                    c = (a.map(function(e) {
                            return r(e)
                        }),
                        u.indexOf(h),
                        n[u.indexOf(h) - 1]);
                return void 0 !== c && (l = a.indexOf(c)) < 0 && (l = s.activeIndex - 1),
                    s.slideTo(l, e, t, i)
            },
            slideReset: function(e, t, i) {
                return void 0 === e && (e = this.params.speed),
                    void 0 === t && (t = !0),
                    this.slideTo(this.activeIndex, e, t, i)
            },
            slideToClosest: function(e, t, i) {
                void 0 === e && (e = this.params.speed),
                    void 0 === t && (t = !0);
                var s = this,
                    n = s.activeIndex,
                    a = Math.floor(n / s.params.slidesPerGroup);
                if (a < s.snapGrid.length - 1) {
                    var o = s.snapGrid[a];
                    (s.snapGrid[a + 1] - o) / 2 < (s.rtlTranslate ? s.translate : -s.translate) - o && (n = s.params.slidesPerGroup)
                }
                return s.slideTo(n, e, t, i)
            },
            slideToClickedSlide: function() {
                var e, t = this,
                    i = t.params,
                    n = t.$wrapperEl,
                    a = "auto" === i.slidesPerView ? t.slidesPerViewDynamic() : i.slidesPerView,
                    o = t.clickedIndex;
                if (i.loop) {
                    if (t.animating)
                        return;
                    e = parseInt(s(t.clickedSlide).attr("data-swiper-slide-index"), 10),
                        i.centeredSlides ? o < t.loopedSlides - a / 2 || o > t.slides.length - t.loopedSlides + a / 2 ? (t.loopFix(),
                            o = n.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(),
                            u.nextTick(function() {
                                t.slideTo(o)
                            })) : t.slideTo(o) : o > t.slides.length - a ? (t.loopFix(),
                            o = n.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(),
                            u.nextTick(function() {
                                t.slideTo(o)
                            })) : t.slideTo(o)
                } else
                    t.slideTo(o)
            }
        },
        b = {
            loopCreate: function() {
                var t = this,
                    i = t.params,
                    n = t.$wrapperEl;
                n.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                var a = n.children("." + i.slideClass);
                if (i.loopFillGroupWithBlank) {
                    var o = i.slidesPerGroup - a.length % i.slidesPerGroup;
                    if (o !== i.slidesPerGroup) {
                        for (var r = 0; r < o; r += 1) {
                            var l = s(e.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                            n.append(l)
                        }
                        a = n.children("." + i.slideClass)
                    }
                }
                "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = a.length),
                    t.loopedSlides = parseInt(i.loopedSlides || i.slidesPerView, 10),
                    t.loopedSlides += i.loopAdditionalSlides,
                    t.loopedSlides > a.length && (t.loopedSlides = a.length);
                var h = [],
                    u = [];
                a.each(function(e, i) {
                    var n = s(i);
                    e < t.loopedSlides && u.push(i),
                        e < a.length && e >= a.length - t.loopedSlides && h.push(i),
                        n.attr("data-swiper-slide-index", e)
                });
                for (var c = 0; c < u.length; c += 1)
                    n.append(s(u[c].cloneNode(!0)).addClass(i.slideDuplicateClass));
                for (var d = h.length - 1; 0 <= d; d -= 1)
                    n.prepend(s(h[d].cloneNode(!0)).addClass(i.slideDuplicateClass))
            },
            loopFix: function() {
                var e, t = this,
                    i = t.params,
                    s = t.activeIndex,
                    n = t.slides,
                    a = t.loopedSlides,
                    o = t.allowSlidePrev,
                    r = t.allowSlideNext,
                    l = t.snapGrid,
                    h = t.rtlTranslate;
                t.allowSlidePrev = !0,
                    t.allowSlideNext = !0;
                var u = -l[s] - t.getTranslate();
                s < a ? (e = n.length - 3 * a + s,
                        t.slideTo(e += a, 0, !1, !0) && 0 !== u && t.setTranslate((h ? -t.translate : t.translate) - u)) : ("auto" === i.slidesPerView && 2 * a <= s || s >= n.length - a) && (e = -n.length + s + a,
                        t.slideTo(e += a, 0, !1, !0) && 0 !== u && t.setTranslate((h ? -t.translate : t.translate) - u)),
                    t.allowSlidePrev = o,
                    t.allowSlideNext = r
            },
            loopDestroy: function() {
                var e = this.params,
                    t = this.slides;
                this.$wrapperEl.children("." + e.slideClass + "." + e.slideDuplicateClass + ",." + e.slideClass + "." + e.slideBlankClass).remove(),
                    t.removeAttr("data-swiper-slide-index")
            }
        },
        y = {
            setGrabCursor: function(e) {
                if (!(c.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked)) {
                    var t = this.el;
                    t.style.cursor = "move",
                        t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab",
                        t.style.cursor = e ? "-moz-grabbin" : "-moz-grab",
                        t.style.cursor = e ? "grabbing" : "grab"
                }
            },
            unsetGrabCursor: function() {
                c.touch || this.params.watchOverflow && this.isLocked || (this.el.style.cursor = "")
            }
        },
        _ = {
            appendSlide: function(e) {
                var t = this,
                    i = t.$wrapperEl,
                    s = t.params;
                if (s.loop && t.loopDestroy(),
                    "object" == typeof e && "length" in e)
                    for (var n = 0; n < e.length; n += 1)
                        e[n] && i.append(e[n]);
                else
                    i.append(e);
                s.loop && t.loopCreate(),
                    s.observer && c.observer || t.update()
            },
            prependSlide: function(e) {
                var t = this,
                    i = t.params,
                    s = t.$wrapperEl,
                    n = t.activeIndex;
                i.loop && t.loopDestroy();
                var a = n + 1;
                if ("object" == typeof e && "length" in e) {
                    for (var o = 0; o < e.length; o += 1)
                        e[o] && s.prepend(e[o]);
                    a = n + e.length
                } else
                    s.prepend(e);
                i.loop && t.loopCreate(),
                    i.observer && c.observer || t.update(),
                    t.slideTo(a, 0, !1)
            },
            addSlide: function(e, t) {
                var i = this,
                    s = i.$wrapperEl,
                    n = i.params,
                    a = i.activeIndex;
                n.loop && (a -= i.loopedSlides,
                    i.loopDestroy(),
                    i.slides = s.children("." + n.slideClass));
                var o = i.slides.length;
                if (e <= 0)
                    i.prependSlide(t);
                else if (o <= e)
                    i.appendSlide(t);
                else {
                    for (var r = e < a ? a + 1 : a, l = [], h = o - 1; e <= h; h -= 1) {
                        var u = i.slides.eq(h);
                        u.remove(),
                            l.unshift(u)
                    }
                    if ("object" == typeof t && "length" in t) {
                        for (var d = 0; d < t.length; d += 1)
                            t[d] && s.append(t[d]);
                        r = e < a ? a + t.length : a
                    } else
                        s.append(t);
                    for (var p = 0; p < l.length; p += 1)
                        s.append(l[p]);
                    n.loop && i.loopCreate(),
                        n.observer && c.observer || i.update(),
                        i.slideTo(n.loop ? r + i.loopedSlides : r, 0, !1)
                }
            },
            removeSlide: function(e) {
                var t = this,
                    i = t.params,
                    s = t.$wrapperEl,
                    n = t.activeIndex;
                i.loop && (n -= t.loopedSlides,
                    t.loopDestroy(),
                    t.slides = s.children("." + i.slideClass));
                var a, o = n;
                if ("object" == typeof e && "length" in e) {
                    for (var r = 0; r < e.length; r += 1)
                        t.slides[a = e[r]] && t.slides.eq(a).remove(),
                        a < o && (o -= 1);
                    o = Math.max(o, 0)
                } else
                    t.slides[a = e] && t.slides.eq(a).remove(),
                    a < o && (o -= 1),
                    o = Math.max(o, 0);
                i.loop && t.loopCreate(),
                    i.observer && c.observer || t.update(),
                    t.slideTo(i.loop ? o + t.loopedSlides : o, 0, !1)
            },
            removeAllSlides: function() {
                for (var e = [], t = 0; t < this.slides.length; t += 1)
                    e.push(t);
                this.removeSlide(e)
            }
        },
        w = function() {
            var i = t.navigator.userAgent,
                s = {
                    ios: !1,
                    android: !1,
                    androidChrome: !1,
                    desktop: !1,
                    windows: !1,
                    iphone: !1,
                    ipod: !1,
                    ipad: !1,
                    cordova: t.cordova || t.phonegap,
                    phonegap: t.cordova || t.phonegap
                },
                n = i.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                a = i.match(/(Android);?[\s\/]+([\d.]+)?/),
                o = i.match(/(iPad).*OS\s([\d_]+)/),
                r = i.match(/(iPod)(.*OS\s([\d_]+))?/),
                l = !o && i.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
            if (n && (s.os = "windows",
                    s.osVersion = n[2],
                    s.windows = !0),
                a && !n && (s.os = "android",
                    s.osVersion = a[2],
                    s.android = !0,
                    s.androidChrome = 0 <= i.toLowerCase().indexOf("chrome")),
                (o || l || r) && (s.os = "ios",
                    s.ios = !0),
                l && !r && (s.osVersion = l[2].replace(/_/g, "."),
                    s.iphone = !0),
                o && (s.osVersion = o[2].replace(/_/g, "."),
                    s.ipad = !0),
                r && (s.osVersion = r[3] ? r[3].replace(/_/g, ".") : null,
                    s.iphone = !0),
                s.ios && s.osVersion && 0 <= i.indexOf("Version/") && "10" === s.osVersion.split(".")[0] && (s.osVersion = i.toLowerCase().split("version/")[1].split(" ")[0]),
                s.desktop = !(s.os || s.android || s.webView),
                s.webView = (l || o || r) && i.match(/.*AppleWebKit(?!.*Safari)/i),
                s.os && "ios" === s.os) {
                var h = s.osVersion.split("."),
                    u = e.querySelector('meta[name="viewport"]');
                s.minimalUi = !s.webView && (r || l) && (1 * h[0] == 7 ? 1 <= 1 * h[1] : 7 < 1 * h[0]) && u && 0 <= u.getAttribute("content").indexOf("minimal-ui")
            }
            return s.pixelRatio = t.devicePixelRatio || 1,
                s
        }();

    function x() {
        var e = this,
            t = e.params,
            i = e.el;
        if (!i || 0 !== i.offsetWidth) {
            t.breakpoints && e.setBreakpoint();
            var s = e.allowSlideNext,
                n = e.allowSlidePrev,
                a = e.snapGrid;
            if (e.allowSlideNext = !0,
                e.allowSlidePrev = !0,
                e.updateSize(),
                e.updateSlides(),
                t.freeMode) {
                var o = Math.min(Math.max(e.translate, e.maxTranslate()), e.minTranslate());
                e.setTranslate(o),
                    e.updateActiveIndex(),
                    e.updateSlidesClasses(),
                    t.autoHeight && e.updateAutoHeight()
            } else
                e.updateSlidesClasses(),
                e.slideTo(("auto" === t.slidesPerView || 1 < t.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slides.length - 1 : e.activeIndex, 0, !1, !0);
            e.allowSlidePrev = n,
                e.allowSlideNext = s,
                e.params.watchOverflow && a !== e.snapGrid && e.checkOverflow()
        }
    }
    var C = {
            init: !0,
            direction: "horizontal",
            touchEventsTarget: "container",
            initialSlide: 0,
            speed: 300,
            preventInteractionOnTransition: !1,
            edgeSwipeDetection: !1,
            edgeSwipeThreshold: 20,
            freeMode: !1,
            freeModeMomentum: !0,
            freeModeMomentumRatio: 1,
            freeModeMomentumBounce: !0,
            freeModeMomentumBounceRatio: 1,
            freeModeMomentumVelocityRatio: 1,
            freeModeSticky: !1,
            freeModeMinimumVelocity: .02,
            autoHeight: !1,
            setWrapperSize: !1,
            virtualTranslate: !1,
            effect: "slide",
            breakpoints: void 0,
            breakpointsInverse: !1,
            spaceBetween: 0,
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerColumnFill: "column",
            slidesPerGroup: 1,
            centeredSlides: !1,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            normalizeSlideIndex: !0,
            centerInsufficientSlides: !1,
            watchOverflow: !1,
            roundLengths: !1,
            touchRatio: 1,
            touchAngle: 45,
            simulateTouch: !0,
            shortSwipes: !0,
            longSwipes: !0,
            longSwipesRatio: .5,
            longSwipesMs: 300,
            followFinger: !0,
            allowTouchMove: !0,
            threshold: 0,
            touchMoveStopPropagation: !0,
            touchStartPreventDefault: !0,
            touchStartForcePreventDefault: !1,
            touchReleaseOnEdges: !1,
            uniqueNavElements: !0,
            resistance: !0,
            resistanceRatio: .85,
            watchSlidesProgress: !1,
            watchSlidesVisibility: !1,
            grabCursor: !1,
            preventClicks: !0,
            preventClicksPropagation: !0,
            slideToClickedSlide: !1,
            preloadImages: !0,
            updateOnImagesReady: !0,
            loop: !1,
            loopAdditionalSlides: 0,
            loopedSlides: null,
            loopFillGroupWithBlank: !1,
            allowSlidePrev: !0,
            allowSlideNext: !0,
            swipeHandler: null,
            noSwiping: !0,
            noSwipingClass: "swiper-no-swiping",
            noSwipingSelector: null,
            passiveListeners: !0,
            containerModifierClass: "swiper-container-",
            slideClass: "swiper-slide",
            slideBlankClass: "swiper-slide-invisible-blank",
            slideActiveClass: "swiper-slide-active",
            slideDuplicateActiveClass: "swiper-slide-duplicate-active",
            slideVisibleClass: "swiper-slide-visible",
            slideDuplicateClass: "swiper-slide-duplicate",
            slideNextClass: "swiper-slide-next",
            slideDuplicateNextClass: "swiper-slide-duplicate-next",
            slidePrevClass: "swiper-slide-prev",
            slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
            wrapperClass: "swiper-wrapper",
            runCallbacksOnInit: !0
        },
        T = {
            update: m,
            translate: g,
            transition: {
                setTransition: function(e, t) {
                    this.$wrapperEl.transition(e),
                        this.emit("setTransition", e, t)
                },
                transitionStart: function(e, t) {
                    void 0 === e && (e = !0);
                    var i = this,
                        s = i.activeIndex,
                        n = i.previousIndex;
                    i.params.autoHeight && i.updateAutoHeight();
                    var a = t;
                    if (a || (a = n < s ? "next" : s < n ? "prev" : "reset"),
                        i.emit("transitionStart"),
                        e && s !== n) {
                        if ("reset" === a)
                            return void i.emit("slideResetTransitionStart");
                        i.emit("slideChangeTransitionStart"),
                            i.emit("next" === a ? "slideNextTransitionStart" : "slidePrevTransitionStart")
                    }
                },
                transitionEnd: function(e, t) {
                    void 0 === e && (e = !0);
                    var i = this,
                        s = i.activeIndex,
                        n = i.previousIndex;
                    i.animating = !1,
                        i.setTransition(0);
                    var a = t;
                    if (a || (a = n < s ? "next" : s < n ? "prev" : "reset"),
                        i.emit("transitionEnd"),
                        e && s !== n) {
                        if ("reset" === a)
                            return void i.emit("slideResetTransitionEnd");
                        i.emit("slideChangeTransitionEnd"),
                            i.emit("next" === a ? "slideNextTransitionEnd" : "slidePrevTransitionEnd")
                    }
                }
            },
            slide: v,
            loop: b,
            grabCursor: y,
            manipulation: _,
            events: {
                attachEvents: function() {
                    var i = this,
                        n = i.params,
                        a = i.touchEvents,
                        o = i.el,
                        r = i.wrapperEl;
                    i.onTouchStart = (function(i) {
                            var n = this,
                                a = n.touchEventsData,
                                o = n.params,
                                r = n.touches;
                            if (!n.animating || !o.preventInteractionOnTransition) {
                                var l = i;
                                if (l.originalEvent && (l = l.originalEvent),
                                    a.isTouchEvent = "touchstart" === l.type,
                                    (a.isTouchEvent || !("which" in l) || 3 !== l.which) && !(!a.isTouchEvent && "button" in l && 0 < l.button || a.isTouched && a.isMoved))
                                    if (o.noSwiping && s(l.target).closest(o.noSwipingSelector ? o.noSwipingSelector : "." + o.noSwipingClass)[0])
                                        n.allowClick = !0;
                                    else if (!o.swipeHandler || s(l).closest(o.swipeHandler)[0]) {
                                    r.currentX = "touchstart" === l.type ? l.targetTouches[0].pageX : l.pageX,
                                        r.currentY = "touchstart" === l.type ? l.targetTouches[0].pageY : l.pageY;
                                    var h = r.currentX,
                                        c = r.currentY,
                                        d = o.edgeSwipeThreshold || o.iOSEdgeSwipeThreshold;
                                    if (!o.edgeSwipeDetection && !o.iOSEdgeSwipeDetection || !(h <= d || h >= t.screen.width - d)) {
                                        if (u.extend(a, {
                                                isTouched: !0,
                                                isMoved: !1,
                                                allowTouchCallbacks: !0,
                                                isScrolling: void 0,
                                                startMoving: void 0
                                            }),
                                            r.startX = h,
                                            r.startY = c,
                                            a.touchStartTime = u.now(),
                                            n.allowClick = !0,
                                            n.updateSize(),
                                            n.swipeDirection = void 0,
                                            0 < o.threshold && (a.allowThresholdMove = !1),
                                            "touchstart" !== l.type) {
                                            var p = !0;
                                            s(l.target).is(a.formElements) && (p = !1),
                                                e.activeElement && s(e.activeElement).is(a.formElements) && e.activeElement !== l.target && e.activeElement.blur(),
                                                (o.touchStartForcePreventDefault || p && n.allowTouchMove && o.touchStartPreventDefault) && l.preventDefault()
                                        }
                                        n.emit("touchStart", l)
                                    }
                                }
                            }
                        }).bind(i),
                        i.onTouchMove = (function(t) {
                            var i = this,
                                n = i.touchEventsData,
                                a = i.params,
                                o = i.touches,
                                r = i.rtlTranslate,
                                l = t;
                            if (l.originalEvent && (l = l.originalEvent),
                                n.isTouched) {
                                if (!n.isTouchEvent || "mousemove" !== l.type) {
                                    var h = "touchmove" === l.type ? l.targetTouches[0].pageX : l.pageX,
                                        c = "touchmove" === l.type ? l.targetTouches[0].pageY : l.pageY;
                                    if (l.preventedByNestedSwiper)
                                        return o.startX = h,
                                            void(o.startY = c);
                                    if (!i.allowTouchMove)
                                        return i.allowClick = !1,
                                            void(n.isTouched && (u.extend(o, {
                                                    startX: h,
                                                    startY: c,
                                                    currentX: h,
                                                    currentY: c
                                                }),
                                                n.touchStartTime = u.now()));
                                    if (n.isTouchEvent && a.touchReleaseOnEdges && !a.loop)
                                        if (i.isVertical()) {
                                            if (c < o.startY && i.translate <= i.maxTranslate() || c > o.startY && i.translate >= i.minTranslate())
                                                return n.isTouched = !1,
                                                    void(n.isMoved = !1)
                                        } else if (h < o.startX && i.translate <= i.maxTranslate() || h > o.startX && i.translate >= i.minTranslate())
                                        return;
                                    if (n.isTouchEvent && e.activeElement && l.target === e.activeElement && s(l.target).is(n.formElements))
                                        return n.isMoved = !0,
                                            void(i.allowClick = !1);
                                    if (n.allowTouchCallbacks && i.emit("touchMove", l), !(l.targetTouches && 1 < l.targetTouches.length)) {
                                        o.currentX = h,
                                            o.currentY = c;
                                        var d, p = o.currentX - o.startX,
                                            f = o.currentY - o.startY;
                                        if (!(i.params.threshold && Math.sqrt(Math.pow(p, 2) + Math.pow(f, 2)) < i.params.threshold))
                                            if (void 0 === n.isScrolling && (i.isHorizontal() && o.currentY === o.startY || i.isVertical() && o.currentX === o.startX ? n.isScrolling = !1 : 25 <= p * p + f * f && (d = 180 * Math.atan2(Math.abs(f), Math.abs(p)) / Math.PI,
                                                    n.isScrolling = i.isHorizontal() ? d > a.touchAngle : 90 - d > a.touchAngle)),
                                                n.isScrolling && i.emit("touchMoveOpposite", l),
                                                void 0 === n.startMoving && (o.currentX === o.startX && o.currentY === o.startY || (n.startMoving = !0)),
                                                n.isScrolling)
                                                n.isTouched = !1;
                                            else if (n.startMoving) {
                                            i.allowClick = !1,
                                                l.preventDefault(),
                                                a.touchMoveStopPropagation && !a.nested && l.stopPropagation(),
                                                n.isMoved || (a.loop && i.loopFix(),
                                                    n.startTranslate = i.getTranslate(),
                                                    i.setTransition(0),
                                                    i.animating && i.$wrapperEl.trigger("webkitTransitionEnd transitionend"),
                                                    n.allowMomentumBounce = !1, !a.grabCursor || !0 !== i.allowSlideNext && !0 !== i.allowSlidePrev || i.setGrabCursor(!0),
                                                    i.emit("sliderFirstMove", l)),
                                                i.emit("sliderMove", l),
                                                n.isMoved = !0;
                                            var m = i.isHorizontal() ? p : f;
                                            o.diff = m,
                                                m *= a.touchRatio,
                                                r && (m = -m),
                                                i.swipeDirection = 0 < m ? "prev" : "next",
                                                n.currentTranslate = m + n.startTranslate;
                                            var g = !0,
                                                v = a.resistanceRatio;
                                            if (a.touchReleaseOnEdges && (v = 0),
                                                0 < m && n.currentTranslate > i.minTranslate() ? (g = !1,
                                                    a.resistance && (n.currentTranslate = i.minTranslate() - 1 + Math.pow(-i.minTranslate() + n.startTranslate + m, v))) : m < 0 && n.currentTranslate < i.maxTranslate() && (g = !1,
                                                    a.resistance && (n.currentTranslate = i.maxTranslate() + 1 - Math.pow(i.maxTranslate() - n.startTranslate - m, v))),
                                                g && (l.preventedByNestedSwiper = !0), !i.allowSlideNext && "next" === i.swipeDirection && n.currentTranslate < n.startTranslate && (n.currentTranslate = n.startTranslate), !i.allowSlidePrev && "prev" === i.swipeDirection && n.currentTranslate > n.startTranslate && (n.currentTranslate = n.startTranslate),
                                                0 < a.threshold) {
                                                if (!(Math.abs(m) > a.threshold || n.allowThresholdMove))
                                                    return void(n.currentTranslate = n.startTranslate);
                                                if (!n.allowThresholdMove)
                                                    return n.allowThresholdMove = !0,
                                                        o.startX = o.currentX,
                                                        o.startY = o.currentY,
                                                        n.currentTranslate = n.startTranslate,
                                                        void(o.diff = i.isHorizontal() ? o.currentX - o.startX : o.currentY - o.startY)
                                            }
                                            a.followFinger && ((a.freeMode || a.watchSlidesProgress || a.watchSlidesVisibility) && (i.updateActiveIndex(),
                                                    i.updateSlidesClasses()),
                                                a.freeMode && (0 === n.velocities.length && n.velocities.push({
                                                        position: o[i.isHorizontal() ? "startX" : "startY"],
                                                        time: n.touchStartTime
                                                    }),
                                                    n.velocities.push({
                                                        position: o[i.isHorizontal() ? "currentX" : "currentY"],
                                                        time: u.now()
                                                    })),
                                                i.updateProgress(n.currentTranslate),
                                                i.setTranslate(n.currentTranslate))
                                        }
                                    }
                                }
                            } else
                                n.startMoving && n.isScrolling && i.emit("touchMoveOpposite", l)
                        }).bind(i),
                        i.onTouchEnd = (function(e) {
                            var t = this,
                                i = t.touchEventsData,
                                s = t.params,
                                n = t.touches,
                                a = t.rtlTranslate,
                                o = t.$wrapperEl,
                                r = t.slidesGrid,
                                l = t.snapGrid,
                                h = e;
                            if (h.originalEvent && (h = h.originalEvent),
                                i.allowTouchCallbacks && t.emit("touchEnd", h),
                                i.allowTouchCallbacks = !1, !i.isTouched)
                                return i.isMoved && s.grabCursor && t.setGrabCursor(!1),
                                    i.isMoved = !1,
                                    void(i.startMoving = !1);
                            s.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                            var c, d = u.now(),
                                p = d - i.touchStartTime;
                            if (t.allowClick && (t.updateClickedSlide(h),
                                    t.emit("tap", h),
                                    p < 300 && 300 < d - i.lastClickTime && (i.clickTimeout && clearTimeout(i.clickTimeout),
                                        i.clickTimeout = u.nextTick(function() {
                                            t && !t.destroyed && t.emit("click", h)
                                        }, 300)),
                                    p < 300 && d - i.lastClickTime < 300 && (i.clickTimeout && clearTimeout(i.clickTimeout),
                                        t.emit("doubleTap", h))),
                                i.lastClickTime = u.now(),
                                u.nextTick(function() {
                                    t.destroyed || (t.allowClick = !0)
                                }), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === n.diff || i.currentTranslate === i.startTranslate)
                                return i.isTouched = !1,
                                    i.isMoved = !1,
                                    void(i.startMoving = !1);
                            if (i.isTouched = !1,
                                i.isMoved = !1,
                                i.startMoving = !1,
                                c = s.followFinger ? a ? t.translate : -t.translate : -i.currentTranslate,
                                s.freeMode) {
                                if (c < -t.minTranslate())
                                    return void t.slideTo(t.activeIndex);
                                if (c > -t.maxTranslate())
                                    return void t.slideTo(t.slides.length < l.length ? l.length - 1 : t.slides.length - 1);
                                if (s.freeModeMomentum) {
                                    if (1 < i.velocities.length) {
                                        var f = i.velocities.pop(),
                                            m = i.velocities.pop(),
                                            g = f.time - m.time;
                                        t.velocity = (f.position - m.position) / g,
                                            t.velocity /= 2,
                                            Math.abs(t.velocity) < s.freeModeMinimumVelocity && (t.velocity = 0),
                                            (150 < g || 300 < u.now() - f.time) && (t.velocity = 0)
                                    } else
                                        t.velocity = 0;
                                    t.velocity *= s.freeModeMomentumVelocityRatio,
                                        i.velocities.length = 0;
                                    var v = 1e3 * s.freeModeMomentumRatio,
                                        b = t.translate + t.velocity * v;
                                    a && (b = -b);
                                    var y, _, w = !1,
                                        x = 20 * Math.abs(t.velocity) * s.freeModeMomentumBounceRatio;
                                    if (b < t.maxTranslate())
                                        s.freeModeMomentumBounce ? (b + t.maxTranslate() < -x && (b = t.maxTranslate() - x),
                                            y = t.maxTranslate(),
                                            w = !0,
                                            i.allowMomentumBounce = !0) : b = t.maxTranslate(),
                                        s.loop && s.centeredSlides && (_ = !0);
                                    else if (b > t.minTranslate())
                                        s.freeModeMomentumBounce ? (b - t.minTranslate() > x && (b = t.minTranslate() + x),
                                            y = t.minTranslate(),
                                            w = !0,
                                            i.allowMomentumBounce = !0) : b = t.minTranslate(),
                                        s.loop && s.centeredSlides && (_ = !0);
                                    else if (s.freeModeSticky) {
                                        for (var C, T = 0; T < l.length; T += 1)
                                            if (l[T] > -b) {
                                                C = T;
                                                break
                                            }
                                        b = -(b = Math.abs(l[C] - b) < Math.abs(l[C - 1] - b) || "next" === t.swipeDirection ? l[C] : l[C - 1])
                                    }
                                    if (_ && t.once("transitionEnd", function() {
                                            t.loopFix()
                                        }),
                                        0 !== t.velocity)
                                        v = a ? Math.abs((-b - t.translate) / t.velocity) : Math.abs((b - t.translate) / t.velocity);
                                    else if (s.freeModeSticky)
                                        return void t.slideToClosest();
                                    s.freeModeMomentumBounce && w ? (t.updateProgress(y),
                                            t.setTransition(v),
                                            t.setTranslate(b),
                                            t.transitionStart(!0, t.swipeDirection),
                                            t.animating = !0,
                                            o.transitionEnd(function() {
                                                t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"),
                                                    t.setTransition(s.speed),
                                                    t.setTranslate(y),
                                                    o.transitionEnd(function() {
                                                        t && !t.destroyed && t.transitionEnd()
                                                    }))
                                            })) : t.velocity ? (t.updateProgress(b),
                                            t.setTransition(v),
                                            t.setTranslate(b),
                                            t.transitionStart(!0, t.swipeDirection),
                                            t.animating || (t.animating = !0,
                                                o.transitionEnd(function() {
                                                    t && !t.destroyed && t.transitionEnd()
                                                }))) : t.updateProgress(b),
                                        t.updateActiveIndex(),
                                        t.updateSlidesClasses()
                                } else if (s.freeModeSticky)
                                    return void t.slideToClosest();
                                (!s.freeModeMomentum || p >= s.longSwipesMs) && (t.updateProgress(),
                                    t.updateActiveIndex(),
                                    t.updateSlidesClasses())
                            } else {
                                for (var k = 0, S = t.slidesSizesGrid[0], E = 0; E < r.length; E += s.slidesPerGroup)
                                    void 0 !== r[E + s.slidesPerGroup] ? c >= r[E] && c < r[E + s.slidesPerGroup] && (S = r[(k = E) + s.slidesPerGroup] - r[E]) : c >= r[E] && (k = E,
                                        S = r[r.length - 1] - r[r.length - 2]);
                                var D = (c - r[k]) / S;
                                if (p > s.longSwipesMs) {
                                    if (!s.longSwipes)
                                        return void t.slideTo(t.activeIndex);
                                    "next" === t.swipeDirection && t.slideTo(D >= s.longSwipesRatio ? k + s.slidesPerGroup : k),
                                        "prev" === t.swipeDirection && t.slideTo(D > 1 - s.longSwipesRatio ? k + s.slidesPerGroup : k)
                                } else {
                                    if (!s.shortSwipes)
                                        return void t.slideTo(t.activeIndex);
                                    "next" === t.swipeDirection && t.slideTo(k + s.slidesPerGroup),
                                        "prev" === t.swipeDirection && t.slideTo(k)
                                }
                            }
                        }).bind(i),
                        i.onClick = (function(e) {
                            this.allowClick || (this.params.preventClicks && e.preventDefault(),
                                this.params.preventClicksPropagation && this.animating && (e.stopPropagation(),
                                    e.stopImmediatePropagation()))
                        }).bind(i);
                    var l = "container" === n.touchEventsTarget ? o : r,
                        h = !!n.nested;
                    if (c.touch || !c.pointerEvents && !c.prefixedPointerEvents) {
                        if (c.touch) {
                            var d = !("touchstart" !== a.start || !c.passiveListener || !n.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                            l.addEventListener(a.start, i.onTouchStart, d),
                                l.addEventListener(a.move, i.onTouchMove, c.passiveListener ? {
                                    passive: !1,
                                    capture: h
                                } : h),
                                l.addEventListener(a.end, i.onTouchEnd, d)
                        }
                        (n.simulateTouch && !w.ios && !w.android || n.simulateTouch && !c.touch && w.ios) && (l.addEventListener("mousedown", i.onTouchStart, !1),
                            e.addEventListener("mousemove", i.onTouchMove, h),
                            e.addEventListener("mouseup", i.onTouchEnd, !1))
                    } else
                        l.addEventListener(a.start, i.onTouchStart, !1),
                        e.addEventListener(a.move, i.onTouchMove, h),
                        e.addEventListener(a.end, i.onTouchEnd, !1);
                    (n.preventClicks || n.preventClicksPropagation) && l.addEventListener("click", i.onClick, !0),
                        i.on(w.ios || w.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", x, !0)
                },
                detachEvents: function() {
                    var t = this,
                        i = t.params,
                        s = t.touchEvents,
                        n = "container" === i.touchEventsTarget ? t.el : t.wrapperEl,
                        a = !!i.nested;
                    if (c.touch || !c.pointerEvents && !c.prefixedPointerEvents) {
                        if (c.touch) {
                            var o = !("onTouchStart" !== s.start || !c.passiveListener || !i.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                            n.removeEventListener(s.start, t.onTouchStart, o),
                                n.removeEventListener(s.move, t.onTouchMove, a),
                                n.removeEventListener(s.end, t.onTouchEnd, o)
                        }
                        (i.simulateTouch && !w.ios && !w.android || i.simulateTouch && !c.touch && w.ios) && (n.removeEventListener("mousedown", t.onTouchStart, !1),
                            e.removeEventListener("mousemove", t.onTouchMove, a),
                            e.removeEventListener("mouseup", t.onTouchEnd, !1))
                    } else
                        n.removeEventListener(s.start, t.onTouchStart, !1),
                        e.removeEventListener(s.move, t.onTouchMove, a),
                        e.removeEventListener(s.end, t.onTouchEnd, !1);
                    (i.preventClicks || i.preventClicksPropagation) && n.removeEventListener("click", t.onClick, !0),
                        t.off(w.ios || w.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", x)
                }
            },
            breakpoints: {
                setBreakpoint: function() {
                    var e = this,
                        t = e.activeIndex,
                        i = e.initialized,
                        s = e.loopedSlides;
                    void 0 === s && (s = 0);
                    var n = e.params,
                        a = n.breakpoints;
                    if (a && (!a || 0 !== Object.keys(a).length)) {
                        var o = e.getBreakpoint(a);
                        if (o && e.currentBreakpoint !== o) {
                            var r = o in a ? a[o] : void 0;
                            r && ["slidesPerView", "spaceBetween", "slidesPerGroup"].forEach(function(e) {
                                var t = r[e];
                                void 0 !== t && (r[e] = "slidesPerView" !== e || "AUTO" !== t && "auto" !== t ? "slidesPerView" === e ? parseFloat(t) : parseInt(t, 10) : "auto")
                            });
                            var l = r || e.originalParams,
                                h = l.direction && l.direction !== n.direction,
                                c = n.loop && (l.slidesPerView !== n.slidesPerView || h);
                            h && i && e.changeDirection(),
                                u.extend(e.params, l),
                                u.extend(e, {
                                    allowTouchMove: e.params.allowTouchMove,
                                    allowSlideNext: e.params.allowSlideNext,
                                    allowSlidePrev: e.params.allowSlidePrev
                                }),
                                e.currentBreakpoint = o,
                                c && i && (e.loopDestroy(),
                                    e.loopCreate(),
                                    e.updateSlides(),
                                    e.slideTo(t - s + e.loopedSlides, 0, !1)),
                                e.emit("breakpoint", l)
                        }
                    }
                },
                getBreakpoint: function(e) {
                    if (e) {
                        var i = !1,
                            s = [];
                        Object.keys(e).forEach(function(e) {
                                s.push(e)
                            }),
                            s.sort(function(e, t) {
                                return parseInt(e, 10) - parseInt(t, 10)
                            });
                        for (var n = 0; n < s.length; n += 1) {
                            var a = s[n];
                            this.params.breakpointsInverse ? a <= t.innerWidth && (i = a) : a >= t.innerWidth && !i && (i = a)
                        }
                        return i || "max"
                    }
                }
            },
            checkOverflow: {
                checkOverflow: function() {
                    var e = this,
                        t = e.isLocked;
                    e.isLocked = 1 === e.snapGrid.length,
                        e.allowSlideNext = !e.isLocked,
                        e.allowSlidePrev = !e.isLocked,
                        t !== e.isLocked && e.emit(e.isLocked ? "lock" : "unlock"),
                        t && t !== e.isLocked && (e.isEnd = !1,
                            e.navigation.update())
                }
            },
            classes: {
                addClasses: function() {
                    var e = this.classNames,
                        t = this.params,
                        i = this.rtl,
                        s = this.$el,
                        n = [];
                    n.push("initialized"),
                        n.push(t.direction),
                        t.freeMode && n.push("free-mode"),
                        c.flexbox || n.push("no-flexbox"),
                        t.autoHeight && n.push("autoheight"),
                        i && n.push("rtl"),
                        1 < t.slidesPerColumn && n.push("multirow"),
                        w.android && n.push("android"),
                        w.ios && n.push("ios"),
                        (d.isIE || d.isEdge) && (c.pointerEvents || c.prefixedPointerEvents) && n.push("wp8-" + t.direction),
                        n.forEach(function(i) {
                            e.push(t.containerModifierClass + i)
                        }),
                        s.addClass(e.join(" "))
                },
                removeClasses: function() {
                    this.$el.removeClass(this.classNames.join(" "))
                }
            },
            images: {
                loadImage: function(e, i, s, n, a, o) {
                    var r;

                    function l() {
                        o && o()
                    }
                    e.complete && a ? l() : i ? ((r = new t.Image).onload = l,
                        r.onerror = l,
                        n && (r.sizes = n),
                        s && (r.srcset = s),
                        i && (r.src = i)) : l()
                },
                preloadImages: function() {
                    var e = this;

                    function t() {
                        null != e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1),
                            e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(),
                                e.emit("imagesReady")))
                    }
                    e.imagesToLoad = e.$el.find("img");
                    for (var i = 0; i < e.imagesToLoad.length; i += 1) {
                        var s = e.imagesToLoad[i];
                        e.loadImage(s, s.currentSrc || s.getAttribute("src"), s.srcset || s.getAttribute("srcset"), s.sizes || s.getAttribute("sizes"), !0, t)
                    }
                }
            }
        },
        k = {},
        S = function(e) {
            function t() {
                for (var i, n, a, o = [], r = arguments.length; r--;)
                    o[r] = arguments[r];
                1 === o.length && o[0].constructor && o[0].constructor === Object ? a = o[0] : (n = (i = o)[0],
                        a = i[1]),
                    a || (a = {}),
                    a = u.extend({}, a),
                    n && !a.el && (a.el = n),
                    e.call(this, a),
                    Object.keys(T).forEach(function(e) {
                        Object.keys(T[e]).forEach(function(i) {
                            t.prototype[i] || (t.prototype[i] = T[e][i])
                        })
                    });
                var l = this;
                void 0 === l.modules && (l.modules = {}),
                    Object.keys(l.modules).forEach(function(e) {
                        var t = l.modules[e];
                        if (t.params) {
                            var i = Object.keys(t.params)[0],
                                s = t.params[i];
                            if ("object" != typeof s || null === s)
                                return;
                            if (!(i in a && "enabled" in s))
                                return;
                            !0 === a[i] && (a[i] = {
                                    enabled: !0
                                }),
                                "object" != typeof a[i] || "enabled" in a[i] || (a[i].enabled = !0),
                                a[i] || (a[i] = {
                                    enabled: !1
                                })
                        }
                    });
                var h = u.extend({}, C);
                l.useModulesParams(h),
                    l.params = u.extend({}, h, k, a),
                    l.originalParams = u.extend({}, l.params),
                    l.passedParams = u.extend({}, a);
                var d = (l.$ = s)(l.params.el);
                if (n = d[0]) {
                    if (1 < d.length) {
                        var p = [];
                        return d.each(function(e, i) {
                                var s = u.extend({}, a, {
                                    el: i
                                });
                                p.push(new t(s))
                            }),
                            p
                    }
                    n.swiper = l,
                        d.data("swiper", l);
                    var f, m, g = d.children("." + l.params.wrapperClass);
                    return u.extend(l, {
                            $el: d,
                            el: n,
                            $wrapperEl: g,
                            wrapperEl: g[0],
                            classNames: [],
                            slides: s(),
                            slidesGrid: [],
                            snapGrid: [],
                            slidesSizesGrid: [],
                            isHorizontal: function() {
                                return "horizontal" === l.params.direction
                            },
                            isVertical: function() {
                                return "vertical" === l.params.direction
                            },
                            rtl: "rtl" === n.dir.toLowerCase() || "rtl" === d.css("direction"),
                            rtlTranslate: "horizontal" === l.params.direction && ("rtl" === n.dir.toLowerCase() || "rtl" === d.css("direction")),
                            wrongRTL: "-webkit-box" === g.css("display"),
                            activeIndex: 0,
                            realIndex: 0,
                            isBeginning: !0,
                            isEnd: !1,
                            translate: 0,
                            previousTranslate: 0,
                            progress: 0,
                            velocity: 0,
                            animating: !1,
                            allowSlideNext: l.params.allowSlideNext,
                            allowSlidePrev: l.params.allowSlidePrev,
                            touchEvents: (f = ["touchstart", "touchmove", "touchend"],
                                m = ["mousedown", "mousemove", "mouseup"],
                                c.pointerEvents ? m = ["pointerdown", "pointermove", "pointerup"] : c.prefixedPointerEvents && (m = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]),
                                l.touchEventsTouch = {
                                    start: f[0],
                                    move: f[1],
                                    end: f[2]
                                },
                                l.touchEventsDesktop = {
                                    start: m[0],
                                    move: m[1],
                                    end: m[2]
                                },
                                c.touch || !l.params.simulateTouch ? l.touchEventsTouch : l.touchEventsDesktop),
                            touchEventsData: {
                                isTouched: void 0,
                                isMoved: void 0,
                                allowTouchCallbacks: void 0,
                                touchStartTime: void 0,
                                isScrolling: void 0,
                                currentTranslate: void 0,
                                startTranslate: void 0,
                                allowThresholdMove: void 0,
                                formElements: "input, select, option, textarea, button, video",
                                lastClickTime: u.now(),
                                clickTimeout: void 0,
                                velocities: [],
                                allowMomentumBounce: void 0,
                                isTouchEvent: void 0,
                                startMoving: void 0
                            },
                            allowClick: !0,
                            allowTouchMove: l.params.allowTouchMove,
                            touches: {
                                startX: 0,
                                startY: 0,
                                currentX: 0,
                                currentY: 0,
                                diff: 0
                            },
                            imagesToLoad: [],
                            imagesLoaded: 0
                        }),
                        l.useModules(),
                        l.params.init && l.init(),
                        l
                }
            }
            e && (t.__proto__ = e);
            var i = {
                extendedDefaults: {
                    configurable: !0
                },
                defaults: {
                    configurable: !0
                },
                Class: {
                    configurable: !0
                },
                $: {
                    configurable: !0
                }
            };
            return ((t.prototype = Object.create(e && e.prototype)).constructor = t).prototype.slidesPerViewDynamic = function() {
                    var e = this,
                        t = e.slides,
                        i = e.slidesGrid,
                        s = e.size,
                        n = e.activeIndex,
                        a = 1;
                    if (e.params.centeredSlides) {
                        for (var o, r = t[n].swiperSlideSize, l = n + 1; l < t.length; l += 1)
                            t[l] && !o && (a += 1,
                                s < (r += t[l].swiperSlideSize) && (o = !0));
                        for (var h = n - 1; 0 <= h; h -= 1)
                            t[h] && !o && (a += 1,
                                s < (r += t[h].swiperSlideSize) && (o = !0))
                    } else
                        for (var u = n + 1; u < t.length; u += 1)
                            i[u] - i[n] < s && (a += 1);
                    return a
                },
                t.prototype.update = function() {
                    var e = this;
                    if (e && !e.destroyed) {
                        var t = e.snapGrid,
                            i = e.params;
                        i.breakpoints && e.setBreakpoint(),
                            e.updateSize(),
                            e.updateSlides(),
                            e.updateProgress(),
                            e.updateSlidesClasses(),
                            e.params.freeMode ? (s(),
                                e.params.autoHeight && e.updateAutoHeight()) : e.slideTo(("auto" === e.params.slidesPerView || 1 < e.params.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slides.length - 1 : e.activeIndex, 0, !1, !0) || s(),
                            i.watchOverflow && t !== e.snapGrid && e.checkOverflow(),
                            e.emit("update")
                    }

                    function s() {
                        var t = Math.min(Math.max(e.rtlTranslate ? -1 * e.translate : e.translate, e.maxTranslate()), e.minTranslate());
                        e.setTranslate(t),
                            e.updateActiveIndex(),
                            e.updateSlidesClasses()
                    }
                },
                t.prototype.changeDirection = function(e, t) {
                    void 0 === t && (t = !0);
                    var i = this,
                        s = i.params.direction;
                    return e || (e = "horizontal" === s ? "vertical" : "horizontal"),
                        e === s || "horizontal" !== e && "vertical" !== e || ("vertical" === s && (i.$el.removeClass(i.params.containerModifierClass + "vertical wp8-vertical").addClass("" + i.params.containerModifierClass + e),
                                (d.isIE || d.isEdge) && (c.pointerEvents || c.prefixedPointerEvents) && i.$el.addClass(i.params.containerModifierClass + "wp8-" + e)),
                            "horizontal" === s && (i.$el.removeClass(i.params.containerModifierClass + "horizontal wp8-horizontal").addClass("" + i.params.containerModifierClass + e),
                                (d.isIE || d.isEdge) && (c.pointerEvents || c.prefixedPointerEvents) && i.$el.addClass(i.params.containerModifierClass + "wp8-" + e)),
                            i.params.direction = e,
                            i.slides.each(function(t, i) {
                                "vertical" === e ? i.style.width = "" : i.style.height = ""
                            }),
                            i.emit("changeDirection"),
                            t && i.update()),
                        i
                },
                t.prototype.init = function() {
                    var e = this;
                    e.initialized || (e.emit("beforeInit"),
                        e.params.breakpoints && e.setBreakpoint(),
                        e.addClasses(),
                        e.params.loop && e.loopCreate(),
                        e.updateSize(),
                        e.updateSlides(),
                        e.params.watchOverflow && e.checkOverflow(),
                        e.params.grabCursor && e.setGrabCursor(),
                        e.params.preloadImages && e.preloadImages(),
                        e.slideTo(e.params.loop ? e.params.initialSlide + e.loopedSlides : e.params.initialSlide, 0, e.params.runCallbacksOnInit),
                        e.attachEvents(),
                        e.initialized = !0,
                        e.emit("init"))
                },
                t.prototype.destroy = function(e, t) {
                    void 0 === e && (e = !0),
                        void 0 === t && (t = !0);
                    var i = this,
                        s = i.params,
                        n = i.$el,
                        a = i.$wrapperEl,
                        o = i.slides;
                    return void 0 === i.params || i.destroyed || (i.emit("beforeDestroy"),
                            i.initialized = !1,
                            i.detachEvents(),
                            s.loop && i.loopDestroy(),
                            t && (i.removeClasses(),
                                n.removeAttr("style"),
                                a.removeAttr("style"),
                                o && o.length && o.removeClass([s.slideVisibleClass, s.slideActiveClass, s.slideNextClass, s.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")),
                            i.emit("destroy"),
                            Object.keys(i.eventsListeners).forEach(function(e) {
                                i.off(e)
                            }), !1 !== e && (i.$el[0].swiper = null,
                                i.$el.data("swiper", null),
                                u.deleteProps(i)),
                            i.destroyed = !0),
                        null
                },
                t.extendDefaults = function(e) {
                    u.extend(k, e)
                },
                i.extendedDefaults.get = function() {
                    return k
                },
                i.defaults.get = function() {
                    return C
                },
                i.Class.get = function() {
                    return e
                },
                i.$.get = function() {
                    return s
                },
                Object.defineProperties(t, i),
                t
        }(p),
        E = {
            name: "device",
            proto: {
                device: w
            },
            static: {
                device: w
            }
        },
        D = {
            name: "support",
            proto: {
                support: c
            },
            static: {
                support: c
            }
        },
        M = {
            name: "browser",
            proto: {
                browser: d
            },
            static: {
                browser: d
            }
        },
        P = {
            name: "resize",
            create: function() {
                var e = this;
                u.extend(e, {
                    resize: {
                        resizeHandler: function() {
                            e && !e.destroyed && e.initialized && (e.emit("beforeResize"),
                                e.emit("resize"))
                        },
                        orientationChangeHandler: function() {
                            e && !e.destroyed && e.initialized && e.emit("orientationchange")
                        }
                    }
                })
            },
            on: {
                init: function() {
                    t.addEventListener("resize", this.resize.resizeHandler),
                        t.addEventListener("orientationchange", this.resize.orientationChangeHandler)
                },
                destroy: function() {
                    t.removeEventListener("resize", this.resize.resizeHandler),
                        t.removeEventListener("orientationchange", this.resize.orientationChangeHandler)
                }
            }
        },
        I = {
            func: t.MutationObserver || t.WebkitMutationObserver,
            attach: function(e, i) {
                void 0 === i && (i = {});
                var s = this,
                    n = new I.func(function(e) {
                        if (1 !== e.length) {
                            var i = function() {
                                s.emit("observerUpdate", e[0])
                            };
                            t.requestAnimationFrame ? t.requestAnimationFrame(i) : t.setTimeout(i, 0)
                        } else
                            s.emit("observerUpdate", e[0])
                    });
                n.observe(e, {
                        attributes: void 0 === i.attributes || i.attributes,
                        childList: void 0 === i.childList || i.childList,
                        characterData: void 0 === i.characterData || i.characterData
                    }),
                    s.observer.observers.push(n)
            },
            init: function() {
                var e = this;
                if (c.observer && e.params.observer) {
                    if (e.params.observeParents)
                        for (var t = e.$el.parents(), i = 0; i < t.length; i += 1)
                            e.observer.attach(t[i]);
                    e.observer.attach(e.$el[0], {
                            childList: e.params.observeSlideChildren
                        }),
                        e.observer.attach(e.$wrapperEl[0], {
                            attributes: !1
                        })
                }
            },
            destroy: function() {
                this.observer.observers.forEach(function(e) {
                        e.disconnect()
                    }),
                    this.observer.observers = []
            }
        },
        z = {
            name: "observer",
            params: {
                observer: !1,
                observeParents: !1,
                observeSlideChildren: !1
            },
            create: function() {
                u.extend(this, {
                    observer: {
                        init: I.init.bind(this),
                        attach: I.attach.bind(this),
                        destroy: I.destroy.bind(this),
                        observers: []
                    }
                })
            },
            on: {
                init: function() {
                    this.observer.init()
                },
                destroy: function() {
                    this.observer.destroy()
                }
            }
        },
        A = {
            update: function(e) {
                var t = this,
                    i = t.params,
                    s = i.slidesPerView,
                    n = i.slidesPerGroup,
                    a = i.centeredSlides,
                    o = t.params.virtual,
                    r = o.addSlidesBefore,
                    l = o.addSlidesAfter,
                    h = t.virtual,
                    c = h.from,
                    d = h.to,
                    p = h.slides,
                    f = h.slidesGrid,
                    m = h.renderSlide,
                    g = h.offset;
                t.updateActiveIndex();
                var v, b, y, _ = t.activeIndex || 0;
                v = t.rtlTranslate ? "right" : t.isHorizontal() ? "left" : "top",
                    a ? (b = Math.floor(s / 2) + n + r,
                        y = Math.floor(s / 2) + n + l) : (b = s + (n - 1) + r,
                        y = n + l);
                var w = Math.max((_ || 0) - y, 0),
                    x = Math.min((_ || 0) + b, p.length - 1),
                    C = (t.slidesGrid[w] || 0) - (t.slidesGrid[0] || 0);

                function T() {
                    t.updateSlides(),
                        t.updateProgress(),
                        t.updateSlidesClasses(),
                        t.lazy && t.params.lazy.enabled && t.lazy.load()
                }
                if (u.extend(t.virtual, {
                        from: w,
                        to: x,
                        offset: C,
                        slidesGrid: t.slidesGrid
                    }),
                    c === w && d === x && !e)
                    return t.slidesGrid !== f && C !== g && t.slides.css(v, C + "px"),
                        void t.updateProgress();
                if (t.params.virtual.renderExternal)
                    return t.params.virtual.renderExternal.call(t, {
                            offset: C,
                            from: w,
                            to: x,
                            slides: function() {
                                for (var e = [], t = w; t <= x; t += 1)
                                    e.push(p[t]);
                                return e
                            }()
                        }),
                        void T();
                var k = [],
                    S = [];
                if (e)
                    t.$wrapperEl.find("." + t.params.slideClass).remove();
                else
                    for (var E = c; E <= d; E += 1)
                        (E < w || x < E) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + E + '"]').remove();
                for (var D = 0; D < p.length; D += 1)
                    w <= D && D <= x && (void 0 === d || e ? S.push(D) : (d < D && S.push(D),
                        D < c && k.push(D)));
                S.forEach(function(e) {
                        t.$wrapperEl.append(m(p[e], e))
                    }),
                    k.sort(function(e, t) {
                        return t - e
                    }).forEach(function(e) {
                        t.$wrapperEl.prepend(m(p[e], e))
                    }),
                    t.$wrapperEl.children(".swiper-slide").css(v, C + "px"),
                    T()
            },
            renderSlide: function(e, t) {
                var i = this,
                    n = i.params.virtual;
                if (n.cache && i.virtual.cache[t])
                    return i.virtual.cache[t];
                var a = s(n.renderSlide ? n.renderSlide.call(i, e, t) : '<div class="' + i.params.slideClass + '" data-swiper-slide-index="' + t + '">' + e + "</div>");
                return a.attr("data-swiper-slide-index") || a.attr("data-swiper-slide-index", t),
                    n.cache && (i.virtual.cache[t] = a),
                    a
            },
            appendSlide: function(e) {
                if ("object" == typeof e && "length" in e)
                    for (var t = 0; t < e.length; t += 1)
                        e[t] && this.virtual.slides.push(e[t]);
                else
                    this.virtual.slides.push(e);
                this.virtual.update(!0)
            },
            prependSlide: function(e) {
                var t = this,
                    i = t.activeIndex,
                    s = i + 1,
                    n = 1;
                if (Array.isArray(e)) {
                    for (var a = 0; a < e.length; a += 1)
                        e[a] && t.virtual.slides.unshift(e[a]);
                    s = i + e.length,
                        n = e.length
                } else
                    t.virtual.slides.unshift(e);
                if (t.params.virtual.cache) {
                    var o = t.virtual.cache,
                        r = {};
                    Object.keys(o).forEach(function(e) {
                            r[parseInt(e, 10) + n] = o[e]
                        }),
                        t.virtual.cache = r
                }
                t.virtual.update(!0),
                    t.slideTo(s, 0)
            },
            removeSlide: function(e) {
                var t = this;
                if (null != e) {
                    var i = t.activeIndex;
                    if (Array.isArray(e))
                        for (var s = e.length - 1; 0 <= s; s -= 1)
                            t.virtual.slides.splice(e[s], 1),
                            t.params.virtual.cache && delete t.virtual.cache[e[s]],
                            e[s] < i && (i -= 1),
                            i = Math.max(i, 0);
                    else
                        t.virtual.slides.splice(e, 1),
                        t.params.virtual.cache && delete t.virtual.cache[e],
                        e < i && (i -= 1),
                        i = Math.max(i, 0);
                    t.virtual.update(!0),
                        t.slideTo(i, 0)
                }
            },
            removeAllSlides: function() {
                var e = this;
                e.virtual.slides = [],
                    e.params.virtual.cache && (e.virtual.cache = {}),
                    e.virtual.update(!0),
                    e.slideTo(0, 0)
            }
        },
        O = {
            name: "virtual",
            params: {
                virtual: {
                    enabled: !1,
                    slides: [],
                    cache: !0,
                    renderSlide: null,
                    renderExternal: null,
                    addSlidesBefore: 0,
                    addSlidesAfter: 0
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    virtual: {
                        update: A.update.bind(e),
                        appendSlide: A.appendSlide.bind(e),
                        prependSlide: A.prependSlide.bind(e),
                        removeSlide: A.removeSlide.bind(e),
                        removeAllSlides: A.removeAllSlides.bind(e),
                        renderSlide: A.renderSlide.bind(e),
                        slides: e.params.virtual.slides,
                        cache: {}
                    }
                })
            },
            on: {
                beforeInit: function() {
                    var e = this;
                    if (e.params.virtual.enabled) {
                        e.classNames.push(e.params.containerModifierClass + "virtual");
                        var t = {
                            watchSlidesProgress: !0
                        };
                        u.extend(e.params, t),
                            u.extend(e.originalParams, t),
                            e.params.initialSlide || e.virtual.update()
                    }
                },
                setTranslate: function() {
                    this.params.virtual.enabled && this.virtual.update()
                }
            }
        },
        H = {
            handle: function(i) {
                var s = this,
                    n = s.rtlTranslate,
                    a = i;
                a.originalEvent && (a = a.originalEvent);
                var o = a.keyCode || a.charCode;
                if (!s.allowSlideNext && (s.isHorizontal() && 39 === o || s.isVertical() && 40 === o))
                    return !1;
                if (!s.allowSlidePrev && (s.isHorizontal() && 37 === o || s.isVertical() && 38 === o))
                    return !1;
                if (!(a.shiftKey || a.altKey || a.ctrlKey || a.metaKey || e.activeElement && e.activeElement.nodeName && ("input" === e.activeElement.nodeName.toLowerCase() || "textarea" === e.activeElement.nodeName.toLowerCase()))) {
                    if (s.params.keyboard.onlyInViewport && (37 === o || 39 === o || 38 === o || 40 === o)) {
                        var r = !1;
                        if (0 < s.$el.parents("." + s.params.slideClass).length && 0 === s.$el.parents("." + s.params.slideActiveClass).length)
                            return;
                        var l = t.innerWidth,
                            h = t.innerHeight,
                            u = s.$el.offset();
                        n && (u.left -= s.$el[0].scrollLeft);
                        for (var c = [
                                [u.left, u.top],
                                [u.left + s.width, u.top],
                                [u.left, u.top + s.height],
                                [u.left + s.width, u.top + s.height]
                            ], d = 0; d < c.length; d += 1) {
                            var p = c[d];
                            0 <= p[0] && p[0] <= l && 0 <= p[1] && p[1] <= h && (r = !0)
                        }
                        if (!r)
                            return
                    }
                    s.isHorizontal() ? (37 !== o && 39 !== o || (a.preventDefault ? a.preventDefault() : a.returnValue = !1),
                            (39 === o && !n || 37 === o && n) && s.slideNext(),
                            (37 === o && !n || 39 === o && n) && s.slidePrev()) : (38 !== o && 40 !== o || (a.preventDefault ? a.preventDefault() : a.returnValue = !1),
                            40 === o && s.slideNext(),
                            38 === o && s.slidePrev()),
                        s.emit("keyPress", o)
                }
            },
            enable: function() {
                this.keyboard.enabled || (s(e).on("keydown", this.keyboard.handle),
                    this.keyboard.enabled = !0)
            },
            disable: function() {
                this.keyboard.enabled && (s(e).off("keydown", this.keyboard.handle),
                    this.keyboard.enabled = !1)
            }
        },
        N = {
            name: "keyboard",
            params: {
                keyboard: {
                    enabled: !1,
                    onlyInViewport: !0
                }
            },
            create: function() {
                u.extend(this, {
                    keyboard: {
                        enabled: !1,
                        enable: H.enable.bind(this),
                        disable: H.disable.bind(this),
                        handle: H.handle.bind(this)
                    }
                })
            },
            on: {
                init: function() {
                    this.params.keyboard.enabled && this.keyboard.enable()
                },
                destroy: function() {
                    this.keyboard.enabled && this.keyboard.disable()
                }
            }
        },
        L = {
            lastScrollTime: u.now(),
            event: -1 < t.navigator.userAgent.indexOf("firefox") ? "DOMMouseScroll" : function() {
                var t = "onwheel",
                    i = t in e;
                if (!i) {
                    var s = e.createElement("div");
                    s.setAttribute(t, "return;"),
                        i = "function" == typeof s[t]
                }
                return !i && e.implementation && e.implementation.hasFeature && !0 !== e.implementation.hasFeature("", "") && (i = e.implementation.hasFeature("Events.wheel", "3.0")),
                    i
            }() ? "wheel" : "mousewheel",
            normalize: function(e) {
                var t = 0,
                    i = 0,
                    s = 0,
                    n = 0;
                return "detail" in e && (i = e.detail),
                    "wheelDelta" in e && (i = -e.wheelDelta / 120),
                    "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120),
                    "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120),
                    "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = i,
                        i = 0),
                    s = 10 * t,
                    n = 10 * i,
                    "deltaY" in e && (n = e.deltaY),
                    "deltaX" in e && (s = e.deltaX),
                    (s || n) && e.deltaMode && (1 === e.deltaMode ? (s *= 40,
                        n *= 40) : (s *= 800,
                        n *= 800)),
                    s && !t && (t = s < 1 ? -1 : 1),
                    n && !i && (i = n < 1 ? -1 : 1), {
                        spinX: t,
                        spinY: i,
                        pixelX: s,
                        pixelY: n
                    }
            },
            handleMouseEnter: function() {
                this.mouseEntered = !0
            },
            handleMouseLeave: function() {
                this.mouseEntered = !1
            },
            handle: function(e) {
                var i = e,
                    s = this,
                    n = s.params.mousewheel;
                if (!s.mouseEntered && !n.releaseOnEdges)
                    return !0;
                i.originalEvent && (i = i.originalEvent);
                var a = 0,
                    o = s.rtlTranslate ? -1 : 1,
                    r = L.normalize(i);
                if (n.forceToAxis)
                    if (s.isHorizontal()) {
                        if (!(Math.abs(r.pixelX) > Math.abs(r.pixelY)))
                            return !0;
                        a = r.pixelX * o
                    } else {
                        if (!(Math.abs(r.pixelY) > Math.abs(r.pixelX)))
                            return !0;
                        a = r.pixelY
                    }
                else
                    a = Math.abs(r.pixelX) > Math.abs(r.pixelY) ? -r.pixelX * o : -r.pixelY;
                if (0 === a)
                    return !0;
                if (n.invert && (a = -a),
                    s.params.freeMode) {
                    s.params.loop && s.loopFix();
                    var l = s.getTranslate() + a * n.sensitivity,
                        h = s.isBeginning,
                        c = s.isEnd;
                    if (l >= s.minTranslate() && (l = s.minTranslate()),
                        l <= s.maxTranslate() && (l = s.maxTranslate()),
                        s.setTransition(0),
                        s.setTranslate(l),
                        s.updateProgress(),
                        s.updateActiveIndex(),
                        s.updateSlidesClasses(),
                        (!h && s.isBeginning || !c && s.isEnd) && s.updateSlidesClasses(),
                        s.params.freeModeSticky && (clearTimeout(s.mousewheel.timeout),
                            s.mousewheel.timeout = u.nextTick(function() {
                                s.slideToClosest()
                            }, 300)),
                        s.emit("scroll", i),
                        s.params.autoplay && s.params.autoplayDisableOnInteraction && s.autoplay.stop(),
                        l === s.minTranslate() || l === s.maxTranslate())
                        return !0
                } else {
                    if (60 < u.now() - s.mousewheel.lastScrollTime)
                        if (a < 0)
                            if (s.isEnd && !s.params.loop || s.animating) {
                                if (n.releaseOnEdges)
                                    return !0
                            } else
                                s.slideNext(),
                                s.emit("scroll", i);
                    else if (s.isBeginning && !s.params.loop || s.animating) {
                        if (n.releaseOnEdges)
                            return !0
                    } else
                        s.slidePrev(),
                        s.emit("scroll", i);
                    s.mousewheel.lastScrollTime = (new t.Date).getTime()
                }
                return i.preventDefault ? i.preventDefault() : i.returnValue = !1, !1
            },
            enable: function() {
                var e = this;
                if (!L.event)
                    return !1;
                if (e.mousewheel.enabled)
                    return !1;
                var t = e.$el;
                return "container" !== e.params.mousewheel.eventsTarged && (t = s(e.params.mousewheel.eventsTarged)),
                    t.on("mouseenter", e.mousewheel.handleMouseEnter),
                    t.on("mouseleave", e.mousewheel.handleMouseLeave),
                    t.on(L.event, e.mousewheel.handle),
                    e.mousewheel.enabled = !0
            },
            disable: function() {
                var e = this;
                if (!L.event)
                    return !1;
                if (!e.mousewheel.enabled)
                    return !1;
                var t = e.$el;
                return "container" !== e.params.mousewheel.eventsTarged && (t = s(e.params.mousewheel.eventsTarged)),
                    t.off(L.event, e.mousewheel.handle), !(e.mousewheel.enabled = !1)
            }
        },
        W = {
            update: function() {
                var e = this,
                    t = e.params.navigation;
                if (!e.params.loop) {
                    var i = e.navigation,
                        s = i.$nextEl,
                        n = i.$prevEl;
                    n && 0 < n.length && (e.isBeginning ? n.addClass(t.disabledClass) : n.removeClass(t.disabledClass),
                            n[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass)),
                        s && 0 < s.length && (e.isEnd ? s.addClass(t.disabledClass) : s.removeClass(t.disabledClass),
                            s[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass))
                }
            },
            onPrevClick: function(e) {
                e.preventDefault(),
                    this.isBeginning && !this.params.loop || this.slidePrev()
            },
            onNextClick: function(e) {
                e.preventDefault(),
                    this.isEnd && !this.params.loop || this.slideNext()
            },
            init: function() {
                var e, t, i = this,
                    n = i.params.navigation;
                (n.nextEl || n.prevEl) && (n.nextEl && (e = s(n.nextEl),
                        i.params.uniqueNavElements && "string" == typeof n.nextEl && 1 < e.length && 1 === i.$el.find(n.nextEl).length && (e = i.$el.find(n.nextEl))),
                    n.prevEl && (t = s(n.prevEl),
                        i.params.uniqueNavElements && "string" == typeof n.prevEl && 1 < t.length && 1 === i.$el.find(n.prevEl).length && (t = i.$el.find(n.prevEl))),
                    e && 0 < e.length && e.on("click", i.navigation.onNextClick),
                    t && 0 < t.length && t.on("click", i.navigation.onPrevClick),
                    u.extend(i.navigation, {
                        $nextEl: e,
                        nextEl: e && e[0],
                        $prevEl: t,
                        prevEl: t && t[0]
                    }))
            },
            destroy: function() {
                var e = this,
                    t = e.navigation,
                    i = t.$nextEl,
                    s = t.$prevEl;
                i && i.length && (i.off("click", e.navigation.onNextClick),
                        i.removeClass(e.params.navigation.disabledClass)),
                    s && s.length && (s.off("click", e.navigation.onPrevClick),
                        s.removeClass(e.params.navigation.disabledClass))
            }
        },
        $ = {
            update: function() {
                var e = this,
                    t = e.rtl,
                    i = e.params.pagination;
                if (i.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                    var n, a = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                        o = e.pagination.$el,
                        r = e.params.loop ? Math.ceil((a - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length;
                    if (e.params.loop ? ((n = Math.ceil((e.activeIndex - e.loopedSlides) / e.params.slidesPerGroup)) > a - 1 - 2 * e.loopedSlides && (n -= a - 2 * e.loopedSlides),
                            r - 1 < n && (n -= r),
                            n < 0 && "bullets" !== e.params.paginationType && (n = r + n)) : n = void 0 !== e.snapIndex ? e.snapIndex : e.activeIndex || 0,
                        "bullets" === i.type && e.pagination.bullets && 0 < e.pagination.bullets.length) {
                        var l, h, u, c = e.pagination.bullets;
                        if (i.dynamicBullets && (e.pagination.bulletSize = c.eq(0)[e.isHorizontal() ? "outerWidth" : "outerHeight"](!0),
                                o.css(e.isHorizontal() ? "width" : "height", e.pagination.bulletSize * (i.dynamicMainBullets + 4) + "px"),
                                1 < i.dynamicMainBullets && void 0 !== e.previousIndex && (e.pagination.dynamicBulletIndex += n - e.previousIndex,
                                    e.pagination.dynamicBulletIndex > i.dynamicMainBullets - 1 ? e.pagination.dynamicBulletIndex = i.dynamicMainBullets - 1 : e.pagination.dynamicBulletIndex < 0 && (e.pagination.dynamicBulletIndex = 0)),
                                u = ((h = (l = n - e.pagination.dynamicBulletIndex) + (Math.min(c.length, i.dynamicMainBullets) - 1)) + l) / 2),
                            c.removeClass(i.bulletActiveClass + " " + i.bulletActiveClass + "-next " + i.bulletActiveClass + "-next-next " + i.bulletActiveClass + "-prev " + i.bulletActiveClass + "-prev-prev " + i.bulletActiveClass + "-main"),
                            1 < o.length)
                            c.each(function(e, t) {
                                var a = s(t),
                                    o = a.index();
                                o === n && a.addClass(i.bulletActiveClass),
                                    i.dynamicBullets && (l <= o && o <= h && a.addClass(i.bulletActiveClass + "-main"),
                                        o === l && a.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"),
                                        o === h && a.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next"))
                            });
                        else if (c.eq(n).addClass(i.bulletActiveClass),
                            i.dynamicBullets) {
                            for (var d = c.eq(l), p = c.eq(h), f = l; f <= h; f += 1)
                                c.eq(f).addClass(i.bulletActiveClass + "-main");
                            d.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"),
                                p.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next")
                        }
                        if (i.dynamicBullets) {
                            var m = Math.min(c.length, i.dynamicMainBullets + 4),
                                g = (e.pagination.bulletSize * m - e.pagination.bulletSize) / 2 - u * e.pagination.bulletSize,
                                v = t ? "right" : "left";
                            c.css(e.isHorizontal() ? v : "top", g + "px")
                        }
                    }
                    if ("fraction" === i.type && (o.find("." + i.currentClass).text(i.formatFractionCurrent(n + 1)),
                            o.find("." + i.totalClass).text(i.formatFractionTotal(r))),
                        "progressbar" === i.type) {
                        var b;
                        b = i.progressbarOpposite ? e.isHorizontal() ? "vertical" : "horizontal" : e.isHorizontal() ? "horizontal" : "vertical";
                        var y = (n + 1) / r,
                            _ = 1,
                            w = 1;
                        "horizontal" === b ? _ = y : w = y,
                            o.find("." + i.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + _ + ") scaleY(" + w + ")").transition(e.params.speed)
                    }
                    "custom" === i.type && i.renderCustom ? (o.html(i.renderCustom(e, n + 1, r)),
                            e.emit("paginationRender", e, o[0])) : e.emit("paginationUpdate", e, o[0]),
                        o[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](i.lockClass)
                }
            },
            render: function() {
                var e = this,
                    t = e.params.pagination;
                if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                    var i = e.pagination.$el,
                        s = "";
                    if ("bullets" === t.type) {
                        for (var n = e.params.loop ? Math.ceil(((e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length) - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length, a = 0; a < n; a += 1)
                            s += t.renderBullet ? t.renderBullet.call(e, a, t.bulletClass) : "<" + t.bulletElement + ' class="' + t.bulletClass + '"></' + t.bulletElement + ">";
                        i.html(s),
                            e.pagination.bullets = i.find("." + t.bulletClass)
                    }
                    "fraction" === t.type && (s = t.renderFraction ? t.renderFraction.call(e, t.currentClass, t.totalClass) : '<span class="' + t.currentClass + '"></span> / <span class="' + t.totalClass + '"></span>',
                            i.html(s)),
                        "progressbar" === t.type && (s = t.renderProgressbar ? t.renderProgressbar.call(e, t.progressbarFillClass) : '<span class="' + t.progressbarFillClass + '"></span>',
                            i.html(s)),
                        "custom" !== t.type && e.emit("paginationRender", e.pagination.$el[0])
                }
            },
            init: function() {
                var e = this,
                    t = e.params.pagination;
                if (t.el) {
                    var i = s(t.el);
                    0 !== i.length && (e.params.uniqueNavElements && "string" == typeof t.el && 1 < i.length && 1 === e.$el.find(t.el).length && (i = e.$el.find(t.el)),
                        "bullets" === t.type && t.clickable && i.addClass(t.clickableClass),
                        i.addClass(t.modifierClass + t.type),
                        "bullets" === t.type && t.dynamicBullets && (i.addClass("" + t.modifierClass + t.type + "-dynamic"),
                            e.pagination.dynamicBulletIndex = 0,
                            t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)),
                        "progressbar" === t.type && t.progressbarOpposite && i.addClass(t.progressbarOppositeClass),
                        t.clickable && i.on("click", "." + t.bulletClass, function(t) {
                            t.preventDefault();
                            var i = s(this).index() * e.params.slidesPerGroup;
                            e.params.loop && (i += e.loopedSlides),
                                e.slideTo(i)
                        }),
                        u.extend(e.pagination, {
                            $el: i,
                            el: i[0]
                        }))
                }
            },
            destroy: function() {
                var e = this,
                    t = e.params.pagination;
                if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                    var i = e.pagination.$el;
                    i.removeClass(t.hiddenClass),
                        i.removeClass(t.modifierClass + t.type),
                        e.pagination.bullets && e.pagination.bullets.removeClass(t.bulletActiveClass),
                        t.clickable && i.off("click", "." + t.bulletClass)
                }
            }
        },
        F = {
            setTranslate: function() {
                var e = this;
                if (e.params.scrollbar.el && e.scrollbar.el) {
                    var t = e.scrollbar,
                        i = t.dragSize,
                        s = t.trackSize,
                        n = t.$dragEl,
                        a = t.$el,
                        o = e.params.scrollbar,
                        r = i,
                        l = (s - i) * e.progress;
                    e.rtlTranslate ? 0 < (l = -l) ? (r = i - l,
                            l = 0) : s < -l + i && (r = s + l) : l < 0 ? (r = i + l,
                            l = 0) : s < l + i && (r = s - l),
                        e.isHorizontal() ? (n.transform(c.transforms3d ? "translate3d(" + l + "px, 0, 0)" : "translateX(" + l + "px)"),
                            n[0].style.width = r + "px") : (n.transform(c.transforms3d ? "translate3d(0px, " + l + "px, 0)" : "translateY(" + l + "px)"),
                            n[0].style.height = r + "px"),
                        o.hide && (clearTimeout(e.scrollbar.timeout),
                            a[0].style.opacity = 1,
                            e.scrollbar.timeout = setTimeout(function() {
                                a[0].style.opacity = 0,
                                    a.transition(400)
                            }, 1e3))
                }
            },
            setTransition: function(e) {
                this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e)
            },
            updateSize: function() {
                var e = this;
                if (e.params.scrollbar.el && e.scrollbar.el) {
                    var t = e.scrollbar,
                        i = t.$dragEl,
                        s = t.$el;
                    i[0].style.width = "",
                        i[0].style.height = "";
                    var n, a = e.isHorizontal() ? s[0].offsetWidth : s[0].offsetHeight,
                        o = e.size / e.virtualSize,
                        r = o * (a / e.size);
                    n = "auto" === e.params.scrollbar.dragSize ? a * o : parseInt(e.params.scrollbar.dragSize, 10),
                        e.isHorizontal() ? i[0].style.width = n + "px" : i[0].style.height = n + "px",
                        s[0].style.display = 1 <= o ? "none" : "",
                        e.params.scrollbar.hide && (s[0].style.opacity = 0),
                        u.extend(t, {
                            trackSize: a,
                            divider: o,
                            moveDivider: r,
                            dragSize: n
                        }),
                        t.$el[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](e.params.scrollbar.lockClass)
                }
            },
            setDragPosition: function(e) {
                var t, i = this,
                    s = i.scrollbar,
                    n = i.rtlTranslate,
                    a = s.$el,
                    o = s.dragSize,
                    r = s.trackSize;
                t = ((i.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY) - a.offset()[i.isHorizontal() ? "left" : "top"] - o / 2) / (r - o),
                    t = Math.max(Math.min(t, 1), 0),
                    n && (t = 1 - t);
                var l = i.minTranslate() + (i.maxTranslate() - i.minTranslate()) * t;
                i.updateProgress(l),
                    i.setTranslate(l),
                    i.updateActiveIndex(),
                    i.updateSlidesClasses()
            },
            onDragStart: function(e) {
                var t = this,
                    i = t.params.scrollbar,
                    s = t.scrollbar,
                    n = t.$wrapperEl,
                    a = s.$el,
                    o = s.$dragEl;
                t.scrollbar.isTouched = !0,
                    e.preventDefault(),
                    e.stopPropagation(),
                    n.transition(100),
                    o.transition(100),
                    s.setDragPosition(e),
                    clearTimeout(t.scrollbar.dragTimeout),
                    a.transition(0),
                    i.hide && a.css("opacity", 1),
                    t.emit("scrollbarDragStart", e)
            },
            onDragMove: function(e) {
                var t = this.scrollbar,
                    i = this.$wrapperEl,
                    s = t.$el,
                    n = t.$dragEl;
                this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1,
                    t.setDragPosition(e),
                    i.transition(0),
                    s.transition(0),
                    n.transition(0),
                    this.emit("scrollbarDragMove", e))
            },
            onDragEnd: function(e) {
                var t = this,
                    i = t.params.scrollbar,
                    s = t.scrollbar.$el;
                t.scrollbar.isTouched && (t.scrollbar.isTouched = !1,
                    i.hide && (clearTimeout(t.scrollbar.dragTimeout),
                        t.scrollbar.dragTimeout = u.nextTick(function() {
                            s.css("opacity", 0),
                                s.transition(400)
                        }, 1e3)),
                    t.emit("scrollbarDragEnd", e),
                    i.snapOnRelease && t.slideToClosest())
            },
            enableDraggable: function() {
                var t = this;
                if (t.params.scrollbar.el) {
                    var i = t.touchEventsTouch,
                        s = t.touchEventsDesktop,
                        n = t.params,
                        a = t.scrollbar.$el[0],
                        o = !(!c.passiveListener || !n.passiveListeners) && {
                            passive: !1,
                            capture: !1
                        },
                        r = !(!c.passiveListener || !n.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                    c.touch ? (a.addEventListener(i.start, t.scrollbar.onDragStart, o),
                        a.addEventListener(i.move, t.scrollbar.onDragMove, o),
                        a.addEventListener(i.end, t.scrollbar.onDragEnd, r)) : (a.addEventListener(s.start, t.scrollbar.onDragStart, o),
                        e.addEventListener(s.move, t.scrollbar.onDragMove, o),
                        e.addEventListener(s.end, t.scrollbar.onDragEnd, r))
                }
            },
            disableDraggable: function() {
                var t = this;
                if (t.params.scrollbar.el) {
                    var i = t.touchEventsTouch,
                        s = t.touchEventsDesktop,
                        n = t.params,
                        a = t.scrollbar.$el[0],
                        o = !(!c.passiveListener || !n.passiveListeners) && {
                            passive: !1,
                            capture: !1
                        },
                        r = !(!c.passiveListener || !n.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                    c.touch ? (a.removeEventListener(i.start, t.scrollbar.onDragStart, o),
                        a.removeEventListener(i.move, t.scrollbar.onDragMove, o),
                        a.removeEventListener(i.end, t.scrollbar.onDragEnd, r)) : (a.removeEventListener(s.start, t.scrollbar.onDragStart, o),
                        e.removeEventListener(s.move, t.scrollbar.onDragMove, o),
                        e.removeEventListener(s.end, t.scrollbar.onDragEnd, r))
                }
            },
            init: function() {
                var e = this;
                if (e.params.scrollbar.el) {
                    var t = e.scrollbar,
                        i = e.$el,
                        n = e.params.scrollbar,
                        a = s(n.el);
                    e.params.uniqueNavElements && "string" == typeof n.el && 1 < a.length && 1 === i.find(n.el).length && (a = i.find(n.el));
                    var o = a.find("." + e.params.scrollbar.dragClass);
                    0 === o.length && (o = s('<div class="' + e.params.scrollbar.dragClass + '"></div>'),
                            a.append(o)),
                        u.extend(t, {
                            $el: a,
                            el: a[0],
                            $dragEl: o,
                            dragEl: o[0]
                        }),
                        n.draggable && t.enableDraggable()
                }
            },
            destroy: function() {
                this.scrollbar.disableDraggable()
            }
        },
        R = {
            setTransform: function(e, t) {
                var i = this.rtl,
                    n = s(e),
                    a = i ? -1 : 1,
                    o = n.attr("data-swiper-parallax") || "0",
                    r = n.attr("data-swiper-parallax-x"),
                    l = n.attr("data-swiper-parallax-y"),
                    h = n.attr("data-swiper-parallax-scale"),
                    u = n.attr("data-swiper-parallax-opacity");
                if (r || l ? (r = r || "0",
                        l = l || "0") : this.isHorizontal() ? (r = o,
                        l = "0") : (l = o,
                        r = "0"),
                    r = 0 <= r.indexOf("%") ? parseInt(r, 10) * t * a + "%" : r * t * a + "px",
                    l = 0 <= l.indexOf("%") ? parseInt(l, 10) * t + "%" : l * t + "px",
                    null != u) {
                    var c = u - (u - 1) * (1 - Math.abs(t));
                    n[0].style.opacity = c
                }
                if (null == h)
                    n.transform("translate3d(" + r + ", " + l + ", 0px)");
                else {
                    var d = h - (h - 1) * (1 - Math.abs(t));
                    n.transform("translate3d(" + r + ", " + l + ", 0px) scale(" + d + ")")
                }
            },
            setTranslate: function() {
                var e = this,
                    t = e.slides,
                    i = e.progress,
                    n = e.snapGrid;
                e.$el.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, s) {
                        e.parallax.setTransform(s, i)
                    }),
                    t.each(function(t, a) {
                        var o = a.progress;
                        1 < e.params.slidesPerGroup && "auto" !== e.params.slidesPerView && (o += Math.ceil(t / 2) - i * (n.length - 1)),
                            o = Math.min(Math.max(o, -1), 1),
                            s(a).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) {
                                e.parallax.setTransform(i, o)
                            })
                    })
            },
            setTransition: function(e) {
                void 0 === e && (e = this.params.speed),
                    this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) {
                        var n = s(i),
                            a = parseInt(n.attr("data-swiper-parallax-duration"), 10) || e;
                        0 === e && (a = 0),
                            n.transition(a)
                    })
            }
        },
        B = {
            getDistanceBetweenTouches: function(e) {
                if (e.targetTouches.length < 2)
                    return 1;
                var t = e.targetTouches[0].pageY,
                    i = e.targetTouches[1].pageY;
                return Math.sqrt(Math.pow(e.targetTouches[1].pageX - e.targetTouches[0].pageX, 2) + Math.pow(i - t, 2))
            },
            onGestureStart: function(e) {
                var t = this,
                    i = t.params.zoom,
                    n = t.zoom,
                    a = n.gesture;
                if (n.fakeGestureTouched = !1,
                    n.fakeGestureMoved = !1, !c.gestures) {
                    if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2)
                        return;
                    n.fakeGestureTouched = !0,
                        a.scaleStart = B.getDistanceBetweenTouches(e)
                }
                a.$slideEl && a.$slideEl.length || (a.$slideEl = s(e.target).closest(".swiper-slide"),
                    0 === a.$slideEl.length && (a.$slideEl = t.slides.eq(t.activeIndex)),
                    a.$imageEl = a.$slideEl.find("img, svg, canvas"),
                    a.$imageWrapEl = a.$imageEl.parent("." + i.containerClass),
                    a.maxRatio = a.$imageWrapEl.attr("data-swiper-zoom") || i.maxRatio,
                    0 !== a.$imageWrapEl.length) ? (a.$imageEl.transition(0),
                    t.zoom.isScaling = !0) : a.$imageEl = void 0
            },
            onGestureChange: function(e) {
                var t = this.params.zoom,
                    i = this.zoom,
                    s = i.gesture;
                if (!c.gestures) {
                    if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2)
                        return;
                    i.fakeGestureMoved = !0,
                        s.scaleMove = B.getDistanceBetweenTouches(e)
                }
                s.$imageEl && 0 !== s.$imageEl.length && (i.scale = c.gestures ? e.scale * i.currentScale : s.scaleMove / s.scaleStart * i.currentScale,
                    i.scale > s.maxRatio && (i.scale = s.maxRatio - 1 + Math.pow(i.scale - s.maxRatio + 1, .5)),
                    i.scale < t.minRatio && (i.scale = t.minRatio + 1 - Math.pow(t.minRatio - i.scale + 1, .5)),
                    s.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"))
            },
            onGestureEnd: function(e) {
                var t = this.params.zoom,
                    i = this.zoom,
                    s = i.gesture;
                if (!c.gestures) {
                    if (!i.fakeGestureTouched || !i.fakeGestureMoved)
                        return;
                    if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !w.android)
                        return;
                    i.fakeGestureTouched = !1,
                        i.fakeGestureMoved = !1
                }
                s.$imageEl && 0 !== s.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, s.maxRatio), t.minRatio),
                    s.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"),
                    i.currentScale = i.scale,
                    i.isScaling = !1,
                    1 === i.scale && (s.$slideEl = void 0))
            },
            onTouchStart: function(e) {
                var t = this.zoom,
                    i = t.gesture,
                    s = t.image;
                i.$imageEl && 0 !== i.$imageEl.length && (s.isTouched || (w.android && e.preventDefault(),
                    s.isTouched = !0,
                    s.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX,
                    s.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY))
            },
            onTouchMove: function(e) {
                var t = this,
                    i = t.zoom,
                    s = i.gesture,
                    n = i.image,
                    a = i.velocity;
                if (s.$imageEl && 0 !== s.$imageEl.length && (t.allowClick = !1,
                        n.isTouched && s.$slideEl)) {
                    n.isMoved || (n.width = s.$imageEl[0].offsetWidth,
                        n.height = s.$imageEl[0].offsetHeight,
                        n.startX = u.getTranslate(s.$imageWrapEl[0], "x") || 0,
                        n.startY = u.getTranslate(s.$imageWrapEl[0], "y") || 0,
                        s.slideWidth = s.$slideEl[0].offsetWidth,
                        s.slideHeight = s.$slideEl[0].offsetHeight,
                        s.$imageWrapEl.transition(0),
                        t.rtl && (n.startX = -n.startX,
                            n.startY = -n.startY));
                    var o = n.width * i.scale,
                        r = n.height * i.scale;
                    if (!(o < s.slideWidth && r < s.slideHeight)) {
                        if (n.minX = Math.min(s.slideWidth / 2 - o / 2, 0),
                            n.maxX = -n.minX,
                            n.minY = Math.min(s.slideHeight / 2 - r / 2, 0),
                            n.maxY = -n.minY,
                            n.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX,
                            n.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !n.isMoved && !i.isScaling) {
                            if (t.isHorizontal() && (Math.floor(n.minX) === Math.floor(n.startX) && n.touchesCurrent.x < n.touchesStart.x || Math.floor(n.maxX) === Math.floor(n.startX) && n.touchesCurrent.x > n.touchesStart.x))
                                return void(n.isTouched = !1);
                            if (!t.isHorizontal() && (Math.floor(n.minY) === Math.floor(n.startY) && n.touchesCurrent.y < n.touchesStart.y || Math.floor(n.maxY) === Math.floor(n.startY) && n.touchesCurrent.y > n.touchesStart.y))
                                return void(n.isTouched = !1)
                        }
                        e.preventDefault(),
                            e.stopPropagation(),
                            n.isMoved = !0,
                            n.currentX = n.touchesCurrent.x - n.touchesStart.x + n.startX,
                            n.currentY = n.touchesCurrent.y - n.touchesStart.y + n.startY,
                            n.currentX < n.minX && (n.currentX = n.minX + 1 - Math.pow(n.minX - n.currentX + 1, .8)),
                            n.currentX > n.maxX && (n.currentX = n.maxX - 1 + Math.pow(n.currentX - n.maxX + 1, .8)),
                            n.currentY < n.minY && (n.currentY = n.minY + 1 - Math.pow(n.minY - n.currentY + 1, .8)),
                            n.currentY > n.maxY && (n.currentY = n.maxY - 1 + Math.pow(n.currentY - n.maxY + 1, .8)),
                            a.prevPositionX || (a.prevPositionX = n.touchesCurrent.x),
                            a.prevPositionY || (a.prevPositionY = n.touchesCurrent.y),
                            a.prevTime || (a.prevTime = Date.now()),
                            a.x = (n.touchesCurrent.x - a.prevPositionX) / (Date.now() - a.prevTime) / 2,
                            a.y = (n.touchesCurrent.y - a.prevPositionY) / (Date.now() - a.prevTime) / 2,
                            Math.abs(n.touchesCurrent.x - a.prevPositionX) < 2 && (a.x = 0),
                            Math.abs(n.touchesCurrent.y - a.prevPositionY) < 2 && (a.y = 0),
                            a.prevPositionX = n.touchesCurrent.x,
                            a.prevPositionY = n.touchesCurrent.y,
                            a.prevTime = Date.now(),
                            s.$imageWrapEl.transform("translate3d(" + n.currentX + "px, " + n.currentY + "px,0)")
                    }
                }
            },
            onTouchEnd: function() {
                var e = this.zoom,
                    t = e.gesture,
                    i = e.image,
                    s = e.velocity;
                if (t.$imageEl && 0 !== t.$imageEl.length) {
                    if (!i.isTouched || !i.isMoved)
                        return i.isTouched = !1,
                            void(i.isMoved = !1);
                    i.isTouched = !1,
                        i.isMoved = !1;
                    var n = 300,
                        a = 300,
                        o = i.currentX + s.x * n,
                        r = i.currentY + s.y * a;
                    0 !== s.x && (n = Math.abs((o - i.currentX) / s.x)),
                        0 !== s.y && (a = Math.abs((r - i.currentY) / s.y));
                    var l = Math.max(n, a);
                    i.currentX = o,
                        i.currentY = r;
                    var h = i.height * e.scale;
                    i.minX = Math.min(t.slideWidth / 2 - i.width * e.scale / 2, 0),
                        i.maxX = -i.minX,
                        i.minY = Math.min(t.slideHeight / 2 - h / 2, 0),
                        i.maxY = -i.minY,
                        i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX),
                        i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY),
                        t.$imageWrapEl.transition(l).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                }
            },
            onTransitionEnd: function() {
                var e = this.zoom,
                    t = e.gesture;
                t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"),
                    t.$imageWrapEl.transform("translate3d(0,0,0)"),
                    e.scale = 1,
                    e.currentScale = 1,
                    t.$slideEl = void 0,
                    t.$imageEl = void 0,
                    t.$imageWrapEl = void 0)
            },
            toggle: function(e) {
                var t = this.zoom;
                t.scale && 1 !== t.scale ? t.out() : t.in(e)
            },
            in: function(e) {
                var t, i, n, a, o, r, l, h, u, c, d, p, f, m = this,
                    g = m.zoom,
                    v = m.params.zoom,
                    b = g.gesture,
                    y = g.image;
                b.$slideEl || (b.$slideEl = m.clickedSlide ? s(m.clickedSlide) : m.slides.eq(m.activeIndex),
                        b.$imageEl = b.$slideEl.find("img, svg, canvas"),
                        b.$imageWrapEl = b.$imageEl.parent("." + v.containerClass)),
                    b.$imageEl && 0 !== b.$imageEl.length && (b.$slideEl.addClass("" + v.zoomedSlideClass),
                        void 0 === y.touchesStart.x && e ? (t = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX,
                            i = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (t = y.touchesStart.x,
                            i = y.touchesStart.y),
                        g.scale = b.$imageWrapEl.attr("data-swiper-zoom") || v.maxRatio,
                        g.currentScale = b.$imageWrapEl.attr("data-swiper-zoom") || v.maxRatio,
                        e ? (p = b.$slideEl[0].offsetWidth,
                            f = b.$slideEl[0].offsetHeight,
                            n = b.$slideEl.offset().left + p / 2 - t,
                            a = b.$slideEl.offset().top + f / 2 - i,
                            l = b.$imageEl[0].offsetHeight * g.scale,
                            c = -(h = Math.min(p / 2 - b.$imageEl[0].offsetWidth * g.scale / 2, 0)),
                            d = -(u = Math.min(f / 2 - l / 2, 0)),
                            (o = n * g.scale) < h && (o = h),
                            c < o && (o = c),
                            (r = a * g.scale) < u && (r = u),
                            d < r && (r = d)) : r = o = 0,
                        b.$imageWrapEl.transition(300).transform("translate3d(" + o + "px, " + r + "px,0)"),
                        b.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + g.scale + ")"))
            },
            out: function() {
                var e = this,
                    t = e.zoom,
                    i = e.params.zoom,
                    n = t.gesture;
                n.$slideEl || (n.$slideEl = e.clickedSlide ? s(e.clickedSlide) : e.slides.eq(e.activeIndex),
                        n.$imageEl = n.$slideEl.find("img, svg, canvas"),
                        n.$imageWrapEl = n.$imageEl.parent("." + i.containerClass)),
                    n.$imageEl && 0 !== n.$imageEl.length && (t.scale = 1,
                        t.currentScale = 1,
                        n.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"),
                        n.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"),
                        n.$slideEl.removeClass("" + i.zoomedSlideClass),
                        n.$slideEl = void 0)
            },
            enable: function() {
                var e = this,
                    t = e.zoom;
                if (!t.enabled) {
                    t.enabled = !0;
                    var i = !("touchstart" !== e.touchEvents.start || !c.passiveListener || !e.params.passiveListeners) && {
                        passive: !0,
                        capture: !1
                    };
                    c.gestures ? (e.$wrapperEl.on("gesturestart", ".swiper-slide", t.onGestureStart, i),
                            e.$wrapperEl.on("gesturechange", ".swiper-slide", t.onGestureChange, i),
                            e.$wrapperEl.on("gestureend", ".swiper-slide", t.onGestureEnd, i)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.on(e.touchEvents.start, ".swiper-slide", t.onGestureStart, i),
                            e.$wrapperEl.on(e.touchEvents.move, ".swiper-slide", t.onGestureChange, i),
                            e.$wrapperEl.on(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)),
                        e.$wrapperEl.on(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                }
            },
            disable: function() {
                var e = this,
                    t = e.zoom;
                if (t.enabled) {
                    e.zoom.enabled = !1;
                    var i = !("touchstart" !== e.touchEvents.start || !c.passiveListener || !e.params.passiveListeners) && {
                        passive: !0,
                        capture: !1
                    };
                    c.gestures ? (e.$wrapperEl.off("gesturestart", ".swiper-slide", t.onGestureStart, i),
                            e.$wrapperEl.off("gesturechange", ".swiper-slide", t.onGestureChange, i),
                            e.$wrapperEl.off("gestureend", ".swiper-slide", t.onGestureEnd, i)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.off(e.touchEvents.start, ".swiper-slide", t.onGestureStart, i),
                            e.$wrapperEl.off(e.touchEvents.move, ".swiper-slide", t.onGestureChange, i),
                            e.$wrapperEl.off(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)),
                        e.$wrapperEl.off(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                }
            }
        },
        j = {
            loadInSlide: function(e, t) {
                void 0 === t && (t = !0);
                var i = this,
                    n = i.params.lazy;
                if (void 0 !== e && 0 !== i.slides.length) {
                    var a = i.virtual && i.params.virtual.enabled ? i.$wrapperEl.children("." + i.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : i.slides.eq(e),
                        o = a.find("." + n.elementClass + ":not(." + n.loadedClass + "):not(." + n.loadingClass + ")");
                    !a.hasClass(n.elementClass) || a.hasClass(n.loadedClass) || a.hasClass(n.loadingClass) || (o = o.add(a[0])),
                        0 !== o.length && o.each(function(e, o) {
                            var r = s(o);
                            r.addClass(n.loadingClass);
                            var l = r.attr("data-background"),
                                h = r.attr("data-src"),
                                u = r.attr("data-srcset"),
                                c = r.attr("data-sizes");
                            i.loadImage(r[0], h || l, u, c, !1, function() {
                                    if (null != i && i && (!i || i.params) && !i.destroyed) {
                                        if (l ? (r.css("background-image", 'url("' + l + '")'),
                                                r.removeAttr("data-background")) : (u && (r.attr("srcset", u),
                                                    r.removeAttr("data-srcset")),
                                                c && (r.attr("sizes", c),
                                                    r.removeAttr("data-sizes")),
                                                h && (r.attr("src", h),
                                                    r.removeAttr("data-src"))),
                                            r.addClass(n.loadedClass).removeClass(n.loadingClass),
                                            a.find("." + n.preloaderClass).remove(),
                                            i.params.loop && t) {
                                            var e = a.attr("data-swiper-slide-index");
                                            if (a.hasClass(i.params.slideDuplicateClass)) {
                                                var s = i.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + i.params.slideDuplicateClass + ")");
                                                i.lazy.loadInSlide(s.index(), !1)
                                            } else {
                                                var o = i.$wrapperEl.children("." + i.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                                i.lazy.loadInSlide(o.index(), !1)
                                            }
                                        }
                                        i.emit("lazyImageReady", a[0], r[0])
                                    }
                                }),
                                i.emit("lazyImageLoad", a[0], r[0])
                        })
                }
            },
            load: function() {
                var e = this,
                    t = e.$wrapperEl,
                    i = e.params,
                    n = e.slides,
                    a = e.activeIndex,
                    o = e.virtual && i.virtual.enabled,
                    r = i.lazy,
                    l = i.slidesPerView;

                function h(e) {
                    if (o) {
                        if (t.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]').length)
                            return !0
                    } else if (n[e])
                        return !0;
                    return !1
                }

                function u(e) {
                    return o ? s(e).attr("data-swiper-slide-index") : s(e).index()
                }
                if ("auto" === l && (l = 0),
                    e.lazy.initialImageLoaded || (e.lazy.initialImageLoaded = !0),
                    e.params.watchSlidesVisibility)
                    t.children("." + i.slideVisibleClass).each(function(t, i) {
                        var n = o ? s(i).attr("data-swiper-slide-index") : s(i).index();
                        e.lazy.loadInSlide(n)
                    });
                else if (1 < l)
                    for (var c = a; c < a + l; c += 1)
                        h(c) && e.lazy.loadInSlide(c);
                else
                    e.lazy.loadInSlide(a);
                if (r.loadPrevNext)
                    if (1 < l || r.loadPrevNextAmount && 1 < r.loadPrevNextAmount) {
                        for (var d = r.loadPrevNextAmount, p = l, f = Math.min(a + p + Math.max(d, p), n.length), m = Math.max(a - Math.max(p, d), 0), g = a + l; g < f; g += 1)
                            h(g) && e.lazy.loadInSlide(g);
                        for (var v = m; v < a; v += 1)
                            h(v) && e.lazy.loadInSlide(v)
                    } else {
                        var b = t.children("." + i.slideNextClass);
                        0 < b.length && e.lazy.loadInSlide(u(b));
                        var y = t.children("." + i.slidePrevClass);
                        0 < y.length && e.lazy.loadInSlide(u(y))
                    }
            }
        },
        q = {
            LinearSpline: function(e, t) {
                var i, s, n, a, o;
                return this.x = e,
                    this.y = t,
                    this.lastIndex = e.length - 1,
                    this.interpolate = function(e) {
                        return e ? (o = function(e, t) {
                                for (s = -1,
                                    i = e.length; 1 < i - s;)
                                    e[n = i + s >> 1] <= t ? s = n : i = n;
                                return i
                            }(this.x, e),
                            (e - this.x[a = o - 1]) * (this.y[o] - this.y[a]) / (this.x[o] - this.x[a]) + this.y[a]) : 0
                    },
                    this
            },
            getInterpolateFunction: function(e) {
                var t = this;
                t.controller.spline || (t.controller.spline = t.params.loop ? new q.LinearSpline(t.slidesGrid, e.slidesGrid) : new q.LinearSpline(t.snapGrid, e.snapGrid))
            },
            setTranslate: function(e, t) {
                var i, s, n = this,
                    a = n.controller.control;

                function o(e) {
                    var t = n.rtlTranslate ? -n.translate : n.translate;
                    "slide" === n.params.controller.by && (n.controller.getInterpolateFunction(e),
                            s = -n.controller.spline.interpolate(-t)),
                        s && "container" !== n.params.controller.by || (i = (e.maxTranslate() - e.minTranslate()) / (n.maxTranslate() - n.minTranslate()),
                            s = (t - n.minTranslate()) * i + e.minTranslate()),
                        n.params.controller.inverse && (s = e.maxTranslate() - s),
                        e.updateProgress(s),
                        e.setTranslate(s, n),
                        e.updateActiveIndex(),
                        e.updateSlidesClasses()
                }
                if (Array.isArray(a))
                    for (var r = 0; r < a.length; r += 1)
                        a[r] !== t && a[r] instanceof S && o(a[r]);
                else
                    a instanceof S && t !== a && o(a)
            },
            setTransition: function(e, t) {
                var i, s = this,
                    n = s.controller.control;

                function a(t) {
                    t.setTransition(e, s),
                        0 !== e && (t.transitionStart(),
                            t.params.autoHeight && u.nextTick(function() {
                                t.updateAutoHeight()
                            }),
                            t.$wrapperEl.transitionEnd(function() {
                                n && (t.params.loop && "slide" === s.params.controller.by && t.loopFix(),
                                    t.transitionEnd())
                            }))
                }
                if (Array.isArray(n))
                    for (i = 0; i < n.length; i += 1)
                        n[i] !== t && n[i] instanceof S && a(n[i]);
                else
                    n instanceof S && t !== n && a(n)
            }
        },
        Y = {
            makeElFocusable: function(e) {
                return e.attr("tabIndex", "0"),
                    e
            },
            addElRole: function(e, t) {
                return e.attr("role", t),
                    e
            },
            addElLabel: function(e, t) {
                return e.attr("aria-label", t),
                    e
            },
            disableEl: function(e) {
                return e.attr("aria-disabled", !0),
                    e
            },
            enableEl: function(e) {
                return e.attr("aria-disabled", !1),
                    e
            },
            onEnterKey: function(e) {
                var t = this,
                    i = t.params.a11y;
                if (13 === e.keyCode) {
                    var n = s(e.target);
                    t.navigation && t.navigation.$nextEl && n.is(t.navigation.$nextEl) && (t.isEnd && !t.params.loop || t.slideNext(),
                            t.a11y.notify(t.isEnd ? i.lastSlideMessage : i.nextSlideMessage)),
                        t.navigation && t.navigation.$prevEl && n.is(t.navigation.$prevEl) && (t.isBeginning && !t.params.loop || t.slidePrev(),
                            t.a11y.notify(t.isBeginning ? i.firstSlideMessage : i.prevSlideMessage)),
                        t.pagination && n.is("." + t.params.pagination.bulletClass) && n[0].click()
                }
            },
            notify: function(e) {
                var t = this.a11y.liveRegion;
                0 !== t.length && (t.html(""),
                    t.html(e))
            },
            updateNavigation: function() {
                var e = this;
                if (!e.params.loop) {
                    var t = e.navigation,
                        i = t.$nextEl,
                        s = t.$prevEl;
                    s && 0 < s.length && (e.isBeginning ? e.a11y.disableEl(s) : e.a11y.enableEl(s)),
                        i && 0 < i.length && (e.isEnd ? e.a11y.disableEl(i) : e.a11y.enableEl(i))
                }
            },
            updatePagination: function() {
                var e = this,
                    t = e.params.a11y;
                e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.bullets.each(function(i, n) {
                    var a = s(n);
                    e.a11y.makeElFocusable(a),
                        e.a11y.addElRole(a, "button"),
                        e.a11y.addElLabel(a, t.paginationBulletMessage.replace(/{{index}}/, a.index() + 1))
                })
            },
            init: function() {
                var e = this;
                e.$el.append(e.a11y.liveRegion);
                var t, i, s = e.params.a11y;
                e.navigation && e.navigation.$nextEl && (t = e.navigation.$nextEl),
                    e.navigation && e.navigation.$prevEl && (i = e.navigation.$prevEl),
                    t && (e.a11y.makeElFocusable(t),
                        e.a11y.addElRole(t, "button"),
                        e.a11y.addElLabel(t, s.nextSlideMessage),
                        t.on("keydown", e.a11y.onEnterKey)),
                    i && (e.a11y.makeElFocusable(i),
                        e.a11y.addElRole(i, "button"),
                        e.a11y.addElLabel(i, s.prevSlideMessage),
                        i.on("keydown", e.a11y.onEnterKey)),
                    e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.$el.on("keydown", "." + e.params.pagination.bulletClass, e.a11y.onEnterKey)
            },
            destroy: function() {
                var e, t, i = this;
                i.a11y.liveRegion && 0 < i.a11y.liveRegion.length && i.a11y.liveRegion.remove(),
                    i.navigation && i.navigation.$nextEl && (e = i.navigation.$nextEl),
                    i.navigation && i.navigation.$prevEl && (t = i.navigation.$prevEl),
                    e && e.off("keydown", i.a11y.onEnterKey),
                    t && t.off("keydown", i.a11y.onEnterKey),
                    i.pagination && i.params.pagination.clickable && i.pagination.bullets && i.pagination.bullets.length && i.pagination.$el.off("keydown", "." + i.params.pagination.bulletClass, i.a11y.onEnterKey)
            }
        },
        V = {
            init: function() {
                var e = this;
                if (e.params.history) {
                    if (!t.history || !t.history.pushState)
                        return e.params.history.enabled = !1,
                            void(e.params.hashNavigation.enabled = !0);
                    var i = e.history;
                    i.initialized = !0,
                        i.paths = V.getPathValues(),
                        (i.paths.key || i.paths.value) && (i.scrollToSlide(0, i.paths.value, e.params.runCallbacksOnInit),
                            e.params.history.replaceState || t.addEventListener("popstate", e.history.setHistoryPopState))
                }
            },
            destroy: function() {
                this.params.history.replaceState || t.removeEventListener("popstate", this.history.setHistoryPopState)
            },
            setHistoryPopState: function() {
                this.history.paths = V.getPathValues(),
                    this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1)
            },
            getPathValues: function() {
                var e = t.location.pathname.slice(1).split("/").filter(function(e) {
                        return "" !== e
                    }),
                    i = e.length;
                return {
                    key: e[i - 2],
                    value: e[i - 1]
                }
            },
            setHistory: function(e, i) {
                if (this.history.initialized && this.params.history.enabled) {
                    var s = this.slides.eq(i),
                        n = V.slugify(s.attr("data-history"));
                    t.location.pathname.includes(e) || (n = e + "/" + n);
                    var a = t.history.state;
                    a && a.value === n || (this.params.history.replaceState ? t.history.replaceState({
                        value: n
                    }, null, n) : t.history.pushState({
                        value: n
                    }, null, n))
                }
            },
            slugify: function(e) {
                return e.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
            },
            scrollToSlide: function(e, t, i) {
                var s = this;
                if (t)
                    for (var n = 0, a = s.slides.length; n < a; n += 1) {
                        var o = s.slides.eq(n);
                        if (V.slugify(o.attr("data-history")) === t && !o.hasClass(s.params.slideDuplicateClass)) {
                            var r = o.index();
                            s.slideTo(r, e, i)
                        }
                    }
                else
                    s.slideTo(0, e, i)
            }
        },
        X = {
            onHashCange: function() {
                var t = this,
                    i = e.location.hash.replace("#", "");
                if (i !== t.slides.eq(t.activeIndex).attr("data-hash")) {
                    var s = t.$wrapperEl.children("." + t.params.slideClass + '[data-hash="' + i + '"]').index();
                    if (void 0 === s)
                        return;
                    t.slideTo(s)
                }
            },
            setHash: function() {
                var i = this;
                if (i.hashNavigation.initialized && i.params.hashNavigation.enabled)
                    if (i.params.hashNavigation.replaceState && t.history && t.history.replaceState)
                        t.history.replaceState(null, null, "#" + i.slides.eq(i.activeIndex).attr("data-hash") || "");
                    else {
                        var s = i.slides.eq(i.activeIndex),
                            n = s.attr("data-hash") || s.attr("data-history");
                        e.location.hash = n || ""
                    }
            },
            init: function() {
                var i = this;
                if (!(!i.params.hashNavigation.enabled || i.params.history && i.params.history.enabled)) {
                    i.hashNavigation.initialized = !0;
                    var n = e.location.hash.replace("#", "");
                    if (n)
                        for (var a = 0, o = i.slides.length; a < o; a += 1) {
                            var r = i.slides.eq(a);
                            if ((r.attr("data-hash") || r.attr("data-history")) === n && !r.hasClass(i.params.slideDuplicateClass)) {
                                var l = r.index();
                                i.slideTo(l, 0, i.params.runCallbacksOnInit, !0)
                            }
                        }
                    i.params.hashNavigation.watchState && s(t).on("hashchange", i.hashNavigation.onHashCange)
                }
            },
            destroy: function() {
                this.params.hashNavigation.watchState && s(t).off("hashchange", this.hashNavigation.onHashCange)
            }
        },
        G = {
            run: function() {
                var e = this,
                    t = e.slides.eq(e.activeIndex),
                    i = e.params.autoplay.delay;
                t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay),
                    e.autoplay.timeout = u.nextTick(function() {
                        e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(),
                            e.slidePrev(e.params.speed, !0, !0),
                            e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0),
                            e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0),
                            e.emit("autoplay")) : e.params.loop ? (e.loopFix(),
                            e.slideNext(e.params.speed, !0, !0),
                            e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0),
                            e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0),
                            e.emit("autoplay"))
                    }, i)
            },
            start: function() {
                var e = this;
                return void 0 === e.autoplay.timeout && !e.autoplay.running && (e.autoplay.running = !0,
                    e.emit("autoplayStart"),
                    e.autoplay.run(), !0)
            },
            stop: function() {
                var e = this;
                return !!e.autoplay.running && void 0 !== e.autoplay.timeout && (e.autoplay.timeout && (clearTimeout(e.autoplay.timeout),
                        e.autoplay.timeout = void 0),
                    e.autoplay.running = !1,
                    e.emit("autoplayStop"), !0)
            },
            pause: function(e) {
                var t = this;
                t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout),
                    t.autoplay.paused = !0,
                    0 !== e && t.params.autoplay.waitForTransition ? (t.$wrapperEl[0].addEventListener("transitionend", t.autoplay.onTransitionEnd),
                        t.$wrapperEl[0].addEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd)) : (t.autoplay.paused = !1,
                        t.autoplay.run())))
            }
        },
        U = {
            setTranslate: function() {
                for (var e = this, t = e.slides, i = 0; i < t.length; i += 1) {
                    var s = e.slides.eq(i),
                        n = -s[0].swiperSlideOffset;
                    e.params.virtualTranslate || (n -= e.translate);
                    var a = 0;
                    e.isHorizontal() || (a = n,
                        n = 0);
                    var o = e.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(s[0].progress), 0) : 1 + Math.min(Math.max(s[0].progress, -1), 0);
                    s.css({
                        opacity: o
                    }).transform("translate3d(" + n + "px, " + a + "px, 0px)")
                }
            },
            setTransition: function(e) {
                var t = this,
                    i = t.slides,
                    s = t.$wrapperEl;
                if (i.transition(e),
                    t.params.virtualTranslate && 0 !== e) {
                    var n = !1;
                    i.transitionEnd(function() {
                        if (!n && t && !t.destroyed) {
                            n = !0,
                                t.animating = !1;
                            for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1)
                                s.trigger(e[i])
                        }
                    })
                }
            }
        },
        K = {
            setTranslate: function() {
                var e, t = this,
                    i = t.$el,
                    n = t.$wrapperEl,
                    a = t.slides,
                    o = t.width,
                    r = t.height,
                    l = t.rtlTranslate,
                    h = t.size,
                    u = t.params.cubeEffect,
                    c = t.isHorizontal(),
                    p = t.virtual && t.params.virtual.enabled,
                    f = 0;
                u.shadow && (c ? (0 === (e = n.find(".swiper-cube-shadow")).length && (e = s('<div class="swiper-cube-shadow"></div>'),
                        n.append(e)),
                    e.css({
                        height: o + "px"
                    })) : 0 === (e = i.find(".swiper-cube-shadow")).length && (e = s('<div class="swiper-cube-shadow"></div>'),
                    i.append(e)));
                for (var m = 0; m < a.length; m += 1) {
                    var g = a.eq(m),
                        v = m;
                    p && (v = parseInt(g.attr("data-swiper-slide-index"), 10));
                    var b = 90 * v,
                        y = Math.floor(b / 360);
                    l && (b = -b,
                        y = Math.floor(-b / 360));
                    var _ = Math.max(Math.min(g[0].progress, 1), -1),
                        w = 0,
                        x = 0,
                        C = 0;
                    if (v % 4 == 0 ? (w = 4 * -y * h,
                            C = 0) : (v - 1) % 4 == 0 ? (w = 0,
                            C = 4 * -y * h) : (v - 2) % 4 == 0 ? (w = h + 4 * y * h,
                            C = h) : (v - 3) % 4 == 0 && (w = -h,
                            C = 3 * h + 4 * h * y),
                        l && (w = -w),
                        c || (x = w,
                            w = 0),
                        _ <= 1 && -1 < _ && (f = 90 * v + 90 * _,
                            l && (f = 90 * -v - 90 * _)),
                        g.transform("rotateX(" + (c ? 0 : -b) + "deg) rotateY(" + (c ? b : 0) + "deg) translate3d(" + w + "px, " + x + "px, " + C + "px)"),
                        u.slideShadows) {
                        var T = g.find(c ? ".swiper-slide-shadow-left" : ".swiper-slide-shadow-top"),
                            k = g.find(c ? ".swiper-slide-shadow-right" : ".swiper-slide-shadow-bottom");
                        0 === T.length && (T = s('<div class="swiper-slide-shadow-' + (c ? "left" : "top") + '"></div>'),
                                g.append(T)),
                            0 === k.length && (k = s('<div class="swiper-slide-shadow-' + (c ? "right" : "bottom") + '"></div>'),
                                g.append(k)),
                            T.length && (T[0].style.opacity = Math.max(-_, 0)),
                            k.length && (k[0].style.opacity = Math.max(_, 0))
                    }
                }
                if (n.css({
                        "-webkit-transform-origin": "50% 50% -" + h / 2 + "px",
                        "-moz-transform-origin": "50% 50% -" + h / 2 + "px",
                        "-ms-transform-origin": "50% 50% -" + h / 2 + "px",
                        "transform-origin": "50% 50% -" + h / 2 + "px"
                    }),
                    u.shadow)
                    if (c)
                        e.transform("translate3d(0px, " + (o / 2 + u.shadowOffset) + "px, " + -o / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + u.shadowScale + ")");
                    else {
                        var S = Math.abs(f) - 90 * Math.floor(Math.abs(f) / 90),
                            E = 1.5 - (Math.sin(2 * S * Math.PI / 360) / 2 + Math.cos(2 * S * Math.PI / 360) / 2),
                            D = u.shadowScale / E;
                        e.transform("scale3d(" + u.shadowScale + ", 1, " + D + ") translate3d(0px, " + (r / 2 + u.shadowOffset) + "px, " + -r / 2 / D + "px) rotateX(-90deg)")
                    }
                n.transform("translate3d(0px,0," + (d.isSafari || d.isUiWebView ? -h / 2 : 0) + "px) rotateX(" + (t.isHorizontal() ? 0 : f) + "deg) rotateY(" + (t.isHorizontal() ? -f : 0) + "deg)")
            },
            setTransition: function(e) {
                var t = this.$el;
                this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),
                    this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e)
            }
        },
        Q = {
            setTranslate: function() {
                for (var e = this, t = e.slides, i = e.rtlTranslate, n = 0; n < t.length; n += 1) {
                    var a = t.eq(n),
                        o = a[0].progress;
                    e.params.flipEffect.limitRotation && (o = Math.max(Math.min(a[0].progress, 1), -1));
                    var r = -180 * o,
                        l = 0,
                        h = -a[0].swiperSlideOffset,
                        u = 0;
                    if (e.isHorizontal() ? i && (r = -r) : (u = h,
                            l = -r,
                            r = h = 0),
                        a[0].style.zIndex = -Math.abs(Math.round(o)) + t.length,
                        e.params.flipEffect.slideShadows) {
                        var c = e.isHorizontal() ? a.find(".swiper-slide-shadow-left") : a.find(".swiper-slide-shadow-top"),
                            d = e.isHorizontal() ? a.find(".swiper-slide-shadow-right") : a.find(".swiper-slide-shadow-bottom");
                        0 === c.length && (c = s('<div class="swiper-slide-shadow-' + (e.isHorizontal() ? "left" : "top") + '"></div>'),
                                a.append(c)),
                            0 === d.length && (d = s('<div class="swiper-slide-shadow-' + (e.isHorizontal() ? "right" : "bottom") + '"></div>'),
                                a.append(d)),
                            c.length && (c[0].style.opacity = Math.max(-o, 0)),
                            d.length && (d[0].style.opacity = Math.max(o, 0))
                    }
                    a.transform("translate3d(" + h + "px, " + u + "px, 0px) rotateX(" + l + "deg) rotateY(" + r + "deg)")
                }
            },
            setTransition: function(e) {
                var t = this,
                    i = t.slides,
                    s = t.activeIndex,
                    n = t.$wrapperEl;
                if (i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),
                    t.params.virtualTranslate && 0 !== e) {
                    var a = !1;
                    i.eq(s).transitionEnd(function() {
                        if (!a && t && !t.destroyed) {
                            a = !0,
                                t.animating = !1;
                            for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1)
                                n.trigger(e[i])
                        }
                    })
                }
            }
        },
        J = {
            setTranslate: function() {
                for (var e = this, t = e.width, i = e.height, n = e.slides, a = e.$wrapperEl, o = e.slidesSizesGrid, r = e.params.coverflowEffect, l = e.isHorizontal(), h = e.translate, u = l ? t / 2 - h : i / 2 - h, d = l ? r.rotate : -r.rotate, p = r.depth, f = 0, m = n.length; f < m; f += 1) {
                    var g = n.eq(f),
                        v = o[f],
                        b = (u - g[0].swiperSlideOffset - v / 2) / v * r.modifier,
                        y = l ? d * b : 0,
                        _ = l ? 0 : d * b,
                        w = -p * Math.abs(b),
                        x = l ? 0 : r.stretch * b,
                        C = l ? r.stretch * b : 0;
                    if (Math.abs(C) < .001 && (C = 0),
                        Math.abs(x) < .001 && (x = 0),
                        Math.abs(w) < .001 && (w = 0),
                        Math.abs(y) < .001 && (y = 0),
                        Math.abs(_) < .001 && (_ = 0),
                        g.transform("translate3d(" + C + "px," + x + "px," + w + "px)  rotateX(" + _ + "deg) rotateY(" + y + "deg)"),
                        g[0].style.zIndex = 1 - Math.abs(Math.round(b)),
                        r.slideShadows) {
                        var T = g.find(l ? ".swiper-slide-shadow-left" : ".swiper-slide-shadow-top"),
                            k = g.find(l ? ".swiper-slide-shadow-right" : ".swiper-slide-shadow-bottom");
                        0 === T.length && (T = s('<div class="swiper-slide-shadow-' + (l ? "left" : "top") + '"></div>'),
                                g.append(T)),
                            0 === k.length && (k = s('<div class="swiper-slide-shadow-' + (l ? "right" : "bottom") + '"></div>'),
                                g.append(k)),
                            T.length && (T[0].style.opacity = 0 < b ? b : 0),
                            k.length && (k[0].style.opacity = 0 < -b ? -b : 0)
                    }
                }
                (c.pointerEvents || c.prefixedPointerEvents) && (a[0].style.perspectiveOrigin = u + "px 50%")
            },
            setTransition: function(e) {
                this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
            }
        },
        Z = {
            init: function() {
                var e = this,
                    t = e.params.thumbs,
                    i = e.constructor;
                t.swiper instanceof i ? (e.thumbs.swiper = t.swiper,
                        u.extend(e.thumbs.swiper.originalParams, {
                            watchSlidesProgress: !0,
                            slideToClickedSlide: !1
                        }),
                        u.extend(e.thumbs.swiper.params, {
                            watchSlidesProgress: !0,
                            slideToClickedSlide: !1
                        })) : u.isObject(t.swiper) && (e.thumbs.swiper = new i(u.extend({}, t.swiper, {
                            watchSlidesVisibility: !0,
                            watchSlidesProgress: !0,
                            slideToClickedSlide: !1
                        })),
                        e.thumbs.swiperCreated = !0),
                    e.thumbs.swiper.$el.addClass(e.params.thumbs.thumbsContainerClass),
                    e.thumbs.swiper.on("tap", e.thumbs.onThumbClick)
            },
            onThumbClick: function() {
                var e = this,
                    t = e.thumbs.swiper;
                if (t) {
                    var i = t.clickedIndex,
                        n = t.clickedSlide;
                    if (!(n && s(n).hasClass(e.params.thumbs.slideThumbActiveClass) || null == i)) {
                        var a;
                        if (a = t.params.loop ? parseInt(s(t.clickedSlide).attr("data-swiper-slide-index"), 10) : i,
                            e.params.loop) {
                            var o = e.activeIndex;
                            e.slides.eq(o).hasClass(e.params.slideDuplicateClass) && (e.loopFix(),
                                e._clientLeft = e.$wrapperEl[0].clientLeft,
                                o = e.activeIndex);
                            var r = e.slides.eq(o).prevAll('[data-swiper-slide-index="' + a + '"]').eq(0).index(),
                                l = e.slides.eq(o).nextAll('[data-swiper-slide-index="' + a + '"]').eq(0).index();
                            a = void 0 === r ? l : void 0 === l ? r : l - o < o - r ? l : r
                        }
                        e.slideTo(a)
                    }
                }
            },
            update: function(e) {
                var t = this,
                    i = t.thumbs.swiper;
                if (i) {
                    var s = "auto" === i.params.slidesPerView ? i.slidesPerViewDynamic() : i.params.slidesPerView;
                    if (t.realIndex !== i.realIndex) {
                        var n, a = i.activeIndex;
                        if (i.params.loop) {
                            i.slides.eq(a).hasClass(i.params.slideDuplicateClass) && (i.loopFix(),
                                i._clientLeft = i.$wrapperEl[0].clientLeft,
                                a = i.activeIndex);
                            var o = i.slides.eq(a).prevAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index(),
                                r = i.slides.eq(a).nextAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index();
                            n = void 0 === o ? r : void 0 === r ? o : r - a == a - o ? a : r - a < a - o ? r : o
                        } else
                            n = t.realIndex;
                        i.visibleSlidesIndexes.indexOf(n) < 0 && (i.params.centeredSlides ? n = a < n ? n - Math.floor(s / 2) + 1 : n + Math.floor(s / 2) - 1 : a < n && (n = n - s + 1),
                            i.slideTo(n, e ? 0 : void 0))
                    }
                    var l = 1,
                        h = t.params.thumbs.slideThumbActiveClass;
                    if (1 < t.params.slidesPerView && !t.params.centeredSlides && (l = t.params.slidesPerView),
                        i.slides.removeClass(h),
                        i.params.loop)
                        for (var u = 0; u < l; u += 1)
                            i.$wrapperEl.children('[data-swiper-slide-index="' + (t.realIndex + u) + '"]').addClass(h);
                    else
                        for (var c = 0; c < l; c += 1)
                            i.slides.eq(t.realIndex + c).addClass(h)
                }
            }
        },
        ee = [E, D, M, P, z, O, N, {
            name: "mousewheel",
            params: {
                mousewheel: {
                    enabled: !1,
                    releaseOnEdges: !1,
                    invert: !1,
                    forceToAxis: !1,
                    sensitivity: 1,
                    eventsTarged: "container"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    mousewheel: {
                        enabled: !1,
                        enable: L.enable.bind(e),
                        disable: L.disable.bind(e),
                        handle: L.handle.bind(e),
                        handleMouseEnter: L.handleMouseEnter.bind(e),
                        handleMouseLeave: L.handleMouseLeave.bind(e),
                        lastScrollTime: u.now()
                    }
                })
            },
            on: {
                init: function() {
                    this.params.mousewheel.enabled && this.mousewheel.enable()
                },
                destroy: function() {
                    this.mousewheel.enabled && this.mousewheel.disable()
                }
            }
        }, {
            name: "navigation",
            params: {
                navigation: {
                    nextEl: null,
                    prevEl: null,
                    hideOnClick: !1,
                    disabledClass: "swiper-button-disabled",
                    hiddenClass: "swiper-button-hidden",
                    lockClass: "swiper-button-lock"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    navigation: {
                        init: W.init.bind(e),
                        update: W.update.bind(e),
                        destroy: W.destroy.bind(e),
                        onNextClick: W.onNextClick.bind(e),
                        onPrevClick: W.onPrevClick.bind(e)
                    }
                })
            },
            on: {
                init: function() {
                    this.navigation.init(),
                        this.navigation.update()
                },
                toEdge: function() {
                    this.navigation.update()
                },
                fromEdge: function() {
                    this.navigation.update()
                },
                destroy: function() {
                    this.navigation.destroy()
                },
                click: function(e) {
                    var t, i = this,
                        n = i.navigation,
                        a = n.$nextEl,
                        o = n.$prevEl;
                    !i.params.navigation.hideOnClick || s(e.target).is(o) || s(e.target).is(a) || (a ? t = a.hasClass(i.params.navigation.hiddenClass) : o && (t = o.hasClass(i.params.navigation.hiddenClass)),
                        i.emit(!0 === t ? "navigationShow" : "navigationHide", i),
                        a && a.toggleClass(i.params.navigation.hiddenClass),
                        o && o.toggleClass(i.params.navigation.hiddenClass))
                }
            }
        }, {
            name: "pagination",
            params: {
                pagination: {
                    el: null,
                    bulletElement: "span",
                    clickable: !1,
                    hideOnClick: !1,
                    renderBullet: null,
                    renderProgressbar: null,
                    renderFraction: null,
                    renderCustom: null,
                    progressbarOpposite: !1,
                    type: "bullets",
                    dynamicBullets: !1,
                    dynamicMainBullets: 1,
                    formatFractionCurrent: function(e) {
                        return e
                    },
                    formatFractionTotal: function(e) {
                        return e
                    },
                    bulletClass: "swiper-pagination-bullet",
                    bulletActiveClass: "swiper-pagination-bullet-active",
                    modifierClass: "swiper-pagination-",
                    currentClass: "swiper-pagination-current",
                    totalClass: "swiper-pagination-total",
                    hiddenClass: "swiper-pagination-hidden",
                    progressbarFillClass: "swiper-pagination-progressbar-fill",
                    progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
                    clickableClass: "swiper-pagination-clickable",
                    lockClass: "swiper-pagination-lock"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    pagination: {
                        init: $.init.bind(e),
                        render: $.render.bind(e),
                        update: $.update.bind(e),
                        destroy: $.destroy.bind(e),
                        dynamicBulletIndex: 0
                    }
                })
            },
            on: {
                init: function() {
                    this.pagination.init(),
                        this.pagination.render(),
                        this.pagination.update()
                },
                activeIndexChange: function() {
                    this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update()
                },
                snapIndexChange: function() {
                    this.params.loop || this.pagination.update()
                },
                slidesLengthChange: function() {
                    this.params.loop && (this.pagination.render(),
                        this.pagination.update())
                },
                snapGridLengthChange: function() {
                    this.params.loop || (this.pagination.render(),
                        this.pagination.update())
                },
                destroy: function() {
                    this.pagination.destroy()
                },
                click: function(e) {
                    var t = this;
                    t.params.pagination.el && t.params.pagination.hideOnClick && 0 < t.pagination.$el.length && !s(e.target).hasClass(t.params.pagination.bulletClass) && (!0 === t.pagination.$el.hasClass(t.params.pagination.hiddenClass) ? t.emit("paginationShow", t) : t.emit("paginationHide", t),
                        t.pagination.$el.toggleClass(t.params.pagination.hiddenClass))
                }
            }
        }, {
            name: "scrollbar",
            params: {
                scrollbar: {
                    el: null,
                    dragSize: "auto",
                    hide: !1,
                    draggable: !1,
                    snapOnRelease: !0,
                    lockClass: "swiper-scrollbar-lock",
                    dragClass: "swiper-scrollbar-drag"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    scrollbar: {
                        init: F.init.bind(e),
                        destroy: F.destroy.bind(e),
                        updateSize: F.updateSize.bind(e),
                        setTranslate: F.setTranslate.bind(e),
                        setTransition: F.setTransition.bind(e),
                        enableDraggable: F.enableDraggable.bind(e),
                        disableDraggable: F.disableDraggable.bind(e),
                        setDragPosition: F.setDragPosition.bind(e),
                        onDragStart: F.onDragStart.bind(e),
                        onDragMove: F.onDragMove.bind(e),
                        onDragEnd: F.onDragEnd.bind(e),
                        isTouched: !1,
                        timeout: null,
                        dragTimeout: null
                    }
                })
            },
            on: {
                init: function() {
                    this.scrollbar.init(),
                        this.scrollbar.updateSize(),
                        this.scrollbar.setTranslate()
                },
                update: function() {
                    this.scrollbar.updateSize()
                },
                resize: function() {
                    this.scrollbar.updateSize()
                },
                observerUpdate: function() {
                    this.scrollbar.updateSize()
                },
                setTranslate: function() {
                    this.scrollbar.setTranslate()
                },
                setTransition: function(e) {
                    this.scrollbar.setTransition(e)
                },
                destroy: function() {
                    this.scrollbar.destroy()
                }
            }
        }, {
            name: "parallax",
            params: {
                parallax: {
                    enabled: !1
                }
            },
            create: function() {
                u.extend(this, {
                    parallax: {
                        setTransform: R.setTransform.bind(this),
                        setTranslate: R.setTranslate.bind(this),
                        setTransition: R.setTransition.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    this.params.parallax.enabled && (this.params.watchSlidesProgress = !0,
                        this.originalParams.watchSlidesProgress = !0)
                },
                init: function() {
                    this.params.parallax.enabled && this.parallax.setTranslate()
                },
                setTranslate: function() {
                    this.params.parallax.enabled && this.parallax.setTranslate()
                },
                setTransition: function(e) {
                    this.params.parallax.enabled && this.parallax.setTransition(e)
                }
            }
        }, {
            name: "zoom",
            params: {
                zoom: {
                    enabled: !1,
                    maxRatio: 3,
                    minRatio: 1,
                    toggle: !0,
                    containerClass: "swiper-zoom-container",
                    zoomedSlideClass: "swiper-slide-zoomed"
                }
            },
            create: function() {
                var e = this,
                    t = {
                        enabled: !1,
                        scale: 1,
                        currentScale: 1,
                        isScaling: !1,
                        gesture: {
                            $slideEl: void 0,
                            slideWidth: void 0,
                            slideHeight: void 0,
                            $imageEl: void 0,
                            $imageWrapEl: void 0,
                            maxRatio: 3
                        },
                        image: {
                            isTouched: void 0,
                            isMoved: void 0,
                            currentX: void 0,
                            currentY: void 0,
                            minX: void 0,
                            minY: void 0,
                            maxX: void 0,
                            maxY: void 0,
                            width: void 0,
                            height: void 0,
                            startX: void 0,
                            startY: void 0,
                            touchesStart: {},
                            touchesCurrent: {}
                        },
                        velocity: {
                            x: void 0,
                            y: void 0,
                            prevPositionX: void 0,
                            prevPositionY: void 0,
                            prevTime: void 0
                        }
                    };
                "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function(i) {
                        t[i] = B[i].bind(e)
                    }),
                    u.extend(e, {
                        zoom: t
                    });
                var i = 1;
                Object.defineProperty(e.zoom, "scale", {
                    get: function() {
                        return i
                    },
                    set: function(t) {
                        i !== t && e.emit("zoomChange", t, e.zoom.gesture.$imageEl ? e.zoom.gesture.$imageEl[0] : void 0, e.zoom.gesture.$slideEl ? e.zoom.gesture.$slideEl[0] : void 0),
                            i = t
                    }
                })
            },
            on: {
                init: function() {
                    this.params.zoom.enabled && this.zoom.enable()
                },
                destroy: function() {
                    this.zoom.disable()
                },
                touchStart: function(e) {
                    this.zoom.enabled && this.zoom.onTouchStart(e)
                },
                touchEnd: function(e) {
                    this.zoom.enabled && this.zoom.onTouchEnd(e)
                },
                doubleTap: function(e) {
                    this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e)
                },
                transitionEnd: function() {
                    this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd()
                }
            }
        }, {
            name: "lazy",
            params: {
                lazy: {
                    enabled: !1,
                    loadPrevNext: !1,
                    loadPrevNextAmount: 1,
                    loadOnTransitionStart: !1,
                    elementClass: "swiper-lazy",
                    loadingClass: "swiper-lazy-loading",
                    loadedClass: "swiper-lazy-loaded",
                    preloaderClass: "swiper-lazy-preloader"
                }
            },
            create: function() {
                u.extend(this, {
                    lazy: {
                        initialImageLoaded: !1,
                        load: j.load.bind(this),
                        loadInSlide: j.loadInSlide.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1)
                },
                init: function() {
                    this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load()
                },
                scroll: function() {
                    this.params.freeMode && !this.params.freeModeSticky && this.lazy.load()
                },
                resize: function() {
                    this.params.lazy.enabled && this.lazy.load()
                },
                scrollbarDragMove: function() {
                    this.params.lazy.enabled && this.lazy.load()
                },
                transitionStart: function() {
                    var e = this;
                    e.params.lazy.enabled && (e.params.lazy.loadOnTransitionStart || !e.params.lazy.loadOnTransitionStart && !e.lazy.initialImageLoaded) && e.lazy.load()
                },
                transitionEnd: function() {
                    this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load()
                }
            }
        }, {
            name: "controller",
            params: {
                controller: {
                    control: void 0,
                    inverse: !1,
                    by: "slide"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    controller: {
                        control: e.params.controller.control,
                        getInterpolateFunction: q.getInterpolateFunction.bind(e),
                        setTranslate: q.setTranslate.bind(e),
                        setTransition: q.setTransition.bind(e)
                    }
                })
            },
            on: {
                update: function() {
                    this.controller.control && this.controller.spline && (this.controller.spline = void 0,
                        delete this.controller.spline)
                },
                resize: function() {
                    this.controller.control && this.controller.spline && (this.controller.spline = void 0,
                        delete this.controller.spline)
                },
                observerUpdate: function() {
                    this.controller.control && this.controller.spline && (this.controller.spline = void 0,
                        delete this.controller.spline)
                },
                setTranslate: function(e, t) {
                    this.controller.control && this.controller.setTranslate(e, t)
                },
                setTransition: function(e, t) {
                    this.controller.control && this.controller.setTransition(e, t)
                }
            }
        }, {
            name: "a11y",
            params: {
                a11y: {
                    enabled: !0,
                    notificationClass: "swiper-notification",
                    prevSlideMessage: "Previous slide",
                    nextSlideMessage: "Next slide",
                    firstSlideMessage: "This is the first slide",
                    lastSlideMessage: "This is the last slide",
                    paginationBulletMessage: "Go to slide {{index}}"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                        a11y: {
                            liveRegion: s('<span class="' + e.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
                        }
                    }),
                    Object.keys(Y).forEach(function(t) {
                        e.a11y[t] = Y[t].bind(e)
                    })
            },
            on: {
                init: function() {
                    this.params.a11y.enabled && (this.a11y.init(),
                        this.a11y.updateNavigation())
                },
                toEdge: function() {
                    this.params.a11y.enabled && this.a11y.updateNavigation()
                },
                fromEdge: function() {
                    this.params.a11y.enabled && this.a11y.updateNavigation()
                },
                paginationUpdate: function() {
                    this.params.a11y.enabled && this.a11y.updatePagination()
                },
                destroy: function() {
                    this.params.a11y.enabled && this.a11y.destroy()
                }
            }
        }, {
            name: "history",
            params: {
                history: {
                    enabled: !1,
                    replaceState: !1,
                    key: "slides"
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    history: {
                        init: V.init.bind(e),
                        setHistory: V.setHistory.bind(e),
                        setHistoryPopState: V.setHistoryPopState.bind(e),
                        scrollToSlide: V.scrollToSlide.bind(e),
                        destroy: V.destroy.bind(e)
                    }
                })
            },
            on: {
                init: function() {
                    this.params.history.enabled && this.history.init()
                },
                destroy: function() {
                    this.params.history.enabled && this.history.destroy()
                },
                transitionEnd: function() {
                    this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex)
                }
            }
        }, {
            name: "hash-navigation",
            params: {
                hashNavigation: {
                    enabled: !1,
                    replaceState: !1,
                    watchState: !1
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    hashNavigation: {
                        initialized: !1,
                        init: X.init.bind(e),
                        destroy: X.destroy.bind(e),
                        setHash: X.setHash.bind(e),
                        onHashCange: X.onHashCange.bind(e)
                    }
                })
            },
            on: {
                init: function() {
                    this.params.hashNavigation.enabled && this.hashNavigation.init()
                },
                destroy: function() {
                    this.params.hashNavigation.enabled && this.hashNavigation.destroy()
                },
                transitionEnd: function() {
                    this.hashNavigation.initialized && this.hashNavigation.setHash()
                }
            }
        }, {
            name: "autoplay",
            params: {
                autoplay: {
                    enabled: !1,
                    delay: 3e3,
                    waitForTransition: !0,
                    disableOnInteraction: !0,
                    stopOnLastSlide: !1,
                    reverseDirection: !1
                }
            },
            create: function() {
                var e = this;
                u.extend(e, {
                    autoplay: {
                        running: !1,
                        paused: !1,
                        run: G.run.bind(e),
                        start: G.start.bind(e),
                        stop: G.stop.bind(e),
                        pause: G.pause.bind(e),
                        onTransitionEnd: function(t) {
                            e && !e.destroyed && e.$wrapperEl && t.target === this && (e.$wrapperEl[0].removeEventListener("transitionend", e.autoplay.onTransitionEnd),
                                e.$wrapperEl[0].removeEventListener("webkitTransitionEnd", e.autoplay.onTransitionEnd),
                                e.autoplay.paused = !1,
                                e.autoplay.running ? e.autoplay.run() : e.autoplay.stop())
                        }
                    }
                })
            },
            on: {
                init: function() {
                    this.params.autoplay.enabled && this.autoplay.start()
                },
                beforeTransitionStart: function(e, t) {
                    this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop())
                },
                sliderFirstMove: function() {
                    this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause())
                },
                destroy: function() {
                    this.autoplay.running && this.autoplay.stop()
                }
            }
        }, {
            name: "effect-fade",
            params: {
                fadeEffect: {
                    crossFade: !1
                }
            },
            create: function() {
                u.extend(this, {
                    fadeEffect: {
                        setTranslate: U.setTranslate.bind(this),
                        setTransition: U.setTransition.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    var e = this;
                    if ("fade" === e.params.effect) {
                        e.classNames.push(e.params.containerModifierClass + "fade");
                        var t = {
                            slidesPerView: 1,
                            slidesPerColumn: 1,
                            slidesPerGroup: 1,
                            watchSlidesProgress: !0,
                            spaceBetween: 0,
                            virtualTranslate: !0
                        };
                        u.extend(e.params, t),
                            u.extend(e.originalParams, t)
                    }
                },
                setTranslate: function() {
                    "fade" === this.params.effect && this.fadeEffect.setTranslate()
                },
                setTransition: function(e) {
                    "fade" === this.params.effect && this.fadeEffect.setTransition(e)
                }
            }
        }, {
            name: "effect-cube",
            params: {
                cubeEffect: {
                    slideShadows: !0,
                    shadow: !0,
                    shadowOffset: 20,
                    shadowScale: .94
                }
            },
            create: function() {
                u.extend(this, {
                    cubeEffect: {
                        setTranslate: K.setTranslate.bind(this),
                        setTransition: K.setTransition.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    var e = this;
                    if ("cube" === e.params.effect) {
                        e.classNames.push(e.params.containerModifierClass + "cube"),
                            e.classNames.push(e.params.containerModifierClass + "3d");
                        var t = {
                            slidesPerView: 1,
                            slidesPerColumn: 1,
                            slidesPerGroup: 1,
                            watchSlidesProgress: !0,
                            resistanceRatio: 0,
                            spaceBetween: 0,
                            centeredSlides: !1,
                            virtualTranslate: !0
                        };
                        u.extend(e.params, t),
                            u.extend(e.originalParams, t)
                    }
                },
                setTranslate: function() {
                    "cube" === this.params.effect && this.cubeEffect.setTranslate()
                },
                setTransition: function(e) {
                    "cube" === this.params.effect && this.cubeEffect.setTransition(e)
                }
            }
        }, {
            name: "effect-flip",
            params: {
                flipEffect: {
                    slideShadows: !0,
                    limitRotation: !0
                }
            },
            create: function() {
                u.extend(this, {
                    flipEffect: {
                        setTranslate: Q.setTranslate.bind(this),
                        setTransition: Q.setTransition.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    var e = this;
                    if ("flip" === e.params.effect) {
                        e.classNames.push(e.params.containerModifierClass + "flip"),
                            e.classNames.push(e.params.containerModifierClass + "3d");
                        var t = {
                            slidesPerView: 1,
                            slidesPerColumn: 1,
                            slidesPerGroup: 1,
                            watchSlidesProgress: !0,
                            spaceBetween: 0,
                            virtualTranslate: !0
                        };
                        u.extend(e.params, t),
                            u.extend(e.originalParams, t)
                    }
                },
                setTranslate: function() {
                    "flip" === this.params.effect && this.flipEffect.setTranslate()
                },
                setTransition: function(e) {
                    "flip" === this.params.effect && this.flipEffect.setTransition(e)
                }
            }
        }, {
            name: "effect-coverflow",
            params: {
                coverflowEffect: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: !0
                }
            },
            create: function() {
                u.extend(this, {
                    coverflowEffect: {
                        setTranslate: J.setTranslate.bind(this),
                        setTransition: J.setTransition.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    var e = this;
                    "coverflow" === e.params.effect && (e.classNames.push(e.params.containerModifierClass + "coverflow"),
                        e.classNames.push(e.params.containerModifierClass + "3d"),
                        e.params.watchSlidesProgress = !0,
                        e.originalParams.watchSlidesProgress = !0)
                },
                setTranslate: function() {
                    "coverflow" === this.params.effect && this.coverflowEffect.setTranslate()
                },
                setTransition: function(e) {
                    "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e)
                }
            }
        }, {
            name: "thumbs",
            params: {
                thumbs: {
                    swiper: null,
                    slideThumbActiveClass: "swiper-slide-thumb-active",
                    thumbsContainerClass: "swiper-container-thumbs"
                }
            },
            create: function() {
                u.extend(this, {
                    thumbs: {
                        swiper: null,
                        init: Z.init.bind(this),
                        update: Z.update.bind(this),
                        onThumbClick: Z.onThumbClick.bind(this)
                    }
                })
            },
            on: {
                beforeInit: function() {
                    var e = this.params.thumbs;
                    e && e.swiper && (this.thumbs.init(),
                        this.thumbs.update(!0))
                },
                slideChange: function() {
                    this.thumbs.swiper && this.thumbs.update()
                },
                update: function() {
                    this.thumbs.swiper && this.thumbs.update()
                },
                resize: function() {
                    this.thumbs.swiper && this.thumbs.update()
                },
                observerUpdate: function() {
                    this.thumbs.swiper && this.thumbs.update()
                },
                setTransition: function(e) {
                    var t = this.thumbs.swiper;
                    t && t.setTransition(e)
                },
                beforeDestroy: function() {
                    var e = this.thumbs.swiper;
                    e && this.thumbs.swiperCreated && e && e.destroy()
                }
            }
        }];
    return void 0 === S.use && (S.use = S.Class.use,
            S.installModule = S.Class.installModule),
        S.use(ee),
        S
});