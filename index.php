<!DOCTYPE html>
<html lang="en">

<head>
    <title>Local Media Summit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  -->
    <meta name="description" content="Local Media Summit">
    <meta name="keywords" content="Suara, Local Media Summit, LMS">

    <meta property="og:title" content="locammediasummit.com" />
    <meta property="og:description"
        content="Local Media Summit - Emerging Local Media Business Viability to Represent the Unrepresented" />
    <meta property="og:url" content="https://microsite.suara.com/localmediasummit" />
    <meta property="og:image" content="./2023/assets/images/jms_thumb.jpg" />

    <link rel="Shortcut icon" href="./2022/assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="assets/js/main.js<?= time() ?>"></script>
    <script src="assets/js/pollyfills.js<?= time() ?>"></script>
    <script src="assets/js/runtime.js<?= time() ?>"></script>
    <script src="assets/js/script.js<?= time() ?>"></script>



</head>

<body>

    <header class="header_wrap" id="header">
        <div class="header"><a class="logo" routerlink="/" href="/"><img alt="LS ELECTRIC"
                    src="assets/images/logo-symphos-hitam.png"></a>
            <div class="gnb">
                <ul class="d1 clear">
                    <li><span routerlink="/about-us" tabindex="0">ABOUT US</span>
                        <div class="gnbSub">
                            <ul class="clear sub_d2 company">
                                <li class="d2_li"><a routerlink="/about-us" href="/about-us">ABOUT US</a>
                                    <ul class="sub_d3">
                                        <li><a routerlink="/about-us/ceo" href="/about-us/ceo">CEO's Message</a></li>
                                        <li><a routerlink="/about-us/company" href="/about-us/company">Company</a></li>
                                        <li><a routerlink="/about-us/vision-philosophy"
                                                href="/about-us/vision-philosophy">Vision &amp; Philosophy</a></li>
                                        <li><a routerlink="/about-us/history" href="/about-us/history">History</a></li>
                                        <li><a routerlink="/about-us/rnd" href="/about-us/rnd">R&amp;D</a></li>
                                        <li><a routerlink="/about-us/ptt" href="/about-us/ptt">PT&amp;T</a></li>
                                        <li><a routerlink="/about-us/sustainability/environmental-management"
                                                href="/about-us/sustainability/environmental-management">Sustainability</a>
                                        </li>
                                        <li><a routerlink="/about-us/investor-relations/financials"
                                                href="/about-us/investor-relations/financials">Investor Relations</a>
                                        </li>
                                        <li><a routerlink="/about-us/lsis-people" href="/about-us/lsis-people">LS
                                                ELECTRIC People</a></li>
                                        <li><a routerlink="/about-us/global-network/korea"
                                                href="/about-us/global-network/korea">Global Network</a></li>
                                    </ul>
                                </li>
                                <li class="d2_li"><a routerlink="/media" href="/media">MEDIA</a>
                                    <ul class="sub_d3">
                                        <li><a routerlink="/media/notice" href="/media/notice">Notice</a></li>
                                        <li><a routerlink="/media/press" href="/media/press">Press</a></li>
                                        <li><a routerlink="/media/news-letter" href="/media/news-letter">Newsletter</a>
                                        </li>
                                        <li><a routerlink="/media/videos" href="/media/videos">Videos</a></li>
                                        <li><a routerlink="/media/brochure" href="/media/brochure">Brochure</a></li>
                                        <li><a routerlink="/media/exhibition" href="/media/exhibition">Events</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li><span tabindex="0">PRODUCTS</span>
                        <div class="gnbSub">
                            <ul class="clear sub_d2 products">
                                <li class="d2_li"><a href="/products/SmartPowerSolution">Smart Power Solution</a>
                                    <ul class="power_category clear">
                                        <li>
                                            <div>Solution</div>
                                            <ul class="sub_d3">
                                                <!---->
                                                <li>
                                                    <!----><a href="/products/category/Smart_Power_Solution/EMS">EMS</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">SCADA</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/SCADA/ECMS">ECMS</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/SCADA/PQMS">PQMS</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/SCADA/SAS">SAS</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <!----><a href="/products/category/Smart_Power_Solution/DMS">DMS</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                                <li>
                                                    <!----><a
                                                        href="/products/category/Smart_Power_Solution/xEMS">xEMS</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                                <li>
                                                    <!----><a href="/products/category/Smart_Power_Solution/DCS">DCS</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                                <li>
                                                    <!----><a
                                                        href="/products/category/Smart_Power_Solution/Micro_Grid">Micro
                                                        Grid</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                                <li>
                                                    <!----><a
                                                        href="/products/category/Smart_Power_Solution/Diagnosis">Diagnosis</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div>System</div>
                                            <ul class="sub_d3">
                                                <!---->
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Power Transmission</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Transmission/Substation">Substation</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Transmission/FACTS">FACTS</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Transmission/HVDC">HVDC</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Transmission/GIS">GIS</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Transmission/Power_Transformer">Power
                                                                Transformer</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <!----><a
                                                        href="/products/category/Smart_Power_Solution/PV_System">PV
                                                        System</a>
                                                    <!---->
                                                    <!---->
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Power Distribution</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution/LV_SWGR">LV
                                                                SWGR</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution/Transformer">Transformer</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution/Busway_System">Busway
                                                                System</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution/FCL">FCL</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution/APS">APS</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution/MV_SWGR">MV
                                                                SWGR</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Modular Substation</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Modular_Substation/E-House">E-House</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Modular_Substation/Mobile_Substation">Mobile
                                                                Substation</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Modular_Substation/CSS">CSS</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">ESS</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/ESS/EnGather_PCS">EnGather
                                                                PCS</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/ESS/Modular_Scalable_Platform">Modular
                                                                Scalable Platform</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div>Device</div>
                                            <ul class="sub_d3">
                                                <!---->
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Low Voltage</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/MCCB_&amp;_ELCB">MCCB
                                                                &amp; ELCB</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/Contactor_&amp;_Overload_Relay">Contactor
                                                                &amp; Overload Relay</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/MMS">MMS</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/MCB_&amp;_CP">MCB
                                                                &amp; CP</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/SPD">SPD</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/ACB">ACB</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/EMPR">EMPR</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage/VSP">VSP</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Medium Voltage</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Medium_Voltage/VI">VI</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Medium_Voltage/VCB">VCB</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Medium_Voltage/Susol_VCS">Susol
                                                                VCS</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">DC Component</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_ACB_&amp;_Switch-Disconnector">DC
                                                                ACB &amp; Switch-Disconnector</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_Compact_Switch-Disconnector">DC
                                                                Compact Switch-Disconnector</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_Relay">DC
                                                                Relay</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/Susol_DC_MCCB_&amp;_Switch-Disconnector">Susol
                                                                DC MCCB &amp; Switch-Disconnector</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_MC">DC
                                                                MC</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_MCB">DC
                                                                MCB</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_SPD">DC
                                                                SPD</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/DC_Component/DC_UL_Switch-Disconnector">DC
                                                                UL Switch-Disconnector</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Protection/Measurement/Meter</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter/Protection">Protection</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter/Measurement">Measurement</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter/Meter">Meter</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="d2_li"><a href="/products/SmartAutomationSolution">Smart Automation
                                        Solution</a>
                                    <ul class="sub_d3">
                                        <!---->
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">Gearbox</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Gearbox/Precision_Planetary_Gearbox">Precision
                                                        Planetary Gearbox</a></li>
                                            </ul>
                                        </li>
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">PLC</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/PLC/MASTER-K_Series">MASTER-K
                                                        Series</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/PLC/XGT_Series_-*XGK,_XGI,_XGR*-">XGT
                                                        Series (XGK, XGI, XGR)</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/PLC/XGB_Series">XGB
                                                        Series</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/PLC/GLOFA-GM_Series">GLOFA-GM
                                                        Series</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/PLC/Smart_I!O_Series">Smart
                                                        I/O Series</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/PLC/Motion_Controller">Motion
                                                        Controller</a></li>
                                            </ul>
                                        </li>
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">Servo/Motion</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Servo!Motion/Motion_Controller">Motion
                                                        Controller</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Servo!Motion/Servo_Drive">Servo
                                                        Drive</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Servo!Motion/Servo_Motor">Servo
                                                        Motor</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Servo!Motion/Integrated_Servo">Integrated
                                                        Servo</a></li>
                                            </ul>
                                        </li>
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">HMI</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/HMI/XGT_Panel">XGT
                                                        Panel</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/HMI/XGT_InfoU">XGT
                                                        InfoU</a></li>
                                            </ul>
                                        </li>
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">Inverter (VFD)</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Soft_Starter">Soft
                                                        Starter</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Low_Voltage_VFD_Panel">Low
                                                        Voltage VFD Panel</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Low_Voltage_VFD">Low
                                                        Voltage VFD</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Medium_Voltage_VFD">Medium
                                                        Voltage VFD</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="d2_li"><a href="/products/SmartRailwaySolution">Smart Railway Solution</a>
                                    <ul class="sub_d3">
                                        <!---->
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">Power Supply System</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Power_Supply_System/AC_System">AC
                                                        System</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Power_Supply_System/DC_System">DC
                                                        System</a></li>
                                            </ul>
                                        </li>
                                        <li class="has_d4">
                                            <!---->
                                            <!----><a href="#">Signaling System</a>
                                            <!---->
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/ETCS_L2">ETCS
                                                        L2</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/CBI">CBI</a>
                                                </li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/CBTC">CBTC</a>
                                                </li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/ETCS_L1">ETCS
                                                        L1</a></li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/ATC">ATC</a>
                                                </li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/CTC">CTC</a>
                                                </li>
                                            </ul>
                                            <ul class="sub_d4">
                                                <li><a
                                                        href="/products/category/Smart_Railway_Solution/Signaling_System/AF_Track_Circuit">AF
                                                        Track Circuit</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <div class="atoz"><a routerlink="/productsA-Z" href="/productsA-Z">PRODUCTS A-Z</a>
                                    </div>
                                    <div class="eworld"><a href="https://www.ls-electric.com/eworld" target="_blank">LS
                                            E-WORLD</a></div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li><span>REFERENCES</span>
                        <div class="gnbSub">
                            <ul class="clear sub_d2 references">
                                <li class="d2_li">
                                    <div><a routerlink="/references/products" href="/references/products">Products</a>
                                    </div>
                                    <div><a routerlink="/references/solutions"
                                            href="/references/solutions">Solutions</a></div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li><span tabindex="0">SUPPORT &amp; SERVICES</span>
                        <div class="gnbSub">
                            <ul class="clear sub_d2 surpport">
                                <li class="d2_li"><a routerlink="/support" href="/support">SUPPORT &amp; SERVICES</a>
                                    <ul class="sub_d3">
                                        <li><a routerlink="/support/download-center"
                                                href="/support/download-center">Download Center (Power)</a></li>
                                        <li><a class="ex"
                                                href="https://sol.ls-electric.com/ww/en/product/category/0?utm_source=lsehomepage&amp;utm_medium=display&amp;utm_campaign=lse_ww_dn"
                                                target="_blank">Download Center (Automation)</a></li>
                                        <li><a routerlink="/support/faq" href="/support/faq">FAQs</a></li>
                                        <li><a routerlink="/support/qna" href="/support/qna">Q&amp;A</a></li>
                                        <li><a routerlink="/support/where-to-buy" href="/support/where-to-buy">Where to
                                                buy</a></li>
                                        <li><a class="ex" href="http://www.ls-electric.com/distributor/"
                                                target="_blank">For Distributors</a></li>
                                        <li><a class="ex" href="#">Product Check</a></li>
                                        <li><a class="ex" href="http://www.ls-electric.com/gearbox/en/motor"
                                                target="_blank">Gearbox Size Manager</a></li>
                                        <li><a class="ex" href="http://www.ls-electric.com/linear_motor/en"
                                                target="_blank">Linear Motor</a></li>
                                    </ul>
                                </li>
                                <li class="d2_li">
                                    <div><a class="ex2" href="https://pfinder.ls-electric.com" target="_blank">PRODUCT
                                            FINDER</a></div>
                                    <div><a class="ex2" href="https://www.youtube.com/channel/UCy4bL2Rz3xMPuvxZ374JJcg"
                                            target="_blank">User Manual(YouTube)</a></div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="util clear">
                <ul>
                    <li><a routerlink="/myProduct" href="/myProduct">My Products</a></li>
                    <li><a routerlink="/myDocument" href="/myDocument">My Documents</a></li>
                    <li class="lang"><a class="sel_lang" href="#">EN</a>
                        <ul class="langList">
                            <li><a href="#">Korean</a></li>
                            <li><a href="#">Japanese</a></li>
                            <li><a href="#">Chinese</a></li>
                        </ul>
                    </li>
                </ul>
            </div><button class="hd_search_btn" type="button"><span class="invisible">Search</span></button><button
                class="mo_gnb_open" type="button"><span class="invisible">Modile Menu Open</span></button>
        </div>
    </header>

    <div _ngcontent-c0="" class="hasVis" id="contents">
        <section _ngcontent-c0="" class="mainContent" id="main">
            <div _ngcontent-c0="" class="section section1">
                <div _ngcontent-c0="" class="text">
                    <div _ngcontent-c0="" class="vis_logo"><img _ngcontent-c0="" alt=""
                            src="/assets/img/common/logo4.png"></div>
                    <p _ngcontent-c0="" class="p1"><span _ngcontent-c0="">SMART ENERGY</span><span _ngcontent-c0="">
                            GLOBAL LEADER</span></p>
                    <p _ngcontent-c0="" class="p2">LS ELECTRIC is starting a new chapter to bring smart energy to light
                        everywhere around the world.</p>
                </div>
            </div>
            <div _ngcontent-c0="" class="section section2">
                <div _ngcontent-c0="" class="conWrap">
                    <div _ngcontent-c0="" class="tit">
                        <h1 _ngcontent-c0="">About us</h1>
                        <p _ngcontent-c0="">We are creating an abundant future by delivering safe clean energy.</p>
                    </div>
                    <div _ngcontent-c0="" class="box">
                        <ul _ngcontent-c0="" class="clear">
                            <li _ngcontent-c0="" class="intro"><a _ngcontent-c0="" routerlink="/about-us/ceo"
                                    href="/about-us/ceo">
                                    <div _ngcontent-c0="" class="text"><span _ngcontent-c0="" class="bar1"></span>
                                        <p _ngcontent-c0="" class="t2">CEO's Message</p>
                                    </div><span _ngcontent-c0="" class="arrow1"></span>
                                    <div _ngcontent-c0="" class="over">
                                        <div _ngcontent-c0="" class="wrap">
                                            <p _ngcontent-c0="">CEO's Message</p><span _ngcontent-c0="">DETAIL
                                                VIEW</span>
                                        </div>
                                    </div>
                                </a></li>
                            <li _ngcontent-c0="" class="row_box">
                                <div _ngcontent-c0="" class="invest"><a _ngcontent-c0=""
                                        routerlink="/about-us/investor-relations/financials"
                                        href="/about-us/investor-relations/financials"><span _ngcontent-c0=""
                                            class="bar2"></span><span _ngcontent-c0="" class="icon"></span>
                                        <div _ngcontent-c0="" class="text">
                                            <p _ngcontent-c0="" class="t1">Investor<br _ngcontent-c0=""> Relations</p>
                                        </div><span _ngcontent-c0="" class="arrow1"></span>
                                        <div _ngcontent-c0="" class="over">
                                            <div _ngcontent-c0="" class="wrap">
                                                <p _ngcontent-c0="">Investor<br _ngcontent-c0=""> Relations</p><span
                                                    _ngcontent-c0="">DETAIL VIEW</span>
                                            </div>
                                        </div>
                                    </a></div>
                                <div _ngcontent-c0="" class="recruit"><a _ngcontent-c0=""
                                        href="https://pfinder.ls-electric.com/main.do" target="_blank"><span
                                            _ngcontent-c0="" class="bar2"></span><span _ngcontent-c0=""
                                            class="icon"></span>
                                        <div _ngcontent-c0="" class="text">
                                            <p _ngcontent-c0="" class="t1">PRODUCT<br _ngcontent-c0=""> FINDER</p>
                                        </div><span _ngcontent-c0="" class="arrow2"></span>
                                        <div _ngcontent-c0="" class="over">
                                            <div _ngcontent-c0="" class="wrap">
                                                <p _ngcontent-c0="">PRODUCT<br _ngcontent-c0=""> FINDER</p><span
                                                    _ngcontent-c0="">DETAIL VIEW</span>
                                            </div>
                                        </div>
                                    </a></div>
                            </li>
                            <li _ngcontent-c0="" class="row_box">
                                <div _ngcontent-c0="" class="eworld"><a _ngcontent-c0=""
                                        href="https://www.ls-electric.com/eworld" target="_blank"><span _ngcontent-c0=""
                                            class="bar2"></span><span _ngcontent-c0="" class="icon"></span>
                                        <div _ngcontent-c0="" class="text">
                                            <p _ngcontent-c0="" class="t1">LS E-WORLD</p>
                                        </div><span _ngcontent-c0="" class="arrow2"></span>
                                        <div _ngcontent-c0="" class="over">
                                            <div _ngcontent-c0="" class="wrap">
                                                <p _ngcontent-c0="">LS E-WORLD</p><span _ngcontent-c0="">DETAIL
                                                    VIEW</span>
                                            </div>
                                        </div>
                                    </a></div>
                                <div _ngcontent-c0="" class="menual"><a _ngcontent-c0=""
                                        href="https://www.youtube.com/channel/UCy4bL2Rz3xMPuvxZ374JJcg"
                                        target="_blank"><span _ngcontent-c0="" class="bar2"></span><span
                                            _ngcontent-c0="" class="icon"></span>
                                        <div _ngcontent-c0="" class="text">
                                            <p _ngcontent-c0="" class="t1">User Manual</p>
                                        </div><span _ngcontent-c0="" class="arrow1"></span>
                                        <div _ngcontent-c0="" class="over">
                                            <div _ngcontent-c0="" class="wrap">
                                                <p _ngcontent-c0="">User Manual</p><span _ngcontent-c0="">DETAIL
                                                    VIEW</span>
                                            </div>
                                        </div>
                                    </a></div>
                            </li>
                            <li _ngcontent-c0="" class="network"><a _ngcontent-c0=""
                                    routerlink="/about-us/global-network/korea"
                                    href="/about-us/global-network/korea"><span _ngcontent-c0=""
                                        class="bar2"></span><span _ngcontent-c0="" class="icon"></span>
                                    <div _ngcontent-c0="" class="text">
                                        <p _ngcontent-c0="" class="t1">Global Network</p>
                                    </div><span _ngcontent-c0="" class="arrow1"></span>
                                    <div _ngcontent-c0="" class="over">
                                        <div _ngcontent-c0="" class="wrap">
                                            <p _ngcontent-c0="">Global Network</p><span _ngcontent-c0="">DETAIL
                                                VIEW</span>
                                        </div>
                                    </div>
                                </a></li>
                            <li _ngcontent-c0="" class="news">
                                <div _ngcontent-c0="" class="text">
                                    <p _ngcontent-c0="" class="t1">Press &amp; notice</p><a _ngcontent-c0=""
                                        class="more" routerlink="/media/press" href="/media/press">MORE</a>
                                </div>
                                <ul _ngcontent-c0="" class="n_list">
                                    <!---->
                                    <li _ngcontent-c0=""><span _ngcontent-c0="" class="elps"><a _ngcontent-c0=""
                                                class="subject" href="#">LS ELECTRIC Unveils a Suit of Innovative
                                                Solutions at the World's Largest Smart Energy Exhibition</a></span></li>
                                    <li _ngcontent-c0=""><span _ngcontent-c0="" class="elps"><a _ngcontent-c0=""
                                                class="subject" href="#">LS ELECTRIC Power Testing &amp; Technology
                                                Institute, Hits Milestone as a Global Top 6 Power-Generation Equipment
                                                Test Lab</a></span></li>
                                    <li _ngcontent-c0=""><span _ngcontent-c0="" class="elps"><a _ngcontent-c0=""
                                                class="subject" href="#">LS ELECTRIC to Ensure Security Even from the
                                                Product R&amp;D Stage</a></span></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div _ngcontent-c0="" class="section section3">
                <div _ngcontent-c0="" class="conWrap">
                    <div _ngcontent-c0="" class="tit">
                        <h1 _ngcontent-c0="">Business Introduction</h1>
                        <p _ngcontent-c0="">We are leading the way towards a new future through innovations that exceed
                            our customers’ expectations.</p>
                    </div>
                    <div _ngcontent-c0="" class="bizCon pc">
                        <ul _ngcontent-c0="" class="clear">
                            <li _ngcontent-c0="">
                                <div _ngcontent-c0="" class="txt">Smart Power Solution</div>
                                <div _ngcontent-c0="" class="desc">
                                    <p _ngcontent-c0="" class="con1">Smart Power Solution</p>
                                    <p _ngcontent-c0="" class="con2">We offer power devices, systems,<br
                                            _ngcontent-c0="">and solutions required for transmitting<br
                                            _ngcontent-c0="">and supplying electricity produced<br _ngcontent-c0="">from
                                        generators. to our customers.</p>
                                    <div _ngcontent-c0="" class="depth2">
                                        <ul _ngcontent-c0="">
                                            <li _ngcontent-c0=""><span _ngcontent-c0="">Solutions</span>
                                                <div _ngcontent-c0="" class="wrap_right">
                                                    <ul _ngcontent-c0="">
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/EMS">EMS</a>
                                                        </li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/SCADA">SCADA</a>
                                                        </li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/DMS">DMS</a>
                                                        </li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/xEMS">xEMS</a>
                                                        </li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/DCS">DCS</a>
                                                        </li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Micro_Grid">Micro
                                                                Grid</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Diagnosis">Diagnosis</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li _ngcontent-c0=""><span _ngcontent-c0="">System</span>
                                                <div _ngcontent-c0="" class="wrap_right">
                                                    <ul _ngcontent-c0="">
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Power_Transmission">Power
                                                                Transmission</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Power_Distribution">Power
                                                                Distribution</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Modular_Substation">Modular
                                                                Substation</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/PV_System">PV
                                                                System</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/ESS">ESS</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li _ngcontent-c0=""><span _ngcontent-c0="">Devices</span>
                                                <div _ngcontent-c0="" class="wrap_right">
                                                    <ul _ngcontent-c0="">
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Low_Voltage">Low
                                                                Voltage</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Medium_Voltage">Medium
                                                                Voltage</a></li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter">Protection/Measurement/Meter</a>
                                                        </li>
                                                        <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                                href="/products/category/Smart_Power_Solution/DC_Component">DC
                                                                Component</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li _ngcontent-c0="">
                                <div _ngcontent-c0="" class="txt">Smart Automation Solution</div>
                                <div _ngcontent-c0="" class="desc">
                                    <p _ngcontent-c0="" class="con1">Smart Automation Solution</p>
                                    <p _ngcontent-c0="" class="con2">LS ELECTRIC offers various automation<br
                                            _ngcontent-c0="">solutions from unit devices to process<br
                                            _ngcontent-c0="">control in order to effectively operate<br
                                            _ngcontent-c0="">in industrial environments.</p>
                                    <div _ngcontent-c0="" class="depth2">
                                        <ul _ngcontent-c0="">
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Automation_Solution/PLC">PLC</a></li>
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Automation_Solution/Servo!Motion">Servo/Motion</a>
                                            </li>
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Automation_Solution/HMI">HMI</a></li>
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Automation_Solution/Gearbox">Gearbox</a>
                                            </li>
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-">Inverter
                                                    (VFD)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li _ngcontent-c0="">
                                <div _ngcontent-c0="" class="txt">Smart Railway Solution</div>
                                <div _ngcontent-c0="" class="desc">
                                    <p _ngcontent-c0="" class="con1">Smart Railway Solution</p>
                                    <p _ngcontent-c0="" class="con2">As the most experienced leader<br
                                            _ngcontent-c0="">in the railway systems,<br _ngcontent-c0="">our
                                        technological prowess<br _ngcontent-c0="">is recognized in the global market.
                                    </p>
                                    <div _ngcontent-c0="" class="depth2">
                                        <ul _ngcontent-c0="">
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Railway_Solution/Signaling_System">Signaling
                                                    System</a></li>
                                            <li _ngcontent-c0=""><a _ngcontent-c0=""
                                                    href="/products/category/Smart_Railway_Solution/Power_Supply_System">Power
                                                    Supply System</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div _ngcontent-c0="" class="bizCon mobile">
                        <div _ngcontent-c0="" class="wrap">
                            <div _ngcontent-c0="" class="slide_wrap">
                                <div _ngcontent-c0="" class="slide_list on">
                                    <p _ngcontent-c0="" class="desc">We offer power devices, systems, and solutions
                                        required for transmitting and supplying electricity produced from generators. to
                                        our customers.</p>
                                </div>
                                <div _ngcontent-c0="" class="slide_list">
                                    <p _ngcontent-c0="" class="desc">LS ELECTRIC offers various automation solutions
                                        from unit devices to process control in order to effectively operate in
                                        industrial environments.</p>
                                </div>
                                <div _ngcontent-c0="" class="slide_list">
                                    <p _ngcontent-c0="" class="desc">As the most experienced leader in the railway
                                        systems, our technological prowess is recognized in the global market.</p>
                                </div>
                            </div>
                            <ul _ngcontent-c0="" class="tab clear">
                                <li _ngcontent-c0=""><a _ngcontent-c0="" class="on" href="#">Smart<br _ngcontent-c0="">
                                        Power Solution</a></li>
                                <li _ngcontent-c0=""><a _ngcontent-c0="" href="#">Smart<br _ngcontent-c0=""> Automation
                                        Solution</a></li>
                                <li _ngcontent-c0=""><a _ngcontent-c0="" href="#">Smart<br _ngcontent-c0=""> Railway
                                        Solution</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div _ngcontent-c0="" class="section section4">
                <div _ngcontent-c0="" class="conWrap">
                    <ul _ngcontent-c0="" class="sum clear">
                        <li _ngcontent-c0="" class="cs1"><a _ngcontent-c0="" routerlink="/support/download-center"
                                href="/support/download-center">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">Download Center<br _ngcontent-c0="">(Power)</p>
                                </div>
                            </a><a _ngcontent-c0=""
                                href="https://sol.ls-electric.com/ww/en/product/category/0?utm_source=lsehomepage&amp;utm_medium=display&amp;utm_campaign=lse_ww_dn"
                                target="_blank">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">Download Center<br _ngcontent-c0="">(Automation)</p>
                                </div>
                            </a></li>
                        <li _ngcontent-c0="" class="cs2"><a _ngcontent-c0="" routerlink="/support/qna"
                                href="/support/qna">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">Q&amp;A</p>
                                </div>
                                <p _ngcontent-c0="" class="con">Questions and Answers<br _ngcontent-c0=""> on LS
                                    ELECTRIC Products</p>
                            </a></li>
                        <li _ngcontent-c0="" class="cs3"><a _ngcontent-c0=""
                                href="http://www.ls-electric.com/distributor/" target="_blank">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">For Distributors</p>
                                </div>
                            </a><a _ngcontent-c0="" href="#">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">Product Check</p>
                                </div>
                            </a><a _ngcontent-c0="" href="http://www.ls-electric.com/gearbox/en/motor" target="_blank">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">Gearbox Size<br _ngcontent-c0=""> Manager</p>
                                </div>
                            </a></li>
                        <li _ngcontent-c0="" class="cs4"><a _ngcontent-c0="" routerlink="/media/news-letter"
                                href="/media/news-letter">
                                <div _ngcontent-c0="" class="tit">
                                    <p _ngcontent-c0="">Newsletter</p>
                                </div>
                                <p _ngcontent-c0="" class="con">Monthly News and Information<br _ngcontent-c0=""> about
                                    LS ELECTRIC</p>
                            </a></li>
                    </ul>
                </div>
            </div>
            <header class="header_wrap" id="header">
                <div class="header"><a class="logo" routerlink="/" href="/"><img alt="LS ELECTRIC"
                            src="assets/images/logo-symphos-hitam.png"></a>
                    <div class="gnb">
                        <ul class="d1 clear">
                            <li><span routerlink="/about-us" tabindex="0">ABOUT US</span>
                                <div class="gnbSub">
                                    <ul class="clear sub_d2 company">
                                        <li class="d2_li"><a routerlink="/about-us" href="/about-us">ABOUT US</a>
                                            <ul class="sub_d3">
                                                <li><a routerlink="/about-us/ceo" href="/about-us/ceo">CEO's Message</a>
                                                </li>
                                                <li><a routerlink="/about-us/company"
                                                        href="/about-us/company">Company</a></li>
                                                <li><a routerlink="/about-us/vision-philosophy"
                                                        href="/about-us/vision-philosophy">Vision &amp; Philosophy</a>
                                                </li>
                                                <li><a routerlink="/about-us/history"
                                                        href="/about-us/history">History</a></li>
                                                <li><a routerlink="/about-us/rnd" href="/about-us/rnd">R&amp;D</a></li>
                                                <li><a routerlink="/about-us/ptt" href="/about-us/ptt">PT&amp;T</a></li>
                                                <li><a routerlink="/about-us/sustainability/environmental-management"
                                                        href="/about-us/sustainability/environmental-management">Sustainability</a>
                                                </li>
                                                <li><a routerlink="/about-us/investor-relations/financials"
                                                        href="/about-us/investor-relations/financials">Investor
                                                        Relations</a></li>
                                                <li><a routerlink="/about-us/lsis-people"
                                                        href="/about-us/lsis-people">LS ELECTRIC People</a></li>
                                                <li><a routerlink="/about-us/global-network/korea"
                                                        href="/about-us/global-network/korea">Global Network</a></li>
                                            </ul>
                                        </li>
                                        <li class="d2_li"><a routerlink="/media" href="/media">MEDIA</a>
                                            <ul class="sub_d3">
                                                <li><a routerlink="/media/notice" href="/media/notice">Notice</a></li>
                                                <li><a routerlink="/media/press" href="/media/press">Press</a></li>
                                                <li><a routerlink="/media/news-letter"
                                                        href="/media/news-letter">Newsletter</a></li>
                                                <li><a routerlink="/media/videos" href="/media/videos">Videos</a></li>
                                                <li><a routerlink="/media/brochure" href="/media/brochure">Brochure</a>
                                                </li>
                                                <li><a routerlink="/media/exhibition"
                                                        href="/media/exhibition">Events</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li><span tabindex="0">PRODUCTS</span>
                                <div class="gnbSub">
                                    <ul class="clear sub_d2 products">
                                        <li class="d2_li"><a href="/products/SmartPowerSolution">Smart Power
                                                Solution</a>
                                            <ul class="power_category clear">
                                                <li>
                                                    <div>Solution</div>
                                                    <ul class="sub_d3">
                                                        <!---->
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/EMS">EMS</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">SCADA</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/SCADA/ECMS">ECMS</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/SCADA/PQMS">PQMS</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/SCADA/SAS">SAS</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/DMS">DMS</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/xEMS">xEMS</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/DCS">DCS</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/Micro_Grid">Micro
                                                                Grid</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/Diagnosis">Diagnosis</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div>System</div>
                                                    <ul class="sub_d3">
                                                        <!---->
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">Power Transmission</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Transmission/Substation">Substation</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Transmission/FACTS">FACTS</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Transmission/HVDC">HVDC</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Transmission/GIS">GIS</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Transmission/Power_Transformer">Power
                                                                        Transformer</a></li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <!----><a
                                                                href="/products/category/Smart_Power_Solution/PV_System">PV
                                                                System</a>
                                                            <!---->
                                                            <!---->
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">Power Distribution</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Distribution/LV_SWGR">LV
                                                                        SWGR</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Distribution/Transformer">Transformer</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Distribution/Busway_System">Busway
                                                                        System</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Distribution/FCL">FCL</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Distribution/APS">APS</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Power_Distribution/MV_SWGR">MV
                                                                        SWGR</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">Modular Substation</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Modular_Substation/E-House">E-House</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Modular_Substation/Mobile_Substation">Mobile
                                                                        Substation</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Modular_Substation/CSS">CSS</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">ESS</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/ESS/EnGather_PCS">EnGather
                                                                        PCS</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/ESS/Modular_Scalable_Platform">Modular
                                                                        Scalable Platform</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div>Device</div>
                                                    <ul class="sub_d3">
                                                        <!---->
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">Low Voltage</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/MCCB_&amp;_ELCB">MCCB
                                                                        &amp; ELCB</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/Contactor_&amp;_Overload_Relay">Contactor
                                                                        &amp; Overload Relay</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/MMS">MMS</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/MCB_&amp;_CP">MCB
                                                                        &amp; CP</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/SPD">SPD</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/ACB">ACB</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/EMPR">EMPR</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Low_Voltage/VSP">VSP</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">Medium Voltage</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Medium_Voltage/VI">VI</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Medium_Voltage/VCB">VCB</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Medium_Voltage/Susol_VCS">Susol
                                                                        VCS</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">DC Component</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_ACB_&amp;_Switch-Disconnector">DC
                                                                        ACB &amp; Switch-Disconnector</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_Compact_Switch-Disconnector">DC
                                                                        Compact Switch-Disconnector</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_Relay">DC
                                                                        Relay</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/Susol_DC_MCCB_&amp;_Switch-Disconnector">Susol
                                                                        DC MCCB &amp; Switch-Disconnector</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_MC">DC
                                                                        MC</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_MCB">DC
                                                                        MCB</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_SPD">DC
                                                                        SPD</a></li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/DC_Component/DC_UL_Switch-Disconnector">DC
                                                                        UL Switch-Disconnector</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="has_d4">
                                                            <!---->
                                                            <!----><a href="#">Protection/Measurement/Meter</a>
                                                            <!---->
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter/Protection">Protection</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter/Measurement">Measurement</a>
                                                                </li>
                                                            </ul>
                                                            <ul class="sub_d4">
                                                                <li><a
                                                                        href="/products/category/Smart_Power_Solution/Protection!Measurement!Meter/Meter">Meter</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="d2_li"><a href="/products/SmartAutomationSolution">Smart Automation
                                                Solution</a>
                                            <ul class="sub_d3">
                                                <!---->
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Gearbox</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Gearbox/Precision_Planetary_Gearbox">Precision
                                                                Planetary Gearbox</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">PLC</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/PLC/MASTER-K_Series">MASTER-K
                                                                Series</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/PLC/XGT_Series_-*XGK,_XGI,_XGR*-">XGT
                                                                Series (XGK, XGI, XGR)</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/PLC/XGB_Series">XGB
                                                                Series</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/PLC/GLOFA-GM_Series">GLOFA-GM
                                                                Series</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/PLC/Smart_I!O_Series">Smart
                                                                I/O Series</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/PLC/Motion_Controller">Motion
                                                                Controller</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Servo/Motion</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Servo!Motion/Motion_Controller">Motion
                                                                Controller</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Servo!Motion/Servo_Drive">Servo
                                                                Drive</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Servo!Motion/Servo_Motor">Servo
                                                                Motor</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Servo!Motion/Integrated_Servo">Integrated
                                                                Servo</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">HMI</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/HMI/XGT_Panel">XGT
                                                                Panel</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/HMI/XGT_InfoU">XGT
                                                                InfoU</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Inverter (VFD)</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Soft_Starter">Soft
                                                                Starter</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Low_Voltage_VFD_Panel">Low
                                                                Voltage VFD Panel</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Low_Voltage_VFD">Low
                                                                Voltage VFD</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Automation_Solution/Inverter_-*VFD*-/Medium_Voltage_VFD">Medium
                                                                Voltage VFD</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="d2_li"><a href="/products/SmartRailwaySolution">Smart Railway
                                                Solution</a>
                                            <ul class="sub_d3">
                                                <!---->
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Power Supply System</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Power_Supply_System/AC_System">AC
                                                                System</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Power_Supply_System/DC_System">DC
                                                                System</a></li>
                                                    </ul>
                                                </li>
                                                <li class="has_d4">
                                                    <!---->
                                                    <!----><a href="#">Signaling System</a>
                                                    <!---->
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/ETCS_L2">ETCS
                                                                L2</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/CBI">CBI</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/CBTC">CBTC</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/ETCS_L1">ETCS
                                                                L1</a></li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/ATC">ATC</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/CTC">CTC</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="sub_d4">
                                                        <li><a
                                                                href="/products/category/Smart_Railway_Solution/Signaling_System/AF_Track_Circuit">AF
                                                                Track Circuit</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <div class="atoz"><a routerlink="/productsA-Z" href="/productsA-Z">PRODUCTS
                                                    A-Z</a></div>
                                            <div class="eworld"><a href="https://www.ls-electric.com/eworld"
                                                    target="_blank">LS E-WORLD</a></div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li><span>REFERENCES</span>
                                <div class="gnbSub">
                                    <ul class="clear sub_d2 references">
                                        <li class="d2_li">
                                            <div><a routerlink="/references/products"
                                                    href="/references/products">Products</a></div>
                                            <div><a routerlink="/references/solutions"
                                                    href="/references/solutions">Solutions</a></div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li><span tabindex="0">SUPPORT &amp; SERVICES</span>
                                <div class="gnbSub">
                                    <ul class="clear sub_d2 surpport">
                                        <li class="d2_li"><a routerlink="/support" href="/support">SUPPORT &amp;
                                                SERVICES</a>
                                            <ul class="sub_d3">
                                                <li><a routerlink="/support/download-center"
                                                        href="/support/download-center">Download Center (Power)</a></li>
                                                <li><a class="ex"
                                                        href="https://sol.ls-electric.com/ww/en/product/category/0?utm_source=lsehomepage&amp;utm_medium=display&amp;utm_campaign=lse_ww_dn"
                                                        target="_blank">Download Center (Automation)</a></li>
                                                <li><a routerlink="/support/faq" href="/support/faq">FAQs</a></li>
                                                <li><a routerlink="/support/qna" href="/support/qna">Q&amp;A</a></li>
                                                <li><a routerlink="/support/where-to-buy"
                                                        href="/support/where-to-buy">Where to buy</a></li>
                                                <li><a class="ex" href="http://www.ls-electric.com/distributor/"
                                                        target="_blank">For Distributors</a></li>
                                                <li><a class="ex" href="#">Product Check</a></li>
                                                <li><a class="ex" href="http://www.ls-electric.com/gearbox/en/motor"
                                                        target="_blank">Gearbox Size Manager</a></li>
                                                <li><a class="ex" href="http://www.ls-electric.com/linear_motor/en"
                                                        target="_blank">Linear Motor</a></li>
                                            </ul>
                                        </li>
                                        <li class="d2_li">
                                            <div><a class="ex2" href="https://pfinder.ls-electric.com"
                                                    target="_blank">PRODUCT FINDER</a></div>
                                            <div><a class="ex2"
                                                    href="https://www.youtube.com/channel/UCy4bL2Rz3xMPuvxZ374JJcg"
                                                    target="_blank">User Manual(YouTube)</a></div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="util clear">
                        <ul>
                            <li><a routerlink="/myProduct" href="/myProduct">My Products</a></li>
                            <li><a routerlink="/myDocument" href="/myDocument">My Documents</a></li>
                            <li class="lang"><a class="sel_lang" href="#">EN</a>
                                <ul class="langList">
                                    <li><a href="#">Korean</a></li>
                                    <li><a href="#">Japanese</a></li>
                                    <li><a href="#">Chinese</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><button class="hd_search_btn" type="button"><span
                            class="invisible">Search</span></button><button class="mo_gnb_open" type="button"><span
                            class="invisible">Modile Menu Open</span></button>
                </div>
            </header>
        </section>
    </div>

    <footer class="footer_wrap" id="footer">
        <div class="footer">
            <div class="area1"><span class="logo"><img alt="LS ELECTRIC" src="assets/images/logo-symphos-putih.png" style="width:200px"></span>
                <ul class="ft_util">
                    <li><a class="privacy" routerlink="/etc/privacy-notice" href="/etc/privacy-notice">Privacy
                            Notice</a></li>
                    <li><a href="https://ethics.lsholdings.co.kr/en/view/report.asp?cn=004000" target="_blank">Ethics
                            Hotline</a></li>
                    <li><a routerlink="/productsA-Z" href="/productsA-Z">Product A-Z</a></li>
                </ul>
            </div>
            <div class="area2 clear">
                <div class="fm1">
                    <div class="ft_menu">
                        <p class="tit">ABOUT US</p>
                        <div class="clear">
                            <ul class="menuList">
                                <li><a routerlink="/about-us/ceo" href="/about-us/ceo">CEO's Message</a></li>
                                <li><a routerlink="/about-us/company" href="/about-us/company">Company</a></li>
                                <li><a routerlink="/about-us/vision-philosophy"
                                        href="/about-us/vision-philosophy">Vision &amp; Philosophy</a></li>
                                <li><a routerlink="/about-us/history" href="/about-us/history">History</a></li>
                                <li><a routerlink="/about-us/rnd" href="/about-us/rnd">R&amp;D</a></li>
                                <li><a routerlink="/about-us/ptt" href="/about-us/ptt">PT&amp;T</a></li>
                                <li><a routerlink="/about-us/sustainability/environmental-management"
                                        href="/about-us/sustainability/environmental-management">Sustainability</a></li>
                                <li><a routerlink="/about-us/investor-relations/financials"
                                        href="/about-us/investor-relations/financials">Investor Relations</a></li>
                                <li><a routerlink="/about-us/lsis-people" href="/about-us/lsis-people">LS ELECTRIC
                                        People</a></li>
                                <li><a routerlink="/about-us/global-network/korea"
                                        href="/about-us/global-network/korea">Global Network</a></li>
                            </ul>
                            <ul class="menuList">
                                <li><a routerlink="/media/notice" href="/media/notice">Notice</a></li>
                                <li><a routerlink="/media/press" href="/media/press">Press</a></li>
                                <li><a routerlink="/media/news-letter" href="/media/news-letter">Newsletter</a></li>
                                <li><a routerlink="/media/videos" href="/media/videos">Videos</a></li>
                                <li><a routerlink="/media/brochure" href="/media/brochure">Brochure</a></li>
                                <li><a routerlink="/media/exhibition" href="/media/exhibition">Exhibition</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="fm2">
                    <div class="ft_menu">
                        <p class="tit">PRODUCTS</p>
                        <ul class="menuList">
                            <li><a href="/products/SmartPowerSolution">Smart Power Solution</a></li>
                            <li><a href="/products/SmartAutomationSolution">Smart Automation Solution</a></li>
                            <li><a href="/products/SmartRailwaySolution">Smart Railway Solution</a></li>
                        </ul>
                    </div>
                    <div class="ft_menu">
                        <p class="tit">REFERENCES</p>
                        <ul class="menuList">
                            <li><a routerlink="/references/products" href="/references/products">Products</a></li>
                            <li><a routerlink="/references/solutions" href="/references/solutions">Solutions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="fm3">
                    <div class="ft_menu">
                        <p class="tit">SUPPORT &amp; SERVICES</p>
                        <ul class="menuList">
                            <li><a routerlink="/support/download-center" href="/support/download-center">Download
                                    Center</a></li>
                            <li><a routerlink="/support/faq" href="/support/faq">FAQs</a></li>
                            <li><a routerlink="/support/qna" href="/support/qna">Q&amp;A</a></li>
                            <li><a routerlink="/support/where-to-buy" href="/support/where-to-buy">Where to buy</a></li>
                            <li><a class="ex" href="http://www.ls-electric.com/distributor/" target="_blank">For
                                    Distributors</a></li>
                            <li><a class="ex" href="#">Product Check</a></li>
                            <li><a class="ex" href="http://www.ls-electric.com/gearbox/en/motor" target="_blank">Gearbox
                                    Size Manager</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="area3">
                <div class="rel">
                    <div class="item rel1"><a class="tit" href="#">Affiliated Companies</a>
                        <ul class="layer clear">
                            <li><a href="http://www.lsholdings.com/en" target="_blank">LS Group</a></li>
                            <li><a href="http://www.lscable.com" target="_blank">LS Cable &amp; System</a></li>
                            <li><a href="http://www.lsmnm.com/english/index.aspx" target="_blank">LS MnM</a></li>
                            <li><a href="http://www.lsmtron.com" target="_blank">LS Mtron</a></li>
                            <li><a href="http://www.gaoncable.com" target="_blank">Gaon Cable</a></li>
                            <li><a href="http://www.e1.co.kr/eng/" target="_blank">E1</a></li>
                            <li><a href="http://www.lsyesco.com/eng/" target="_blank">YESCO</a></li>
                            <li><a href="http://www.ls-electric.com/usa/" target="_blank">LS ELECTRIC America</a></li>
                        </ul>
                    </div>
                    <div class="item rel2"><a class="tit" href="#">Subsidiary Companies</a>
                        <ul class="layer">
                            <li><a href="http://www.lsmetal.biz" target="_blank">LS Metal</a></li>
                            <li><a href="http://www.lsmecapion.com/eng/" target="_blank">LS Mecapion</a></li>
                            <li><a href="http://www.sauter.co.kr" target="_blank">LS Sauter</a></li>
                            <li><a href="http://www.ls-es.com/" target="_blank">LS Energy Solutions</a></li>
                            <li><a href="https://www.lsems.com/en/" target="_blank">LS e-Mobility Solutions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="copyright">
                    <div class="sns"><a href="https://www.linkedin.com/company/lselectric" target="_blank"><img
                                alt="링크드인" src="https://www.ls-electric.com/assets/img/common/sns_linkedin.png"></a><a
                            href="https://www.instagram.com/lselectric_global/" target="_blank"><img alt="인스타그램"
                                src="https://www.ls-electric.com/assets/img/common/sns_instagram.png"></a><a
                            href="https://www.facebook.com/lselectricglobal/" target="_blank"><img alt="페이스북"
                                src="https://www.ls-electric.com/assets/img/common/sns_facebook.png"></a><a
                            href="https://www.youtube.com/channel/UCS4SwwqhnNK4072O8BDZtLg" target="_blank"><img
                                alt="유투브" src="https://www.ls-electric.com/assets/img/common/sns_youtube.png"></a></div>
                    <div class="text">Copyright ⓒ 2020 LS ELECTRIC Co., Ltd. All Rights Reserved.</div>
                </div>
            </div>
            <div class="derwent">Derwent Top 100<br>Global Innovators 2011-20</div>
        </div>
    </footer>

</body>

</html>